<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class followingStore extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'following_stores'; 
								
	public static function storeFollow($user_id,$following_store_id){
			
				$followingStore = new followingStore;
				$followingStore->user_id = $user_id; 
				$followingStore->following_store_id = $following_store_id;
				$followingStore->save();
                $store = store :: where('store_id','=',$following_store_id)->get();
			    $increment = $store[0]['store_follow_count'];
			    store :: where('store_id','=',$following_store_id)
				->update(array('store_follow_count' => $increment+1));				
				return array('status'=>'1','id'=>$followingStore->id);
	}
	public static function storeUnfollow($userId,$follow_id){	
			
			$query = followingStore::where('following_store_id', '=', $follow_id)
			->where('user_id', '=', $userId)
			->delete();
			$store = store :: where('store_id','=',$follow_id)->get();
			    $increment = $store[0]['store_follow_count'];
			    store :: where('store_id','=',$follow_id)
				->update(array('store_follow_count' => $increment-1));
			if(count($query) != '0'){
				return array('status'=>'1');	
			}else{
				return array('status'=>'0');	
			}
			
	} 
	public static function getStoreFollowingUsers($storeId,$pageNumber,$numberOfProduct){
	    $query1 = followingStore::leftJoin('users','users.user_id','=','following_stores.user_id')
		->where('following_stores.following_store_id',$storeId)
		->groupBy('users.user_id')
		->get();
		$take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1));
		$query = followingStore::leftJoin('users','users.user_id','=','following_stores.user_id')
		->where('following_stores.following_store_id',$storeId)
		->groupBy('users.user_id')
		->take($take) 
		->skip($skip)
		->get();
		if(count($query)!= '0'){
			return array('status'=>'1','count'=>count($query),'data'=>$query,'people_count'=>count($query1));
		}else{
			return array('status'=>'0','count'=>count($query),'people_count'=>count($query1));
		}
	}

}