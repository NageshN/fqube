<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class CategoryModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	//protected $fillable = array('username','email_address','password','gender');
	protected $table = 'user_tagged_category'; 
								
	public static function AddCategoryController($user_id,$cat){
	    Session::put('userInfo',$user_id);
		for($i=0;$i<count($cat);$i++)
		  {
			$Category = new CategoryModel;
			$Category->user_id = $user_id;
			$Category->category_id = $cat[$i];
			$Category->save();	
		}
		return 1;
	}
	public static function categoryTagRemove($userId,$categoryId){
		$category = CategoryModel :: where('user_id','=',$userId)
									->where('category_id', '=', $categoryId)
									->delete();
		if($category){
			return 1;
		}
		else{
			return 0;
		}
	}		
	public static function getCategory($userId){
		$query = CategoryModel :: where('user_id', '=', $userId)->get();
		return $query;
	} 
	public static function getCategoryBasedUsers($categories,$pageNumber,$numberOfProduct){
			$categories = explode(',', $categories);
			$take=(($numberOfProduct));
	        $skip=(($numberOfProduct)*($pageNumber-1));	
			$query = CategoryModel::whereIn('category_id', $categories)
			->Join('users','users.user_id','=','user_tagged_category.user_id')
			->groupBy('username')
			->orderBy('users.user_followers_count','DESC')
			->take($take) 
		    ->skip($skip)
			->get();
			return array('status'=>'1','count'=>count($query),'data'=>$query);
	}
	public static function getAllCategories(){
			$query = CategoryModel::groupBy('category_id')
			->get();
			return array('status'=>'1','data'=>$query);
	}	
	
}