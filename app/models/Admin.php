 <?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Admin extends Eloquent implements UserInterface, RemindableInterface {
public $timestamps = false;
protected $fillable = array('username','email_id','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'admin';
	protected $primaryKey = "admin_id";

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token'); 
	
	
	//get user info 
	public static function getAdminInfo($emailId){
		$admin = Admin::Where('email_id','=',$emailId)->get();
		return $admin;
	}
	
	//get all abused products
	public static function getAbusedProducts(){
		$result = DB::table('abused_product')
		           ->leftjoin('products','products.product_id','=','abused_product.product_id')
				   ->select('products.product_id','abused_product.abuse_count','abused_product.abuse_id','products.product_name','products.product_description','products.product_image','products.product_image','products.store_id','products.user_id','products.category_id')
				   ->get();
		return $result;
	}
	//get product to be deleted
	public static function deleteProduct($productId){
		$result = DB::table('abused_product')
		           ->leftjoin('products','products.product_id','=','abused_product.product_id')
				   ->select('products.product_id','abused_product.abuse_count','abused_product.abuse_id','products.product_name','products.product_description','products.product_image','products.product_image','products.store_id','products.user_id','products.category_id')
				   ->get();
		return $result;
	}
	//get all abused products
	public static function getProductsTaggedToOthers(){
		$result = DB::table('products')->where('category_id','=',13)
				   //->select('products.product_id','products.product_name','products.product_description','products.product_image','products.product_image','products.store_id','products.user_id','products.category_id')
				   ->get();
		return $result;
	}
	
}
