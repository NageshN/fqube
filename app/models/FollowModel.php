<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class FollowModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'following_users'; 
								
	public static function FollowController($user_id,$following_user_id){
			
				$FollowModel = new FollowModel;
				$FollowModel->user_id = $user_id; 
				$FollowModel->following_user_id = $following_user_id;
				$FollowModel->save();
                $user = User :: where('user_id','=',$following_user_id)->get();
			    $increment = $user[0]['user_followers_count'];
			    User :: where('user_id','=',$following_user_id)
				->update(array('user_followers_count' => $increment+1));				
				return array('status'=>'1','id'=>$FollowModel->id);
	}
	public static function UnfollowController($user_id,$following_user_id){	
			 $query = FollowModel :: where('user_id', '=', $user_id)->where('following_user_id','=',$following_user_id)->delete();
			 $user = User :: where('user_id','=',$following_user_id)->get();
			 $increment = $user[0]['user_followers_count'];
			 User :: where('user_id','=',$following_user_id)
			 ->update(array('user_followers_count' => $increment-1));	
			return $query;
	}
	public static function searchByUserName($userStr){

			$query = User::where('username','LIKE','%'.$userStr.'%')
			->get();
			$count = count($query);
			if($count != '0'){
				return array('status'=>'1','count'=>$count,'data'=>$query);
			}else{
				return array('status'=>'0','count'=>$count,'data'=>$query);
			}
	}	
	
	/*public static function followers_profile($userId){
		return $result = DB::select('SELECT f.*,p.*,u.* FROM `following_users` as f left join products as p on f.user_id = p.user_id left join users as u on p.user_id = u.user_id WHERE f.following_user_id =  '.$userId.'');
	}*/

}