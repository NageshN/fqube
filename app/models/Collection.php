<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Collection extends Eloquent implements UserInterface, RemindableInterface {
 public $timestamps = false;
protected $fillable = array('username','email_address','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_collections';
	
	//Get my Collection
	public static function getMyCollections($userId){
		$result = Collection::where('user_id','=',$userId)->get();
		if(count($result) > 0){
			$response = array('status' => 'success','response' => 'fetch my collection','result' => $result);
		}
		else{
			$response = array('status' => 'failure','response' => 'no collection found');
		}
		return $response;
	}
	
	//collect a Product
	public static function collectProduct($details){
		//if its a new collection
		$productExist = Collection ::join('products_mappedto_collection', 'products_mappedto_collection.collection_id', '=', 'users_collections.collections_id')
		                           ->where('products_mappedto_collection.product_id','=',$details['productId'])
		                           ->where('users_collections.user_id','=',$details['userId'])->get();
		if(count($productExist) > 0){						
                
	        }
			else {
			    $product = Product :: where('product_id','=',$details['productId'])->get();
			    $increment = $product[0]['product_save_count'];
			    Product :: where('product_id','=',$details['productId'])
				->update(array('product_save_count' => $increment+1));
			} 
		    if($details['collectionId'] == 'null'){ 
			$collectionExist = Collection :: where('user_id','=',$details['userId'])
											->where('collection_name','=',$details['collectionName'])->get();
			if(count($collectionExist) > 0){	

            }
            else {			
				$collection = new Collection;
				$collection->user_id = $details['userId']; 
				$collection->collection_name = $details['collectionName'];
				$collection->save();
				$collectionId = $collection->id; 
				$response = ProductsMap :: addProductToCollection($details['userId'],$collectionId,$details['productId']);
				}
		    }
            if($details['collectionName'] == 'null'){ 
		    $productExist1 = ProductsMap :: where('collection_id','=',$details['collectionId'])
											->where('product_id','=',$details['productId'])->get();
			
			if(count($productExist1) > 0){ 
				$response = array('status' => 'failure','data'=>$productExist1,'response' => 'Collection name already exist');
			}
			else{
			$response = ProductsMap :: addProductToCollection($details['userId'],$details['collectionId'],$details['productId']);
			}
		}
		return $response;
	}
	
	//collect a Product
	public static function collectProductOnUrl($details){ 
	    if($details['collectionId'] == 'null'){ 
			$collectionExist = Collection :: where('user_id','=',$details['userId'])
											->where('collection_name','=',$details['collectionName'])->get();
			if(count($collectionExist) > 0){	

            }
            else {			
				$collection = new Collection;
				$collection->user_id = $details['userId']; 
				$collection->collection_name = $details['collectionName'];
				$collection->save();
				$collectionId = $collection->id; 
				$productExist1 = ProductsMap :: where('user_id','=',$details['userId'])
									 ->where('product_id','=',$details['productId'])
									 ->update(array('collection_id' => $collectionId));
				$response = array('status' => 'success','data'=>$productExist1,'response' => 'Added successfully');
				}
		    }
	    else{
		//if its a new collection
		$productExist1 = ProductsMap :: where('user_id','=',$details['userId'])
									 ->where('product_id','=',$details['productId'])
									 ->update(array('collection_id' => $details['collectionId']));
		$response = array('status' => 'success','data'=>$productExist1,'response' => 'Added successfully');
		}
		return $response;
	}
	
	public static function getUserCollections($id)
	{
		return $id; 
	} 
	
	//Create Default Collection for user 
	public static function createDefaultCollection($userId){
	        $collection = new Collection;
			$collection->user_id = $userId;
			$collection->collection_name = 'Things I want as gifts';
			$collection->save();
			$collection = new Collection;
			$collection->user_id = $userId;
			$collection->collection_name = 'Things I Like';
			$collection->save();
			$result = array('status'=> 'success');
	}
		
	//get collection profile Products
	public static function collectionProfile($userId,$collectionId,$pageNumber,$numberOfProduct){
	    $take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1));
		$collection = DB::select('SELECT uc.*,pmc.product_id,p.*, pl.liked_by as liked_status FROM `users_collections` as uc left join products_mappedto_collection as pmc on (uc.collections_id = pmc.collection_id) left join products as p on p.product_id = pmc.product_id 
		left join product_likes as pl on p.product_id = pl.product_id
		WHERE uc.collections_id = '.$collectionId.'
		GROUP BY p.product_id
		ORDER BY p.product_id DESC
		LIMIT '.$take.'
		OFFSET '.$skip.''); 
		$collection1 = DB::select('SELECT uc.*,pmc.product_id,p.*,u.*, pl.liked_by as liked_status FROM `users_collections` as uc left join products_mappedto_collection as pmc on (uc.collections_id = pmc.collection_id) left Join products as p on p.product_id = pmc.product_id 
		left join product_likes as pl on p.product_id = pl.product_id left join users as u on u.user_id = '.$userId.'
		WHERE uc.collections_id = '.$collectionId.'
		GROUP BY p.product_id ORDER BY p.product_id DESC');
		$collection2 = DB::select('SELECT uc.*,pmc.product_id,p.*,u.*, pl.liked_by as liked_status FROM `users_collections` as uc left join products_mappedto_collection as pmc on (uc.collections_id = pmc.collection_id) left Join products as p on p.product_id = pmc.product_id 
		left join product_likes as pl on p.product_id = pl.product_id left join users as u on u.user_id = '.$userId.'
		WHERE uc.collections_id = '.$collectionId.'
	    ORDER BY p.product_id DESC');
		if(count($collection) != '0'){
				return array('status'=>'1','count'=>count($collection),'data'=>$collection,'product_count'=>count($collection1),'details'=>$collection1 ,'likeDetails'=>$collection2);
			}else{
				return array('status'=>'0','count'=>count($collection),'product_count'=>count($collection1)); 
			}	
	}
	
	//get collection profile
	public static function collectionProfileDetails($collectionId){
		$query = DB::table('users_collections')
            ->leftJoin('users', 'users.user_id', '=', 'users_collections.user_id')
            ->select('users.user_id', 'users.username', 'users_collections.user_id', 'users_collections.collections_id', 'users_collections.collection_name')
            ->where('users_collections.collections_id', '=', $collectionId)
            ->first();
		
		if(count($query) != '0'){
				return array('status'=>'1','count'=>count($query),'data'=>$query);
			}else{
				return array('status'=>'0','count'=>count($query));
			}		
	}
	//get collection profile follow status
	public static function collectionProfileUserFollowStatus($collectionId,$userId){
			$query =DB::table('following_collection')
			->where('user_id', '=', $userId)
			->where('collection_id', '=', $collectionId)
            ->get();
		if(count($query) != '0'){
				return array('status'=>'1');
			}else{
				return array('status'=>'0');
				
			}		
	}
	
}