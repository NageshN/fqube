<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ProductsMap extends Eloquent implements UserInterface, RemindableInterface {
 public $timestamps = false;
protected $fillable = array('username','email_address','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products_mappedto_collection';
	
	public static function addProductToCollection($userId,$collectionId,$productId){
		$productMappedAlready = ProductsMap :: where('collection_id','=',$collectionId)
											->where('product_id','=',$productId)->get();
		
		if(count($productMappedAlready) > 0 ){
			$response = array('status' => 'failure','response' => 'Added already');
		}
		else{
			$productMap = new ProductsMap;
			$productMap->collection_id = $collectionId;
			$productMap->product_id = $productId;
			$productMap->user_id = $userId;
			$productMap->save();
			$response = array('status' => 'success','data'=>$productMappedAlready,'response' => 'Added successfully');
		}
		return $response;
	}
}