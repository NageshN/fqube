<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class store extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'stores'; 
								
	public static function getAllStores($pageNumber,$numberOfProduct){
	    $take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1)); 
		$query = store::GroupBy('store_id')
		->orderBy('store_follow_count','DESC')
		->take($take) 
		->skip($skip)
		->get();
			if(count($query) != '0'){
				return array('status'=>'1','count'=>count($query),'data'=>$query);
			}else{
				return array('status'=>'0','count'=>count($query)); 
			}
	}
	
	public static function getAllStoreFollowedByUser($userId){
			$query = DB::table('following_stores')
            ->select('following_stores.following_store_id')
            ->where('following_stores.user_id', '=', $userId)
            ->get();
			$count = count($query);
			if($count != '0'){
				return array('status'=>'1','count'=>$count,'data'=>$query);
			}else{
				return array('status'=>'0','count'=>$count,'data'=>$query);
			}
	}
	
	public static function searchByStore($str){
			$query = store::where('store_url','like','%'.$str.'%')->get();
			if(count($query) != '0'){
				return array('status'=>'1','count'=>count($query),'data'=>$query);
			}else{
				return array('status'=>'0');
			}
	} 
	public static function storeProfile($storeId){
			$query = store::where('store_id',$storeId)->first();
		if(count($query) != '0'){
				return array('status'=>'1','count'=>count($query),'data'=>$query);
			}else{
				return array('status'=>'0');
			}		
	} 
	public static function storeProfileUserFollowStatus($storeId,$userId){
			$query =DB::table('users')
            ->join('following_stores', 'following_stores.user_id', '=', 'users.user_id')
            ->join('stores', 'stores.store_id', '=', 'following_stores.following_store_id')
            ->select('following_stores.user_id as followed_status')
			->where('following_stores.user_id', '=', $userId)
			->where('following_stores.following_store_id', '=', $storeId)
            ->get();
		if(count($query) != '0'){
				return array('status'=>'1','count'=>count($query));
			}else{
				return array('status'=>'0');
				
			}		
	}
	public static function sameStore($storeId)
	{
		return Product::join('stores','stores.store_id','=','products.store_id')
				->select('stores.store_name','stores.store_id','products.product_image','products.product_name')
				->where('stores.store_id','=',$storeId)
				->orderBy('products.product_id','DESC')
				->take(4)
				->get();
	}
	
	public static function getProductsByStore($storeId,$pageNumber,$numberOfProduct){
	    $take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1));
		$query1 = DB::table('products')
            ->leftJoin('users', 'users.user_id', '=', 'products.user_id')
            ->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
            ->select('users.user_id', 'users.username', 'users.profile_picture', 'users.email_address', 'products.product_id', 'products.product_image', 'products.product_description', 'products.product_name', 'products.product_image', 'products.store_id', 'products.product_like_count', 'product_likes.liked_by as liked_status')
            ->where('products.store_id',$storeId)
            ->groupBy('products.product_id')
			->get();
		$query = DB::table('products')
            ->leftJoin('users', 'users.user_id', '=', 'products.user_id')
            ->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
            ->select('users.user_id', 'users.username', 'users.profile_picture', 'users.email_address', 'products.product_id', 'products.product_image', 'products.product_description', 'products.product_name', 'products.product_image', 'products.store_id', 'products.product_like_count', 'product_likes.liked_by as liked_status')
            ->where('products.store_id',$storeId)
            ->groupBy('products.product_id')
            ->orderBy('products.product_id','DESC')
			->take($take) 
			->skip($skip)
			->get();
	    $queryCount = DB::table('products')
            ->leftJoin('users', 'users.user_id', '=', 'products.user_id')
            ->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
            ->select('products.product_id','products.product_like_count', 'product_likes.liked_by as liked_status')
            ->where('products.store_id',$storeId)
            ->orderBy('products.product_id','DESC')
			->get();
		if(count($query) != '0'){
				return array('status'=>'1','count'=>count($query),'data'=>$query,'product_count'=>count($query1),'likeDetails'=>$queryCount);
			}else{
				return array('status'=>'0','count'=>count($query),'product_count'=>count($query1));
			}		
	}
	//add a store once the product has posted 
	public static function addAStore($details){
		$storeArray = explode(".", $details['store_link']);
		if($storeArray['1'] == 'com' || $storeArray['1'] == 'in'){
		$storeName = $storeArray['0'];
		}
		else{
		$storeName = $storeArray['1'];
		}
		$store = new store;
		$store->store_name = $storeName;
		$store->store_image = 'template/img/user/user-avatar.png';
		$store->store_url = $details['store_link'];
		$store->save();
		return $storeId = $store->id;
	}
}