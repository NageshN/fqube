<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Product extends Eloquent implements UserInterface, RemindableInterface {
public $timestamps = false;
protected $fillable = array('username','email_address','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';
	
	
	//search a product
	public static function searchProduct($searchText,$numberOfProduct,$pageNumber){
		$take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1));
		$result = Product :: where('product_name','like','%'.$searchText['search_text'].'%')
							->join('users','users.user_id','=','products.user_id')
							->orderBy('product_id','DESC')
							->take($take) 
			                ->skip($skip)
							->get();
		if(count($result) > 0){
			$response = array('status' => 'success','details' => $result, 'count' => count($result));
		}
		else{
			$response = array('status' => 'failure','product_name' => 'No result Found');
		}
			return $response;
	} 
	
	//my feed in homepage
	
	public static function myfeed($userId,$num_products,$page_nos){ 
	     $take=(($num_products));
		 $skip=(($num_products)*($page_nos-1)); 
		$result = Product :: leftjoin('user_tagged_category','user_tagged_category.category_id','=','products.category_id')
							->leftjoin('following_users','following_users.following_user_id','=','products.user_id')
							->leftjoin('following_stores','following_stores.following_store_id','=','products.store_id')
							->leftjoin('users','users.user_id','=','products.user_id')
							->leftjoin('product_likes','product_likes.product_id','=','products.product_id')
							//->leftjoin('users_collections','users_collections.user_id','=',$userId)
							//->leftjoin('products_mappedto_collection','products_mappedto_collection.product_id','=','products.product_id')
							->where('user_tagged_category.user_id','=',$userId)
							->orWhere('following_users.user_id','=',$userId)
							->orWhere('following_stores.user_id','=',$userId)
							->orWhere('products.user_id','=',$userId)
				            ->select('products.product_id','products.user_id','products.product_image','products.products_site_url','products.product_description','products.product_currency','products.product_price','products.product_name','user_tagged_category.category_id','following_users.following_user_id','following_stores.following_store_id','product_likes.liked_by','products.product_like_count','users.user_id','users.username','users.email_address','users.user_followers_count','users.profile_picture','users.registerd_through','users.gender')
							->groupBy('products.product_id')
							->orderBy('products.product_id','DESC')
							->take($take) 
			                ->skip($skip)  
							->get();
							
		$likeDetails = ProductLike::where('product_likes.liked_by','=',$userId)
							->get();
		$response	= array('status' => 'success', 'data' => $result, 'likeDetails' => $likeDetails);		
		return $response;
	     
	} 
	
	public static function myfeed1($userId,$num_products,$page_nos){ 
		$skip=(($num_products)*($page_nos-1)); 
		 $result = DB::select('SELECT utc.category_id,fu.following_user_id,fs.following_store_id,u.*,pl.liked_by like_status,pmc.product_id collection_status,p.* 
		from user_tagged_category as utc 
		join products as p join following_users as fu join following_stores as fs 
		on 
		(utc.category_id = p.category_id || fu.following_user_id = p.user_id || fs.following_store_id = p.store_id) 
		left join users as u on (u.user_id = p.user_id)
		left join product_likes as pl on 
		('.$userId.' = pl.liked_by AND p.product_id = pl.product_id) left Join users_collections as uc on ('.$userId.' = uc.user_id) left join products_mappedto_collection as pmc on
		(pmc.product_id = p.product_id)
		where utc.user_id = '.$userId.' AND fs.user_id = '.$userId.' OR fu.user_id = '.$userId.' group by p.product_id desc limit '.$skip.','.$num_products.'')->toSql();
		
		return $result;
	}
	
	public static function myfeed_modified($userId){ 
		$result = Product :: join('user_tagged_category','user_tagged_category.category_id','=','products.category_id')
							->join('following_users','following_users.following_user_id','=','products.user_id')
							->join('product_likes','product_likes.product_id','=','products.product_id')
							->where('user_tagged_category.user_id','=',$userId)
							->orWhere('following_users.user_id','=',$userId	)
				->select('products.product_id','products.user_id','products.product_image','products.product_name','product_likes.product_id','product_likes.liked_by',DB::raw('count(product_likes.product_id)'))
							->groupBy('product_likes.product_id')
							//->groupBy('products.product_id')
							->get();
		
		return $result;
	}
	
	//product list which is collected more time 
	public static function highestCollection(){
	
	  $skip=(($num_products)*($page_nos-1)); 
		$result = DB :: select('select u.*,pmc.*,prod.*,count(pmc.product_id) as collect_count from products_mappedto_collection as pmc left join products as prod on prod.product_id = pmc.product_id 
								join users as u on u.user_id = prod.user_id
								GROUP BY pmc.product_id 
								ORDER BY collect_count DESC');
		return $result;
	}
	 
	//product which has got highest number of likes
	public static function highestLike($userId,$num_products,$page_nos){
	    $skip=(($num_products)*($page_nos-1)); 
		$result = DB :: select('select u.*,pl.*,pl.liked_by like_status,pmc.product_id collection_status,prod.*,count(pl.product_id) as like_count 
		from product_likes as pl join products as prod on (prod.product_id = pl.product_id) OR
		('.$userId.' = pl.liked_by AND prod.product_id = pl.product_id) left Join users_collections as uc on ('.$userId.' = uc.user_id) left join products_mappedto_collection as pmc on
				(pmc.product_id = prod.product_id)
				join users as u on u.user_id = prod.user_id 
									GROUP BY pl.product_id 
									ORDER BY like_count DESC limit '.$skip.','.$num_products.'');
		$resultlikeCount = DB :: select('select u.*,pl.*,pl.liked_by like_status,pmc.product_id collection_status,prod.* 
		from product_likes as pl join products as prod on (prod.product_id = pl.product_id) OR
		('.$userId.' = pl.liked_by AND prod.product_id = pl.product_id) left join products_mappedto_collection as pmc on
				(pmc.product_id = prod.product_id)
				join users as u on u.user_id = prod.user_id');
		$response	= array('status' => 'success', 'data' => $result, 'likeDetails' => $resultlikeCount);
		return $response;
	}
	
	//get the product details
	public static function getProductDetails($productId){
		$result = Product::where('product_id','=',$productId)
							->leftjoin('users', 'users.user_id', '=', 'products.user_id')
							->leftjoin('stores', 'stores.store_id', '=', 'products.store_id')
							->leftjoin('categories', 'categories.category_id', '=', 'products.category_id')
							->get();
	 	$collectionDetail = DB::select('select pmc.collection_id,uc.* from products_mappedto_collection as pmc join users_collections as uc on pmc.collection_id = uc.collections_id where pmc.product_id = '.$productId.' AND uc.user_id = '.$result[0]['user_id'].'');
		$likeCount = DB::select('select count(*) as like_count from product_likes where product_id = '.$productId.'');
	$response	= array('status' => 'success', 'product_detail' => $result, 'collection_details' => $collectionDetail,'product_like' => $likeCount);
		return $response;
	}
	
	//Get product list from the same category
	public static function getProductFromTheSameCategory($categoryId,$productId,$userId){
		//$result = Product::where('category_id','=',$categoryId)->get();
		$result1 = Product::leftjoin('users','users.user_id','=','products.user_id')
		                  //->where('products.user_id','=',$result[0]['user_id'])
						//->where('products.product_id','!=',$productId)
						->where('category_id','=',$categoryId)
						->groupBy('products.product_id')
						->orderBy('products.product_id','DESC')->take(4)->get();
		$likeDetails = ProductLike::where('product_likes.liked_by','=',$userId)
							->get();
										
		return array('status'=>'1','count'=>count($result1),'data'=>$result1,'likeDetails'=>$likeDetails);
		
	}
	
	//no of products in that Category starts
	public static function noOfProductsinCategory($categoryId){
		$result = Product::where('category_id','=',$categoryId)->get();
        $result1 = categories::where('category_id','=',$categoryId)->get();
		$response	= array('status' => 'success', 'product_count' => count($result), 'category_details' => $result1);
		return $response;
		
	}
	
	//get Product list From The Posted User 
	public static function getProductFromThePostedUser($productId){
		$result = Product::where('product_id','=',$productId)->get();
		$result = Product::leftjoin('users','users.user_id','=','products.user_id')
		//->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
		->where('products.user_id','=',$result[0]['user_id'])
						->where('products.product_id','!=',$productId)
						->groupBy('products.product_id')
						->orderBy('products.product_id','DESC')->take(4)->get();
		$resultCount = Product::leftjoin('users','users.user_id','=','products.user_id')
		->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
		->where('products.user_id','=',$result[0]['user_id'])
						->where('products.product_id','!=',$productId)
						->orderBy('products.product_id','DESC')->get();
		return array('status'=>'1','count'=>count($result),'data'=>$result,'likeDetails'=>$resultCount);
	}
	
	//no of products in user profile starts
	public static function noOfProductsOfUser($userId){
		$result = Product::where('user_id','=',$userId)->get();
		$result1 = User::where('user_id','=',$userId)->get();
		$response	= array('status' => 'success', 'product_count' => count($result), 'products' => $result, 'username' => $result1);
		return $response;
		
	}
	
	//homePage Products
	
	public static function homePageproduct($userId){
		if($userId == 'null'){
			$result = Product::where('uploaded_by','=','admin')->get();
		}
		else{
			$result = DB :: select('SELECT DISTINCT p.product_id,p.*,pl.liked_by like_status,uc.collections_id,pmc.product_id collection_status FROM `products` as p left join product_likes as pl on 
						('.$userId.' = pl.liked_by AND p.product_id = pl.product_id) left Join users_collections as uc on ('.$userId.' = uc.user_id) left join products_mappedto_collection as pmc on
						(pmc.product_id = p.product_id) where p.uploaded_by = "admin"');
		}
		
		
		return $result;
	}
	
	//post a product 
	public static function postProduct($details){
		$productExist = Product :: where('user_id', '=', $details['user_id'])
									->where('products_site_url', '=', $details['products_site_url'])->get();
		if(count($productExist) > 0){
			return array('status' => 'failure','response' => 'already posted this product');
		}
		//check store is already exist
		//$storeArray = explode(".", $details['store_link']);
		$storeExist = store :: searchByStore($details['store_link']); 
		if($storeExist['status'] == '1'){
			$storeId = $storeExist['data'][0]['store_id'];
		}
		else{
			$storeId = store :: addAStore($details);
		}
		//get category id
		$category = categories :: getAllCategories();
        $set = 0;
		$title = strtolower($details['product_name']);
		for($i=0; $i<count($category['data']);$i++){
			if($category['data'][$i]['category_name'] == "women"){
			 if (strpos($title,'women') || strpos($title,'tops') || strpos($title,'wedge') || strpos($title,'bellies') || strpos($title,'ballet') || strpos($title,'salwars') || strpos($title,'churidars') || strpos($title,'dress material') || strpos($title,'bra') || strpos($title,'handbag') || strpos($title,'tote') || strpos($title,'clutch') || strpos($title,'legging') || strpos($title,'pallazo') || strpos($title,'saree') || strpos($title,'kurta') || strpos($title,'kurti') || strpos($title,'makeup kit') || strpos($title,'women sunglasses') || strpos($title,'dress') || strpos($title,'shrug') || strpos($title,'skirt') || strpos($title,'woman') || strpos($title,'heel') || strpos($title,'unisex') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "men"){
			 if (strpos($title,'men') || strpos($title,'sneaker') || strpos($title,'loafer') || strpos($title,'unisex') || strpos($title,'collared tee') || strpos($title,'ties') || strpos($title,'cufflink') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "kids"){
			 if (strpos($title,'kid') || strpos($title,'boy') || strpos($title,'frock') || strpos($title,'toy') || strpos($title,'clog') || strpos($title,'stationary') || strpos($title,'wipes') || strpos($title,'diaper') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "gadgets"){
			 if (strpos($title,'gadget') || strpos($title,'mobile') || strpos($title,'phone') || strpos($title,'notebook') || strpos($title,'zenfone') || strpos($title,'Asus') || strpos($title,'motorola') || strpos($title,'huawei') || strpos($title,'lenovo') || strpos($title,'mi') || strpos($title,'asus') || strpos($title,'samsung') || strpos($title,'microsoft') || strpos($title,'moto') || strpos($title,'redmi') || strpos($title,'tablet') || strpos($title,'laptop') || strpos($title,'hp') || strpos($title,'dell') || strpos($title,'compaq') || strpos($title,'computer') || strpos($title,'printer') || strpos($title,'monitor') || strpos($title,'cartridge') || strpos($title,'usb') || strpos($title,'router') || strpos($title,'power bank') || strpos($title,'pendrive') || strpos($title,'headphone') || strpos($title,'headset') || strpos($title,'mouse') || strpos($title,'keyboard') || strpos($title,'hard disk') || strpos($title,'tv') || strpos($title,'television') || strpos($title,'micromax') || strpos($title,'refrigerator') || strpos($title,'washing machine') || strpos($title,'air conditioner') || strpos($title,'whirlpool') || strpos($title,'vacuum cleaners') || strpos($title,'water purifier') || strpos($title,'fan') || strpos($title,'kitchen appliance') || strpos($title,'microwave oven') || strpos($title,'mixer')|| strpos($title,'appliance') || strpos($title,'induction') || strpos($title,'shaver') || strpos($title,'speaker') || strpos($title,'ipod') || strpos($title,'accessories') || strpos($title,'camera') || strpos($title,'gaming') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "lifestyle"){
			 if (strpos($title,'lifestyle') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "furniture"){                                                               
			 if (strpos($title,'furniture') || strpos($title,'sofa') || strpos($title,'loveseat') || strpos($title,'recliner') || strpos($title,'chair') || strpos($title,'dining') || strpos($title,'table') || strpos($title,'bench') || strpos($title,'stool') || strpos($title,'ottoman') || strpos($title,'study set') || strpos($title,'bar unit') || strpos($title,'storage') || strpos($title,'bookshelf') || strpos($title,'display unit') || strpos($title,'wall shelf') || strpos($title,'shoe rack') || strpos($title,'cabinet') || strpos($title,'tv unit') || strpos($title,'sideboard') || strpos($title,'balcony set') || strpos($title,'bed') || strpos($title,'drawers') || strpos($title,'desk') || strpos($title,'mirror') || strpos($title,'dresser') || strpos($title,'wardrobe') || strpos($title,'carpet') || strpos($title,'decor') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "hair & beauty"){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
			 if (strpos($title,'hair') || strpos($title,'beauty') || strpos($title,'grooming') || strpos($title,'skincare') || strpos($title,'makeup') || strpos($title,'fragrance') || strpos($title,'bath & body') || strpos($title,'diet') || strpos($title,'wine') || strpos($title,'lakme') || strpos($title,'maybelline') || strpos($title,'kaya') || strpos($title,'olay') || strpos($title,'wella') || strpos($title,'neutrogena') || strpos($title,'shop') || strpos($title,'scholl') || strpos($title,'dove') || strpos($title,'livon') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "sports"){
			 if (strpos($title,'sport') || strpos($title,'football') || strpos($title,'cricket') || strpos($title,'basketball') || strpos($title,'golf') || strpos($title,'outdoor') || strpos($title,'outdoor shoe') || strpos($title,'outdoor clothing') || strpos($title,'running') || strpos($title,'tennis') || strpos($title,'climachill') || strpos($title,'running shoe') || strpos($title,'tracksuit') || strpos($title,'gym') || strpos($title,'tights') || strpos($title,'gym wear') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "jewellery"){
			 if (strpos($title,'jewellery') || strpos($title,'earring') || strpos($title,'pendant') || strpos($title,'bangle') || strpos($title,'bracelet') || strpos($title,'necklace') || strpos($title,'nosepin') || strpos($title,'mangalsutra') || strpos($title,'chain') || strpos($title,'diamond') || strpos($title,'gemstone') || strpos($title,'platinum') || strpos($title,'gold coin') || strpos($title,'solitaire') || strpos($title,'ring') || strpos($title,'junk jewellery ') || strpos($title,'dangler') || strpos($title,'drops') || strpos($title,'jhumka') || strpos($title,'hoop') || strpos($title,'stud') || strpos($title,'anklet') || strpos($title,'maang tikka') || strpos($title,'keychain') || strpos($title,'artificial set') || strpos($title,'baju band') || strpos($title,'brooch') || strpos($title,'nose ring') || strpos($title,'waist belt') || strpos($title,'pearl') || strpos($title,'filigree') || strpos($title,'terracotta') === true) 
             {
                $set = 1;
			    break;
             }
			}
			elseif($category['data'][$i]['category_name'] == "gifts"){
			 if (strpos($title,'gift') === true) 
             {
                $set = 1;
			    break;
             }
			}
 			
		}
		 if($set == 1) {
			   $categoryId =  $category['data'][$i]['category_id'];
			}
			else{
				$categoryId = 13;
			}
		
		
		$product = new Product;
		$product->store_id = $storeId;
		$product->user_id = $details['user_id'];
		$product->products_site_url = $details['products_site_url'];
		$product->product_image = $details['product_image'];
		$product->product_description = $details['product_description'];
		$product->product_name = $details['product_name'];
		$product->product_price = $details['product_price'];
		$product->product_currency = $details['product_currency'];
		$product->category_id = $categoryId;
		$product->save();
		$productId = $product->id;
		//By default add the procduct to default collection
		$ProductsMap = Collection::where('user_id', '=', $details['user_id'])
		                          ->where('collection_name', '=', 'Things I Like')
								  ->get();
		$product = Product :: where('product_id','=',$productId)->get();
	    $increment = $product[0]['product_save_count'];
	    Product :: where('product_id','=',$productId)
		->update(array('product_save_count' => $increment+1));
		$CollectionId = $ProductsMap[0]['collections_id'];
        $ProductsMapCollection = new ProductsMap;
        $ProductsMapCollection->user_id = $details['user_id'];
        $ProductsMapCollection->collection_id = $CollectionId;
        $ProductsMapCollection->product_id = $productId;
        $ProductsMapCollection->save();
         		
		$response = Product :: where('product_id', '=', $productId)->get();
		return array('status' => 'success','product_info' => $response);
		
	}
	
	
	//get followers details
	
	public static function getFollowersDetails1($userId,$profile_id){ 

		  $result =FollowModel::leftjoin('users','users.user_id','=','following_users.user_id')
								->leftjoin('products','products.user_id','=','users.user_id')
								->where('following_users.following_user_id','=',$userId)
								->select('following_users.user_id','products.product_id','products.product_name','users.username','users.user_id','users.profile_picture','products.product_image','users.user_followers_count')
								->orderBy('users.user_followers_count','DESC')
								//->skip($skip)
								//->take($numProducts)
								->get();
		if($result != '[]')
		{
			for($i=0;$i<count($result);$i++)
			{
				$foll_id[$i]=$result[$i]['user_id'];
			}
			  $check=FollowModel::whereIn('following_user_id',$foll_id)->where('user_id','=',$profile_id)->get(array('following_user_id'));
		
					
		$json= json_encode(array('data'=>$result,'count'=>count($result),'follow_status'=>$check));
		$jsonObject = json_decode($json);
		$categories = array();
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->user_id])) {
		$categories[$element->user_id] = array();
		}
		$categories[$element->user_id][] = $element;
		//unset($element->user_id);
		}
		$new_array = array_values($categories); 
		return array('data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check);
		
		}
		else{
		}
			
		
	}
	
	public static function getFollowingUsersDetails($userId,$profile_id){
		
		
		   $result =FollowModel::leftjoin('users','users.user_id','=','following_users.following_user_id')
								->leftjoin('products','products.user_id','=','following_users.following_user_id')
								->where('following_users.user_id','=',$userId)
								->select('following_users.following_user_id','products.product_id','products.product_name','users.username','users.user_id','users.profile_picture','products.product_image','users.user_followers_count')
								->orderBy('users.user_followers_count','DESC')
								//->skip($skip)
								//->take($numProducts)
								->get();
								
							if($result != '[]') 
		{
			for($i=0;$i<count($result);$i++)
			{
				$follo_id[$i]=$result[$i]['following_user_id'];
			}
			  $check=FollowModel::whereIn('following_user_id',$follo_id)->where('user_id','=',$profile_id)->get(array('following_user_id'));	
				$json= json_encode(array('data'=>$result,'count'=>count($result)));
		$jsonObject = json_decode($json);
		$categories = array();
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->following_user_id])) {
		$categories[$element->following_user_id] = array();
		}
		$categories[$element->following_user_id][] = $element;
		//unset($element->user_id);
		}
		$new_array = array_values($categories);
		return array('status'=>'success','data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check);	
			}
else{
return array('status'=>'success','data'=>$result,'count'=>count($result));
}			
		
	}
	
	public static function getStoreDetails($userId,$profile_id){
		//$result = DB :: select('SELECT p.*,s.* FROM `products` as p join stores as s on p.store_id = s.store_id where p.user_id = '.$userId.' order by p.store_id ASC ');
		$result=followingStore::join('stores','stores.store_id','=','following_stores.following_store_id')
								->join('products','products.store_id','=','stores.store_id') 
								->join('users','users.user_id','=','products.user_id')
								//->join('products','products.user_id','=','users.user_id')
								->select('stores.store_name','stores.store_image','users.profile_picture','products.product_image','users.user_id','products.product_id','stores.store_id','stores.store_follow_count')
								->where('following_stores.user_id','=',$userId)
								->get();
	
				if($result !="[]") 
				{
					for($i=0;$i<count($result);$i++)
				{
					$store_follow[$i]=$result[$i]['store_id'];
					$product_id[$i]=$result[$i]['product_id'];
				}
				//$userSaves=DB::table('users_saved_list')->where('user_id','=',$userId)->whereIn('product_id',$product_id)->get(array('product_id'));
				
				$check_store=followingStore::whereIn('following_store_id',$store_follow)->where('user_id','=',$profile_id)->get(array('following_store_id'));
				$json= json_encode(array('data'=>$result,'count'=>count($result)));
				$jsonObject = json_decode($json);
			$categories = array();
		
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->store_id])) {
		$categories[$element->store_id] = array();
		}
		$categories[$element->store_id][] = $element;
		//unset($element->store_name);
		}
		$new_array = array_values($categories);
		//return $new_array;
		return array('data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check_store);
				}
				else{
				}
		
		
	}
}