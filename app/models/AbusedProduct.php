<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AbusedProduct extends Eloquent implements UserInterface, RemindableInterface {
public $timestamps = false;

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'abused_product';
	
	
	//search a product
	public static function abuseAnProduct($userId,$productId){
		$alreadyAbused = AbusedProduct :: where('product_id','=',$productId)
		                ->where('abused_by','=',$userId)
						->get();
			$alreadyAbused1 = Product :: where('product_id','=',$productId)
						->get();				
		if(count($alreadyAbused) > 0){
			$result = array('status' => 'failure','response' => 'already reported');
		} 
		else{
		$alreadyAbused1 = Product :: where('product_id','=',$productId)
							->update(array('number_of_abuse' => $alreadyAbused1[0]['number_of_abuse']+1));
			$abuse = new AbusedProduct;
			$abuse->product_id = $productId;
			$abuse->abused_by = $userId;
			$abuse->abuse_count = 1;
			$abuse->save();
			$result = array('status' => 'success','response' => 'abused successfully');
		}
		return $result;
	}
	
	//get the product abused result
	public static function getProductAbusedResult($userId,$productId){
	 $alreadyAbused = AbusedProduct :: where('product_id','=',$productId)
		                ->where('abused_by','=',$userId)
						->get();
	if(count($alreadyAbused) > 0){
			$result = array('status' => 'failure','response' => 'already reported');
		} 
	else{
            $result = array('status' => 'success','response' => 'abused successfully');
        }	
		return $result;
	}
}