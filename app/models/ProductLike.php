<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ProductLike extends Eloquent implements UserInterface, RemindableInterface {
public $timestamps = false;
protected $fillable = array('username','email_address','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_likes';
	
	public static function likeProduct($details){
		
		$alreadyLiked = ProductLike :: where('product_id','=',$details['product_id'])
									->where('liked_by','=',$details['user_id'])->get();
		if(count($alreadyLiked) > 0){
			$response = array('status' => 'failure', 'response' => 'already you have liked');
		}
		else{
			$product = new ProductLike;
			$product->product_id = $details['product_id'];
			$product->liked_by = $details['user_id'];
			$product->save();
			$product = Product :: where('product_id','=',$details['product_id'])->get();
			$increment = $product[0]['product_like_count'];
			Product :: where('product_id','=',$details['product_id'])
										->update(array('product_like_count' => $increment+1));
			$response = array('status' => 'success', 'response' => 'product liked');
		}
		return $response;
	}
	
}