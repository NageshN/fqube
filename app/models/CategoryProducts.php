<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class CategoryProducts extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public $timestamps = false;
	protected $table = 'products'; 
	
	//Get category Based Products 
	public static function getProductsByCategory($categoryId,$pageNumber,$numberOfProduct){
		$take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1));
		$result = DB::table('products')
            ->leftJoin('users', 'users.user_id', '=', 'products.user_id')
            ->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
            ->select('users.user_id', 'users.username', 'users.profile_picture', 'users.email_address', 'products.product_id', 'products.product_image', 'products.product_description', 'products.product_name', 'products.product_image', 'products.store_id', 'products.product_like_count', 'products.product_price', 'products.product_currency', 'product_likes.liked_by as liked_status')
            ->where('products.category_id',$categoryId)
			->groupBy('products.product_id')
			->orderBy('products.product_id','DESC')
			->take($take) 
			->skip($skip)
			->get();
		$resulttotalCount = DB::table('products')
            ->leftJoin('users', 'users.user_id', '=', 'products.user_id')
            ->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
            ->select('users.user_id', 'users.username', 'users.profile_picture', 'users.email_address', 'products.product_id', 'products.product_image', 'products.product_description', 'products.product_name', 'products.product_image', 'products.store_id', 'products.product_like_count', 'products.product_price', 'products.product_currency', 'product_likes.liked_by as liked_status')
            ->where('products.category_id',$categoryId)
			->groupBy('products.product_id')
			->orderBy('products.product_id','DESC')
			->get();
        $resultCount = DB::table('products')
            ->leftJoin('users', 'users.user_id', '=', 'products.user_id')
            ->leftJoin('product_likes', 'product_likes.product_id', '=', 'products.product_id')
            ->select('users.user_id', 'users.username', 'users.profile_picture', 'users.email_address', 'products.product_id', 'products.product_image', 'products.product_description', 'products.product_name', 'products.product_image', 'products.store_id', 'products.product_like_count', 'products.product_price', 'products.product_currency', 'product_likes.liked_by as liked_status')
            ->where('products.category_id',$categoryId)
			->orderBy('products.product_id','DESC')
			->get();
		if(count($result) != '0'){	
				return array('status'=>'1','count'=>count($result),'data'=>$result,'likeDetails'=>$resultCount,'totalCount'=>count($resulttotalCount));
			}else{
				return array('status'=>'0');
			}
	}
	
	
	//follow Collection by people
	public static function getFollowPeopleDetailsCollection($profile_id,$userId,$user){ 
		  $result =FollowCollection::leftjoin('users','users.user_id','=','following_collection.user_id')
								->leftjoin('products','products.user_id','=','users.user_id') 
								->leftjoin('following_users','following_users.following_user_id','=','following_collection.user_id')
								->where('following_collection.user_id','=',$userId)
								->select('following_collection.user_id','following_collection.collection_id','products.product_id','products.product_name','users.username','users.profile_picture','users.user_followers_count','products.product_image','products.product_currency')
								->groupBy('products.product_id')
								->orderBy('users.user_id','DESC')
								->get();
		if($result != '[]')
		{
			for($i=0;$i<count($result);$i++)
			{
				$foll_id[$i]=$result[$i]['user_id'];
			} 
			  $check=FollowModel::whereIn('following_user_id',$foll_id)->where('user_id','=',$user)->get(array('following_user_id'));
		
					
		$json= json_encode(array('data'=>$result,'count'=>count($result),'follow_status'=>$check));
		$jsonObject = json_decode($json);
		$categories = array();
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->user_id])) {
		$categories[$element->user_id] = array();
		}
		$categories[$element->user_id][] = $element;
		//unset($element->user_id);
		}
		$new_array = array_values($categories); 
		return array('data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check);
		
		}
		else{  
		}
			
		
	} 
	  
	//follow Store by people
	public static function getFollowPeopleDetailsStore($profile_id,$userId,$user){ 
		  $result =followingStore::leftjoin('users','users.user_id','=','following_stores.user_id')
								->leftjoin('products','products.user_id','=','users.user_id')
								->leftjoin('following_users','following_users.following_user_id','=','following_stores.user_id')
								->where('following_stores.user_id','=',$userId)
								->select('following_stores.user_id','following_stores.following_store_id','products.product_id','products.product_name','users.username','users.profile_picture','users.user_followers_count','products.product_image','products.product_currency')
								->groupBy('products.product_id')
								->orderBy('users.user_id','DESC')
								->get();
		if($result != '[]')
		{
			for($i=0;$i<count($result);$i++)
			{
				$foll_id[$i]=$result[$i]['user_id'];
			}
			  $check=FollowModel::whereIn('following_user_id',$foll_id)->where('user_id','=',$user)->get(array('following_user_id'));
		
					
		$json= json_encode(array('data'=>$result,'count'=>count($result),'follow_status'=>$check));
		$jsonObject = json_decode($json);
		$categories = array();
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->user_id])) {
		$categories[$element->user_id] = array();
		}
		$categories[$element->user_id][] = $element;
		//unset($element->user_id);
		}
		$new_array = array_values($categories); 
		return array('data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check);
		
		}
		else{
		}
			
		
	}
	
	
        //store list in register page to follow
        public static function getStoreDetailsToFollow($storeId,$userId){
		  $result= Product::join('stores','stores.store_id','=','products.store_id')
				->join('users','users.user_id','=','products.user_id')
				->select('stores.store_name','stores.store_follow_count','stores.store_id','stores.store_image','users.profile_picture','products.product_image','products.product_image','products.product_image','users.user_id','products.product_id','products.store_id','products.product_currency')
				->where('products.store_id','=',$storeId)
				->groupBy('products.product_id')
				->get(); 
				
				if($result !="[]") 
				{
					for($i=0;$i<count($result);$i++)  
				{
					$store_follow[$i]=$result[$i]['store_id'];
					$product_id[$i]=$result[$i]['product_id'];
				}
				//$userSaves=DB::table('users_saved_list')->where('user_id','=',$userId)->whereIn('product_id',$product_id)->get(array('product_id'));
				
				$check_store=followingStore::whereIn('following_store_id',$store_follow)->where('user_id','=',$userId)->get(array('following_store_id'));
				$json= json_encode(array('data'=>$result,'count'=>count($result)));
				$jsonObject = json_decode($json);
			$categories = array();
		
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->store_id])) {
		$categories[$element->store_id] = array();
		}
		$categories[$element->store_id][] = $element;
		//unset($element->store_id);
		}
		$new_array = array_values($categories);
		//return $new_array;
		//return $new_array;
		return array('data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check_store);
				}
				else{
				}
		
		
	}	
	
	//people list in register page to follow
        public static function getPeopleDetailsToFollow($userId,$user){ 
		
		$result =User::leftjoin('products','products.user_id','=','users.user_id')
								->select('users.user_id','products.product_id','products.product_name','users.username','users.profile_picture','users.user_followers_count','products.product_image')
								->where('users.user_id','=',$userId)
								->where('users.user_id','!=',$user)
								->orderBy('users.user_id','DESC')
								->get();
		if($result != '[]')
		{ 
			for($i=0;$i<count($result);$i++)
			{
				$foll_id[$i]=$result[$i]['user_id'];
			}
			  $check=FollowModel::whereIn('following_user_id',$foll_id)->where('user_id','=',$user)->get(array('following_user_id'));
		
		$productCount = ProductsMap::where('user_id','=',$userId)
                                    ->get();		
		$json= json_encode(array('data'=>$result,'count'=>count($result),'follow_status'=>$check,'productCount'=>count($productCount)));
		$jsonObject = json_decode($json);
		$categories = array();
		foreach($jsonObject->data as $element) {
		if ( ! isset($categories[$element->username])) {
		$categories[$element->username] = array();
		}
		$categories[$element->username][] = $element;
		//unset($element->user_id);
		}
		$new_array = array_values($categories); 
		return array('data'=>$new_array,'count'=>count($new_array),'follow_status'=>$check,'productCount'=>count($productCount));
		
		}
		else{
		}
			
		
	}	
	

}