<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class categories extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait; 
	public $timestamps = false;
	protected $table = 'categories'; 
								
	public static function getAllCategories(){
			$query = categories::groupBy('category_id')
			->get();
			return array('status'=>'1','data'=>$query);
	}
	
	public static function getAllCategoriesSelected($userId){
			$query = DB::table('user_tagged_category')
            ->select('user_tagged_category.category_id')
            ->where('user_tagged_category.user_id', '=', $userId)
			->groupBy('category_id')
            ->get();
			$count = count($query);
			if($count != '0'){
				return array('status'=>'1','count'=>$count,'data'=>$query);
			}else{
				return array('status'=>'0','count'=>$count,'data'=>$query);
			}
	}
	
	public static function searchByCategoryName($categoryStr){

			$query = categories::where('category_name','LIKE','%'.$categoryStr.'%')
			->get();
			$count = count($query);
			if($count != '0'){
				return array('status'=>'1','count'=>$count,'data'=>$query);
			}else{
				return array('status'=>'0','count'=>$count,'data'=>$query);
			}
	}

}