<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
 public $timestamps = false;
protected $fillable = array('username','email_address','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $primaryKey = "user_id";

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	//User Signup
	public static function createUser($details){
		$userExist = User :: where('email_address','=',$details['email_address'])->get(); 
		if(count($userExist) > 0){
			$response = array('status' => 'failure', 'response_code'=> '101','response' => 'email Id already exist');
		}
		else{
			$user = new User;
			$user->username = ($details['username']);
			$user->email_address = $details['email_address'];
			$user->profile_picture = 'template/img/user/user-avatar.png';
			$user->password = Hash::make($details['password']);
			$user->registerd_through = ('self');
			$user->save();		
			$response = array('status' => 'success',  'response_code'=> '100','response' => 'registered successfully','email_address' => $details['email_address'], 'response_code'=> '100','password' => $details['password']);
		}
		return $response;
		
	}
	public static function editUserinfo($details)
	{
		$isdere=User::where('user_id','=',$details['id'])->get();
		if(count($isdere)>0)
		{
			$user_changed=User::where('user_id', $details['id'])
           ->update(array('username' =>$details['name']));
		   if($user_changed)
		   {
		   return array('status'=>'success');
		   }
		   else{
		   return array('status'=>'failed');
		   }
			
		}
		else{
		return array('status'=>'User doesnot exist');
		}
	}
	//get user info 
	public static function getUserInfo($emailId){
		$user = User::Where('email_address','=',$emailId)->get();
		return $user;
	}
	//create User from facebook
	public static function createUserFromFacebook($data){
		$socialUserId = $data['id'];
		
		$user = User::where('social_site_user_id','=',$socialUserId)->first();
		if(empty($user)){
			$users = User :: where('email_address','=',$data['email'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				//email Id already exist
				return 409;
			}
			else{
				$user = new User;
				$user->username = ($data['first_name']);
				$user->email_address = $data['email'];
				$user->social_site_user_id = $socialUserId;
				$user->profile_picture = 'https://graph.facebook.com/'.$socialUserId.'/picture?type=large';
				$user->gender = $data['gender'];
				$user->password = Hash::make('fqubefbuser');
				$user->registerd_through = ('facebook');
				$user->save();
				return 1;
			}
		}
		else{
			return 1;
		}
	}
	
	//create User from facebook
	public static function createUserFromGoogle($data){
		$socialUserId = $data['id'];
		
		$user = User::where('social_site_user_id','=',$socialUserId)->first();
		if(empty($user)){
			$users = User :: where('email_address','=',$data['email'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				//email Id already exist
				return 409;
			}
			else{
				$user = new User;
				$user->username = ($data['name']);
				$user->email_address = $data['email'];
				$user->social_site_user_id = $socialUserId;
				$user->profile_picture = $data['picture'];
				//$user->gender = $data['gender'];
				$user->password = Hash::make('fqubefbuser');
				$user->registerd_through = ('gmail');
				$user->save();
				return 1;
			}
		}
		else{
			return 1;
		}
	}

	public static function getUserDetails($id,$profile_id){

		 $query = User::where('user_id','=',$id)->get();
			
		 $following = FollowModel::leftJoin('users','users.user_id','=','following_users.following_user_id')
		->where('following_users.user_id',$id)
		->groupBy('following_users.following_user_id')->get();
		
		$followers = FollowModel::leftJoin('users','users.user_id','=','following_users.user_id')
		->where('following_users.following_user_id',$id)
		->groupBy('following_users.user_id')->get(); 
		
		$store_followers=followingStore::leftJoin('users','users.user_id','=','following_stores.user_id')
		->where('users.user_id','=',$id)
		->groupBy('following_stores.user_id')->get();
		
		$follow_status=FollowModel::where('following_user_id','=',$id)->where('user_id','=',$profile_id)->get();
		
	
		
		
		if((count($query) != '0')){	
			return array('status'=>'1','data'=>$query,'count'=>count($following),'followers_count'=>count($followers),'store_count'=>count($store_followers),'follow_status'=>$follow_status);
		}else{
			return array('status'=>'0');
		}
	}
	public static function followingUsers($id){
		$query = FollowModel::leftJoin('users','users.user_id','=','following_users.following_user_id')
		->where('following_users.user_id',$id)
		->groupBy('following_users.following_user_id')->get();
		if(count($query) != '0'){	
				return array('status'=>'1','count'=>count($query),'data'=>$query);
			}else{
				return array('status'=>'0');
			}
	}
	public static function getUserCollections($id){
		$query = Collection::where('user_id',$id)->get();
			if(count($query) != '0'){	
				return array('status'=>'1','count'=>count($query),'data'=>$query);
			}else{
				return array('status'=>'0');
			}
	}
	public static function userProfile($userId){
		$user = DB::select('select u.*,count(pl.liked_by) as like_count, count(uc.user_id) as collection_count from users_collections as uc  join users as u on u.user_id = uc.user_id join product_likes as pl on pl.liked_by = u.user_id where u.user_id = '.$userId.'');
		return $user;
	}
	
	//edit user profile
	public static function editProfile($details){ 
	$user = User :: where('user_id','=',$details['user_id'])
							->get();
	
						if(count($_FILES) == 0){
								$profilePicture = $user[0]['profile_picture'];
						 }
						 else{
						 $fileName = time().$_FILES["picture"]["name"];
						     $move =  move_uploaded_file($_FILES["picture"]["tmp_name"],"template/img/user/".$fileName);

						   if($move){
								$profilePicture = "template/img/user/".$fileName;
							}
						}
		$result = User :: where('user_id', '=', $details['user_id'])
							->update(array('username' => $details['username'],'about_me' => $details['about_me'],'profile_picture' => $profilePicture));
						Session::put('userInfo', $details['user_id']);
						Session::put('username', $details['username']);
						Session::put('profile_picture', $profilePicture);
			return $result = array('status' => 'success');
			
		}
		
		//edit user password
		public static function changePasswordset($password,$email){ 
		    $password = base64_encode($password);
		    $password = json_encode($password,true);
		    $result = User :: where('email_address', '=', $email)
							->update(array('password' => $password));
		
			return $result = array('status' => 'success');
			
		}

		public static function deleteProduct($product_id){
					
					$deleteProducts = DB::table('products')->where('product_id', '=',$product_id)->delete();

					$deleteProductsMappedTo = DB::table('products_mappedto_collection')->where('product_id', '=',$product_id)->delete();

					$deleteProductLikes = DB::table('product_likes')->where('product_id', '=',$product_id)->delete();

					$deleteAbusedProduct = DB::table('abused_product')->where('product_id', '=',$product_id)->delete();

					return array('products'=> $deleteProducts,'products-mapped-to'=>$deleteProductsMappedTo,'product-likes'=>$deleteProductLikes,'abused-product'=>$deleteAbusedProduct);
		}
	
}
