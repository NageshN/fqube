<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class FollowCollection extends Eloquent implements UserInterface, RemindableInterface {
public $timestamps = false;
protected $fillable = array('username','email_address','password','gender');

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'following_collection';
	
	public static function followCollectionByUser($details){
		$alreadyFollowed = FollowCollection :: where('user_id', '=', $details['userId'])
												->where('collection_id', '=', $details['collectionId'])
												->get();
	
		if(count($alreadyFollowed) > 0){
			$response = array('status' => 'failure', 'response' => 'already followed');
		}else{
			$followCollection = new FollowCollection;
			$followCollection->user_id = $details['userId'];
			$followCollection->collection_id = $details['collectionId'];
			$followCollection->save();
			$response = array('status' => 'success', 'response' => 'collection has been followed');
		}
		
		return $response;
	}
	
	public static function unFollowCollectionByUser($details){
		$alreadyFollowed = FollowCollection :: where('user_id', '=', $details['userId'])
												->where('collection_id', '=', $details['collectionId'])
												->get();
	
		if(count($alreadyFollowed) > 0){
			FollowCollection :: where('user_id', '=', $details['userId'])
								->where('collection_id', '=', $details['collectionId'])->delete();
			$response = array('status' => 'success','response' => 'unfollowed'); 
		}else{
			$response = array('status' => 'failure', 'response' => 'nothing to unfollow');
		}
		
		return $response;
	}
	
	public static function collectionFollowersDetails($details){
			$collectionFollowed = DB::select('SELECT fc.*,u.*,p.*,uc.* FROM `following_collection` as fc left join users as u on u.user_id = fc.user_id  
					left join products as p on p.user_id = u.user_id
					left join users_collections as uc on u.user_id = uc.user_id WHERE fc.collection_id = '.$details['collectionId'].'');
			if(count($collectionFollowed) > 0){
				$response = array('status' => 'success', 'details' => $collectionFollowed);
			}
			else{
				$response = array('status' => 'failure');
			}
			return $response;
	}
	
	public static function collectionFollowCount($collectionId,$pageNumber,$numberOfProduct){
	    $collectionFollow1 = FollowCollection::leftJoin('users','users.user_id','=','following_collection.user_id')
		->where('following_collection.collection_id',$collectionId)
		->groupBy('users.user_id')
		->get();
		$take=(($numberOfProduct));
	    $skip=(($numberOfProduct)*($pageNumber-1));
		$collectionFollow = FollowCollection::leftJoin('users','users.user_id','=','following_collection.user_id')
		->where('following_collection.collection_id',$collectionId)
		->groupBy('users.user_id')
		->take($take) 
		->skip($skip)
		->get();
		if(count($collectionFollow)!= '0'){
			return array('status'=>'1','count'=>count($collectionFollow),'data'=>$collectionFollow,'people_count'=>count($collectionFollow1));
		}else{
			return array('status'=>'0','count'=>count($collectionFollow),'people_count'=>count($collectionFollow1));
		}
	}
}
	