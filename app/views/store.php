<?php
include('header1.php');
include('connect.php');
?>
<style>
.login-btn{
display:none;
}
.header-new{
    background-image: url(http://codewave.co.in/fqube/template/img/store-image.jpg);
  background-size: 100% 570px;
  background-repeat: no-repeat;
  height: 570px;
  background-color: #f6f6f6;
  }
.sub-points li {
  list-style-type: disc!important;
  font-size:24px;
  color:#5AC3A2;
  padding:13px;
}
.sub-points li span{
font-size:18px;
color:#454545;
}
.btn-default2:hover{
background-image:linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
border:1px solid #3b3b3b;
text-shadow:none!important;
}
.fa-check{
color:#5AC3A2;
}
.main-top{
  text-align: center;
  padding-top: 15px;
}
</style>
<div class="row heads-up" style="">
<div class="top-head" style="font-size:39px;margin-top:52px;color:#3b3b3b"><span>FQUBE FOR STORES<br></span></div>
<div class="head-desc">Make it simpler for customers to discover and share your products throught Fqube Storefront.</div>
<div class="col-md-12" style="">
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="head-points col-md-8"><div class="col-md-1"><i class="fa fa-check-square-o"></i></div><div class="col-md-11 head-points-desc">When a product is added to Fqube by a user, a 'buy' button on the item page links to your online store where he product can be purchased</div></div>
<div class="col-md-2"></div>
</div>
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="head-points col-md-8"><div class="col-md-1"><i class="fa fa-check-square-o"></i></div><div class="col-md-11 head-points-desc">Stores are automatically created when a user saves a product from your website. We then aggregate all products found by the Fqube community to your store page. Users can follow your store page to see all the latest items saved by users on their live feed</div></div>
<div class="col-md-2"></div>
</div>
</div>
<div class="container read-more-div" style="float:left">
<a href="#myAnchor" id="anchor1"><div class="mouse"><div class="wheel"></div></div><span class="unu"></span><span class="doi"></span><span class="trei"></span></a>
</div>
 </div>
</div>

<div class="row" id="myAnchor" style="background: white;">
<div class="container"  style="padding-top:24px;padding-bottom:24px"> 
<head>
				<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
		
<link rel="stylesheet" id="edd-styles-css" href="<?=$baseUrl;?>template/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-retemplate-css" href="<?=$baseUrl;?>template/assets/edd-retemplate.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="<?=$baseUrl;?>template/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="<?=$baseUrl;?>template/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" async="" src="http://tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js"></script><script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/themes.designcrumbs.com\/stocky\/wp-admin\/admin-ajax.php","position_in_cart":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/stocky\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"http:\/\/themes.designcrumbs.com\/stocky\/checkout\/","permalinks":"1","quantities_enabled":""};
/* ]]> */
</script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fitvids.js?ver=4.1"></script>

<div style="clear:both"></div></a><section id="site_wrap">			
			<section class="wrapper">
			<div class="col-md-12">
			<div class="main-top">Use Fqube to grow your business and boost your sales and exposure</div>
			<div class="container clearfix">
				<div class="col-md-6" style=""><img src="<?=$baseUrl;?>template/img/store-2.png"></div>
				<div class="col-md-6">
				<div class="close-division">
				<ul class="sub-points">
				<li><span>Drive more traffic to your website by publishing your products on FQUBE.</span></li>
				<li><span>See which of your products are trending.</span></li>
				<li><span>Promote your items through FQUBE'S store and feed.</span></li>
				<li><span>Reach customers based on their purchasing behaviour and Fqube activity.</span></li>
				</ul>
				</div>
				</div>
				</div>

</div></section>		
</section>

		<div id="fb-root"></div>

<link rel="stylesheet" id="css-stocky-css" href="<?=$baseUrl;?>template/assets/style.css?ver=4.1" type="text/css" media="all">
</div>
</div>
<div class="row" style="background:#f6f6f6;">
 <div class="container" style="padding-top:26px;padding-bottom:26px">

				<div class="col-md-6" style="padding: 0px 34px 0px 34px;"><div class="main-top">Mobile commerce enabled</div>
				<div class="close-division">
				<ul class="sub-points">
				<li><span>Get a free, mobile optimized store page offering your customers a streamlined browse and buy experience</span></li>
				<li><span>FQUBE will be available on Android and IOS, so your customers can reach you anywhere from their personal device</span></li>
				</ul>
				</div>
				
				</div>
				 <div class="col-md-6" style="padding:58px"><img src="<?=$baseUrl;?>template/img/mobile-tab.png"></div>
  </div>
</div>
<div class="row" style="background:#5AC3A2;margin-bottom: 200px;">
 <div class="container" style="padding-top:20px">
		<div class="col-md-12" style="padding-right:39px">
		<div class="col-md-8 describe">If you would to drive more traffic to your online store, please get in touch with us to have your store and products featured on Fqube</div>
		<div class="col-md-1"></div>
		<div class="col-md-3"><div class="load-more"><a href="mailto:hello@fqube.com" class="btn2 btn-default2" role="button" style="border:1px solid white;height: 46px;width: 95%;padding: 12px;color:white">GET IN TOUCH</a></div></div>
			</div>	 
				 </div>
</div>

<?php
include('footer.php');
?>
<script>
$("#anchor1").on("click",function( e ){
    e.preventDefault();
    $("body, html").animate({ 
        scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);
});
</script>