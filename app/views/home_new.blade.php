<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<link rel="shortcut icon" href="http://codewave.co.in/fancythat/laravel/app/views/img/fav.png">
	<link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/fonts/glyphicons-halflings-regular.woff">
    <link rel="stylesheet" type="text/css" href="http://codewave.co.in/fancythat/laravel/app/views/fonts/glyphicons-halflings-regular.woff2">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
 
	
	<style> 
		body {
		font-family: 'Roboto', sans-serif;
		}
		.header-new {
		background-image:url(http://codewave.co.in/fancythat/laravel/app/views/img/background.jpg);
		background-size: 100%;
		background-repeat: no-repeat;
        height: 540px;
		}
	    .login-btn {
        padding: 25px 0px 0px 0px;
		}
		.logo-img {
		float: left;
		padding: 25px 0px 0px 0px;
		}
		.login {
		 padding: 5px 10px 5px 10px;
         float: left;
         margin-right: 30px;
         font-size: 18px;
         color: #fff;
         list-style: none; 
         cursor:pointer;		 
		}
		.login:hover {
		 border: 1px solid #fff;
         border-radius: 8px;
		 padding: 5px 10px 5px 10px;
		}
		.sign-up:hover {
		 border: 1px solid #fff;
         border-radius: 8px;
		 padding: 5px 10px 5px 10px;
		}
		.sign-up {
		 padding: 5px 10px 5px 10px;
         font-size: 18px;
         color: #fff;
         float: left;
         list-style: none;
		 cursor:pointer;
		}
		.top-head{
		 position: absolute;
         left: 50%;
         top: 50%;
         margin-left: -330px;
         margin-top: -100px;
         text-align: center;
         color: #fff;
		}
		.top-search {
		 position: absolute;
         left: 50%;
         top: 50%;
         margin-left: -335px;
         text-align: center;
         color: #fff;
         width: 47%;
		}
		.search-btn{
		 background-color:#5AC3A2;
		}
		.btn-default1 {
        text-shadow: 0 1px 0 #5AC3A2;
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #dbdbdb;
        border-color: #5AC3A2;
       }
	   .hero-line {
       position: relative;
       display: block;
       width: 80px;
       height: 1px;
       margin-right: auto;
       margin-left: auto;
       background-color: #737c84;
      }
	  .how-col4 {
       padding-top: 60px; 
       padding-bottom: 60px;
	  }
	  .how-first {
	   font-size: 25px;
	   font-weight: 200;
       color: #5AC3A2;
	  }
	  .how-text {
	   font-size: 13px;
       line-height: 17px;
       color: #606060;
       text-align: left;
	  }
	  .how-sec {
	   margin-top: 35px;
	  }
	  .copyright {
	   text-align:center;
	   color:#5AC3A2;
	   font-size:18px;
	   margin-bottom: 40px;
	  }
	  .load-more {
	  text-align:center;
	  margin-bottom: 40px;
	  }
	  .btn2 {
      display: inline-block;
      outline: none;
      padding: 6px 100px;
      margin-bottom: 0;
      font-size: 16px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 4px;
     }
	 .btn-default2 {
        text-shadow: 0 1px 0 #5AC3A2;
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #dbdbdb;
        border-color: #5AC3A2;
       }
	   .circle-icon {
	   background-color: #f6f6f6;
       width: 70px;
       height: 70px;
       border-radius: 50%;
       position: absolute;
       left: 50%;
       margin-top: -42px;
       margin-left: -35px;
	   }
	   .how-it-works {
	   text-align:center;
	   color:#5AC3A2;
	   font-family: 'Raleway', sans-serif;
	   margin-top: 50px;
	   }
	   	a.close {
		background-color: rgb(204,204,204);
	border-radius: 50%;
	color: rgb(255,255,255);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: .8em;
	padding: .2em .5em;
	position: absolute;
	top: 1.25rem;
	transition: all 400ms ease;
	right: 1.25rem;
}
	
	a.close:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	}

/*
*	LOG-IN BOX
*/
div.overlay {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay > div.login-wrapper {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 5px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay > div.login-wrapper > div.login-content {
			background-color: #F6F6F6;
			border-radius: 5px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay > div.login-wrapper > div.login-content > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		
/*
*	FORM
*/
form label {
	color: rgb(0,0,0);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: 1.25em;
	margin: .75em 0;	
}

	form input[type="text"],
	form input[type="email"],
	form input[type="number"],
	form input[type="search"],
	form input[type="password"],
	form textarea {
		background-color: rgb(255,255,255);
		border: 1px solid rgb( 186, 186, 186 );
		border-radius: 1px;
		box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.08);
		display: block;
		font-size: .65em;
		margin: 6px 0 12px 0;
		padding: .8em .55em;	
		text-shadow: 0 1px 1px rgba(255, 255, 255, 1);
		transition: all 400ms ease;
		width: 90%;
	}
	
	form input[type="text"]:focus,
	form input[type="email"]:focus,
	form input[type="number"]:focus,
	form input[type="search"]:focus,
	form input[type="password"]:focus,
	form textarea:focus,
	form select:focus { 
		border-color: #4195fc;
		box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1), 0 0 8px #4195fc;
	}
	
		form input[type="text"]:invalid:focus,
		form input[type="email"]:invalid:focus,
		form input[type="number"]:invalid:focus,
		form input[type="search"]:invalid:focus,
		form input[type="password"]:invalid:focus,
		form textarea:invalid:focus,
		form select:invalid:focus { 
			border-color: rgb(248,66,66);
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1), 0 0 8px rgb(248,66,66);
		}
	
form button {
	background-color: #50c1e9;
	border: 1px solid rgba(0,0,0,.1);
	color: rgb(255,255,255);
	font-family: 'Varela Round', sans-serif;
	font-size: .85em;
	padding: .55em .9em;
	transition: all 400ms ease;	
}

	form button:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	}
	form input[type="text"], form input[type="email"], form input[type="number"], form input[type="search"], form input[type="password"], form textarea{
	background: #DFDFDF;
	box-shadow:none;
	border:none;
	height:37px;
	font-size:17px;
	width:100%;
	text-shadow:none;
	border-radius:5px;
	margin-bottom: 20px;
	}
	.fb{
	float:left;
	padding: 8px;
	background:#3b5998;
	width:46%;
	text-align: center;
	}
	.gm{
	float:right;
	padding: 8px;
	background:#dd4b39;
	width:46%;
	text-align: center;
	}
	.already{
	padding-top: 7px;
	text-align:center;
	}
	.forgot{
	float :left;
	padding: 33px 13px 13px 0px;
	}
	.register{
	float:right;
	padding: 33px 0px 13px 13px;
	}
input { font-family: 'FontAwesome'; } 
.row1:before,
.row1:after {
    background-color: rgb(165,165,165);
    content: '';
    display: inline-block;
    height: 1px;
    position: relative;
    vertical-align: middle;
    width: 46.3%;
}
.row1:before {
    right: 0.5em;
    margin-left: 2%;
}
.row1:after {
    left: 0.5em;
    margin-right: -50%;
}
.icon{
float: left;
width: 93px;
padding-bottom: 20px;
}
 div.overlay1 {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay1 > div.login-wrapper1 {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 5px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay1 > div.login-wrapper1 > div.login-content1 {
			background-color: #F6F6F6;
			border-radius: 5px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay1 > div.login-wrapper1 > div.login-content1 > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		

a.close1 {
		background-color: rgb(204,204,204);
	border-radius: 50%;
	color: rgb(255,255,255);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: .8em;
	padding: .2em .5em;
	position: absolute;
	top: 1.25rem;
	transition: all 400ms ease;
	right: 1.25rem;
}
	
	a.close1:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	}
	</style>
</head> 
<body>
<div class="row" style="margin-left: 0px;margin-right: 15px;">
 <div class="row header-new">
 <div class="container">
  <div class="logo-img"><img src="http://codewave.co.in/fancythat/laravel/app/views/img/logo.png" alt="" style="width: 75%;"></div>
  <div class="login-btn"><ul style="float: right;"><li class="login overlayLink" id="log">Login</li><li class="sign-up overlayLink1" id="sig">Sign Up</li></u></div>
  <div class="top-head"><h2>FIND AMAZING PRODUCTS, SAVE THE THINGS<br>YOU LOVE AND BUY IT ALL IN ONE PLACE!</h2><br></div>
 <div class="col-lg-6 top-search">
    <div class="input-group">
      <input type="text" style="z-index:0" class="form-control" placeholder="Search here...">
      <span class="input-group-btn">
        <button class="btn btn-default1 search-btn" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
      </span>
    </div>
  </div>
  </div>
  
</div>
</div>
<div class="container">
<div class="row">
<a href="#jump-down"><div class="circle-icon"><img src="http://codewave.co.in/fancythat/laravel/app/views/img/arrow.png" alt="" style="margin-top: 9px;margin-left: 9px;width:75%;"></div></a>
</div>
</div>
<div class="container">
<div class="row">
<html lang="en-US" class=""><head>
				<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="http://themes.designcrumbs.com/stocky/xmlrpc.php">	
		<link rel="alternate" type="application/rss+xml" title="Stocky » Feed" href="http://themes.designcrumbs.com/stocky/feed/">
<link rel="alternate" type="application/rss+xml" title="Stocky » Comments Feed" href="http://themes.designcrumbs.com/stocky/comments/feed/">
<link rel="alternate" type="application/rss+xml" title="Stocky » Home Comments Feed" href="http://themes.designcrumbs.com/stocky/home/feed/">
<link rel="stylesheet" id="edd-styles-css" href="http://codewave.co.in/fancythat/laravel/app/views/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-reviews-css" href="http://codewave.co.in/fancythat/laravel/app/views/assets/edd-reviews.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="font-Roboto-css" href="http://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C400italic%2C700%2C700italic&amp;ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="font-awesome-css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="http://codewave.co.in/fancythat/laravel/app/views/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="http://codewave.co.in/fancythat/laravel/app/views/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" async="" src="http://tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js"></script><script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script><script type="text/javascript" src="http://themes.designcrumbs.com/stocky/wp-includes/js/jquery/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/themes.designcrumbs.com\/stocky\/wp-admin\/admin-ajax.php","position_in_cart":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/stocky\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"http:\/\/themes.designcrumbs.com\/stocky\/checkout\/","permalinks":"1","quantities_enabled":""};
/* ]]> */
</script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery.fitvids.js?ver=4.1"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://themes.designcrumbs.com/stocky/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://themes.designcrumbs.com/stocky/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.1">
<link rel="canonical" href="http://themes.designcrumbs.com/stocky/">
<link rel="shortlink" href="http://themes.designcrumbs.com/stocky/">

<meta name="generator" content="Easy Digital Downloads v2.2.3">
<meta name="generator" content="EDD FES v2.2.4">
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-19161846-20']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<style type="text/css">
a {
	color:#1e73be;
}

a:hover,
.meta_block a:hover {
	color:#fff;
}

#header,
body.page-template-page-login-php {
	background-image:url("http://themes.designcrumbs.com/stocky/files/edd/2014/06/header.jpg");
}
#home_cta {
	background-image:url("http://themes.designcrumbs.com/stocky/files/edd/2014/07/photographer.jpg");
}
</style><script type="text/javascript">
/* <![CDATA[  */

	
jQuery(document).ready(function($){

	// load mobile menu
	$('#main_menu ul.menu').mobileMenu();
    $('select.select-menu').each(function(){
        var title = $(this).attr('title');
        if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
        $(this)
            .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
            .after('<span class="select">' + title + '</span>')
            .change(function(){
                val = $('option:selected',this).text();
                $(this).next().text(val);
            })
    });

	/* Masonry */
	var $container = $('.edd_downloads_list');
	// initialize Masonry after all images have loaded  
	$container.imagesLoaded( function() {
		$container.masonry( { itemSelector: '.edd_download' } );
	});
	
	/* Parallax */
	(function(){
		var ua = navigator.userAgent,
		isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);
		
		/* only show if not on mobile */
		if (isMobileWebkit) {
			$('html').addClass('stocky_mobile');
		} else {
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 40
			});
		}
	
	})();
	
	// FitVids
	$("#content").fitVids();

	/* Menu Toggle */
	$('#menu_toggle').click(function() {
		$('#main_menu').slideToggle('fast');
	});
	
	/* Search Toggle */
	$('#search_toggle').click(function() {
		$('#search_wrap').slideToggle('fast');
	});
	
	/* Ratings */
	$( '.comment_form_rating .edd_reviews_rating_box' ).find('a').on('click', function (e) {
		e.preventDefault();
		$( '.comment_form_rating .edd_reviews_rating_box' ).find('a').removeClass( 'active' );
		$( this ).addClass( 'active' );
	});
	
	/* Add span within comment reply title, move the <small> outside of it */
	$('#reply-title').wrapInner('<span>').append( $('#reply-title small') );
	
	// Increase counter on add to cart
	$('.purAddToCart').ajaxComplete(function(event,request, settings) {
		if(JSON.parse(request.responseText).msgId == 0) {
			var currentCount = parseInt($('#header_cart_count').text());
			var newCount = currentCount + 1;
			$('#header_cart_count').text(newCount);
		}
	});
	
	// Fancybox
	if( $('.lightbox').length ) {
		$(".lightbox").attr('rel', 'gallery').fancybox({
			'transitionIn'		: 'fade',
			'transitionOut'		: 'fade',
			'showNavArrows' 	: 'true'
		});
	}

});

/* ]]> */
</script>	<img src="http://ib.adnxs.com/seg?t=2&amp;add=1965074" width="1" height="1" border="0"><img src="http://pixel.prfct.co/seg/?add=1965074:dcs_stocky" width="1" height="1" border="0"><style type="text/css">.fancybox-margin{margin-right:17px;}</style><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></head>
	<body class="home page page-id-2 page-template page-template-page-home page-template-page-home-php button_light content_left edd-purchase-history edd-page edd-test-mode">
		<section id="site_wrap">			
			<section class="wrapper" id="content">				<div class="container clearfix">
		<section id="image_grid" class="image_grid_full clearfix">
			
					<div id="stocky_downloads_list" class="edd_downloads_list edd_download_columns_0 masonry" style="position: relative; height: 1192px;">
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_169" style="width: inherit; float: left; position: absolute; left: 0px; top: 0px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Coffee &amp; Code">
<img width="548" height="548" src="http://codewave.co.in/fancythat/laravel/app/views/assets/DSCF0726-square-548x548.jpg" class="attachment-product_main wp-post-image" alt="DSCF0726-square">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Coffee &amp; Code" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_166" style="width: inherit; float: left; position: absolute; left: 280px; top: 0px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="The Bart">
<img width="548" height="363" src="http://codewave.co.in/fancythat/laravel/app/views/assets/DSCF1196-548x363.jpg" class="attachment-product_main wp-post-image" alt="DSCF1196">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="The Bart" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="166" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_88" style="width: inherit; float: left; position: absolute; left: 560px; top: 0px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Fountain Music">
<img width="280" height="185" src="http://codewave.co.in/fancythat/laravel/app/views/assets/35H-280x185.jpg" class="attachment-product_main wp-post-image" alt="35H">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Fountain Music" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="88" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_85" style="width: inherit; float: left; position: absolute; left: 840px; top: 0px;">
<div class="edd_download_inner" id="jump-down">
<div class="edd_download_image">
<a href="#" title="Cabs">
<img width="280" height="210" src="http://codewave.co.in/fancythat/laravel/app/views/assets/25H-280x210.jpg" class="attachment-product_main wp-post-image" alt="25H">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Cabs" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="85" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_82" style="width: inherit; float: left; position: absolute; left: 560px; top: 188px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Mountains and Shorelines">
<img width="280" height="210" src="http://codewave.co.in/fancythat/laravel/app/views/assets/15hzwjF-280x210.jpg" class="attachment-product_main wp-post-image" alt="15hzwjF">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Mountains and Shorelines" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="82" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_79" style="width: inherit; float: left; position: absolute; left: 280px; top: 189px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Electronic Grid">
<img width="280" height="175" src="http://codewave.co.in/fancythat/laravel/app/views/assets/13UUIs4-280x175.jpg" class="attachment-product_main wp-post-image" alt="13UUIs4">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Electronic Grid" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="79" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_76" style="width: inherit; float: left; position: absolute; left: 840px; top: 212px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Seagull">
<img width="280" height="185" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1odGkUB-280x185.jpg" class="attachment-product_main wp-post-image" alt="1odGkUB">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Seagull" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="76" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_73" style="width: inherit; float: left; position: absolute; left: 0px; top: 280px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Time To Work">
<img width="280" height="186" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1j5JfNg-280x186.jpg" class="attachment-product_main wp-post-image" alt="1j5JfNg">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Time To Work" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="73" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_71" style="width: inherit; float: left; position: absolute; left: 280px; top: 368px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Field">
<img width="280" height="186" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1drS3wE-280x186.jpg" class="attachment-product_main wp-post-image" alt="1drS3wE">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Field" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="71" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_67" style="width: inherit; float: left; position: absolute; left: 560px; top: 400px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Co-Work">
<img width="280" height="280" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1cHlp7N-280x280.jpg" class="attachment-product_main wp-post-image" alt="1cHlp7N">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Co-Work" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="67" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_64" style="width: inherit; float: left; position: absolute; left: 840px; top: 400px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Snow Mobiles">
<img width="280" height="408" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1cexZy0-280x408.jpg" class="attachment-product_main wp-post-image" alt="1cexZy0">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Snow Mobiles" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="64" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_60" style="width: inherit; float: left; position: absolute; left: 0px; top: 469px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Rolling Hill Tops">
<img width="280" height="151" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1aj5Gd2-280x151.jpg" class="attachment-product_main wp-post-image" alt="1aj5Gd2">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Rolling Hill Tops" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="60" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_57" style="width: inherit; float: left; position: absolute; left: 280px; top: 557px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Foggy">
<img width="280" height="280" src="http://codewave.co.in/fancythat/laravel/app/views/assets/149muhS-280x280.jpg" class="attachment-product_main wp-post-image" alt="149muhS">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Foggy" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="57" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_54" style="width: inherit; float: left; position: absolute; left: 0px; top: 625px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Trombone">
<img width="280" height="381" src="http://codewave.co.in/fancythat/laravel/app/views/assets/26H-280x381.jpg" class="attachment-product_main wp-post-image" alt="26H">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Trombone" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="54" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_172" style="width: inherit; float: left; position: absolute; left: 560px; top: 680px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Make Your Mark">
<img width="548" height="363" src="http://codewave.co.in/fancythat/laravel/app/views/assets/DSCF0492-548x363.jpg" class="attachment-product_main wp-post-image" alt="DSCF0492">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Make Your Mark" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="172" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_51" style="width: inherit; float: left; position: absolute; left: 840px; top: 803px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Video Product">
<img width="548" height="361" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1d4U0A31-548x361.jpg" class="attachment-product_main wp-post-image" alt="1d4U0A3">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Video Product" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="51" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_48" style="width: inherit; float: left; position: absolute; left: 280px; top: 837px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Desktop">
<img width="280" height="187" src="http://codewave.co.in/fancythat/laravel/app/views/assets/14lL9AE-280x187.jpg" class="attachment-product_main wp-post-image" alt="SONY DSC">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Desktop" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="48" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_45" style="width: inherit; float: left; position: absolute; left: 560px; top: 869px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="A Bike">
<img width="280" height="205" src="http://codewave.co.in/fancythat/laravel/app/views/assets/38H-280x205.jpg" class="attachment-product_main wp-post-image" alt="38H">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="A Bike" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="45" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_35" style="width: inherit; float: left; position: absolute; left: 840px; top: 991px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Logjammin’">
<img width="548" height="365" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1fhhlK6-548x365.jpg" class="attachment-product_main wp-post-image" alt="1fhhlK6">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Logjammin’" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="35" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div itemscope="" itemtype="http://schema.org/Product" class="edd_download masonry-brick" id="edd_download_10" style="width: inherit; float: left; position: absolute; left: 0px; top: 1002px;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Piling Up">
<img width="280" height="187" src="http://codewave.co.in/fancythat/laravel/app/views/assets/1epgtU4-280x187.jpg" class="attachment-product_main wp-post-image" alt="1epgtU4">		</a>
<div class="stocky_hover_details stocky_wish_list_on">
<div class="stocky_hover_lines">
<a href="#" title="Piling Up" class="dcs_view_details">Details</a>
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="10" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
<a href="#" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="10" data-price-option="0"><i class="glyphicon glyphicon-heart"></i><span class="label">Add to wish list</span></a>			
</div>
</div>
</div>
</div>
</div>
<div style="clear:both;" class="masonry-brick"></div>
<div id="edd_download_pagination" class="navigation masonry-brick"></div>
</div>
</section>
</div></section>		
</section>
<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    _pa.productId = "dcs_stocky";
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
<script type="text/javascript">
	    jQuery(document).on("DOMNodeInserted", function(){
	        // Lock uploads to "Uploaded to this post"
	        jQuery('select.attachment-filters [value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
	    });
		</script>
		<div id="fb-root"></div><script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/jquery.masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/comment-reply.min.js?ver=4.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_wl_scripts = {"wish_list_page":"http:\/\/themes.designcrumbs.com\/stocky\/wish-lists\/","wish_list_add":"http:\/\/themes.designcrumbs.com\/stocky\/wish-lists\/create\/","ajax_nonce":"30ec9cc0df"};
/* ]]> */
</script>
<link rel="stylesheet" id="css-stocky-css" href="http://codewave.co.in/fancythat/laravel/app/views/assets/style.css?ver=4.1" type="text/css" media="all">
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/edd-wl.min.js?ver=1.0.6"></script>
<script type="text/javascript" src="http://codewave.co.in/fancythat/laravel/app/views/assets/modal.min.js?ver=1.0.6"></script>
	<div class="modal fade" id="edd-wl-modal" tabindex="-1" role="dialog" aria-labelledby="edd-wl-modal-label" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    		    </div>
	  </div>
	</div>

		
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style></body></html>
<div class="load-more"><a href="#" class="btn2 btn-default2" role="button">Load More</a></div>
</div>
</div>
</div>
<div class="row" style="margin-left: 0px;margin-right: 0px;background:#fff;">
 <div class="container">
 <div class="row">
 <div class="how-it-works"><h2>HOW IT WORKS</h2></div>
 <div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>
  <div class="col-md-4 how-col4"><div class="col-md-6"><img src="http://codewave.co.in/fancythat/laravel/app/views/img/how-it-works1.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">FOLLOW</span><br><p class="how-text">Follow people<br> & collections</p></div></div>
  <div class="col-md-4 how-col4"><div class="col-md-6"><img src="http://codewave.co.in/fancythat/laravel/app/views/img/how-it-works2.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">SHORTLIST</span><br><p class="how-text">Shortlist all<br> your favorites</p></div></div>
  <div class="col-md-4 how-col4"><div class="col-md-6"><img src="http://codewave.co.in/fancythat/laravel/app/views/img/how-it-works3.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">SHARE</span><br><p class="how-text">Share with your<br> family & friends</p></div></div>
</div>
  </div>
</div>
<div class="row" style="margin-left: 0px;margin-right: 0px;background:#000;">
 <div class="container">
 <div class="row">
  <div id="socnets_wrap"><div id="socnets">
	    <a href="#" title="Facebook" class="socnet-facebook"></a>
		<a href="#" title="Twitter" class="socnet-twitter"></a>
		<a href="#" title="Google+" class="socnet-google"></a>
		<a href="#" title="Pinterest" class="socnet-pinterest"></a>
		<a href="#" title="Flickr" class="socnet-flickr"></a>
		</div>
		<div class="clear"></div> 
  </div>
  <div class="copyright"><p><span class="glyphicon glyphicon-copyright-mark" style="font-size:22px;" aria-hidden="true"></span>&nbsp;&nbsp;fancythat! 2015</p></div>
</div>
  </div>
</div>
<div class="overlay" style="display: none;">
	<div class="login-wrapper">
		<div class="login-content">
			<a class="close">x</a>
			<div class="icon"><img src=""/></div><h3 style="float:left;padding-top:8px">Login</h3>
			<form action="{{ URL::route('account-sign-in-post') }}" method="post">
		<input style=" font-family: 'FontAwesome','roboto';" type="email" name="email_address1" placeholder="&#xf007;    email/username">
		<div style="color:red" class="error">
		@if($errors->has('email_address1'))
		{{ $errors->first('email_address1') }}
		@endif
		</div>
		<input style=" font-family: 'FontAwesome','roboto';" type='password' name='password1' placeholder='&#xf023;     password'/>
		<div style="color:red" class="error">
		@if($errors->has('password1'))
		{{ $errors->first('password1') }}
		@endif
		</div>
		
		<input style="width:100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px" type='submit' value='SIGN IN'/><br>
		{{ Form::token() }}
		</form>
		<br>
		
		<div class="horizontal"><span class="row1">or</span></div><br>
		<div>
		<div class="fb"><a style="text-decoration:none;color:white;" href="http://codewave.co.in/fancythat/laravel/new/sign-in-with-facebook">SIGN IN WITH FACEBOOK</a></div>
		<div class="gm"><a style="text-decoration:none;color:white;" href="http://codewave.co.in/fancythat/laravel/new/sign-in-with-google">SIGN IN WITH GOOGLE+</a><br></div>
		</div>
		
		<div class="forgot"><a style="color:rgb(165,165,165);text-decoration:none" href="">Forgot Password?</a></div>
		<div class="register"><a href="#" style="color:rgb(165,165,165);text-decoration:none" onclick="popup('over1')">Don't have an account?</a><br></div>
		
		</div>
	</div>
</div>	
<div class="overlay1" id="over1" style="display: none;">
	<div class="login-wrapper1">
		<div class="login-content1">
			<a class="close1">x</a>
			<div class="icon"><img src=""/></div><h3 style="float:left;padding-top:8px">Register</h3>
			<form action="{{ URL::route('account-create-post') }}" method="post">
		<input style=" font-family: 'FontAwesome','roboto';" type="email" name="email_address" placeholder="@    enter mail id">
		<div style="color:red">
		@if($errors->has('email_address'))
		{{ $errors->first('email_address') }}
		@endif
		</div>
		<input style=" font-family: 'FontAwesome','roboto';" type="text" name="username" placeholder="&#xf007;    enter name"/>
		<div style="color:red">
		@if($errors->has('username'))
		{{ $errors->first('username') }}
		@endif
		</div>
		<input style=" font-family: 'FontAwesome','roboto';" type="password" name="password" placeholder="&#xf023;     password"/>
		<div style="color:red">
		@if($errors->has('password'))
		{{ $errors->first('password') }}
		@endif
		</div>
		<input style=" font-family: 'FontAwesome','roboto';" type="password" name="password_again" placeholder="&#xf023;     retype-password"/>
		<div style="color:red">
		@if($errors->has('password_again'))
		{{ $errors->first('password_again') }}
		@endif
		</div>
		<input id="submit" style="width:100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px" type='submit' value='GET STARTED'/>
			{{Form::token()}}
			</form>
		<br><br>
		<div>
		<div class="already">Already have an account?<a href="" style="text-decoration:none;color:#5AC3A2"> Sign in</a><br></div>
		</div>
		</div>
	</div>
</div>


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="http://codewave.co.in/fancythat/laravel/app/views/js/bootstrap.js"></script>
	<script src="http://codewave.co.in/fancythat/laravel/app/views/js/jquery-1.4.2.min.js"></script>
	<script src="http://codewave.co.in/fancythat/laravel/app/views/js/jquery-2.0.0b1.js"></script>
    <script src="http://codewave.co.in/fancythat/laravel/app/views/js/bootstrap.min.js"></script>
	<script>
	$("#submit").click(function{
	jquery('.error').css({"margin-top:-10px"});
	});
	</script>
	<script>
	$(document).ready(function() {
	$("#log").click(function( event ){
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink").click(function(event){
		event.preventDefault();
		event.stopPropagation();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content" ).html( data );
		});	
		
		$(".overlay").fadeToggle("fast");
	});
	
	$(".close").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay").hide();
	});
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay").fadeToggle("fast");
		}
	});
	});
	</script>
	<script>
	$(document).ready(function() {
	$("#sig").click(function( event ){
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink1").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content1" ).html( data );
		});	
		
		$(".overlay1").fadeToggle("fast");
	});
	
	$(".close1").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
	});
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay1").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay1").fadeToggle("fast");
		}
	});
	
	});
	</script>
</body>
</html>
