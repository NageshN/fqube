<?php
session_start(); 

	$baseUrl = 'http://codewave.co.in/fqube/';
	 $userId = Session::get('userInfo');
	 $userName = Session::get('username');
	 
	 $userName1 = (explode(" ",$userName));
	 $profile_pic = Session::get('profile_picture');
	 $registerd_through = Session::get('registerd_through');
	 $about_me = Session::get('about_me');
	if($registerd_through == "facebook" || $registerd_through == "gmail"){
	 $hideclass = 'hideClass';
	}
	else {
	 $hideclass = 'showClass';
	}

?>

<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Fqube - Fashion, Friends, Fun</title>
    <link rel="shortcut icon" href="<?=$baseUrl;?>template/img/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Oswald:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/fonts/glyphicons-halflings-regular.woff">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/fonts/glyphicons-halflings-regular.woff2">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/responsive.css">
    <link rel="alternate" type="application/rss+xml" href="http://themes.designcrumbs.com/stocky/comments/feed/">
<link rel="alternate" type="application/rss+xml" href="http://themes.designcrumbs.com/stocky/home/feed/">
<link rel="stylesheet" id="edd-styles-css" href="<?=$baseUrl;?>template/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-reviews-css" href="<?=$baseUrl;?>template/assets/edd-reviews.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="<?=$baseUrl;?>template/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="font-Roboto-css" href="http://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C400italic%2C700%2C700italic&amp;ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="<?=$baseUrl;?>template/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<script type='text/javascript'>
  var default_text = 'JavaScript/CSS URL',
      add_external_resource_url = "/_add_external_resource/";
</script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/themes.designcrumbs.com\/stocky\/wp-admin\/admin-ajax.php","position_in_cart":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/stocky\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"http:\/\/themes.designcrumbs.com\/stocky\/checkout\/","permalinks":"1","quantities_enabled":""};
/* ]]> */
</script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fitvids.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/js/masonry.js"></script>

<style type="text/css">
a {
	color:#1e73be;
}
.hideClass{
display:none;
}
.showClass{
display:block;
}
 .linkhover:hover{
  color:#5AC3A2!important;
  text-decoration:underline;  
  }
a:hover,
.meta_block a:hover {
	color:#fff;
}
body{
  font-family: 'Roboto', sans-serif!important;
}
.next-finish{
cursor:pointer;
}
.proc_name:hover{
color:#5AC3A2!important;
}
.logout_btn
{
  float: right;
  border: 1px solid #fff; 
  width:150px;
  position: absolute;
  left: 74.5%;
  top: 100%;
  background: white;
  display: none;
  box-shadow:0 2px 10px rgba(0,0,0,.3);
  display:none;
}
.logout_btn1
{
  float: right;
  border: 1px solid #fff; 
  width:150px;
  position: absolute;
  left: 79.5%;
  top: 100%;
  background: white;
  display: none;
  box-shadow:0 2px 10px rgba(0,0,0,.3);
  display:none;
}
#ascrail2000{
	    display:none!important;
	   }
	   #ascrail2000-hr{
	    display:none!important;
	   }
input{
 outline: none!important;
}
</style>

<script type="text/javascript">
/* <![CDATA[  */

	
jQuery(document).ready(function($){

	// load mobile menu
	$('#main_menu ul.menu').mobileMenu();
    $('select.select-menu').each(function(){
        var title = $(this).attr('title');
        if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
        $(this)
            .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
            .after('<span class="select">' + title + '</span>')
            .change(function(){
                val = $('option:selected',this).text();
                $(this).next().text(val);
            })
    });

	/* Masonry */
	var $container = $('.edd_downloads_list');
	// initialize Masonry after all images have loaded  
	$container.imagesLoaded( function() {
		$container.masonry( { itemSelector: '.edd_download' } );
	});
	
	/* Parallax */
	(function(){
		var ua = navigator.userAgent,
		isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);
		
		/* only show if not on mobile */
		if (isMobileWebkit) {
			$('html').addClass('stocky_mobile');
		} else {
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 40
			});
		}
	
	})();
	
	// FitVids
	$("#content").fitVids();

	/* Menu Toggle */
	$('#menu_toggle').click(function() {
		$('#main_menu').slideToggle('fast');
	});
	
	/* Search Toggle */
	$('#search_toggle').click(function() {
		$('#search_wrap').slideToggle('fast');
	});
	
	/* Ratings */
	$( '.comment_form_rating .edd_reviews_rating_box' ).find('a').on('click', function (e) {
		e.preventDefault();
		$( '.comment_form_rating .edd_reviews_rating_box' ).find('a').removeClass( 'active' );
		$( this ).addClass( 'active' );
	});
	
	/* Add span within comment reply title, move the <small> outside of it */
	$('#reply-title').wrapInner('<span>').append( $('#reply-title small') );
	
	// Increase counter on add to cart
	$('.purAddToCart').ajaxComplete(function(event,request, settings) {
		if(JSON.parse(request.responseText).msgId == 0) {
			var currentCount = parseInt($('#header_cart_count').text());
			var newCount = currentCount + 1;
			$('#header_cart_count').text(newCount);
		}
	});
	
	// Fancybox
	if( $('.lightbox').length ) {
		$(".lightbox").attr('rel', 'gallery').fancybox({
			'transitionIn'		: 'fade',
			'transitionOut'		: 'fade',
			'showNavArrows' 	: 'true'
		});
	}

});

/* ]]> */
</script>

<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    _pa.productId = "dcs_stocky";
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
<script type="text/javascript">
	    jQuery(document).on("DOMNodeInserted", function(){
	        // Lock uploads to "Uploaded to this post"
	        jQuery('select.attachment-filters [value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
	    });
		</script> 
		<div id="fb-root"></div><script type="text/javascript" src="<?=$baseUrl;?>template/assets/masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/comment-reply.min.js?ver=4.1"></script>

<link rel="stylesheet" id="css-stocky-css" href="<?=$baseUrl;?>template/assets/style.css?ver=4.1" type="text/css" media="all">

<script type="text/javascript" src="<?=$baseUrl;?>template/assets/modal.min.js?ver=1.0.6"></script>
	<div class="modal fade" id="edd-wl-modal" tabindex="-1" role="dialog" aria-labelledby="edd-wl-modal-label" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    		    </div>
	  </div>
	</div>

		
<style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style>
 
 
	
	<style>
		body {
		font-family: 'Roboto', sans-serif;
		}
		.glyphicon-search{
	    color:#fff;
	   }
	   .glyphicon-search:hover{
	    color:#3b3b3b;
	   } 
	   #collection_prods{  
          margin-top: 45px;
	   }
		.header-new {
		background-image:url(<?=$baseUrl;?>template/img/background.jpg);
		background-size: 100% 540px;
		background-repeat: no-repeat;
        height: 540px;
		background-color:#f6f6f6;
		}
	    .login-btn {
        padding: 16px 0px 0px 0px;
		}
		#save1_wrapper{
		  overflow: inherit!important;
		}
		#report_wrapper{
		 overflow: inherit!important;
		}
		.logo-img {
		float: left;
		padding: 16px 0px 0px 0px;
		}
		.login {
		 padding: 5px 10px 5px 10px;
         float: left;
         margin-right: 30px;
		 font-weight: 200;
         font-size: 18px;
         color: #5AC3A2;
		 border-radius: 8px;
         list-style: none; 
         cursor:pointer;		 
		}
		.login:hover {
		 border: 1px solid #5AC3A2;
         border-radius: 8px;
		 padding: 5px 10px 5px 10px;
		}
	    .hover-black{
	    
	    }
		.already_liked
        {
         background: url(http://codewave.co.in/fqube/template/img/sprite-like.png) 0px 33px !important;
        }
		.edd_downloads_list{
		  padding-bottom: 30px!important;
		}
		.sign-up:hover {
		 border: 1px solid #5AC3A2;
         border-radius: 8px;
		 padding: 5px 10px 5px 10px;
		}
		
		.sign-up {
		 padding: 5px 10px 5px 10px;
         font-size: 18px;
         color: #5AC3A2;
         float: left;
		 font-weight: 200;
		 border-radius: 8px;
         list-style: none;
		 cursor:pointer;
		}
		.follow-new{
		padding-right:29px;
		}
		.top-head{
		 position: absolute;
         left: 50%;
         top: 50%;
         margin-left: -330px;
         margin-top: -100px;
         text-align: center;
         color: #fff;
		}
		.top-search {
		 position: absolute;
         left: 50%;
         top: 50%;
         margin-left: -335px;
         text-align: center;
         color: #fff;
         width: 47%;
		}
		.search-btn{
		 background-color:#5AC3A2;
		 padding-bottom: 7px;
		}
		.btn-default1 {
        text-shadow: 0 1px 0 #5AC3A2;
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #dbdbdb;
        border-color: #5AC3A2;
       }
	   .hero-line {
       position: relative;
       display: block;
       width: 80px;
       height: 1px;
       margin-right: auto;
       margin-left: auto;
       background-color: #3b3b3b;
      }
	  .how-col4 {
       padding-top: 60px; 
       padding-bottom: 60px;
	  }
	  .how-first {
	   font-size: 25px;
	   font-weight: 200;
       color: #5AC3A2;
	  }
	  .how-text {
	   font-size: 13px;
       line-height: 17px;
       color: #3b3b3b;
       text-align: left;
	  }
	  .how-sec {
	   margin-top: 35px;
	  }
	  .copyright {
	   text-align:center;
	   color:#5AC3A2;
	   font-size:18px;
	   margin-bottom: 40px;
	  }
	  .load-more {
	  text-align:center;
	  margin-bottom: 40px;
	  margin-top: -25px;
	  }
	  .btn2 {
      display: inline-block;
      outline: none;
      padding: 6px 100px;
      margin-bottom: 0;
	  color:#fff;
      font-size: 16px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
     }
	 .btn-default2 {
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border:none;
       }
	   .btn-default4 {
        background-image: -webkit-linear-gradient(top,#3b3b3b 0,#3b3b3b 100%);
        background-image: -o-linear-gradient(top,#3b3b3b 0,#3b3b3b 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#3b3b3b),to(#3b3b3b));
        background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border:none;
       }
	   .circle-icon {
	   background-color: #f6f6f6;
       width: 70px;
       height: 70px;
       border-radius: 50%;
       position: absolute;
       left: 50%;
       margin-top: -32px;
       margin-left: -35px;
	   }
	   .how-it-works {
	   text-align:center;
	   color:#5AC3A2; 
	   font-family: 'Raleway', sans-serif;
	   margin-top: 50px;
	   }
	   input {
        font-family: 'Roboto', sans-serif!important;
       }
	   .similar-products {
	   text-align:center;
	   color:#5AC3A2; 
	   font-family: 'Raleway', sans-serif;
	   margin-top: 36px;
	   }
	   .view-all{
	   float:right;
	   margin-top: -42px;
       color: #5AC3A2;
	   width:33%;
	   padding-right:34px;
	   font-size:17px;
	   }
	   .hover-fancy {
	    float: left;
		margin-top: 0px;
		width: 20%;
	   }
	   .follow:hover {
		border-color: #3b3b3b;
		color:white;
		background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
		}
		.btn-default2:hover{
		border-color: #3b3b3b;
		color:white;
		background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
		}
		span.btn-default2:hover{
		border-color: #3b3b3b;
		color:white;
		background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
		}
	   .hover-short { 
	    float: left;
		margin-top: 0px;
		width: 11%; 
	   } 
	   #ascrail2000 {
	    display:none;
	   }
	   #ascrail2000-hr {
	    display:none;
	   }
	    header{
         position: fixed;
         width: 102%;
         font-size: 72px;
         height: 72px;
         background: transparent;
         color: #fff;
		 z-index: 1000000;
         font-family: 'Roboto', sans-serif;
         // set animation
         -webkit-transition: all 0.4s ease;
         transition: all 0.4s ease;
        }
        .attachment-img1{
		  width: 70px;
          height: 70px;
		}
        header.sticky {
         font-size: 24px;
         height: 72px; 
         background: #fff;
	     z-index: 1000000;
         //padding-left: 20px;
        }
		a.close {
		background-color: rgb(204,204,204);
	border-radius: 50%;
	color: rgb(255,255,255);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: .8em;
	padding: .2em .5em;
	position: absolute;
	top: 1.25rem;
	transition: all 400ms ease;
	right: 1.25rem;
}
	
	a.close:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	}

/*
*	LOG-IN BOX
*/
div.overlay {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay > div.login-wrapper {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 5px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay > div.login-wrapper > div.login-content {
			background-color: #F6F6F6;
			border-radius: 5px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay > div.login-wrapper > div.login-content > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		
/*
*	FORM
*/
form label {
	color: rgb(0,0,0);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: 1.25em;
	margin: .75em 0;	
}

	form input[type="text"],
	form input[type="email"],
	form input[type="number"],
	form input[type="search"],
	form input[type="password"],
	form textarea {
		background-color: rgb(255,255,255);
		border: 1px solid rgb( 186, 186, 186 );
		border-radius: 1px;
		box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.08);
		display: block;
		font-size: .65em;
		margin: 6px 0 12px 0;
		padding: .8em .55em;	
		text-shadow: 0 1px 1px rgba(255, 255, 255, 1);
		transition: all 400ms ease;
		width: 90%;
	}
	
	form input[type="text"]:focus,
	form input[type="email"]:focus,
	form input[type="number"]:focus,
	form input[type="search"]:focus,
	form input[type="password"]:focus,
	form textarea:focus,
	form select:focus { 
		border-color: #4195fc;
		box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1), 0 0 8px #4195fc;
	}
	
		form input[type="text"]:invalid:focus,
		form input[type="email"]:invalid:focus,
		form input[type="number"]:invalid:focus,
		form input[type="search"]:invalid:focus,
		form input[type="password"]:invalid:focus,
		form textarea:invalid:focus,
		form select:invalid:focus { 
			border-color: rgb(248,66,66);
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1), 0 0 8px rgb(248,66,66);
		}
	
form button {
	background-color: #50c1e9;
	border: 1px solid rgba(0,0,0,.1);
	color: rgb(255,255,255);
	font-family: 'Varela Round', sans-serif;
	font-size: .85em;
	padding: .55em .9em;
	transition: all 400ms ease;	
}

	form button:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	}
	form input[type="text"], form input[type="email"], form input[type="number"], form input[type="search"], form input[type="password"], form textarea{
	background: #DFDFDF;
	box-shadow:none;
	border:none;
	height:37px;
	font-size:17px;
	width:100%;
	text-shadow:none;
	border-radius:5px;
	margin-bottom: 20px;
	}
	.fb{
	float:left;
	padding: 8px;
	background:#3b5998;
	width:46%;
	text-align: center;
	}
	.gm{
	float:right;
	padding: 8px;
	background:#dd4b39;
	width:46%;
	text-align: center;
	}
	.already{
	padding-top: 7px;
	text-align:center;
	}
	.forgot{
	float :left;
	padding: 33px 13px 13px 0px;
	}
	.register{
	float:right;
	padding: 33px 0px 13px 13px;
	}
input { font-family: 'FontAwesome'; } 
.row1:before,
.row1:after {
    background-color: rgb(165,165,165);
    content: '';
    display: inline-block;
    height: 1px;
    position: relative;
    vertical-align: middle;
    width: 46.3%;
}
.row1:before {
    right: 0.5em;
    margin-left: 2%;
}
.row1:after {
    left: 0.5em;
    margin-right: -50%;
}
.icon{
float: left;
width: 93px;
padding-bottom: 20px;
}
 div.overlay1 {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay1 > div.login-wrapper1 {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 5px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay1 > div.login-wrapper1 > div.login-content1 {
			background-color: #F6F6F6;
			border-radius: 5px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay1 > div.login-wrapper1 > div.login-content1 > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		

a.close1 {
		background-color: rgb(204,204,204);
	border-radius: 50%;
	color: rgb(255,255,255);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: .8em;
	padding: .2em .5em;
	position: absolute;
	top: 1.25rem;
	transition: all 400ms ease;
	right: 1.25rem;
}
	
	a.close1:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	} 
	.header-other {
         position: fixed;
         width: 102%;
         font-size: 72px;
         height: 72px;
         background: #fff;
         color: #fff;
         font-family: 'Roboto', sans-serif;
         // set animation
         -webkit-transition: all 0.4s ease;
         transition: all 0.4s ease;	  
	}
	#ascrail2000-hr div {
	 width:100%!important;
	}
	.row{
	 margin-left:0px;
	 margin-right:0px;
	}
	.explore{
		color:#5AC3A2;
		float:left;
		padding:27px;
	}
	.myfeed12{
		color:#5AC3A2;
		float:left;
		padding:27px;
	}
	.post{

		color:#5AC3A2;
		float:left;
		padding:27px;
	}
	.tag-image {
border-radius: 9px;
}
.tagging {
float: left;
padding: 9px;
}
	#demo {
        margin: 0 58px 6px -40px;
        float:right;
        width:202px;
    }
    #demo1 {
        margin:-54px 18px 0px 140px;
        float:left;
        width:60px;
		cursor:pointer;
    }
    #demo .wrapper {
        //display: inline-block;
        //width: 180px;
       // margin: 0 10px 0 0;
        height: 20px;*/
        position: relative;
        //margin:20px;
    }
	#demo1 .wrapper {
        //display: inline-block;
        //width: 180px;
       // margin: 0 10px 0 0;
        height: 20px;*/
        position: relative;
        //margin:20px;
    }
    
    #demo .parent {
        height: 100%;
        width: 100%;
        display: block;
        cursor: pointer;
        line-height: 0px;
        height: 0px;
        border-radius: 5px;
       /* background: #F9F9F9;
        border: 1px solid #AAA;
        border-bottom: 1px solid #777;
        color: #282D31;*/
        font-weight: bold;
        z-index: 2;
        position: relative;
        -webkit-transition: border-radius .1s linear, background .1s linear, z-index 0s linear;
        -webkit-transition-delay: .8s;
        text-align: center;
    }
    #demo1 .parent1 {
        height: 100%;
        width: 100%;
        display: block;
        cursor: pointer;
        line-height: 0px;
        height: 0px;
        border-radius: 5px;
       /* background: #F9F9F9;
        border: 1px solid #AAA;
        border-bottom: 1px solid #777;
        color: #282D31;*/
        font-weight: bold;
        z-index: 2;
        position: relative;
        -webkit-transition: border-radius .1s linear, background .1s linear, z-index 0s linear;
        -webkit-transition-delay: .8s;
        text-align: center;
    }
    #demo .parent:hover,
    #demo .content:hover ~ .parent {
        background: #fff;
        -webkit-transition-delay: 0s, 0s, 0s;
    }
    
    #demo .content:hover ~ .parent {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        z-index: 0;
    }
    #demo1 .parent:hover,
    #demo1 .content:hover ~ .parent {
        background: #fff;
        -webkit-transition-delay: 0s, 0s, 0s;
    }
    
    #demo1 .content:hover ~ .parent {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        z-index: 0;
    }
    #demo .content {
        position: absolute;
        top: 0;
        display: block;
        z-index: 1;
        height: 0;
        width: 180px;
        padding-top: 30px;
        /*-webkit-transition: height .5s ease;
        -webkit-transition-delay: .4s;
        border: 1px solid #777;
        border-radius: 5px;
        box-shadow: 0 1px 2px rgba(0,0,0,.4);*/
    }
	#demo1 .content {
        position: absolute;
        top: 0;
        display: block;
        z-index: 1;
        height: 0;
        width: 180px;
        padding-top: 30px;
        /*-webkit-transition: height .5s ease;
        -webkit-transition-delay: .4s;
        border: 1px solid #777;
        border-radius: 5px;
        box-shadow: 0 1px 2px rgba(0,0,0,.4);*/
    }
    
    #demo .wrapper:active .content {
        height: 123px;
        z-index: 3;
        -webkit-transition-delay: 0s;
    }
    
    #demo .content:hover {
        height: 123px;
        z-index: 3;
        -webkit-transition-delay: 0s;
    }
	#demo1 .wrapper:active .content {
        height: 123px;
        z-index: 3;
        -webkit-transition-delay: 0s;
    }
    
    #demo1 .content:hover {
        height: 123px;
        z-index: 3;
        -webkit-transition-delay: 0s;
    }
    
    
    #demo .content ul {
        background: #fff;
        margin: 0;
        padding: 0;
        overflow: hidden;
        height: 100%;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
	#demo1 .content ul {
        background: #fff;
        margin: 0;
        padding: 0;
        overflow: hidden;
        height: 100%;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    
    #demo .content ul a {
        text-decoration: none;
    }
    
    #demo .content li:hover {
        background: #eee;
        color: #333;
    }
	#demo1 .content ul a {
        text-decoration: none;
    }
    
    #demo1 .content li:hover {
        background: #eee;
        color: #333;
    }
    
    #demo .content li {
        list-style: none;
        text-align: left;
        color: #888;
        font-size: 14px;
        line-height: 30px;
        height: 30px;
        padding-left: 10px;
        border-top: 1px solid #ccc;
    }
    
    #demo .content li:last-of-type {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
	#demo1 .content li {
        list-style: none;
        text-align: left;
        color: #888;
        font-size: 14px;
        line-height: 30px;
        height: 30px;
        padding-left: 10px;
        border-top: 1px solid #ccc;
    }
    
    #demo1 .content li:last-of-type {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .welcome{
    	float:left;
    	padding-top: 32px!important;	
    	font-weight:lighter;
		font-size:16px;
    }
  .user
{
  float: left;
  padding: 0px;
  padding-top: 6px;
  margin-left: 2px;
  color: #5AC3A2;
  font-weight: lighter;
  font-size: 16px;
  line-height: 35px;
  width: 50%;
  text-align: left;
}
	

.container{
	font-weight:lighter;
	font-size:14px;
}
.icon1 {
display: block !important;
width: 35px;
height: 32px;
margin-top: -6px;
float: left;
line-height: 98px !important;
margin-right: auto;
margin-left: auto;
//border-radius: 100px;
background-color: #f6f6f6;
color: #444;
font-size: 50px;
text-align: center;
<!---webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;-->
}
.icon {
display: block !important;
width: 42px;
height: 42px;
line-height: 65px !important;
margin-right: auto;
margin-left: auto;
border-radius: 100px;
background-color: #f6f6f6;
color: #444; 
font-size: 50px;
text-align: center;
<!---webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;-->
}
.icon2 {
display: block !important;
width: 35px;
float:left;
height: 35px;
margin-top: -6px;
line-height: 98px !important;
margin-right: auto;
margin-left: auto;
border-radius: 100px;
background-color: #f6f6f6;
color: #444; 
font-size: 50px;
text-align: center;
-webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;
}
.icon22 {
display: block !important;
width: 25px;
float:left;
height: 23px;
margin-top: 5px;
line-height: 98px !important;
margin-right: auto;
margin-left: auto;
border-radius: 100px;
background-color: #f6f6f6;
color: #444; 
font-size: 50px;
text-align: center;
-webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;
}
collect1:hover .collect{
<!--background: url(<?=$baseUrl;?>template/img/sprite-collect.png) 0px 35px!important;-->
}

.liked
{
background: url(<?=$baseUrl;?>template/img/sprite-like.png) 0px 32px!important;
}
.not_liked{
background: url(<?=$baseUrl;?>template/img/sprite-like.png) 0px 0px!important;
}
.liked_small
{
background: url(<?=$baseUrl;?>template/img/heart-2.png) 0px 24px!important;
}
.not_liked_small{
background: url(<?=$baseUrl;?>template/img/heart-2.png) 0px 0px!important;
}
#save1_wrapper {
overflow:inherit!important;
}
.liveurl {
margin-top: 10px;
border-top: 1px solid #CCC;
}
.small-menu{
float: left;
text-transform:none;
width: 50%;
left: 28%;
padding: 14px 20px 14px 20px;
margin-top: 83px;
font-family: Montserrat,sans-serif;
background: #f6f6f6;
display:none;
position:fixed;
z-index:110;
-webkit-box-shadow: 0px 4px 10px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 4px 10px 0px rgba(0,0,0,0.1);
box-shadow: 0px 4px 10px 0px rgba(0, 0, 0, 0.1);
}
.smallest{
	padding: 8px;
	text-transform: capitalize;
}

.smaller{
	float:left;
	padding:5px;
	padding-right:80px;
}
.small-menu:before{
width: 0;
height: 0;
border-style: solid;
border-width: 10px 10px 0;
border-color: #fff transparent transparent;
content: ' ';
position: absolute;
top: 0px;
left: 42%;
z-index: 10;
}
#profileEdit {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 10px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
#profileEdit:hover {
color: #5AC3A2;
background-color: transparent;
}
#accountSet {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 5px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
#accountSet:hover {
color: #5AC3A2;
background-color: transparent;
}
#signOut {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 5px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
#signOut:hover {
color: #5AC3A2;
background-color: transparent;
}
.gb_pa, .gb_qa {
  left: auto;
  right: 6.5px;
}
.gb_qa {
  border-color: transparent;
  border-style: dashed dashed solid;
  border-width: 0 8.5px 8.5px;
  display: none;
  position: absolute;
  right: 15px;
  z-index: 1;
  height: 0;
  width: 0;
  -webkit-animation: gb__a .2s;
  animation: gb__a .2s;
  border-bottom-color: #5AC3A2;
  border-bottom-color: rgba(0,0,0,.2);
  top: -9px;
}
.gb_Ca .gb_pa {
  border-bottom-color: #fef9db;
}
.gb_pa {
  border-color: transparent;
  border-bottom-color: #fff;
  border-style: dashed dashed solid;
  border-width: 0 8.5px 8.5px;
  display: none;
  position: absolute;
  right: 15px;
  top: -9px;
  z-index: 1;
  height: 0;
  width: 0;
  -webkit-animation: gb__a .2s;
  animation: gb__a .2s;
}
.smallest{
text-transform: capitalize;
font-weight:lighter;
}
.similar-products {
  text-align: center; 
  color: #454545;
  font-family: 'Raleway', sans-serif;
  margin-top: 36px;
  text-transform:uppercase;
}
.message-error {
  font-size: 20px;
  text-align: center;
    margin-top: 20px;
  margin-bottom: 20px;
}
.edd-wl-button  {
text-decoration: none;
outline: none;
}
a{
  text-decoration: none;
  outline: none;
}
.edd-wl-button2  {
text-decoration: none;
outline: none;
}
#save1 {
width:40%;
}
a:hover {
  outline: none!important;
}
div.bx-pager div:nth-child(4){
display:none!important;
}
.upload-new {
  position: relative;
  overflow: hidden;
}
upload-new {
  color: #fff;
}
.upload-new {
  display: inline-block;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.upload-new input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  -ms-filter: 'alpha(opacity=0)';
  font-size: 200px;
  direction: ltr;
  cursor: pointer;
}
div#save1_wrapper
{
overflow:inherit !important;
}
.tag-image {
width: 60px;
height: 43px;
}
.followed
{
	border-color: #3b3b3b !important;
	text-decoration:none;
  background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%) !important;
  padding: 6px 15px!important;
}
div#edit_profile_wrapper
{
overflow:inherit !important;
}

.about{
font-size:17px;
font-weight:100;
text-transform:none;
float:right;
color:#3b3b3b;
}
.about-me{
  width: 100%;
  border-radius: 3px;
  border: 1px solid #ccc;
  outline:none;
  height: 70px;
  margin-top: -25px;
}
#name{
  width: 100%;
  border-radius: 3px;
  border: 1px solid #ccc;
  outline:none;

}
.half{
	padding: 19px 0px 0px 0px;
}
.btn-default2{
margin:33px;
}
.after{
margin-top: -28px;
  background: rgba(0, 0, 0, .6);
  z-index: 2;
  position: absolute;
  left: 198px;
  width: 148px;
  color: white;
  font-size: 12px;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
}
textarea {
    resize: none;
}
.profileEdit {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 10px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
.profileEdit a:hover {
color: #5AC3A2;
background-color: transparent;
}
.accountSet {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 5px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
.accountSet a:hover {
color: #5AC3A2;
background-color: transparent;
}
.signOut {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 5px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
.menu-about:hover{
color: #5AC3A2!important;
background-color: transparent;
}
.menu-how:hover{
color: #5AC3A2!important;
background-color: transparent;
}
.menu-store:hover{
color: #5AC3A2!important;
background-color: transparent;
}
.signOut a:hover {
color: #5AC3A2!important;
background-color: transparent;
}
.noCollections
{
display:none;
}
a.close1 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
background-color: transparent!important; 
} 
	
	a.close1:hover {
		background-color: transparent!important; 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	} 
#post15_wrapper {
  overflow: inherit !important;
}
body > .site-loader.pe-disabled {
opacity: 0;
}
body > .site-loader {
position: fixed;
background: white;
top: 0px;
left: 0px;
bottom: 0px;
right: 0px;
z-index: 9999;
opacity: 1;
-webkit-transition: opacity 0.3s; 
transition: opacity 0.3s;
}
body > .site-loader.pe-disabled {
opacity: 0;
}
.pageloader-new {
  background: none;
  z-index: 100;
  position: absolute;
  top: 42%; 
  left: 48%;
  }
 .cube1 {
   background-color: rgba(0, 0, 0, 0);
  border: 5px solid rgba(255, 255, 255, 0.9);
  width: 60px;
  height: 60px;
   margin: 0 auto; 
  -webkit-animation: cubeanim 2s infinite ease-in-out;
  -moz-animation: cubeanim 2s infinite ease-in-out;
  -o-animation: cubeanim 2s infinite ease-in-out;
  animation: cubeanim 2s infinite ease-in-out;
  }
</style>
	
</head> 
<body>
<div class="site-loader">
<div class="pageloader-new" style="margin-bottom: -20px;margin-top: 20px;">
 <div class="cube1"></div>
</div>
</div>
<input type="hidden" id="user_id" value="<?=$userId;?>">	
<input type="hidden" id="user_name" value="<?=$userName;?>">	
<input type="hidden" id="about_me" value="<?=$about_me;?>">	
<input type="hidden" id="registerd_through" value="<?=$registerd_through;?>">	
<div class="parallax" style="background: #f6f6f6;margin-bottom: 200px;width:100%;margin-right: 0px;margin-left: 0px;">
<nav class="navbar navbar-inverse navbar-fixed-top" style="margin-left: 0px;margin-right: 0px;box-shadow: 0px 0px 8px 2px rgba(0, 0, 0, 0.1);height: 82px;">
      <div class="container">
        <div class="navbar-header"> 
         <a href="my-feed" style="text-decoration:none;outline:none;"> <div class="logo-img"><img class="logo" src="<?=$baseUrl;?>template/img/logo-fqube.png" alt="" style="width:70%;"></div></a>
        </div>
        <div class="col-md-2"></div>
         <div class="col-md-6 mob-men-post" style="">
	 <div class="myfeed12"><a href="myfeed" style="color:#5AC3A2"><i class="fa fa-newspaper-o" style="padding-right:9px;color:#5AC3A2"></i><span class="mob-hide">MY FEED</span></a></div>
     <div class="explore"><a href="#" style="color:#5AC3A2"><i class="fa fa-bars" style="padding-right:9px;color:#5AC3A2"></i><span class="mob-hide">EXPLORE</span></a></div>
	<div class="post"><a class="post15_open" style="color:#5AC3A2;cursor:pointer;"><span class="glyphicon glyphicon-pencil" style="color:#5AC3A2;padding-right:9px;" aria-hidden="true"></span><span class="mob-hide">POST</span></a></div>
        </div>
        <div id="demo">
        	<div class="tagging" style="width:75px;height:60px;margin-top: 5px;"><a href="<?=$baseUrl;?>profile?id=<?=$userId;?>&name=<?=$userName?>"><img src="<?php echo $profile_pic;?>" class="wp-post-image tag-image" alt="DSCF0726-square" style="  width: 65px;height: 55px;"></a></div>
        <div class="wrapper" style="width:auto">  
		<div class="parent"> 
        <div class="welcome"><i>Welcome</i></div><div class="user"><?php echo substr($userName1[0],0,11); ?></div>
		<i style="color:#5AC3A2;padding-top: 34px;margin-left: -40px;" class="fa fa-caret-down"></i></div>
    </div>
    <!--/.nav-collapse -->
      </div>
	  <div style="position: relative;margin: 82px -50px -82px 130px;">
	  <div id="dropdown_menu" class="logout_btn slide"  onclick="" >
	  <div class="gb_qa"></div>
	  <div class="gb_pa"></div>
	  <a href="<?=$baseUrl;?>profile?id=<?=$userId;?>&name=<?=$userName?>"><div class="" id="profileEdit">Profile</div></a>
	  <div class="edit_profile_open" id="accountSet">Settings</div> 
	  <div class="" id="signOut">Logout</div></div></div>
      <div id="demo1">
    <div class="wrapper" style="width:auto">
        <div class="content" style="display:none">
        </div>
        <div class="parent1"><i class="fa fa-bars fa-6" style="font-size: 24px;color: #5AC3A2;float: right;width: 40px;height: 40px;margin-top: 4px;margin-right: -1px;"></i></div>
    </div>
    <!--/.nav-collapse -->
      </div>
	 <div style="position: relative;margin: 164px -50px 0px 130px;"> 
	 <div id="dropdown_menu1" class="logout_btn1 slide"  onclick="" >
	  <div class="gb_qa"></div>
	  <div class="gb_pa"></div>
	  <div class="profileEdit"><a class="menu-about" href="aboutus" style="color: #454545;">About Us</a></div>
	  <div class="accountSet"><a class="menu-how" href="howitworks" style="color: #454545;">How It Works</a></div> 
	  <div class="signOut"><a class="menu-store" href="store" style="color: #454545;">Fqube Store</a></div></div></div>
    </nav>

	<div class="small-menu">
		<div class="mega-menu-category container"> 
		<div class="smaller"></div>
		<div class="smaller"></div>		 
	</div>
	</div>
	<div class="collection-overlay" id="fade" class="well" style="width:30%;display:none">
	<a class="close1 fade_close"><i class="fa fa-times"></i></a>
	<div class="login-wrapper1" style="background:white;padding-left:19px;">
		<div class="login-content1" style="height:201px;background:white">
			<a class="close1"><i class="fa fa-times"></i></a>
			<div style="padding: 10px 0 10px 0;">Add this product to a collection</div>
			<form style="font-weight:lighter">
			<select id="selector">
			<option value="choose">Loading...</option>
			</select>
		<div class="horizontal"><span class="row1">Or</span></div>
		<input style="  font-family: 'FontAwesome','roboto';font-weight:lighter;border:1px solid rgb( 186, 186, 186 ); width:79%;float:left; margin-top: 2px;" type='text' id="collection" name='collection' placeholder='Add a new collection'     />
		<div style="float:left;background: #5AC3A2;border-radius: 18px;height: 25px;width: 25px;font-size: 20px;margin-left: 10px;margin-top: 6px;padding-left:5px"><i class="fa fa-plus" style="color:white"></i></div>
		<!--<input style=" font-family: 'FontAwesome','roboto';" id="password2" type="password" name="password_again" placeholder="&#xf023;     retype-password"/>-->
		<div style="color:rgb(211, 72, 54);display:none" class="error3">
		Mandatory fields are missing
		</div>
			
			</form>
		<br><br>
		<div>
		<input type="hidden" id="fake1_class">
		</div>
		</div>
	</div>
</div>
<div class="row top-row" style="padding-top: 77px;margin-right: 0px;margin-left: 0px;">

<!--Save popup starts-->
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<div id="save1" class="well" style="background:white;box-shadow:none;margin-bottom:0px;display:none;"> 
<a class="close1 save1_close"><i class="fa fa-times"></i></a>
	<div class="login-wrapper1" style="background:white">
		<div class="login-content1" style="height:189px;background:white">
			<div style="padding: 0px 0 31px 0;font-size: 20px;color: #454545;font-weight: 100;">Add this product to a collection</div>
			<form style="font-weight:lighter" action="javascript:void(0);" onsubmit="addCollection(this);">
			<select id="myCustomSelector1" style="padding: 4px 10px;width: 100%;height: 45px;background: #f6f6f6;border: none;border-radius: 4px;">
		<option value="choose">Loading...</option>
  
</select>
		<div class="horizontal" style="width: 50%;margin-left: auto; margin-right: auto;"><span class="row1">Or</span></div>
		
		<input type="hidden" value="" id="productID">
		<input style="  font-family: 'FontAwesome','roboto';font-weight:lighter;border:1px solid #ccc;border-radius:3px; width: 90%;float:left;  margin-right: 8px; margin-top: 0px;height:45px  background: #fff;" type='text' id="collection" name='collection' placeholder='Add a new collection' /><div onclick="addSave(this)" class="fqubeBtnHover" style="width: 8%;height:37px;font-size: 16px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px"><div class="glyphicon glyphicon-plus" aria-hidden="true" style="padding: 4px 10px 0px 11px;color: #fff;font-size: 20px;float:left" id="profile_pic"></div></div>
        <input class="feed_open fqubeBtnHover" style="width: 100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px" type="submit" value="Add to Collection">
		</form>
		<br><br>
		<div>
		</div>
		</div>
		<div style="width: 100%;float: left;">
		<div id="loader-save" class="loader" style="margin-bottom: -20px;margin-top: 20px;">
        <div class="cube"></div>
        </div></div>
	</div>
</div>

<!--Edit profile popup-->
<div class="row" id="edit_profile" style="background: #f6f6f6;margin-top:20px;padding: 10px 2px 10px 2px;display:none;">
<div class="container"> 
		<div class="col-md-12" style="padding-bottom:10px">
			<div class="col-md-6" style="font-size:18px;">
				Edit Profile
			</div>
			<div class="col-md-6">
				<div class="<?=$hideclass;?> ch-pass" style="float:right;" onclick="changeTab(this);"><!--<span class="glyphicon glyphicon-pencil " style="padding-right:4px;color: #000;" aria-hidden="true"></span>--><span style="color:#3b3b3b;text-decoration: underline;cursor:pointer;">change password</span><i class="fa fa-angle-right" style="padding-left:7px"></i></div>
				<div class="<?=$hideclass;?> ed-pro" style="float:right;margin-right: 30px;display:none;" onclick="changeTabPro(this);"><!--<span class="glyphicon glyphicon-pencil" style="padding-right:4px;color: #000;" aria-hidden="true"></span>--><span style="color:#3b3b3b;text-decoration: underline;cursor:pointer;">edit profile</span><i class="fa fa-angle-right" style="padding-left:7px"></i></div>
				<!--<div style="float:right;margin-right: 30px;"><a href="select-follow" style="color:#3b3b3b;text-decoration: underline;">edit my feed</a><i class="fa fa-angle-right" style="padding-left:7px"></i></div>-->
			</div>
		</div>
		<form id="my_form" action="javascript:void(0);">
		<div class="col-md-12" style="padding-top:25px;background:#ffffff">
		<div class="col-md-6">
		<div style="width: 150px;height: 150px;text-align: center;margin-left: 275px;margin-right: auto;"><img id="profile-pic" class="image-container" src="<?php echo $profile_pic;?>" style="border-radius:3px;height: 150px;width: 150px;"/></div>
		<div class="after" style="display: block!important;margin-left: 93px;"><i class="fa fa-camera" style="padding-right:10px;padding-left: 6px;"></i><div class="upload-new"> <div>upload new image</div><input id="input_file" type="file" accept="image/*" value="upload new image">
		<input class="" id="file_path" type="hidden" value=""></div></div>
		<div class="col-md-12 half">
		<div class="col-md-4" style="padding-right:0px"><label class="about" style="margin-top: 8px;">Name:</label></div><div class="col-md-8"><input type="text" id="edit_name" value="<?=$userName;?>"/></div>
		</div>
		<div class="col-md-12 half"> 
		<div class="col-md-4" style="padding-right:0px"><label class="about" style="margin-top: -25px;">About me:</label></div><div class="col-md-8"><textarea class="about-me" id="edit_about" value=""><?=$about_me;?></textarea></div>
		</div>
		</div>
		<div class="col-md-6" style="text-align:center;padding-top:80px">
		<p style="font-size:20px;">Edit My Feed</p>
		<p><a href="tagcategory" class="linkhover" style="color: #3b3b3b;">Follow or unfollow categories</a></p>
		<p><a href="peopletofollow" class="linkhover" style="color: #3b3b3b;">Follow or unfollow people</a></p>
		<p><a href="storetofollow" class="linkhover" style="color: #3b3b3b;">Follow or unfollow store</a></p>
		</div>  
		<div id="error" style="display:none;float:left;border:1px solid red;  width: 100%;text-align: center;margin-top: 30px;">Please fill mandatory fields</div>
		<div id="success" style="display:none;float:left;border:1px solid green;  width: 100%;text-align: center;margin-top: 30px;">Your profile is saved</div>
		</div>
		<div class="col-md-12" style="text-align:center;background:#ffffff">
		<button class="btn2 btn-default2" style="width:auto;padding:6px 29px; margin-bottom: 30px;" id="save_it">SAVE</button>
		<button class="btn2 btn-default2 edit_profile_close" onclick="closeBox(this);" style="width:auto;padding:6px 15px; margin-bottom: 30px;background:#ccc;color:#3b3b3b">CLOSE</button>
		</div>
		</form> 
		<form id="change_password" action="javascript:void(0);" style="display:none;"> 
		<div class="col-md-12" style="padding-top:25px;background:#ffffff">
		<div class="col-md-12 half" style="padding: 0px 100px 0px 100px;">
		<div class="col-md-4" style="padding-right:0px"><label class="about" style="margin-top: 8px;">Current Password:</label></div><div class="col-md-8"><input type="password" id="current_password" value=""></div>
		<div style="color:red;display:none;text-align: center;border:1px solid red;float: left;width: 100%;" class="error5">Mandatory fields are missing</div>
		</div>
		</div>
		<div class="col-md-12" style="text-align:center;background:#ffffff">
		<button class="btn2 btn-default2" style="width:auto;padding:6px 29px; margin-bottom: 30px;" id="compare_pass">SUBMIT</button>
		<button class="btn2 btn-default2 edit_profile_close" onclick="closeBox(this);" style="width:auto;padding:6px 15px;background:#ccc;color:#3b3b3b">CLOSE</button>
		</div>
		</form>
		<form id="new_password" onsubmit="return checkFormPassword(this);" action="javascript:void(0);" style="display:none;">
		<div class="col-md-12" style="padding-top:25px;background:#ffffff;">
		<div class="col-md-12 half" style="padding: 0px 100px 0px 100px;">
		<div class="col-md-4" style="padding-right:0px"><label class="about" style="margin-top: 8px;">New Password:</label></div><div class="col-md-8"><input type="password" id="new_password" name="new_password" value=""></div>
		<div class="col-md-4" style="padding-right:0px"><label class="about" style="margin-top: 8px;">Re Type Password:</label></div><div class="col-md-8"><input type="password" id="re_new_password" name="re_new_password" value=""></div>
		<div style="color:red;display:none;text-align: center;border:1px solid red;float: left;width: 100%;" class="error3">Mandatory fields are missing</div>
		<div id="success1" style="display:none;float:left;border:1px solid green;width: 100%;text-align: center;margin-top: 30px;">Password changed successfully</div>
		</div>
		</div>
		<div class="col-md-12" style="text-align:center;background:#ffffff">
		<button class="btn2 btn-default2" style="width:auto;padding:6px 29px; margin-bottom: 30px;" type="submit">SUBMIT</button>
		<button class="btn2 btn-default2 edit_profile_close" onclick="closeBox(this);" style="width:auto;padding:6px 15px;background:#ccc;color:#3b3b3b">CLOSE</button>
		</div>
		</form>
		</div>
		</div>
<div id="post15" class="well" style="background:white;box-shadow:none;margin-bottom:0px;width: 65%;max-width: 65%;  padding: 0px;height: auto;display:none;">
    <a class="close1 post15_close"><i class="fa fa-times"></i></a>
	<div class="login-wrapper1" style="background:white">
		<div class="login-content1" style="background:white;height:auto;">
		<div style="padding: 20px 0 15px 0;font-size: 20px;color: #454545;font-weight: 100;text-align:center;">Post a product</div>		
	    <div class="center" style="margin-top: 10px;margin-bottom: 20px;margin-left: 50px;">
        <div class="linkPreview" id="lp2"></div>
        </div>
		<div>
		</div>
		</div> 
		<div style="width: 100%;float: left;">
		<div class="loader" style="margin-bottom: 10px;margin-top: 5px;">
        <div class="cube"></div>
        </div></div>
		<div class="login-content1" style="height:200px;background:#f1f1f1;  float: left;width: 100%;">
		<div style="padding: 20px 0 15px 0;font-size: 20px;color: #454545;font-weight: 100;text-align:center;">Want to post products like a pro?<br>Install this awesome bookmarklet.</div>
		  <p style="text-align:center"> <a href="javascript:void((function(url){if(!window.fqubeBookmarklet){var productURL=encodeURIComponent(url),cacheBuster=Math.floor(Math.random()*1e3),element=document.createElement('script');element.setAttribute('src','//codewave.co.in/fqube/app/views/bookmarklet/bookNow.js?*='+cacheBuster+'&v=2.4&url='+productURL),element.onload=init,element.setAttribute('type','text/javascript'),document.getElementsByTagName('head')[0].appendChild(element)}else init();function init(){window.fqubeBookmarklet()}})(window.location.href))" style="width: 50%;height:37px;background-color:#5AC3A2;padding: 10px 40px 10px 40px;cursor:pointer;color:white;border:none;border-radius:4px">Post on fqube</a></p>
	      <p style="text-align:center;margin-top: -10px;margin-bottom: 0px;"><i class="fa fa-arrow-up fa-6"></i></p> 		
	      <p style="text-align:center">Drag to your browser's Bookmarks bar</p> 		
		</div> 
	</div>
</div>
<!---upload file-->
<script>
jQuery(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                jQuery('#profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    jQuery("#input_file").on('change', function(){
        readURL(this);
    });
    
    jQuery(".upload-new").on('click', function() {
       jQuery("#input_file").click();
    });
});
</script>
<!---upload file-->
<!--change edit popup tab-->
<script>
 function changeTab(){
jQuery('#my_form').hide();
jQuery('.ed-pro').show();
jQuery('.ch-pass').hide();
jQuery('#new_password').hide();
jQuery('#change_password').show();
 }
</script>
<script>
 function changeTabPro(){
jQuery('#my_form').show();
jQuery('.ed-pro').hide();
jQuery('.ch-pass').show()
jQuery('#new_password').hide();
jQuery('#change_password').hide();
 }
</script>
<script>
 function closeBox(){
jQuery('#my_form').show();
jQuery('#change_password').hide();
jQuery('#new_password').hide();
jQuery('#success1').hide();
jQuery('#success').hide();
jQuery('#error').hide();
jQuery('#error1').hide();
 }
</script>
<!--change edit popup tab-->
<!--post popup starts-->
<script>
 jQuery('#post15').popup({
      transition: 'all 0.3s',
      scrolllock: false,
	  opentransitionend: function () {
           jQuery('#text_lp2').focus();
           jQuery('#text_lp2').css({
                            "border": "1px solid #5AC3A2",
							"-moz-box-shadow": "0 0 5px #5AC3A2",
                            "-webkit-box-shadow": "0 0 5px #5AC3A2",
                            "box-shadow": "0 0 5px #5AC3A2"
                        });
        }
});
</script>
<!--post popup ends-->
<script>
		function cancel()
		{
		document.getElementById("my_form").reset();
		}
		</script>
<script>
jQuery('#edit_profile').popup({

 transition: 'all 0.3s'
});
</script>

<script>
 jQuery('#save1').popup({
transition: 'all 0.3s',
scrolllock: false 
});
</script>
<!--Save popup ends-->

<script>
jQuery('#signOut').click(function(){

jQuery.ajax({
			url : 'http://codewave.co.in/fqube/user/logout',
			type: 'POST',
			
			datatype: "json",
			contentType: "application/json; charset=utf-8",
			sync : true,
			crossDomain: true,
			success: function (data) {
			if(data.status=='success')
			{
			
			 localStorage.clear();
			window.location.href="http://codewave.co.in/fqube";
			}
					
						
			}
			});



});
</script>
<script>
jQuery(document).ready(function(){ 
jQuery('.explore').hover(function(){
        jQuery('.small-menu').show();// You can chose, slow,fast or type milliseconds
		jQuery('.navbar-inverse').css({"box-shadow":"none"});
},function(){
        jQuery('.small-menu').hide();
		jQuery('.navbar-inverse').css({"box-shadow":"0px 0px 8px 2px rgba(0, 0, 0, 0.1)"});
});
});
</script>
<script>
jQuery(document).ready(function(){ 
jQuery('.small-menu').hover(function(){
        jQuery(this).show();// You can chose, slow,fast or type milliseconds
		jQuery('.navbar-inverse').css({"box-shadow":"none"});
},function(){
        jQuery(this).hide();
		jQuery('.navbar-inverse').css({"box-shadow":"0px 0px 8px 2px rgba(0, 0, 0, 0.1)"});
});
});
</script>
<script>
jQuery(document).ready(function(){ 
jQuery('.stocky_hover_details').hover(function(){
        //jQuery('.like-bottom').show();// You can chose, slow,fast or type milliseconds
		jQuery('.like-bottom').css({"display":"none"});
},function(){
        //jQuery('.small-menu').hide();
		jQuery('.like-bottom').css({"display":"block"});
});
});

</script>
<script>
jQuery(function(){
jQuery("#add-coll-save").keypress(function (e) {
        if (e.keyCode == 13) {
           addCollection(this);
		   alert('dsvcsd');
        }
    });
jQuery("#add-collec").keypress(function (e) {
        if (e.keyCode == 13) {
		   alert('dsds');
           addSave(this);
        }
    });
});
 </script>
