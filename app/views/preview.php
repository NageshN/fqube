<?php
include('header2.php');
?>
<head>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
  <script src="http://cdn.embed.ly/jquery.embedly-3.0.5.min.js" type="text/javascript"></script>
  <script src="http://cdn.embed.ly/jquery.preview-0.3.2.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="http://cdn.embed.ly/jquery.preview-0.3.2.css" />
</head>
<form action="/update" method="POST">
    <input id="url" type="text" name="url"/>
 
    <!-- Placeholder that tells Preview where to put the selector-->
    <div class="selector-wrapper"></div>
</form>
<script>
    // Set up preview.
    $('#url').preview({key:'your_embedly_key'})
 
    // On submit add hidden inputs to the form.
    $('form').on('submit', function(){
      $(this).addInputs($('#url').data('preview'));
      return true;
    });
</script>