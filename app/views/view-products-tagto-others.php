<?php
	$userId = Session::get('admin_id');
	$userName = Session::get('email_id');
	if(!isset($userId)){

		header("Location: adminPanel");
	}
	else{
?>

<?php include('sideboard.php'); ?>
 
<?php
include('connect.php');
?>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Fqube Admin Panel</title>
<meta name="generator" content="Bootply" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->
<style>
.error-text-box{
border:1px solid #ff1100 !important;
}
</style>
</head>
<body>

<h2 style="text-align:center;">Products Tagged to Others</h2>
<!--<table id="abused-pro" class='table table-bordered table-striped table-hover' style='width:70%;margin-left:22%;margin-top:3%;text-align:center;'>
<tr><th>SL.no</th><th>Product Id</th><th>Product Name</th><th>Category Id</th><th>User Id</th></tr>

</table>-->
<div class="container">
		<section>
     	<table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
					    <th>SL.no</th>
						<th>Product Id</th>
						<th>Product Name</th>
						<th>Abused Count</th>
						<th>Details</th>
					</tr>
				</thead>

				<tfoot>
					<tr>
						<th>SL.no</th>
						<th>Product Id</th>
						<th>Product Name</th>
						<th>Abused Count</th>
						<th>Details</th>
					</tr>
				</tfoot>

				<tbody id="abused-pro">
					
				</tbody>
			</table>
		</section>
	</div>
</div>
</div> 
<?php
} 
?>
<script>
jQuery(document).ready(function(){ 
var url=FQUBE.baseUrl+'admin/getProductsTaggedToOthers';
jQuery.ajax({ 
			type: "GET",    
			url:url,      
			dataType : 'json',
			success: function (data)  
			 {
			 console.log(data);
			 //console.log(data[0].product_id);
			 
			 for(var i=0;i<data.length;i++){
			 j=i+1;
             
			 jQuery('#abused-pro').append('<tr><td style="text-align: center;vertical-align: middle;">'+j+'</td><td style="text-align: center;vertical-align: middle;">'+data[i].product_id+'</td><td style="text-align: center;vertical-align: middle;">'+data[i].product_name+'</td><td style="text-align: center;vertical-align: middle;">'+data[i].category_id+'</td><td style="text-align: center;vertical-align: middle;">'+data[i].user_id+'</td></tr>');
			 }
			 }

});
}); 
</script>