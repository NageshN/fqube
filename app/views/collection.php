<?php
include('header2.php');
?>
<style>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
.edd_download_image {
  display: inline-block;
  width: 250px;
  height: 250px;
  background: white;
  border-style: solid;
  border-color: #fff;
  border-width: 1px 1px 0 1px;
  text-align: left;
  vertical-align: top;
}
.profile-image{
width: 153px;
height:153px;
float:right;
border-radius: 9px;
border: 1px solid #f6f6f6;
background: #3b3b3b;
color: #fff;
font-size: 22px;
font-weight: 400;
text-transform: uppercase;
text-align: center;
}
.profile-image p{
margin-top: 60px;
}
.desc {
font-size: 22px;
}
.desc-text {
font-size: 17px;
line-height: 20px;
font-weight: lighter;
text-align: justify;
}
.one-liners{
color:#cccccc;
font-size:17px;
font-weight:lighter;
padding: 5px 0 5px 0px;
}
/*.glyphicon-tag{
transform:rotate(90deg);
}*/
.cover{

    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
	padding:0px!important;
	
}
.tagging{
width:26%;
float:left;
padding:9px;
}
.tag-author{
width:100%;
float:left;

}
.blocks{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
padding: 0px;
margin: 15px;
width:22%;
}
.edd_download_inner{
margin:0px!important;
padding:0px!important;
}
.tag-image{
border-radius:9px;
}
.hero-line{
margin-bottom:21px;
}
.similar-products{
color:#454545!important;
}
.new-short{
background:#ccc!important;
border:1px solid #ccc!important;
text-shadow:0 1px 0 #454545!important;
color:#454545!important;
}
.fa-heart{
color:white;
}
@media only screen and (max-width: 949px) {
.blocks{
width:auto!important;
}
}
a.close1 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
background-color: transparent!important; 
} 
	
	a.close1:hover {
		background-color: transparent!important; 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	} 

.collect{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
background:white;
padding-right:0px;
}
.main-image{
padding:15px 0px 15px 0px;
}
.small-image{
padding:0px 15px 15px 0px;
}
.follow{
float: right;
font-size: 13px;
border: none!important;
background: #5AC3A2!important;
color: white;
text-shadow: none;
}
.total{
padding-bottom:53px;
}
.follow-btn{
margin-top: 20px;
float: right;
font-weight: 100;
}
.follow-btn1{
margin-top: 20px;
float: right;
font-weight: 100;
border-color: #5AC3A2;
background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
padding: 6px 10px!important;
background: #3b3b3b!important;
}
.follow-top {
width: 100%;
float: left;
margin-bottom: 12px;
}
.box-div {
background-color: #fff;
padding: 10px 0px 10px 0px;
margin-bottom: 40px;
margin-left: 55px;
border-radius: 2px;
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
}
.attachment-img {
border-radius: 5px;
height: 140px;
width: 140px;
margin-left: 12px;
}
.btn-default3:hover {
border-color: #3b3b3b;
background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
}
.btn-default3 {
background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
background-repeat: repeat-x;
}
.btn3 {
display: inline-block;
outline: none;
padding: 6px 35px;
margin-bottom: 0;
font-size: 15px;
color:#fff;
font-weight: 100;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
background:#5AC3A2;
margin-right:15px;
}
.attachment-img1{
border-radius:5px;
}
#tab2{
color:#5AC3A2;
} 
#tabs-3{
display:none;
}
</style> 
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<input type="hidden" id="url_id" value=""/>
<div class="row" style="padding: 24px 0px; background: none repeat scroll 0% 0% rgb(255, 255, 255);">
<div class="collection-profile container" style="padding:12px 35px 0px;">
	
	 
</div>
</div>  
<div id="tabs-2">
 <div class="row" style="padding-bottom:35px;padding-top:35px;">
 <div class="collection-products container">
 
  </div>   
  </div>
   <div id="loader" style="margin-bottom: -10px;">
  <div class="cube"></div>
</div>
<div style="text-align:center;  margin-bottom: 40px;margin-top: 0px;"><span class="load-more btn2 btn-default2" style="display:none;" id="viewcollectionproduct" data-id="2" onclick="viewMoreCollectionProducts(this);">VIEW MORE PRODUCTS</span></div>     
 </div>

  <div id="tabs-3">
  <div class="row" style="padding-bottom:35px;padding-top:47px;">
 <div class="collection-followers container">
  
  </div>  
  </div>
 <div id="loader1" style="margin-bottom: -10px;">
  <div class="cube"></div>
</div>
  <div style="text-align:center;  margin-bottom: 40px;margin-top: 0px;"><span class="load-more btn2 btn-default2" style="display:none;" id="viewcollectionpeople" data-id="2" onclick="viewMoreCollectionPeople(this);">VIEW MORE PEOPLE</span></div>   
 </div>
<!-- delete confirmation popup-->
<div class="row" id="deleteProduct" style="background: #f6f6f6;margin-top:20px;padding: 10px 2px 10px 2px;display:none;">
  <div class="container"> 
    <div class="col-md-12" style="padding-bottom:10px">
      <div class="col-md-6" style="font-size:18px;">
        Are You Sure?
      </div>
    </div>
      <div>
            <div class="col-md-12" style="padding-top:25px;background:#ffffff;text-align: center;font-size: 18px;">
               Are you sure you want to remove this product from your collection?
            </div>
            <div  id="deletePopupContent" class="col-md-12" style="text-align:center;background:#ffffff">
            <button class="btn2 btn-default2" style="width:auto;padding:6px 29px" id="delete_it">YES</button>
            <button class="btn2 btn-default2 deleteProduct_close" onclick="closeBox(this);" style="width:auto;padding:6px 30px;background:#ccc;color:#3b3b3b">NO</button>

            <div id="deletePopFailure" style="display: none;">
                Oops! There was an error, please try again in a while.
            </div> 

            <div id="deletePopSuccess" style="display: none;  text-align: center;font-size: 18px;padding-bottom: 20px;">
               please wait.. while we remove the product.
            </div>
            </div>
      </div>
      

    </div> 
</div> 
<!-- delete confirmation popup-->    

<script>
   jQuery('#deleteProduct').popup({
      transition: 'all 0.3s',
      scrolllock: false
});
</script>
<?php
include('footer1.php');
?>
<script src="<?=$baseUrl;?>template/fcubejs/collection.js"></script>