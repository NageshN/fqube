<?php
include('header.php');
include('connect.php');
?>
</style>
<style>
div#registerPop_wrapper {
  overflow: visible !important;
}
.custom_display
{
display:none !important;
}
#forgotPass_wrapper{
overflow:inherit!important;
}
.home-input{
background-color: rgba(255,255,255,0.4);
padding: 2px 14px 2px 14px;
border-radius: 2px;
}
#invalid_email{
color: #fff!important;
  background: #D27265;
  margin-top: 28px;
  padding: 4px;
  border-radius: 3px;
  float: left;
  width: 100%;
}
.error3{
color: #fff!important;
  background: #D27265;
  margin-top: 28px;
  padding: 4px;
  border-radius: 3px;
  float: left;
  width: 100%;
}
#home-search-form {
width: 50%;
text-align: center;
margin-left: auto;
margin-right: auto;
}
</style>
<div class="top-head"><span>DISCOVER UNIQUE PRODUCTS, SAVE THE THINGS<br>YOU LOVE AND BUY IT ALL THROUGH FQUBE!<br></span></div>
<form id="home-search-form"><div class="input-group home-input">
      <input type="text" class="form-control" id="home-search" style="z-index: 0;" placeholder="search here...">
      <span class="input-group-btn">
        <button id="home-search-submit" onclick="FQUBE.search.search_text(this)" class="btn btn-default1 search-btn" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
</span>
</div></form>

</div>
<div class="row" style="margin-right:0px;background: #f6f6f6;">
<div class="container">
<a href="#myAnchor" class="jumper"><div class="circle-icon"><img src="<?=$baseUrl;?>template/img/arrow.png" alt="" style="margin-top: 9px;margin-left: 9px;width:75%;"></div></a>
</div>
</div>
<a name="myAnchor" id="myAnchor"> 
<div class="row" style="background: #f6f6f6;">
<div class="container">
<head>
<meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link rel="stylesheet" id="edd-styles-css" href="<?=$baseUrl;?>template/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-retemplate-css" href="<?=$baseUrl;?>template/assets/edd-retemplate.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="<?=$baseUrl;?>template/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="<?=$baseUrl;?>template/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" async="" src="http://tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js"></script><script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/themes.designcrumbs.com\/stocky\/wp-admin\/admin-ajax.php","position_in_cart":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/stocky\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"http:\/\/themes.designcrumbs.com\/stocky\/checkout\/","permalinks":"1","quantities_enabled":""};
/* ]]> */
</script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fitvids.js?ver=4.1"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://themes.designcrumbs.com/stocky/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://themes.designcrumbs.com/stocky/wp-includes/wlwmanifest.xml"> 
<script type="text/javascript">
/* <![CDATA[  */

	
jQuery(document).ready(function($){

	// load mobile menu
	$('#main_menu ul.menu').mobileMenu();
    $('select.select-menu').each(function(){
        var title = $(this).attr('title');
        if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
        $(this)
            .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
            .after('<span class="select">' + title + '</span>')
            .change(function(){
                val = $('option:selected',this).text();
                $(this).next().text(val);
            })
    });

	/* Masonry */
	var $container = $('.edd_downloads_list');
	// initialize Masonry after all images have loaded  
	$container.imagesLoaded( function() {
		$container.masonry( { itemSelector: '.edd_download' } );
	});
	
	/* Parallax */
	(function(){
		var ua = navigator.userAgent,
		isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);
		
		/* only show if not on mobile */
		if (isMobileWebkit) {
			$('html').addClass('stocky_mobile');
		} else {
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 40
			});
		}
	
	})();
	
	// FitVids
	$("#content").fitVids();

	/* Menu Toggle */
	$('#menu_toggle').click(function() {
		$('#main_menu').slideToggle('fast');
	});
	
	/* Search Toggle */
	$('#search_toggle').click(function() {
		$('#search_wrap').slideToggle('fast');
	});
	
	/* Ratings */
	$( '.comment_form_rating .edd_retemplate_rating_box' ).find('a').on('click', function (e) {
		e.preventDefault();
		$( '.comment_form_rating .edd_retemplate_rating_box' ).find('a').removeClass( 'active' );
		$( this ).addClass( 'active' );
	});
	
	/* Add span within comment reply title, move the <small> outside of it */
	$('#reply-title').wrapInner('<span>').append( $('#reply-title small') );
	
	// Increase counter on add to cart
	$('.purAddToCart').ajaxComplete(function(event,request, settings) {
		if(JSON.parse(request.responseText).msgId == 0) {
			var currentCount = parseInt($('#header_cart_count').text());
			var newCount = currentCount + 1;
			$('#header_cart_count').text(newCount);
		}
	});
	
	// Fancybox
	if( $('.lightbox').length ) {
		$(".lightbox").attr('rel', 'gallery').fancybox({
			'transitionIn'		: 'fade',
			'transitionOut'		: 'fade',
			'showNavArrows' 	: 'true'
		});
	}

});

/* ]]> */
</script>	<style type="text/css">.fancybox-margin{margin-right:17px;}</style><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></head>
<div style="clear:both"></div></a><section id="site_wrap">		
<section class="wrapper" id="content"><div class="container clearfix">
<div id="loader1" style="margin-bottom: -10px;">
 <div class="cube"></div>
</div>	
<section id="image_grid" class="image_grid_full clearfix" style="margin-top: -60px;">
<div id="stocky_downloads" class="edd_downloads_list edd_download_columns_0 masonry" style="position: relative; height: auto !important;display:none;margin-top: 60px;">

<div id="edd_download_pagination" class="navigation masonry-brick"></div>
<div style="clear:both;" class="masonry-brick"></div>
<div id="edd_download_pagination" class="navigation masonry-brick"></div>
</div>	
<div id="loader" style="margin-bottom:0px;margin-top:20px;">
  <div class="cube"></div>
</div>
<div id="loadmorepopsearch" class="load-more" style="display:none;"><span class="load-more btn2 btn-default2 fade1_open">LOAD MORE</span></div>
<div id="stocky_downloads_list" class="edd_downloads_list edd_download_columns_0 masonry" style="position: relative; height: auto !important;">

<div style="clear:both;" class="masonry-brick"></div>
<div id="edd_download_pagination" class="navigation masonry-brick"></div>
</div>	
<div id="stocky_downloads_list5" class="edd_downloads_list edd_download_columns_0 masonry" style="position: relative; height: 1192px;">
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_169" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image"> 
<a href="#" title="Coffee &amp; Code">
<img id="img1" width="548" height="548" src="http://codewave.co.in/fqube/template/assets/DSCF0726-square-548x548.jpg" class="attachment-product_main wp-post-image" alt="DSCF0726-square">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_166" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="The Bart">
<img id="img2" width="548" height="363" src="http://codewave.co.in/fqube/template/assets/DSCF1196-548x363.jpg" class="attachment-product_main wp-post-image" alt="DSCF1196">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_88" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Fountain Music">
<img id="img3" width="280" height="185" src="http://codewave.co.in/fqube/template/assets/35H-280x185.jpg" class="attachment-product_main wp-post-image" alt="35H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_85" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner" id="jump-down">
<div class="edd_download_image">
<a href="#" title="Cabs">
<img id="img4" width="280" height="210" src="http://codewave.co.in/fqube/template/assets/25H-280x210.jpg" class="attachment-product_main wp-post-image" alt="25H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_82" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Mountains and Shorelines">
<img id="img5" width="280" height="210" src="http://codewave.co.in/fqube/template/assets/15hzwjF-280x210.jpg" class="attachment-product_main wp-post-image" alt="15hzwjF">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_79" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Electronic Grid">
<img id="img6" width="280" height="175" src="http://codewave.co.in/fqube/template/assets/13UUIs4-280x175.jpg" class="attachment-product_main wp-post-image" alt="13UUIs4">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_76" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Seagull">
<img id="img7" width="280" height="185" src="http://codewave.co.in/fqube/template/assets/1odGkUB-280x185.jpg" class="attachment-product_main wp-post-image" alt="1odGkUB">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_73" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Time To Work">
<img id="img8" width="280" height="186" src="http://codewave.co.in/fqube/template/assets/1j5JfNg-280x186.jpg" class="attachment-product_main wp-post-image" alt="1j5JfNg">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_71" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Field">
<img id="img9" width="280" height="186" src="http://codewave.co.in/fqube/template/assets/1drS3wE-280x186.jpg" class="attachment-product_main wp-post-image" alt="1drS3wE">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_67" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Co-Work">
<img id="img10" width="280" height="280" src="http://codewave.co.in/fqube/template/assets/1cHlp7N-280x280.jpg" class="attachment-product_main wp-post-image" alt="1cHlp7N">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_64" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Snow Mobiles">
<img id="img11" width="280" height="408" src="http://codewave.co.in/fqube/template/assets/1cexZy0-280x408.jpg" class="attachment-product_main wp-post-image" alt="1cexZy0">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_60" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Rolling Hill Tops">
<img  id="img12" width="280" height="151" src="http://codewave.co.in/fqube/template/assets/1aj5Gd2-280x151.jpg" class="attachment-product_main wp-post-image" alt="1aj5Gd2">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_57" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Foggy">
<img  id="img13" width="280" height="280" src="http://codewave.co.in/fqube/template/assets/149muhS-280x280.jpg" class="attachment-product_main wp-post-image" alt="149muhS">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_54" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Trombone">
<img id="img14" width="280" height="381" src="http://codewave.co.in/fqube/template/assets/26H-280x381.jpg" class="attachment-product_main wp-post-image" alt="26H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_172" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Make Your Mark">
<img id="img15" width="548" height="363" src="http://codewave.co.in/fqube/template/assets/DSCF0492-548x363.jpg" class="attachment-product_main wp-post-image" alt="DSCF0492">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_51" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Video Product">
<img  id="img16" width="548" height="361" src="http://codewave.co.in/fqube/template/assets/1d4U0A31-548x361.jpg" class="attachment-product_main wp-post-image" alt="1d4U0A3">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_48" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Desktop">
<img id="img17" width="280" height="187" src="http://codewave.co.in/fqube/template/assets/14lL9AE-280x187.jpg" class="attachment-product_main wp-post-image" alt="SONY DSC">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_45" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="A Bike">
<img id="img18" width="280" height="205" src="http://codewave.co.in/fqube/template/assets/38H-280x205.jpg" class="attachment-product_main wp-post-image" alt="38H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_35" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Logjammin’">
<img id="img19" width="548" height="365" src="http://codewave.co.in/fqube/template/assets/1fhhlK6-548x365.jpg" class="attachment-product_main wp-post-image" alt="1fhhlK6">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_10" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Piling Up">
<img id="img20" width="280" height="187" src="http://codewave.co.in/fqube/template/assets/1epgtU4-280x187.jpg" class="attachment-product_main wp-post-image" alt="1epgtU4">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_169" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image"> 
<a href="#" title="Coffee &amp; Code">
<img id="img21" width="548" height="548" src="http://codewave.co.in/fqube/template/assets/DSCF0726-square-548x548.jpg" class="attachment-product_main wp-post-image" alt="DSCF0726-square">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_166" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="The Bart">
<img id="img22" width="548" height="363" src="http://codewave.co.in/fqube/template/assets/DSCF1196-548x363.jpg" class="attachment-product_main wp-post-image" alt="DSCF1196">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_88" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Fountain Music">
<img id="img23" width="280" height="185" src="http://codewave.co.in/fqube/template/assets/35H-280x185.jpg" class="attachment-product_main wp-post-image" alt="35H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_85" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner" id="jump-down">
<div class="edd_download_image">
<a href="#" title="Cabs">
<img id="img24" width="280" height="210" src="http://codewave.co.in/fqube/template/assets/25H-280x210.jpg" class="attachment-product_main wp-post-image" alt="25H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_82" style="width: inherit; float: left; position: absolute;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Mountains and Shorelines">
<img id="img25" width="280" height="210" src="http://codewave.co.in/fqube/template/assets/15hzwjF-280x210.jpg" class="attachment-product_main wp-post-image" alt="15hzwjF">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box" id="edd_download_79" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Electronic Grid">
<img id="img26" width="280" height="175" src="http://codewave.co.in/fqube/template/assets/13UUIs4-280x175.jpg" class="attachment-product_main wp-post-image" alt="13UUIs4">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box1" id="edd_download_76" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Seagull">
<img id="img27" width="280" height="185" src="http://codewave.co.in/fqube/template/assets/1odGkUB-280x185.jpg" class="attachment-product_main wp-post-image" alt="1odGkUB">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box2" id="edd_download_73" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Time To Work">
<img id="img28" width="280" height="186" src="http://codewave.co.in/fqube/template/assets/1j5JfNg-280x186.jpg" class="attachment-product_main wp-post-image" alt="1j5JfNg">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box3" id="edd_download_71" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Field">
<img id="img29" width="280" height="186" src="http://codewave.co.in/fqube/template/assets/1drS3wE-280x186.jpg" class="attachment-product_main wp-post-image" alt="1drS3wE">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box4" id="edd_download_67" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Co-Work">
<img id="img30" width="280" height="280" src="http://codewave.co.in/fqube/template/assets/1cHlp7N-280x280.jpg" class="attachment-product_main wp-post-image" alt="1cHlp7N">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box5" id="edd_download_169" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image"> 
<a href="#" title="Coffee &amp; Code">
<img id="img31" width="548" height="548" src="http://codewave.co.in/fqube/template/assets/DSCF0726-square-548x548.jpg" class="attachment-product_main wp-post-image" alt="DSCF0726-square">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box6" id="edd_download_166" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="The Bart">
<img id="img32" width="548" height="363" src="http://codewave.co.in/fqube/template/assets/DSCF1196-548x363.jpg" class="attachment-product_main wp-post-image" alt="DSCF1196">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box7" id="edd_download_88" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Fountain Music">
<img id="img33" width="280" height="185" src="http://codewave.co.in/fqube/template/assets/35H-280x185.jpg" class="attachment-product_main wp-post-image" alt="35H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box8" id="edd_download_85" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner" id="jump-down">
<div class="edd_download_image">
<a href="#" title="Cabs">
<img id="img34" width="280" height="210" src="http://codewave.co.in/fqube/template/assets/25H-280x210.jpg" class="attachment-product_main wp-post-image" alt="25H">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box9" id="edd_download_82" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Mountains and Shorelines">
<img id="img35" width="280" height="210" src="http://codewave.co.in/fqube/template/assets/15hzwjF-280x210.jpg" class="attachment-product_main wp-post-image" alt="15hzwjF">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box10" id="edd_download_79" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Electronic Grid">
<img id="img36" width="280" height="175" src="http://codewave.co.in/fqube/template/assets/13UUIs4-280x175.jpg" class="attachment-product_main wp-post-image" alt="13UUIs4">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box11" id="edd_download_76" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Seagull">
<img id="img37" width="280" height="185" src="http://codewave.co.in/fqube/template/assets/1odGkUB-280x185.jpg" class="attachment-product_main wp-post-image" alt="1odGkUB">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box12" id="edd_download_73" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Time To Work"> 
<img id="img38" width="280" height="186" src="http://codewave.co.in/fqube/template/assets/1j5JfNg-280x186.jpg" class="attachment-product_main wp-post-image" alt="1j5JfNg">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box13" id="edd_download_71" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Field">
<img id="img39" width="280" height="186" src="http://codewave.co.in/fqube/template/assets/1drS3wE-280x186.jpg" class="attachment-product_main wp-post-image" alt="1drS3wE">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open display-box14" id="edd_download_67" style="width: inherit; float: left; position: absolute;display:none;">
<div class="edd_download_inner">
<div class="edd_download_image">
<a href="#" title="Co-Work">
<img id="img40" width="280" height="280" src="http://codewave.co.in/fqube/template/assets/1cHlp7N-280x280.jpg" class="attachment-product_main wp-post-image" alt="1cHlp7N">		</a>
<div class="stocky_hover_details stocky_wish_list_on"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like " id="1" onclick="like(this)" style="background: url(http://codewave.co.in/fqube/template/img/sprite-like.png);"></div><span class="label" style="margin-left: -55px;">Like</span></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect" style="background: url(http://codewave.co.in/fqube/template/img/sprite-collect.png);"></div><span class="label opens overlayLinks" id="180" onclick="getProdUserid(this)" style="margin-left: -48px;">Save</span></a></div></div>
</div>
</div>
</div>
<div style="clear:both;" class="masonry-brick"></div>
<div id="edd_download_pagination" class="navigation masonry-brick"></div>
</div>
</section>
</div></section>		
</section>

		<div id="fb-root"></div><script type="text/javascript" src="<?=$baseUrl;?>template/assets/masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/comment-reply.min.js?ver=4.1"></script>

<link rel="stylesheet" id="css-stocky-css" href="<?=$baseUrl;?>template/assets/style.css?ver=4.1" type="text/css" media="all">
  
<!--<script type="text/javascript" src="<?=$baseUrl;?>template/assets/modal.min.js?ver=1.0.6"></script>-->

	<div class="modal fade" id="edd-wl-modal" tabindex="-1" role="dialog" aria-labelledby="edd-wl-modal-label" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    		    </div>
	  </div>
	</div>
<div id="loadmore" class="load-more"><span class="load-more btn2 btn-default2" onclick="loadMoreHome(this);">LOAD MORE</span></div>
<div id="loadmorepop" class="load-more" style="display:none;"><span class="load-more btn2 btn-default2 fade1_open">LOAD MORE</span></div>
</div>
</div>
<div class="row" style="background:#fff;">
 <div class="container">
 <div class="how-it-works"><h2>HOW IT WORKS</h2></div>
 <div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>
  <div class="col-md-4 how-col4"><div class="col-md-6 how-img-center"><img src="<?=$baseUrl;?>template/img/how-it-works1.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">FOLLOW</span><br><p class="how-text">Follow people, <br>collections & stores</p></div></div>
  <div class="col-md-4 how-col4"><div class="col-md-6 how-img-center"><img src="<?=$baseUrl;?>template/img/collect.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">SAVE</span><br><p class="how-text">Save all<br> your favorites</p></div></div>
  <div class="col-md-4 how-col4"><div class="col-md-6 how-img-center"><img src="<?=$baseUrl;?>template/img/buy.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">BUY</span><br><p class="how-text">Buy it all in<br> one place</p></div></div>
  </div>
</div>
<div id="fberror" class="popup-success" style="color:#6B6969;font-weight: 100;padding:10px;width:400px;background:#f6f6f6;height:260px;border-radius:5px;display:none;overflow:inherit;"><div class="col-md-12"><div class="col-md-1"></div><div class="col-md-10" style="font-size: 23px;font-family:Verdana, sans-serif;padding-top: 41px;text-align: center;">Login Error</div><div class="col-md-1"></div></div><div class="col-md-12" style="font-size: 15px;color: grey;line-height: 22px;text-align: center;padding-top: 26px;padding-bottom:30px">Email address already exist</div><div class="col-md-12" style="text-align:center"><a class="close fberror_close"><i class="fa fa-times"></i></a></div></div>
<div id="gperror" class="popup-success" style="color:#6B6969;font-weight: 100;padding:10px;width:400px;background:#f6f6f6;height:260px;border-radius:5px;display:none;overflow:inherit;"><div class="col-md-12"><div class="col-md-1"></div><div class="col-md-10" style="font-size: 23px;font-family:Verdana, sans-serif;padding-top: 41px;text-align: center;">Login Error</div><div class="col-md-1"></div></div><div class="col-md-12" style="font-size: 15px;color: grey;line-height: 22px;text-align: center;padding-top: 26px;padding-bottom:30px">Email address already exist</div><div class="col-md-12" style="text-align:center"><a class="close gperror_close"><i class="fa fa-times"></i></a></div></div>
<input id="fberror1" class="" type="hidden"/>
<input id="gperror1" class="" type="hidden"/>
<?php
include('footer.php');
?>
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<!--dhanush's popup-->
<div class="well" id="fade1" style="display:none;">
	<div class="login-wrapper"> 
	 	<div class="login-content"> 
			<div class="closure">
			<div class="icon" style="display:none!important;"><img src="<?=$baseUrl;?>template/img/login-icon.png"/></div><h3 style="float:left;font-weight:lighter;font-size: 28px;margin-left: 10px;" class="home_login_popup">Login</h3>
			<a class="close fade1_close" onclick="closeError(this);"><i class="fa fa-times"></i></a>
			</div>
			<form style="float:left" id="signinForm" action="javascript:void(0)" onSubmit="FQUBE.user.signin();" >
			
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf007;" disabled=""><input style="   font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;font-weight:lighter" type="email" id="email" name="email" placeholder="email_address"        />
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf023;" disabled=""><input style="font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;font-weight:lighter" type='password' id="password" name='password' placeholder='password'     />
		<input type="submit"  class="feed_open fQubeSignIn" style="margin-top: 6px;width:100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px"  type='button'  value='SIGN IN'/><br>
		<div id="invalid_email" style="font-size: 12px;margin-top: 29px;display:none;text-align: center;">Invalid Email Address/Password</div>
		<div style="display:none;text-align: center;" class="error2">Mandatory fields are missing</div>
		</form>
		<div class="horizontal">
		<span class="row1">or</span></div>
		<div class="social-links"><div class="facebook1" style="height: 45px;float: left;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#3B4A98;padding: 8px;padding-right: 18px;padding-left: 18px;"><i style="color:white" class="fa fa-facebook"></i></div>
		<div class="fb" style="height: 45px;border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithFacebook">Login with facebook</a></div>
		<div class="gm" style="height: 45px;border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithGoogle">Login with google+</a><br></div>
		<div class="google1" style="height: 45px;float: right;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#AD2412;padding: 8px;padding-right: 16px;padding-left: 16px;"><i style="color:white" class="fa fa-google-plus"></i></div>
		</div>
		
		<div class="forgot forgotPass_open"><a style="color:rgb(165,165,165);text-decoration:none" href="">Forgot Password?</a></div>
		<div class="register1"><a class="overlayLink1" style="color:rgb(165,165,165);text-decoration:none" id="dont" href="#">Don't have an account?</a><br></div>
		<div id="forgotPass" class="well" style="padding: 50px;"><div class="closure">
			<div class="icon" style="display:none!important;"><img src="http://codewave.co.in/fqube/template/img/login-icon.png"></div><h3 style="float:left;font-weight:lighter;font-size: 28px;margin-top: -5px;padding-bottom: 20px;" class="home_login_popup">Forgot Password</h3>
			</div><div style="width: 100%;float: left;"><form action="javascript:void(0);"><input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:10%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;  margin-top: 0px;" type="email" placeholder="&nbsp;&#xf007;" disabled=""><input style="   font-family: 'FontAwesome','roboto';width:90%;   margin-top: 0px;border-top-left-radius: 0px; border-bottom-left-radius: 0px;font-weight:lighter" type="email" id="forgot_pass" name="forgot_pass" placeholder="enter email-id"><div style="margin-left: auto;margin-right: auto; text-align: center;"><button class="btn2 btn-default2" id="forgot_click1" style="padding: 6px 140px;">Submit</button></div></form><a class="close forgotPass_close"><i class="fa fa-times"></i></a></div></div>	
		</div>
	</div>
</div>
   
<!--dhanush's popup ends-->
<div class="well"  id="registerPop" style="display: none;width:38%;">
	<div class="login-wrapper1" style="">
		<div class="login-content1"> 
		<div class="closure">
			<a class="close1 registerPop_close"><i class="fa fa-times"></i></a>
			</div> 
			<div class="icon" style="display:none!important;"><img src="<?=$baseUrl;?>template/img/login-icon.png"/></div><h3 style="float:left;padding-top: 21px;font-weight:lighter;font-size: 28px;margin-left: 10px;  padding-bottom: 0px;">Register</h3>
		<form style="float:left" id="signupForm" onsubmit="return checkForm(this);" action="javascript:void(0);">
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="@" disabled/><input style="  font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;" id="email_address" type="email" name="email_addr" placeholder="enter mail id"/>
		
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf007;" disabled/><input style=" font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;" id="username" type="text" name="username" placeholder="enter name"/>
		
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf023;" disabled/><input style=" font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;" id="password1" type="password" name="password1" placeholder="password"/>
		
		<input id="custom_popup" class="fQubeSignIn" style="margin-top: 6px;width:100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px" type='submit' value='GET STARTED'/>
		<div style="color:red;display:none;text-align: center;" class="error3">Mandatory fields are missing</div>
		<div id="email_exist" style="color: white;font-size: 14px;border-radius:3px;background:#D27265;margin-bottom:12px;display:none;float:left;width:100%;margin-top:17px;text-align: center;">Email-id already exist.</div>
		<div id="mand_exist" style="color: white;font-size: 14px;border-radius:3px;background:#D27265;margin-bottom:12px;display:none;float:left;width:100%;margin-top:17px;text-align: center;">Mandatory Fields Missing/Invalid Email Address</div>	
		</form>
		<div class="horizontal">
		<span class="row1">or</span></div>
		<div class="social-links"><div class="facebook1" style="height: 45px;float: left;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#3B4A98;padding: 8px;padding-right: 18px;padding-left: 18px;"><i style="color:white" class="fa fa-facebook"></i></div>
		<div class="fb" style="height: 45px;border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithFacebook">Sign up with facebook</a></div>
		<div class="gm" style="height: 45px;border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithGoogle">Sign up with google+</a><br></div>
		<div class="google1" style="height: 45px;float: right;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#AD2412;padding: 8px;padding-right: 16px;padding-left: 16px;"><i style="color:white" class="fa fa-google-plus"></i></div>
		</div>
		<br><br>
		<div>
		<div class="already">Already have an account?<a href="" style="text-decoration:none;color:#5AC3A2" class="fade1_open" id="signed"> Sign in</a><br></div>
		</div>
		</div>
	</div>
</div>
<script>
function closeError(){
jQuery('.error3').hide();
jQuery('#email_exist').hide();
jQuery('#mand_exist').hide();
jQuery('.error2').hide();
jQuery('#invalid_email').hide();
}
</script>
<script>
$(".jumper").on("click",function( e ){
    e.preventDefault();
    $("body, html").animate({ 
        scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);
});
</script>
<script>
//script for fetching url parameter in javascript
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)  
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        } 
    }
}  
</script>
<script>

var fb;
var fbId = getUrlParameter('fb');
if(fbId == 400){
jQuery('#fberror1').addClass('fberror_open');
}
else {

}
var gp;
var gpId = getUrlParameter('gp');
if(gpId == 400){
alert('hi');
jQuery('#gperror1').addClass('gperror_open');
}
else {

} 
</script>
<script>
jQuery('#gperror').popup({

 transition: 'all 0.3s'
});
</script>
<script>
jQuery('#fberror').popup({

 transition: 'all 0.3s'
});
</script>
<script>
	jQuery('#submit1').click(function(){
	var email = jQuery('#email').val();
	var password=jQuery('#password').val();
	var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var url = '/fancythat/laravel/app/template/signin.php'; 
	var hasError = false;
		if((email == '') || (!pattern.test(email))){
			jQuery('.error2').show();
			jQuery('#email').addClass('error-border');
			var hasError = true;
			}
		else{
			jQuery('.error2').hide();
			jQuery('#email').removeClass('error-border');
			var hasError = false;
		}
		if(password == ''){
			jQuery('.error2').show();
			jQuery('#password').addClass('error-border');
			var hasError = true;
			}
		else{
			jQuery('.error2').hide();
			jQuery('#password').removeClass('error-border');
			var hasError = false;
		}
			if(!hasError){
			jQuery.ajax({
			type:'POST',
			url:url,
			data: {email:email,password:password},
			dataType: 'text',
			cache: false,
			success: function(data)
			
		{	
				   if(jQuery.trim(data) === "success"){
					 jQuery('.succeed').show();
					 //jQuery("#form1")[0].reset();
					 location.reload();
				}
					else{
						jQuery('.error2').show();
						jQuery('.error2').html("email-id/password incorrect");
					}
						
					}
			});
		}
		else
		{
			jQuery('.error2').show();
			
		}
	});
</script>
<script>
jQuery('#submit12').click(function(){
	var email = jQuery('#email_address').val();
	var name = jQuery('#username').val();
	var password1=jQuery('#password1').val();
	//var password2=jQuery('#password2').val();
	var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	//var hasError = false;
		if((email == '') || (!pattern.test(email))){
		jQuery('#email_address').addClass('error-border');
			jQuery('.error3').show();
			//var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#email_address').removeClass('error-border');
			//var hasError = false;
		}
		if(name == ''){
			jQuery('.error3').show();
			jQuery('#username').addClass('error-border');
			//var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#username').removeClass('error-border');
			//var hasError = false;
		}
		if(password1 == ''){
			jQuery('.error3').show();
			jQuery('#password1').addClass('error-border');
			//var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#password1').removeClass('error-border');
			//var hasError = false;
		}
		/*if(password2 == '' || password2 != password1){
			jQuery('.error3').show();
			jQuery('#password2').addClass('error-border');
			var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#password2').removeClass('error-border');
			var hasError = false;
		}*/
			//if(!hasError){
			var url= '/fancythat/laravel/register/'+email+'/'+name+'/'+password1;
			jQuery.ajax({
			type:'GET',
			url:url,
			dataType : 'text',
			success: function (data) 
			
		{	
				   //if(jQuery.trim(data) === "success"){
				   if(data){
					 jQuery('.succeed').show();
					 //jQuery("#form1")[0].reset();
					// header('Location: http://codewave.co.in/fancythat/laravel/select');
					window.location = "http://codewave.co.in/fancythat/laravel/select-new";
				}
					else{
						jQuery('.error3').show();
			jQuery('.error3').html("You are already registered");
					}
						
					}
			});
		/*}
		else
		{
			jQuery('.error3').show();
			
			
		}*/
	});
</script>

	<script>
	$(document).ready(function() {
	$("#log").click(function( event ){
	$('#fade1').show();
	$('#fade1_background').show();
	$('#fade1_wrapper').show();
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
	
		$.get( "ajax/" + action, function( data ) {
		alert(data);
			$( ".login-content" ).html( data );
		});	
		
		$(".overlay").fadeIn("slow");
	});
	
	$(".close").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay").fadeOut('slow');
		$(".error2").fadeOut('slow');
		document.getElementById("signinForm").reset();
		jQuery('#password').removeClass('error-border');
		jQuery('#email').removeClass('error-border');
	});
	/*(".overlay").click(function(){
		$(".login-wrapper").fadeToggle("fast");
		$(".overlay").hide();
		$(".error2").hide();
		//(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password').removeClass('error-border');
			})*/
	
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay").fadeToggle("fast");
		}
	});
	});
	</script>
	
	<script>
	$(document).ready(function() {
	$("#dont").click(function( event ){
	$('#fade1').hide();
	$('#fade1_background').hide();
	$('#fade1_wrapper').hide();
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink1").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content1" ).html( data );
		});	
		
		$(".overlay1").fadeIn("slow");
	});
	
	$(".close1").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").fadeOut('slow');
		$(".error3").fadeOut('slow');
		document.getElementById("signupForm").reset();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});

	/*$("body").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
		$(".error3").hide();
		//$(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});*/
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay1").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay1").fadeToggle("fast");
		}
	});
	
	});
	</script>
<script>
	$(document).ready(function() {
	$("#signed").click(function( event ){
	$('#fade1_background').show();
	$('#fade1_wrapper').show();
	$('#fade1').show();
	$('.overlay1').hide();
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	});
	</script>

	<script>
	$(document).ready(function() {
	/* $("#sig").click(function( event ){
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	}); */
	
	$(".overlayLink1").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content1" ).html( data );
		});	
		
		$(".overlay1").fadeIn("slow");
	});
	
	$(".close1").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").fadeOut('slow');
		$(".error3").fadeOut('slow');
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});

	/*$("body").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
		$(".error3").hide();
		//$(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});*/
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay1").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay1").fadeToggle("fast");
		}
	});
	
	});
	</script>
	
		
	<script>
	$(document).ready(function() {
	$(".opens").click(function( event ){
	
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink2").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content2" ).html( data );
		});	
		
		$(".overlay2").fadeToggle("fast");
	});
	
	$(".close2").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay2").hide();
		$(".error3").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
		jQuery('#collection').removeClass('error-border');
		jQuery('.error5').hide();
	});

	/*$("body").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
		$(".error3").hide();
		//$(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});*/
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay2").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay2").fadeToggle("fast");
		}
	});
	
	});
	</script>
	<script>
jQuery('#add').click(function(){
	var collection = jQuery('#collection').val();
	var hasError= false;
	if(collection == ''){
		jQuery('#collection').addClass('error-border');
			jQuery('.error5').show();
			var hasError = true;
			}
		else{
			jQuery('.error5').hide();
			jQuery('#collection').removeClass('error-border');
			var hasError = false;
		}
			var url= 'app/views/add.php';
			if(!hasError){
			jQuery.ajax({
			type:'POST',
			url:url,
			data: {collection:collection},
			dataType: 'text',
			cache: false,
			success: function(data)
			
			
		{	
				   if(jQuery.trim(data) === "success"){
				  
					 alert('added');
				}
					else{
						alert('error');
					}
						
					}
			});
			}
		
	});
</script>
<script>
 jQuery('#registerPop').popup({

	
      transition: 'all 0.3s',
      scrolllock: false
   
});
jQuery('#log').click(function()
{
jQuery('#fade1').removeClass('custom_display');
jQuery('#fade1_background').removeClass('custom_display');
});

jQuery('.forgotPass_open').click(function() 
{
jQuery('#fade1').addClass('custom_display');
jQuery('#fade1_background').addClass('custom_display');
});
 jQuery('#forgotPass').popup({

	
      transition: 'all 0.3s',
      scrolllock: false
   
});	
</script>
<script> 
function loadMoreHome(){
jQuery('.display-box').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 550px;top: 1284px;').slideDown();
jQuery('.display-box1').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 0px;top: 1332px;').slideDown();
jQuery('.display-box2').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 275px;top: 1358px;').slideDown();
jQuery('.display-box3').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 825px;top: 1450px;').slideDown();
jQuery('.display-box4').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 550px;top: 1461px;').slideDown();
jQuery('.display-box5').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 0px;top: 1517px;').slideDown();
jQuery('.display-box6').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 275px;top: 1543px;').slideDown();
jQuery('.display-box7').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 825px;top: 1636px;').slideDown();
jQuery('.display-box8').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 275px;top: 1729px;').slideDown();
jQuery('.display-box9').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 550px;top: 1736px;').slideDown();
jQuery('.display-box10').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 0px;top: 1792px;').slideDown();
jQuery('.display-box11').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 825px;top: 1822px;').slideDown();
jQuery('.display-box12').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 275px;top: 1937px;').slideDown();
jQuery('.display-box13').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 550px;top: 1945px;').slideDown();
jQuery('.display-box14').attr('style','width: inherit; float: left; position: absolute;display:block!important;left: 0px;top: 1969px;').slideDown();
jQuery('#stocky_downloads_list5').attr('style','height:2300px!important;').slideDown();
jQuery('#site_wrap').attr('style','overflow-x: inherit;');
jQuery('#loadmore').attr('style','display:none;');
jQuery('#loadmorepop').attr('style','display:block!important;');  
}
</script> 
<script src="<?=$baseUrl;?>template/fcubejs/products.js"></script>
<script src="<?=$baseUrl;?>template/fcubejs/search.js"></script>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
    <link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>