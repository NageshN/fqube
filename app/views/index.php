<!DOCTYPE html>
<html lang="en">
<?php 
$baseUrl ="http://codewave.co.in/fqube/";

?>
	<head>
		<meta charset="utf-8" />
		<title>Fqube Admin Login</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!--basic styles-->

		<link href="app/views/admin/css/bootstrap.min.css" rel="stylesheet" />
		<link href="app/views/admin/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="app/views/admin/assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->
        <link rel="icon" href="app/views/admin/images/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="app/views/admin/images/favicon.ico" type="image/x-icon">

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="app/views/admin/css/ace.min.css" />
		<link rel="stylesheet" href="app/views/admin/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="app/views/admin/css/ace-skins.min.css" />
		<style>
		.error-text-box{
			border:1px solid #ff1100 !important;
		}
	</style>

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

	<body class="login-layout">
		<div class="main-container container-fluid">
			<div class="main-content">
				<div class="row-fluid">
					<div class="span12">
						<div class="login-container">
							<div class="row-fluid">
								<div class="center">
									<h1>
										<img src="app/views/admin/images/logo.png">
									</h1>
									 <!--<h4 class="blue">Admin Panel</h4>-->
								</div>
							</div>

							<div class="space-6"></div>

							<div class="row-fluid">
								<div class="position-relative">
									<div id="login-box" class="login-box visible widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header blue lighter bigger">
													<i class="icon-coffee green"></i>
													Please Enter Your Information
												</h4>

												<div class="space-6"></div>

												<form>
													<fieldset>
														<label>
															<span class="block input-icon input-icon-right">
																<input type="text" class="span12" placeholder="Username" id="email" />
																<i class="icon-user"></i>
															</span>
														</label>

														<label>
															<span class="block input-icon input-icon-right">
																<input type="password" class="span12" placeholder="Password" id="password" />
																<i class="icon-lock"></i>
															</span>
														</label>

														<div class="space"></div>

														<div class="clearfix">
											<i class="icon-key"></i><input type="button" class="btn btn-primary btn-small" id="login" value="Login">

															
														</div>

														<div class="" id="alert_failure" style="text-align:center;color:#FF0000;font-weight:bold;display:none;">Login Failed</div>
														<div class="" id="missing" style="text-align:center;color:#FF0000;font-weight:bold;display:none;">Email/password is missing</div>
													</fieldset>
												</form>

												
											</div><!--/widget-main-->

											
										</div><!--/widget-body-->
									</div><!--/login-box-->

									
<!--/signup-box-->
								</div><!--/position-relative-->
							</div>
						</div>
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div>
		</div>
		<?php include('connect.php'); ?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		

		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?=$baseUrl;?>template/fcubejs/custom.js" /></script>
	</body>
	<script>
	$('#login').click(function(){
		var email = $('#email').val();
		var password = $('#password').val();
		
		var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
		var hasError = false;
		if((email == '') || (!pattern.test(email))){
			$('#email').addClass('error-text-box');
			$('#missing').show('slow');
			var hasError = true;
			
		}
		else
		{
			$('#email').removeClass('error-text-box');
			$('#missing').hide('slow');
		}
		if(password == ''){
			$('#password').addClass('error-text-box');
			$('#missing').show('slow');
			var hasError = true;
			
		}
		else
		{
			$('#password').removeClass('error-text-box');
			$('#missing').hide('slow');
		}
		
		var url = FQUBE.baseUrl+'admin/signinadmin/'+email+'/'+password;
		
		if(!hasError){
			$.ajax({
			type:'POST',
			url:url,
			//data: {email:email,password:password},
			cache: false,
			success: function(data)
				{	
					if(data.status == "success"){
							window.location.href='adminHome';
						}
					else{
						$( "#alert_failure" ).show('slow');
						$("#missing").hide();
					}
						
					}
			});
		}
	});
</script>
	
</html>
