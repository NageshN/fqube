<?php
include('header2.php'); 
?>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
<section id="site_wrap">		
<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
			<section class="wrapper" id="content">  
<div class="similar-products" style="margin-top: 60px;"><h2 class="similar-products-h2">Products IN </h2></div>	
<div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>		
<div aria-labelledby="ui-id-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-hidden="false" style="display: block;">
<div class="container clearfix"> 
<section id="image_grid" class="image_grid_full clearfix" style="margin-bottom: -60px;">
<div id="stocky_downloads_list" class="edd_downloads_list edd_download_columns_0 masonry" style="position: relative; height:auto!important;">


<div style="clear:both;" class="masonry-brick"></div>
<div id="edd_download_pagination" class="navigation masonry-brick"></div> 
</div>
</section>
<div id="loader" style="  padding-top: 30px;">
  <div class="cube"></div>
</div>
</div></div>
<div style="text-align:center;  margin-bottom: 40px;margin-top: 0px;"><span class="load-more btn2 btn-default2" style="display:none;" id="viewproduct" data-id="2" onclick="viewMoreCategoryProducts(this);">VIEW MORE PRODUCTS</span></div> 
</section>		
</div> 
</section>
<script src="<?=$baseUrl;?>template/fcubejs/category.js"></script>
<?php
include('footer1.php');
?>
