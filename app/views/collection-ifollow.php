<?php
include('header2.php');
$user_Id=$_REQUEST['id'];
$user_name=$_REQUEST['name'];

?>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
<style>

div#save1_wrapper
{
overflow:inherit !important;
}
.followed
{
	border-color: #3b3b3b !important;
	text-decoration:none;
  background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%) !important;
  padding: 6px 15px!important;
}
div#edit_profile_wrapper
{
overflow:inherit !important;
}
.small-image img{
width:91px;
height:91px;
}
.about{
font-size:17px;
font-weight:100;
text-transform:none;
float:right;
color:#3b3b3b;
}
.about-me{
  width: 100%;
  border-radius: 3px;
  border: 1px solid #ccc;
  outline:none;

}
#name{
  width: 100%;
  border-radius: 3px;
  border: 1px solid #ccc;
  outline:none;

}
.half{
	padding: 19px 0px 0px 0px;
}
.btn-default2{
margin:33px;
}
.after{
margin-top: -28px;
  background: rgba(0, 0, 0, .6);
  z-index: 2;
  position: absolute;
  left: 198px;
  width: 148px;
  color: white;
  font-size: 12px;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
}
textarea {
    resize: none;
}
.noCollections
{
display:none;
}
.desc {
font-size: 22px;
}
.desc-text {
font-size: 17px;
line-height: 20px;
font-weight: lighter;
text-align: justify;
}
.btn-default2{

}
.one-liners{
color:#cccccc;
font-size:17px;
font-weight:lighter;
padding: 5px 0 5px 0px;
}
/*.glyphicon-tag{
transform:rotate(90deg);
}*/
.cover{
box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
padding:0px!important;
}
.tagging{
width:26%;
float:left;
padding:9px;
}
.tag-author{
width:74%;
float:left;
}
.blocks{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
padding: 0px;
margin: 15px;
width:22%;
}
.edd_download_inner{
margin:0px!important;
padding:0px!important;
}
.tag-image{
border-radius:9px;
}
.hero-line{
margin-bottom:21px;
}
.similar-products{
color:#454545!important;
}
.new-short{
background:#ccc!important;
border:1px solid #ccc!important;
text-shadow:0 1px 0 #454545!important;
color:#454545!important;
}
.fa-heart{
color:white;
}
/*@media only screen and (max-width: 949px) {
.blocks{
width:auto!important;
}
}*/
a.close1 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
background-color: transparent!important; 
} 
	
	a.close1:hover {
		background-color: transparent!important; 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	} 

.collects{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
background:white;
padding-right:0px;
}
.main-image{
padding:15px 0px 15px 0px;
}
.small-image{
padding:0px 15px 15px 0px;
}
.follow{
float: right;
font-size: 13px;
border: none!important;
background: #5AC3A2;
color: white;
text-shadow: none;
}
.total{
padding-bottom:53px;
}

#tab1:hover{
	color:#5AC3A2!important;
	}
	#tab2:hover{
	color:#5AC3A2!important;
	}
	#tab3:hover{
	color:#5AC3A2!important;
	}
	#tab4:hover{
	color:#5AC3A2!important;
	}
	#tabs-2{
	display:none;
	}
	#tabs-3{
	display:none;
	}
	#tabs-5{
	display:none;
	}
	#tabs-6{
	display:none;
	}

.total{
padding-bottom:53px;
}
.follow-btn{
margin-top: 20px;
float: right;
font-weight: 100;
}
.follow-top {
width: 100%;
float: left;
margin-bottom: 12px;
}
.box-div {
background-color: #fff;
padding: 10px 0px 10px 0px;

border-radius: 2px;
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
}
.attachment-img {
border-radius: 5px;
height: 140px;
width: 140px;
margin-left: 12px;
}
.btn-default3:hover {
border-color: #3b3b3b;
background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
}
.btn-default3 {
background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
background-repeat: repeat-x;
}
.btn3 {

outline: none;
padding: 6px 35px;
margin-bottom: 0;
font-size: 15px;
color:#fff;
font-weight: 100;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 3px;
background:#5AC3A2;
}
.attachment-img1{
border-radius:5px;
height:60px;
}
.upload-new {
  position: relative;
  overflow: hidden;
}
upload-new {
  color: #fff;
}
.upload-new {
  display: inline-block;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.upload-new input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  -ms-filter: 'alpha(opacity=0)';
  font-size: 200px;
  direction: ltr;
  cursor: pointer;
}
</style> 

<input type="hidden" id="user_profile_id" value="<?php echo $user_Id;?>">
<input type="hidden" id="user_profile_name" value="<?php echo $user_name;?>">

  <div class="row" style="">
 <div class="container" style="padding-bottom:42px;padding-top:26px">
 <div class="similar-products" style="text-transform:uppercase;"><h2>FROM COLLECTIONS <?php echo $user_name;?> FOLLOW</h2></div>
 <div class="view-all"></div>
 <div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>
  <div class="col-md-12" id="collection_prods">
  
 


  </div>
 
  </div>  
  </div>
  
  
<?php
include('footer1.php');
?>
<!--Second collection-->
<script>
var id=jQuery('#user_profile_id').val();
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/followingCollection/'+id,
		cache: false,
		success: function(data) 
		{
			console.log(data);
			var i;
			var length=data.count;
			//jQuery('#collection_count').html(length);
			if(length==0)
			{
				jQuery('#collection_prods').html("<div class='message-error'>No collections Found.</div>");	
                jQuery('.message-error').attr("style","font-size: 20px;text-align: center;margin-top: 10px;margin-bottom: 40px;");
			}
			else
			{
			
			for(i=0;i<length;i++)
			{
		
				jQuery.ajax({
				type : 'get',  
				url: 'http://codewave.co.in/fqube/user/collectionNames/'+data.dataUser[i]['user_id']+'/'+data.data[i]['collections_id'],
				cache: false,
				success: function(follCollections) 
				{
				console.log(follCollections);
				var i;
				var image11;
				var image22;
				var image33;
				var image44;
				
				for(i=0;i<follCollections.data.length;i++)
				{
				var collect ="";
				var length_collections=follCollections.data.length;
				if(length_collections==0)
				{
				 image11='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image22='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image33='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==1)
				{
				 image11=follCollections.data[0].product_image;
				 image22='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image33='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==2)
				{
				image11=	follCollections.data[0].product_image;
				 image22=follCollections.data[1].product_image;
				image33='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==3)
				{
				image11=follCollections.data[0].product_image;
				 image22=follCollections.data[1].product_image;
				 image33=follCollections.data[2].product_image;
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections>=4)
				{
				image11=follCollections.data[0].product_image;
				 image22=follCollections.data[1].product_image;
				 image33=follCollections.data[2].product_image;
				 image44=follCollections.data[3].product_image;
				}
				
					collect += '<div class="col-md-4 total">'+
  '<div class="col-md-12" style="background: #f6f6f6;padding-top:15px;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);padding-bottom: 12px;"><a href="/fqube/collection?collection_id='+follCollections.data[i].collection_id+'" style="color:#454545">'+follCollections.data[i].parent+'</a> <input id="hiddenidcollection-collection'+follCollections.data[i].collection_id+'" type="hidden" value="1"> <button type="button" onclick="followCollectionFollow(this);" class="btn3 btn-default3 follow followed" id="collection'+follCollections.data[i].collection_id+'">FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span></button></div>'+
   '<div class="col-md-12 collects">'+
 '<a href="/fqube/collection?collection_id='+follCollections.data[i].collection_id+'"><div class="main-image" style="background:url('+image11+') 5px 8px no-repeat;background-size:100% 100%;border-radius:3px;height:250px;width:298px;margin-bottom: 15px;margin-top: 8px;"></div></a>'+
  '<div class="col-md-4 small-image"><img src="'+image22+'" style="border-radius:3px"/></div>'+
  '<div class="col-md-4 small-image"><img src="'+image33+'" style="border-radius:3px"/></div>'+
  '<div class="col-md-4 small-image"><img src="'+image44+'" style="border-radius:3px"/></div>'+
  '</div> </div>'

				
				
				}
					jQuery('#collection_prods').append(collect);
					
				}
				});
			}
			}
		
		}
		});
</script>


 <script src="<?=$baseUrl;?>template/fcubejs/profile.js"></script>

 