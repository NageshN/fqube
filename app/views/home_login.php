<?php
include('header.php');
include('connect.php');
?>
<meta property="og:title" id="meta-title" content="Fqube - Fashion, Friends, Fun" /> 
<meta property="og:description" id="meta-desc" content="Fqube (Fashion, Friends, Fun) is an online community for all of the world's shopping" />
</style>
<style>
.custom_display
{
display:none !important;
}
#demo
{
display:none !important;
}
@media only screen and (max-width: 479px){
.top-head {
  margin: 110px 0px 13px 0px!important;
}
.logo {
  width: 70%!important;
  margin-top: -6px;
}
}
#forgotPass_wrapper{
overflow:inherit!important;
}
.home-input{
background-color: rgba(255,255,255,0.4);
padding: 2px 14px 2px 14px;
border-radius: 2px;
}
#invalid_email{
color: #fff!important;
  background: #D27265;
  margin-top: 28px;
  padding: 4px;
  border-radius: 3px;
  float: left;
  width: 100%;
}
.error3{
color: #fff!important;
  background: #D27265;
  margin-top: 28px;
  padding: 4px;
  border-radius: 3px;
  float: left;
  width: 100%;
}
#home-search-form {
width: 50%;
text-align: center; 
margin-left: auto;
margin-right: auto;
}
</style>
<div class="top-head"><span>DISCOVER UNIQUE PRODUCTS, SAVE THE THINGS<br>YOU LOVE AND BUY IT ALL THROUGH FQUBE!<br></span></div>
</div>
<div class="row" style="margin-right:0px;background: #f6f6f6;">
<div class="container">
<a href="#myAnchor" class="jumper"><div class="circle-icon"><img src="<?=$baseUrl;?>template/img/arrow.png" alt="" style="margin-top: 9px;margin-left: 9px;width:75%;"></div></a>
</div>
</div>
<a name="myAnchor" id="myAnchor"> 
<div class="row" style="background: #f6f6f6;">
<div class="container">
<head>
<meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link rel="stylesheet" id="edd-styles-css" href="<?=$baseUrl;?>template/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-retemplate-css" href="<?=$baseUrl;?>template/assets/edd-retemplate.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="<?=$baseUrl;?>template/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="<?=$baseUrl;?>template/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" async="" src="http://tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js"></script><script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/themes.designcrumbs.com\/stocky\/wp-admin\/admin-ajax.php","position_in_cart":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/stocky\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"http:\/\/themes.designcrumbs.com\/stocky\/checkout\/","permalinks":"1","quantities_enabled":""};
/* ]]> */
</script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fitvids.js?ver=4.1"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://themes.designcrumbs.com/stocky/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://themes.designcrumbs.com/stocky/wp-includes/wlwmanifest.xml"> 
<style type="text/css">.fancybox-margin{margin-right:17px;}</style><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></head>
<div style="clear:both"></div></a><section id="site_wrap">		
<section class="wrapper" id="content"><div class="container clearfix">
<section id="image_grid" class="image_grid_full clearfix" style="margin-top: -60px;">
<div id="stocky_downloads_list" class="edd_downloads_list edd_download_columns_0 masonry" style="position: relative; height: auto !important;">

<div style="clear:both;" class="masonry-brick"></div>
<div id="edd_download_pagination" class="navigation masonry-brick"></div>
</div>	

</section>
</div></section>		
</section>

		<div id="fb-root"></div><script type="text/javascript" src="<?=$baseUrl;?>template/assets/masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.masonry.min.js?ver=3.1.2"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/comment-reply.min.js?ver=4.1"></script>

<link rel="stylesheet" id="css-stocky-css" href="<?=$baseUrl;?>template/assets/style.css?ver=4.1" type="text/css" media="all">
  
<!--<script type="text/javascript" src="<?=$baseUrl;?>template/assets/modal.min.js?ver=1.0.6"></script>-->

	<div class="modal fade" id="edd-wl-modal" tabindex="-1" role="dialog" aria-labelledby="edd-wl-modal-label" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    		    </div>
	  </div>
	</div>
</div>
</div>
<div class="row" style="background:#fff;margin-bottom: 200px;">
 <div class="container">
 <div class="how-it-works"><h2>HOW IT WORKS</h2></div>
 <div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>
  <div class="col-md-4 how-col4"><div class="col-md-6 how-img-center"><img src="<?=$baseUrl;?>template/img/how-it-works1.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">FOLLOW</span><br><p class="how-text">Follow people, <br>collections & stores</p></div></div>
  <div class="col-md-4 how-col4"><div class="col-md-6 how-img-center"><img src="<?=$baseUrl;?>template/img/collect.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">SAVE</span><br><p class="how-text">Save all<br> your favorites</p></div></div>
  <div class="col-md-4 how-col4"><div class="col-md-6 how-img-center"><img src="<?=$baseUrl;?>template/img/buy.png" alt=""></div><div class="col-md-6 how-sec"><span class="how-first">BUY</span><br><p class="how-text">Buy it all in<br> one place</p></div></div>
  </div>
</div>
<div id="fberror" class="popup-success" style="color:#6B6969;font-weight: 100;padding:10px;width:400px;background:#f6f6f6;height:260px;border-radius:5px;display:none;overflow:inherit;"><div class="col-md-12"><div class="col-md-1"></div><div class="col-md-10" style="font-size: 23px;font-family:Verdana, sans-serif;padding-top: 41px;text-align: center;">Login Error</div><div class="col-md-1"></div></div><div class="col-md-12" style="font-size: 15px;color: grey;line-height: 22px;text-align: center;padding-top: 26px;padding-bottom:30px">Email address already exist</div><div class="col-md-12" style="text-align:center"><a class="close fberror_close"><i class="fa fa-times"></i></a></div></div>
<div id="gperror" class="popup-success" style="color:#6B6969;font-weight: 100;padding:10px;width:400px;background:#f6f6f6;height:260px;border-radius:5px;display:none;overflow:inherit;"><div class="col-md-12"><div class="col-md-1"></div><div class="col-md-10" style="font-size: 23px;font-family:Verdana, sans-serif;padding-top: 41px;text-align: center;">Login Error</div><div class="col-md-1"></div></div><div class="col-md-12" style="font-size: 15px;color: grey;line-height: 22px;text-align: center;padding-top: 26px;padding-bottom:30px">Email address already exist</div><div class="col-md-12" style="text-align:center"><a class="close gperror_close"><i class="fa fa-times"></i></a></div></div>
<input id="fberror1" class="" type="hidden"/>
<input id="gperror1" class="" type="hidden"/>
<?php
include('footer.php');
?>
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<!--dhanush's popup-->
<div class="well" id="fade1">
	<div class="login-wrapper"> 
	 	<div class="login-content"> 
			<a class="close fade1_close" onclick="closeError(this);"><i class="fa fa-times"></i></a>
			<div class="closure">
			<div class="icon" style="display:none!important;"><img src="<?=$baseUrl;?>template/img/login-icon.png"/></div><h3 style="float:left;font-weight:lighter;font-size: 28px;margin-left: 10px;" class="home_login_popup">Login</h3>
			</div>
			<form style="float:left" id="signinForm">
			
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf007;" disabled=""><input style="   font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;font-weight:lighter" type="email" id="email" name="email" placeholder="email_address"        />
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf023;" disabled=""><input style="font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;font-weight:lighter" type='password' id="password" name='password' placeholder='password'     />
		<input onclick="FQUBE.user.signin();" class="feed_open" style="margin-top: 6px;width:100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px"  type='button' value='SIGN IN'/><br>
		<div id="invalid_email" style="font-size: 12px;margin-top: 29px;display:none;text-align: center;">Invalid Email Address/Password</div>
		<div style="display:none;text-align: center;" class="error2">Mandatory fields are missing</div>
		</form>
		
		<div class="horizontal">
		<div id="loader1" style="margin-bottom: -10px;">
        <div class="cube"></div> 
        </div>
		<span class="row1">or</span></div>
		<div class="social-links"><div class="facebook1" style="float: left;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#3B4A98;padding: 8px;padding-right: 18px;padding-left: 18px;"><i style="color:white" class="fa fa-facebook"></i></div>
		<div class="fb" style="border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithFacebook">Login with facebook</a></div>
		<div class="gm" style="border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithGoogle">Login with google+</a><br></div>
		<div class="google1" style="float: right;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#AD2412;padding: 8px;padding-right: 16px;padding-left: 16px;"><i style="color:white" class="fa fa-google-plus"></i></div>
		</div>
		
		<div class="forgot forgotPass_open"><a style="color:rgb(165,165,165);text-decoration:none" href="">Forgot Password?</a></div>
		<div class="register"><a class="overlayLink1" style="color:rgb(165,165,165);text-decoration:none" id="dont" href="#">Don't have an account?</a><br></div>
		<div id="forgotPass" class="well" style="padding: 50px;"><div class="closure">
			<div class="icon" style="display:none!important;"><img src="http://codewave.co.in/fqube/template/img/login-icon.png"></div><h3 style="float:left;font-weight:lighter;font-size: 28px;margin-top: -5px;padding-bottom: 20px;" class="home_login_popup">Forgot Password</h3>
			</div><div style="width: 100%;float: left;"><form action="javascript:void(0);"><input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:10%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;  margin-top: 0px;" type="email" placeholder="&nbsp;&#xf007;" disabled=""><input style="   font-family: 'FontAwesome','roboto';width:90%;   margin-top: 0px;border-top-left-radius: 0px; border-bottom-left-radius: 0px;font-weight:lighter" type="email" id="forgot_pass" name="forgot_pass" placeholder="enter email-id"><div style="margin-left: auto;margin-right: auto; text-align: center;"><button class="btn2 btn-default2" id="forgot_click1" style="padding: 6px 140px;">Submit</button></div></form><a class="close forgotPass_close"><i class="fa fa-times"></i></a></div></div>	
		</div>
	</div>
</div>
   
<!--dhanush's popup ends-->
<div class="overlay1" style="display: none;">
	<div class="login-wrapper1" style="">
		<div class="login-content1"> 
		<div class="closure">
			<a class="close1" onclick="closeError(this);"><i class="fa fa-times"></i></a>
			</div> 
			<div class="icon" style="display:none!important;"><img src="<?=$baseUrl;?>template/img/login-icon.png"/></div><h3 style="float:left;padding-top: 21px;font-weight:lighter;font-size: 28px;margin-left: 10px;  padding-bottom: 0px;">Register</h3>
		<form style="float:left" id="signupForm" onsubmit="return checkForm(this);" action="javascript:void(0);">
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="@" disabled/><input style="  font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;" id="email_address" type="email" name="email_addr" placeholder="enter mail id"/>
		
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf007;" disabled/><input style=" font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;" id="username" type="text" name="username" placeholder="enter name"/>
		
		<input class="placeholder" style="  font-family: 'FontAwesome','roboto';width:6%;float:left; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" type="email" placeholder="&#xf023;" disabled/><input style=" font-family: 'FontAwesome','roboto';width:94%;float:left; border-top-left-radius: 0px; border-bottom-left-radius: 0px;" id="password1" type="password" name="password1" placeholder="password"/>
		
		<input id="custom_popup" style="margin-top: 6px;width:100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px" type='submit' value='GET STARTED'/>
		<div style="color:red;display:none;text-align: center;" class="error3">Mandatory fields are missing</div>
		<div id="email_exist" style="color: white;font-size: 14px;border-radius:3px;background:#D27265;margin-bottom:12px;display:none;float:left;width:100%;margin-top:17px;text-align: center;">Email-id already exist.</div>
		<div id="mand_exist" style="color: white;font-size: 14px;border-radius:3px;background:#D27265;margin-bottom:12px;display:none;float:left;width:100%;margin-top:17px;text-align: center;">Mandatory Fields Missing/Invalid Email Address</div>	
		</form>
		<div class="horizontal"><div id="loader1" style="margin-bottom: -10px;">
        <div class="cube"></div>
        </div>
		<span class="row1">or</span></div>
		<div class="social-links"><div class="facebook1" style="float: left;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#3B4A98;padding: 8px;padding-right: 18px;padding-left: 18px;"><i style="color:white" class="fa fa-facebook"></i></div>
		<div class="fb" style="border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithFacebook">Sign up with facebook</a></div>
		<div class="gm" style="border-top-right-radius:2px;border-bottom-right-radius:2px;color:white;cursor:pointer;"><a style="text-decoration:none;color:white;font-size: 14px;margin-left: -10px;" href="<?=$baseUrl?>user/loginWithGoogle">Sign up with google+</a><br></div>
		<div class="google1" style="float: right;border-top-left-radius:2px;border-bottom-left-radius:2px;background:#AD2412;padding: 8px;padding-right: 16px;padding-left: 16px;"><i style="color:white" class="fa fa-google-plus"></i></div>
		</div>
		<br><br>
		<div>
		<div class="already">Already have an account?<a href="" style="text-decoration:none;color:#5AC3A2" class="fade1_open" id="signed"> Sign in</a><br></div>
		</div>
		</div>
	</div>
</div>
<script>
function closeError(){
jQuery('.error3').hide();
jQuery('#email_exist').hide();
jQuery('#mand_exist').hide();
jQuery('.error2').hide();
jQuery('#invalid_email').hide();
}
</script>
<script>
$(".jumper").on("click",function( e ){
    e.preventDefault();
    $("body, html").animate({ 
        scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);
});
</script>
<script>
//script for fetching url parameter in javascript
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)  
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        } 
    }
}  
</script>
<script>

var fb;
var fbId = getUrlParameter('fb');
if(fbId == 400){
jQuery('#fberror1').addClass('fberror_open');
}
else {

}
var gp;
var gpId = getUrlParameter('gp');
if(gpId == 400){
alert('hi');
jQuery('#gperror1').addClass('gperror_open');
}
else {

} 
</script>
<script>
jQuery('#gperror').popup({

 transition: 'all 0.3s'
});
</script>
<script>
jQuery('#fberror').popup({

 transition: 'all 0.3s'
});
</script>
<script>
	jQuery('#submit1').click(function(){
	var email = jQuery('#email').val();
	var password=jQuery('#password').val();
	var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var url = '/fancythat/laravel/app/template/signin.php'; 
	var hasError = false;
		if((email == '') || (!pattern.test(email))){
			jQuery('.error2').show();
			jQuery('#email').addClass('error-border');
			var hasError = true;
			}
		else{
			jQuery('.error2').hide();
			jQuery('#email').removeClass('error-border');
			var hasError = false;
		}
		if(password == ''){
			jQuery('.error2').show();
			jQuery('#password').addClass('error-border');
			var hasError = true;
			}
		else{
			jQuery('.error2').hide();
			jQuery('#password').removeClass('error-border');
			var hasError = false;
		}
			if(!hasError){
			jQuery.ajax({
			type:'POST',
			url:url,
			data: {email:email,password:password},
			dataType: 'text',
			cache: false,
			success: function(data)
			
		{	
				   if(jQuery.trim(data) === "success"){
					 jQuery('.succeed').show();
					 //jQuery("#form1")[0].reset();
					 location.reload();
				}
					else{
						jQuery('.error2').show();
						jQuery('.error2').html("email-id/password incorrect");
					}
						
					}
			});
		}
		else
		{
			jQuery('.error2').show();
			
		}
	});
</script>
<script>
jQuery('#submit12').click(function(){
	var email = jQuery('#email_address').val();
	var name = jQuery('#username').val();
	var password1=jQuery('#password1').val();
	//var password2=jQuery('#password2').val();
	var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	//var hasError = false;
		if((email == '') || (!pattern.test(email))){
		jQuery('#email_address').addClass('error-border');
			jQuery('.error3').show();
			//var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#email_address').removeClass('error-border');
			//var hasError = false;
		}
		if(name == ''){
			jQuery('.error3').show();
			jQuery('#username').addClass('error-border');
			//var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#username').removeClass('error-border');
			//var hasError = false;
		}
		if(password1 == ''){
			jQuery('.error3').show();
			jQuery('#password1').addClass('error-border');
			//var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#password1').removeClass('error-border');
			//var hasError = false;
		}
		/*if(password2 == '' || password2 != password1){
			jQuery('.error3').show();
			jQuery('#password2').addClass('error-border');
			var hasError = true;
			}
		else{
			jQuery('.error3').hide();
			jQuery('#password2').removeClass('error-border');
			var hasError = false;
		}*/
			//if(!hasError){
			var url= '/fancythat/laravel/register/'+email+'/'+name+'/'+password1;
			jQuery.ajax({
			type:'GET',
			url:url,
			dataType : 'text',
			success: function (data) 
			
		{	
				   //if(jQuery.trim(data) === "success"){
				   if(data){
					 jQuery('.succeed').show();
					 //jQuery("#form1")[0].reset();
					// header('Location: http://codewave.co.in/fancythat/laravel/select');
					window.location = "http://codewave.co.in/fancythat/laravel/select-new";
				}
					else{
						jQuery('.error3').show();
			jQuery('.error3').html("You are already registered");
					}
						
					}
			});
		/*}
		else
		{
			jQuery('.error3').show();
			
			
		}*/
	});
</script>

	<script>
	$(document).ready(function() {
	$("#log").click(function( event ){
	$('#fade1').show();
	$('#fade1_background').show();
	$('#fade1_wrapper').show();
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
	
		$.get( "ajax/" + action, function( data ) {
		alert(data);
			$( ".login-content" ).html( data );
		});	
		
		$(".overlay").fadeIn("slow");
	});
	
	$(".close").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay").fadeOut('slow');
		$(".error2").fadeOut('slow');
		document.getElementById("signinForm").reset();
		jQuery('#password').removeClass('error-border');
		jQuery('#email').removeClass('error-border');
	});
	/*(".overlay").click(function(){
		$(".login-wrapper").fadeToggle("fast");
		$(".overlay").hide();
		$(".error2").hide();
		//(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password').removeClass('error-border');
			})*/
	
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay").fadeToggle("fast");
		}
	});
	});
	</script>
	
	<script>
	$(document).ready(function() {
	$("#dont").click(function( event ){
	$('#fade1').hide();
	$('#fade1_background').hide();
	$('#fade1_wrapper').hide();
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink1").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content1" ).html( data );
		});	
		
		$(".overlay1").fadeIn("slow");
	});
	
	$(".close1").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").fadeOut('slow');
		$(".error3").fadeOut('slow');
		document.getElementById("signupForm").reset();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});

	/*$("body").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
		$(".error3").hide();
		//$(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});*/
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay1").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay1").fadeToggle("fast");
		}
	});
	
	});
	</script>
<script>
	$(document).ready(function() {
	$("#signed").click(function( event ){
	$('#fade1_background').show();
	$('#fade1_wrapper').show();
	$('#fade1').show();
	$('.overlay1').hide();
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	});
	</script>

	<script>
	$(document).ready(function() {
	$("#sig").click(function( event ){
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink1").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content1" ).html( data );
		});	
		
		$(".overlay1").fadeIn("slow");
	});
	
	$(".close1").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").fadeOut('slow');
		$(".error3").fadeOut('slow');
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});

	/*$("body").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
		$(".error3").hide();
		//$(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});*/
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay1").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay1").fadeToggle("fast");
		}
	});
	
	});
	</script>
	
		
	<script>
	$(document).ready(function() {
	$(".opens").click(function( event ){
	
		event.preventDefault();
    	//$(".overlay").fadeToggle("fast");
		
  	});
	
	$(".overlayLink2").click(function(event){
		event.preventDefault();
		var action = $(this).attr('data-action');
		
		$.get( "ajax/" + action, function( data ) {
			$( ".login-content2" ).html( data );
		});	
		
		$(".overlay2").fadeToggle("fast");
	});
	
	$(".close2").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay2").hide();
		$(".error3").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
		jQuery('#collection').removeClass('error-border');
		jQuery('.error5').hide();
	});

	/*$("body").click(function(){
		//$(".overlay").fadeToggle("fast");
		$(".overlay1").hide();
		$(".error3").hide();
		//$(".login-wrapper1").hide();
		jQuery('#email_address').removeClass('error-border');
		jQuery('#username').removeClass('error-border');
		jQuery('#password1').removeClass('error-border');
		jQuery('#password2').removeClass('error-border');
	});*/
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay2").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay2").fadeToggle("fast");
		}
	});
	
	});
	</script>
	<script>
jQuery('#add').click(function(){
	var collection = jQuery('#collection').val();
	var hasError= false;
	if(collection == ''){
		jQuery('#collection').addClass('error-border');
			jQuery('.error5').show();
			var hasError = true;
			}
		else{
			jQuery('.error5').hide();
			jQuery('#collection').removeClass('error-border');
			var hasError = false;
		}
			var url= 'app/views/add.php';
			if(!hasError){
			jQuery.ajax({
			type:'POST',
			url:url,
			data: {collection:collection},
			dataType: 'text',
			cache: false,
			success: function(data)
			
			
		{	
				   if(jQuery.trim(data) === "success"){
				  
					 alert('added');
				}
					else{
						alert('error');
					}
						
					}
			});
			}
		
	});
</script>
<script>
jQuery('#fade1').popup({
      transition: 'all 0.3s',
      scrolllock: false,
	  autoopen: true
});	
</script>
<script>
jQuery('#log').click(function()
{
jQuery('#fade1').removeClass('custom_display');
jQuery('#fade1_background').removeClass('custom_display');
});

jQuery('.forgotPass_open').click(function() 
{
jQuery('#fade1').addClass('custom_display');
jQuery('#fade1_background').addClass('custom_display');
});
 jQuery('#forgotPass').popup({

	
      transition: 'all 0.3s',
      scrolllock: false
   
});	
</script>

<script src="<?=$baseUrl;?>template/fcubejs/products.js"></script>
<script src="<?=$baseUrl;?>template/fcubejs/search.js"></script>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
    <link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>