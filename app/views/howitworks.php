<?php
include('header1.php');
include('connect.php');
?>
<style>
.login-btn{
display:none;
}
.header-new{
    background-image: url(http://codewave.co.in/fqube/template/img/how-it-works.jpg);
  background-size: 100% 570px;
  background-repeat: no-repeat;
  height: 570px;
  background-color: #f6f6f6;
  }
.sub-points li {
  list-style-type: disc!important;
  font-size:24px;
  color:#5AC3A2;
  padding:13px;
}
.sub-points li span{
font-size:18px;
color:#454545;
}
.btn-default2:hover{
background-image:linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
border:1px solid #3b3b3b;
text-shadow:none!important;
}
.fa-check{
color:#5AC3A2;
}
.main-top{
  text-align: center;
  padding-top: 15px;
}
.read-more-div {
  text-align: center;
  left: 50%!important;
  top: 85%!important;
  position: absolute!important;
}
</style>
<div class="row heads-up" style="">
<div class="top-head" style="font-size:39px;margin-top:52px;color:#3b3b3b"><span>HOW IT WORKS<br></span></div>
<div class="head-desc">Discover, save and share products curated by others.</div>
<div class="col-md-12" style="">
<!--<div class="col-md-12">
<div class="col-md-2"></div>
<div class="head-points col-md-8"><div class="col-md-1"><i class="fa fa-check-square-o"></i></div><div class="col-md-11 head-points-desc">When a product is added to Fqube by a user, a 'buy' button on the item page links to your online store where he product can be purchased</div></div>
<div class="col-md-2"></div>
</div>
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="head-points col-md-8"><div class="col-md-1"><i class="fa fa-check-square-o"></i></div><div class="col-md-11 head-points-desc">Stores are automatically created when a user saves a product from your website. We then aggregate all products found by the Fqube community to your store page. Users can follow your store page to see all the latest items saved by users on their live feed</div></div>
<div class="col-md-2"></div>
</div>-->
</div>
<div class="container read-more-div" style="float:left;margin-left: -20px;">
<a href="#myAnchor" id="anchor1"><div class="mouse"><div class="wheel"></div></div><span class="unu"></span><span class="doi"></span><span class="trei"></span></a>
</div>
 </div>
</div>
<link rel="stylesheet" id="edd-styles-css" href="<?=$baseUrl;?>template/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-retemplate-css" href="<?=$baseUrl;?>template/assets/edd-retemplate.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="<?=$baseUrl;?>template/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="<?=$baseUrl;?>template/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" async="" src="http://tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js"></script><script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<link rel="stylesheet" id="css-stocky-css" href="<?=$baseUrl;?>template/assets/style.css?ver=4.1" type="text/css" media="all">
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fitvids.js?ver=4.1"></script>

<div class="row" id="myAnchor" style="background: #f6f6f6;">
<div class="container"  style="padding-top:60px;padding-bottom:60px"> 
<div style="clear:both"></div></a>
<section id="site_wrap">			
<section class="wrapper">
			<div class="col-md-12">
			<div class="container clearfix">
				<div class="col-md-4" style=""><img src="<?=$baseUrl;?>template/img/howitworks-2.png"></div>
				<div class="col-md-8">
				<div class="main-top">Create your own collections</div>
				<div class="close-division">
				<ul class="sub-points">
				<li><span>Save and share products you like into your own categorical collections such as 'Interiors for my dream bedroom'</span></li>
				</ul>
				</div>
				</div>
				</div>
</div></section>		
</section>
</div>
</div>
<div class="row" id="myAnchor" style="background: #fff;">
<div class="container"  style="padding-top:60px;padding-bottom:60px"> 
<div style="clear:both"></div></a>
<section id="site_wrap">			
<section class="wrapper">
			<div class="col-md-12">
			<div class="container clearfix">
				<div class="col-md-8">
				<div class="main-top">Follow stores, friends and tastemakers</div>
				<div class="close-division">
				<ul class="sub-points">
				<li><span>Start your own personalized shopping experience by following your favorite stores and people that inspire the fashionista in you. Your feed will be filled with products saved by people and stores you follow.</span></li>
				</ul>
				</div>
				</div>
				<div class="col-md-4" style=""><img src="<?=$baseUrl;?>template/img/howitworks-1.png"></div>
				</div>
</div></section>		
</section>
</div>
</div>
<div class="row" style="background:#f6f6f6;">
 <div class="container" style="padding-top:40px;padding-bottom:40px">

		<div class="login-content1" style="height:200px;  float: left;width: 100%;">
		<div style="padding: 10px 0 25px 0;font-size: 24px;color: #454545;font-weight: 100;text-align:center;">+FQUBE</div>
		  <p style="text-align:center"> <a href="javascript:void((function(url){if(!window.fqubeBookmarklet){var productURL=encodeURIComponent(url),cacheBuster=Math.floor(Math.random()*1e3),element=document.createElement('script');element.setAttribute('src','//codewave.co.in/fqube/app/views/bookmarklet/bookNow.js?*='+cacheBuster+'&v=2.4&url='+productURL),element.onload=init,element.setAttribute('type','text/javascript'),document.getElementsByTagName('head')[0].appendChild(element)}else init();function init(){window.fqubeBookmarklet()}})(window.location.href))" style="width: 50%;height:37px;background-color:#5AC3A2;padding: 10px 40px 10px 40px;cursor:pointer;color:white;border:none;border-radius:4px">Post on fqube</a></p>
	      <p style="text-align:center;margin-top: -10px;margin-bottom: 0px;"><i class="fa fa-arrow-up fa-6"></i></p> 		
	      <p style="text-align:center">Drag this button to your bookmarks bar to easily save products from online stores.</p> 		
	      <p style="text-align:center">When you see something you like whilst browsing, save it to FQUBE using the bookmarklet feature and it'll go straight into your personal collection, for you to view or buy later.</p> 		
		</div> 
  </div>
</div>

<?php
include('footer.php');
?>
<script>
$("#anchor1").on("click",function( e ){
    e.preventDefault();
    $("body, html").animate({ 
        scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);
});
</script>