<?php
	$baseUrl = 'http://codewave.co.in/fqube/';
	$userId = Session::get('userInfo');
	$url = $_SERVER['REQUEST_URI'];
	if($userId != ''){
	header("Location: http://codewave.co.in/fqube/myfeed");
	die();
	}
	else {
	
	}
	
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fqube - Fashion, Friends, Fun</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?=$baseUrl;?>template/img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/fonts/glyphicons-halflings-regular.woff">
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/fonts/glyphicons-halflings-regular.woff2">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/css/responsive.css"> 
	<style>
		body{
         font-family: 'Roboto', sans-serif!important;
         }
		.header-new {
		background-image:url(<?=$baseUrl;?>template/img/fqube-bg.jpg);
		background-size: 100% 380px;
		background-repeat: no-repeat;
		height: 380px;
		margin-top:100px;
		background-color:#f6f6f6;
		}
		.head-points{
		color:white;
		font-size:20px;
		padding:19px 0px 19px 0px;
		}
		.head-points-desc{
		padding:0px!important;
		text-align:justify;
		color:#3b3b3b;
		font-size:18px;
		}
		.tag-image {
         width: 60px;
         height: 43px;
        }
		.fa-check-square-o{
		color:#5AC3A2;
		}
		span.unu {
    display: block;
  width: 5px;
  height: 5px;
  -ms-transform: rotate(45deg); /* IE 9 */
  -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
  transform: rotate(45deg);
  border-right: 2px solid #5AC3A2;
  border-bottom: 2px solid #5AC3A2;
  margin: 0 0 2px 6px;
}
 .btn2 {
      display: inline-block;
      outline: none;
      padding: 6px 100px;
      margin-bottom: 0;
	  color:#fff;
      font-size: 16px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
     }
	 .btn-default2:hover{
		border-color: #3b3b3b;
		color:white;
		background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
		}
		.display-box{
		  display:none;
		}
	 .btn-default2 {
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border:none;
       }
.unu
{
  -webkit-animation-delay: .1s;
  -moz-animation-delay: .1s;
  -webkit-animation-direction: alternate;
  margin-top:4px!important;
}
.unu, .doi, .trei {
  -webkit-animation: mouse-scroll 1s infinite;
  -moz-animation: mouse-scroll 1s infinite;
}
span.doi {
    display: block;
  width: 5px;
  height: 5px;
  -ms-transform: rotate(45deg); /* IE 9 */
  -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
  transform: rotate(45deg);
  border-right: 2px solid #5AC3A2;
  border-bottom: 2px solid #5AC3A2;
  margin: 0 0 2px 6px;
}
.doi {
  -webkit-animation-delay: .2s;
  -moz-animation-delay: .2s;
  -webkit-animation-direction: alternate;
}
span.trei {
    display: block;
  width: 5px;
  height: 5px;
  -ms-transform: rotate(45deg); /* IE 9 */
  -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
  transform: rotate(45deg);
  border-right: 2px solid #5AC3A2;
  border-bottom: 2px solid #5AC3A2;
  margin: 0 0 2px 6px;
}
.trei {
  -webkit-animation-delay: .3s;
  -moz-animation-delay: .3s;
  -webkit-animation-direction: alternate;
}

.mouse {
  height: 23px;
  width: 17px;
  border-radius: 10px;
  transform: none;
  border: 2px solid #5AC3A2;
  top: 170px;
}
#socnets a:hover {
  background-color: #5AC3A2;
  border-radius: 50%;
  width: 50px;
  height: 50px;
}
.wheel {
  height: 5px;
  width: 2px;
  display: block;
  margin: 5px auto;
  background: #5AC3A2;
  position: relative;
}
.wheel {
  -webkit-animation: mouse-wheel 1.2s ease infinite;
  -moz-animation: mouse-wheel 1.2s ease infinite;
}
@-webkit-keyframes mouse-wheel
{
   0% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }

  100% {
    opacity: 0;
    -webkit-transform: translateY(6px);
    -ms-transform: translateY(6px);
    transform: translateY(6px);
  }
}

@-moz-keyframes mouse-wheel
{
  0% { top: 1px; }
  50% { top: 2px; }
  100% { top: 3px;}
}

@-webkit-keyframes mouse-scroll {

  0%   { opacity: 0;}
  50%  { opacity: .5;}
  100% { opacity: 1;}
}
@-moz-keyframes mouse-scroll {

  0%   { opacity: 0; }
  50%  { opacity: .5; }
  100% { opacity: 1; }
}
@-o-keyframes mouse-scroll {

  0%   { opacity: 0; }
  50%  { opacity: .5; }
  100% { opacity: 1; }
}
@keyframes mouse-scroll {

  0%   { opacity: 0; }
  50%  { opacity: .5; }
  100% { opacity: 1; }
}
.read-more-div {
  margin-top: 35px!important;
  text-align: center;
  left: 50%;
  position: relative;
  padding-bottom: 20px;
}
.read-more-div {
  width: 100px;
  
}
		.main-top{
		font-size: 24px;
		line-height: 33px;
		padding-bottom: 38px;
		}
		.head-desc{
		color:#3b3b3b;
		font-size:24px;
		text-align:center;
		padding:19px;
		}
	    .login-btn {
        padding: 30px 0px 0px 0px;
		}
		.logo-img {
		float: left;
		padding: 18px 0px 0px 0px;
		}
		.close-division{
		padding-left: 30px;
		}
		.login {
		 padding: 5px 10px 5px 10px;
         float: left;
		 //border: 1px solid #5AC3A2;
         margin-right: 15px;
		 font-weight: 200;
         font-size: 16px;
         color: #5AC3A2;
		 border-radius: 3px;
         list-style: none; 
         cursor:pointer;		 
		}
/*		.login:hover {
		 background:#5AC3A2;
		 color:#fff;
         border-radius: 3px;
		 padding: 5px 10px 5px 10px;
		}*/
/*		.sign-up:hover {
		 background-color:transparent;
		 //border: 1px solid #5AC3A2;
		 color:#5AC3A2;
         border-radius: 3px;
		 padding: 5px 10px 5px 10px;
		}*/
		.sign-up {
		 //border: 1px solid #5AC3A2;
		 padding: 5px 10px 5px 10px;
         font-size: 15px;
         color: #fff;
		 background-color:#5AC3A2;
         float: left;
		 font-weight: 200;
		 border-radius: 3px;
         list-style: none;
		 cursor:pointer;
		 
		}
		.top-head{
		 text-align: center;
		color: white;
		font-size: 25px;
		font-weight: lighter;
		line-height: 40px;
		margin: 120px 0px 20px 0px;

		}
		.search-btn{
		border-radius:3px!important;
		}
		#home-search{
		border-top-left-radius:3px!important;
		border-bottom-left-radius:3px!important;
		background:rgba(255, 255, 255, 0.9)!important;
		}
		.top-search {
		 position: absolute;
         left: 50%;
         top: 50%;
         margin-left: -335px;
         text-align: center;
         color: #fff;
         width: 47%;
		}
		.search-box{
		 position: absolute;
         top: 30%;
		}
		.search-btn{
		 background-color:#5AC3A2;
		 padding-bottom: 7px;
		}
		.btn-default1 {
        text-shadow: 0 1px 0 #5AC3A2;
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #dbdbdb;
        border-color: #5AC3A2;
       }
	   .hero-line {
       position: relative;
       display: block;
       width: 80px;
       height: 1px;
       margin-right: auto;
       margin-left: auto;
       background-color: #a9a9a9;
      }
	  .how-col4 {
       padding-top: 60px; 
       padding-bottom: 60px;
	  }
	  .how-img-center {
	    text-align:center;
		cursor: default;
	  }
	  .how-first {
	   font-size: 25px;
	   font-weight: 200;
       color: #5AC3A2;
	   cursor: default;
	  }
	  .how-text {
	   font-size: 13px;
       line-height: 17px;
       color: #848484;
       text-align: left;
	  }
	  .how-sec {
	   margin-top: 35px;
	  }
	  .copyright {
	   text-align:center;
	   color:#5AC3A2;
	   font-size:18px;
	   margin-bottom: 40px;
	  }
	  .load-more {
	  text-align:center;
	  margin-top: 25px;
	  margin-bottom: 40px;
	  }
	  .btn2 {
      display: inline-block;
      outline: none;
      padding: 6px 100px;
      margin-bottom: 0;
      font-size: 16px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 3px;
     }
	 .btn-default2 {
        
        background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
        background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
        background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        background-repeat: repeat-x;
        border-color: #dbdbdb;
        border-color: #5AC3A2;
       }
	   .glyphicon-search{
	    color:#fff;
	   }
	   
	   .circle-icon {
	   background-color: #f6f6f6;
       width: 70px;
       height: 70px;
       border-radius: 50%;
       position: absolute;
       left: 50%;
       margin-top: -32px;
       margin-left: -35px;
	   }
	   .describe{
	    color: white;
		font-size: 18px;
		font-weight: lighter;
		margin-top: 16px;
	   }
	   .how-it-works {
	   text-align:center;
	   color:#5AC3A2;
	   font-family: 'Raleway', sans-serif;
	   margin-top: 50px;
	   }
	   .hover-fancy {
	    float: left;
		margin-top: 0px;
		width: 22%;
		color:white!important;
	   }
	   .hover-short {
	    float: left;
		margin-top: 0px;
		width: 11%;
	   } 
	   #ascrail2000 {
	    display:none;
	   }
	   #ascrail2000-hr {
	    display:none;
	   }
	    nav{
         position: fixed;
         width: 100%; 
         font-size: 72px;
         height: 108px;
         background: transparent!important;
		 border:none!important;
         color: #fff;
         font-family: 'Roboto', sans-serif;
         // set animation
         -webkit-transition: all 0.4s ease;
         transition: all 0.4s ease;
        }
        
        nav.sticky {
		 width:100%;
		 background: #fff!important;
         font-size: 24px;
         height: 72px; 
         background: #fff;
	     z-index: 1000000;
        }
		header{
         position: fixed;
         width: 100%; 
         font-size: 72px;
         height: 100px; 
		 margin-top:-100px;
         background: rgb(255,255,255)!important;
		 border:none!important;
         color: #fff;
         font-family: 'Roboto', sans-serif;
         // set animation
         -webkit-transition: all 0.4s ease;
         transition: all 0.4s ease;
        }
        
        header.sticky {
		 width:100%;
		 background: #fff!important;
         font-size: 24px;
         height: 72px; 
	     z-index: 100;
        }
		a.close {
		
display: block!important;
font-family: 'Varela Round', sans-serif!important;
font-size: 17px!important;
padding: 7px 10px 8px 10px!important;
position: absolute!important;
top: 1.25rem!important;
transition: all 400ms ease!important;
right: 1.25rem!important;
border: 1px solid !important;
border-radius: 41px!important;
}
	
	a.close:hover {
		/*background-color: #1bc5b3;*/
		cursor: pointer;
		color:#5AC3A2!important;
	}

/*
*	LOG-IN BOX
*/
div.overlay {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay > div.login-wrapper {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 5px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay > div.login-wrapper > div.login-content {
			background-color: #F6F6F6;
			border-radius: 5px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay > div.login-wrapper > div.login-content > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		
/*
*	FORM
*/
form label {
	color: rgb(0,0,0);
	display: block;
	font-family: 'Varela Round', sans-serif;
	font-size: 1.25em;
	margin: .75em 0;	
}

	form input[type="text"],
	form input[type="email"],
	form input[type="number"],
	form input[type="search"],
	form input[type="password"],
	form textarea {
		background-color: rgb(255,255,255);
		border: 1px solid rgb( 186, 186, 186 );
		border-radius: 1px;
		box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.08);
		display: block;
		font-size: .65em;
		margin: 6px 0 12px 0;
		padding: .8em .55em;	
		text-shadow: 0 1px 1px rgba(255, 255, 255, 1);
		transition: all 400ms ease;
		width: 90%;
	}
	
	form input[type="text"]:focus,
	form input[type="email"]:focus,
	form input[type="number"]:focus,
	form input[type="search"]:focus,
	form input[type="password"]:focus,
	form textarea:focus,
	form select:focus { 
		border-color: #4195fc;
		box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1), 0 0 8px #4195fc;
	}
	
		form input[type="text"]:invalid:focus,
		form input[type="email"]:invalid:focus,
		form input[type="number"]:invalid:focus,
		form input[type="search"]:invalid:focus,
		form input[type="password"]:invalid:focus,
		form textarea:invalid:focus,
		form select:invalid:focus { 
			border-color: rgb(248,66,66);
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1), 0 0 8px rgb(248,66,66);
		}
	
form button {
	background-color: #50c1e9;
	border: 1px solid rgba(0,0,0,.1);
	color: rgb(255,255,255);
	font-family: 'Varela Round', sans-serif;
	font-size: .85em;
	padding: .55em .9em;
	transition: all 400ms ease;	
}

	form button:hover {
		background-color: #1bc5b3;
		cursor: pointer;
	}
	#email:focus{
	//border:1px solid red!important;
	box-shadow:none!important;
	border-color:none!important;
	
	}
	form input[type="text"], form input[type="email"], form input[type="number"], form input[type="search"], form input[type="password"], form textarea{
	background: #DFDFDF;
	box-shadow:none;
	border:none;
	height:37px;
	font-size:14px;
	width:100%;
	text-shadow:none;
	border-radius:3px;
	margin-bottom: 20px;
	z-index: 0;
	}
	.fb{
	float: left;
padding: 8px;
background: #3b5998;
width: 37%;
text-align: center;
font-size: 12px;
font-weight:lighter;
	}
	.gm{
	font-weight:lighter;
	float:right;
	padding: 8px;
	background:#D34836;
	width:37%;
	text-align: center;
	font-size: 12px;
	}
	.already{
	padding-top: 20px;
	text-align: center;
	float: left;
	padding-bottom: 20px;
	width: 100%;
	font-size:14px;
	color:rgb(165,165,165);
	}
	.forgot{
	float :left;
	padding: 7px 13px 7px 0px;
	font-weight:lighter;
	font-size:14px;
	}
.register{
   float: right;
  padding: 6px 6px 5px 19px;
  font-weight: lighter;
  font-size: 14px;
  width: 82px;
 }
 .register1{
   float: right;
  padding: 6px 6px 5px 19px;
  font-weight: lighter;
  font-size: 14px;
 }
input { font-family: 'FontAwesome'; } 
.row1:before,
.row1:after {
    background-color: rgb(165,165,165);
    content: '';
    display: inline-block;
    height: 1px;
    position: relative;
    vertical-align: middle;
    width: 46.3%;
}
.row1:before {
    right: 0.5em;
    margin-left: 2%;
}
.row1:after {
    left: 0.5em;
    margin-right: -50%;
}
.icon{
float: left;
width: 93px;
padding-bottom: 20px;
background:none!important;
}
 div.overlay1 {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay1 > div.login-wrapper1 {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 3px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay1 > div.login-wrapper1 > div.login-content1 {
			background-color: #F6F6F6;
			border-radius: 3px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay1 > div.login-wrapper1 > div.login-content1 > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		

a.close1 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
} 
	
	a.close1:hover {
		/*background-color: #1bc5b3;*/ 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	} 
	/*.error-border{
	border:1px solid red!important;
	}*/
	.row{
	 margin-left:0px;
	 margin-right:0px;
	}
	
div.overlay2 {
	background-color: rgba(0,0,0,.75);
	bottom: 0;
	display: flex;
	justify-content: center;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

	div.overlay2 > div.login-wrapper2 {
		align-self: center;
		background-color: #F6F6F6;
		border-radius: 5px;
		padding: 6px;
		width: 514px;
	}
	
		div.overlay2 > div.login-wrapper2 > div.login-content2 {
			background-color: #F6F6F6;
			border-radius: 5px;
			padding: 15px;	
			position: relative;
		}
		
			div.overlay2 > div.login-wrapper2 > div.login-content2 > h3 {
				color: #3b3b3b;
				font-family: 'Varela Round', sans-serif;
				font-size: 1.8em;
				margin: 0 0 1.25em;
				padding: 0;
			}
		

a.close2 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
} 
	
	a.close2:hover {
		/*background-color: #1bc5b3;*/ 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	}
.homepage-caption{
font-size: 18px;
margin-left: 12px;
margin-top: 5px;
font-family: 'Roboto';
}
	
.form-control:focus{
border:none!important;
}
.header-new {
margin-left: auto;
margin-right: auto;
text-align: center;
}
#demo {
        margin: 32px 0px 6px 20px;
        float:right;
        width:50px;
    }
    
    #demo .wrapper {
        display: inline-block;
        /*width: 180px;
        margin: 0 10px 0 0;
        height: 20px;*/
        position: relative;
       // margin:20px;
    }
    
    #demo .parent {
        height: 100%;
        width: 100%;
        display: block;
        cursor: pointer;
        line-height: 0px;
        height: 0px;
        border-radius: 5px;
       /* background: #F9F9F9;
        border: 1px solid #AAA;
        border-bottom: 1px solid #777;
        color: #282D31;*/
        font-weight: bold;
        z-index: 2;
        position: relative;
        -webkit-transition: border-radius .1s linear, background .1s linear, z-index 0s linear;
        -webkit-transition-delay: .8s;
        text-align: center;
    }
    
    #demo .parent:hover,
    #demo .content:hover ~ .parent {
        background: #fff;
        -webkit-transition-delay: 0s, 0s, 0s;
    }
    
    #demo .content:hover ~ .parent {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        z-index: 0;
    }
    
    #demo .content {
        position: absolute;
        top: 0;
        display: block;
        z-index: 1;
        height: 0;
        width: 180px;
        padding-top: 30px;
        /*-webkit-transition: height .5s ease;
        -webkit-transition-delay: .4s;
        border: 1px solid #777;
        border-radius: 5px;
        box-shadow: 0 1px 2px rgba(0,0,0,.4);*/
    }
    
    #demo .wrapper:active .content {
        height: 123px;
        z-index: 3;
        -webkit-transition-delay: 0s;
    }
    
    #demo .content:hover {
        height: 123px;
        z-index: 3;
        -webkit-transition-delay: 0s;
    }
    
    
    #demo .content ul {
        background: #fff;
        margin: 0;
        padding: 0;
        overflow: hidden;
        height: 100%;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    
    #demo .content ul a {
        text-decoration: none;
    }
    
    #demo .content li:hover {
        background: #eee;
        color: #333;
    }
    
    #demo .content li {
        list-style: none;
        text-align: left;
        color: #888;
        font-size: 14px;
        line-height: 30px;
        height: 30px;
        padding-left: 10px;
        border-top: 1px solid #ccc;
    }
    
    #demo .content li:last-of-type {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .welcome{
    	float:left;
    	padding-top:14px;
    	font-weight:lighter;
		font-size:16px;
    }
  .user
{
  float: left;
  padding: 0px;
  margin-left: 5px;
  color: #5AC3A2;
  font-weight: lighter;
  font-size: 16px;
  line-height:35px;
  width: 50%;
  text-align: left;
}
	

.container{
	font-weight:lighter;
	font-size:14px;
}
.icon1 {
display: block !important;
width: 35px;
height: 32px;
margin-top: -6px;
float: left;
line-height: 98px !important;
margin-right: auto;
margin-left: auto;
//border-radius: 100px;
background-color: #f6f6f6;
color: #444;
font-size: 50px;
text-align: center;
<!---webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;-->
}
.icon {
display: block !important;
width: 42px;
height: 42px;
line-height: 65px !important;
margin-right: auto;
margin-left: auto;
border-radius: 100px;
background-color: #f6f6f6;
color: #444; 
font-size: 50px;
text-align: center;
<!---webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;-->
}
.icon2 {
display: block !important;
width: 35px;
float:left;
height: 35px;
margin-top: -6px;
line-height: 98px !important;
margin-right: auto;
margin-left: auto;
border-radius: 100px;
background-color: #f6f6f6;
color: #444; 
font-size: 50px;
text-align: center;
-webkit-transition: all 500ms ease;
-o-transition: all 500ms ease;
transition: all 500ms ease;
}
collect1:hover .collect{
<!--background: url(<?=$baseUrl;?>template/img/sprite-collect.png) 0px 35px!important;-->
}

.liked
{
background: url(<?=$baseUrl;?>template/img/sprite-like.png) 0px 32px!important;
}
#save1_wrapper {
overflow:inherit!important;
}
.liveurl {
margin-top: 10px;
border-top: 1px solid #CCC;
}
.small-menu{
float: left;
text-transform:none;
width: 50%;
left: 28%;
padding: 14px 20px 14px 20px;
margin-top: 83px;
font-family: Montserrat,sans-serif;
background: #f6f6f6;
display:none;
position:fixed;
z-index:110;
-webkit-box-shadow: 0px 4px 10px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 4px 10px 0px rgba(0,0,0,0.1);
box-shadow: 0px 4px 10px 0px rgba(0, 0, 0, 0.1);
}
.smallest{
	padding: 8px;
	text-transform: capitalize;
}

.smaller{
	float:left;
	padding:5px;
	padding-right:80px;
}
.small-menu:before{
width: 0;
height: 0;
border-style: solid;
border-width: 10px 10px 0;
border-color: #fff transparent transparent;
content: ' ';
position: absolute;
top: 0px;
left: 42%;
z-index: 10;
}
.profileEdit {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 10px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
.profileEdit a:hover {
color: #5AC3A2;
background-color: transparent;
}
.accountSet {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 5px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
.accountSet a:hover {
color: #5AC3A2;
background-color: transparent;
}
.signOut {
position: relative;
color: #454545;
background-color: transparent;
text-align: left;
margin-left: 15px;
margin-top: 5px;
margin-bottom: 5px;
font-size: 16px;
padding: 8px 0px 8px 0px;
cursor:pointer;
}
.menu-about:hover{
color: #5AC3A2!important;
background-color: transparent;
}
.menu-how:hover{
color: #5AC3A2!important;
background-color: transparent;
}
.menu-store:hover{
color: #5AC3A2!important;
background-color: transparent;
}
.signOut a:hover {
color: #5AC3A2!important;
background-color: transparent;
}
.gb_pa, .gb_qa {
  left: auto;
  right: 6.5px;
}
.gb_qa {
  border-color: transparent;
  border-style: dashed dashed solid;
  border-width: 0 8.5px 8.5px;
  display: none;
  position: absolute;
  right: 15px;
  z-index: 1;
  height: 0;
  width: 0;
  -webkit-animation: gb__a .2s;
  animation: gb__a .2s;
  border-bottom-color: #5AC3A2;
  border-bottom-color: rgba(0,0,0,.2);
  top: -9px;
}
.gb_Ca .gb_pa {
  border-bottom-color: #fef9db;
}
.gb_pa {
  border-color: transparent;
  border-bottom-color: #fff;
  border-style: dashed dashed solid;
  border-width: 0 8.5px 8.5px;
  display: none;
  position: absolute;
  right: 15px;
  top: -9px;
  z-index: 1;
  height: 0;
  width: 0;
  -webkit-animation: gb__a .2s;
  animation: gb__a .2s;
}
.logout_btn
{
  float: right;  
  border: 1px solid #fff; 
  width:150px;
 // margin: 72px -75px 0px -75px;
  position: absolute;
   left: 81%;
  top: 100%;
  background: white;
  display: none;
  box-shadow:0 2px 10px rgba(0,0,0,.3);
  display:none;
}
.subMenu {
    display: none;
}
body > .site-loader.pe-disabled {
opacity: 0;
}
body > .site-loader {
position: fixed;
background: white;
top: 0px;
left: 0px;
bottom: 0px;
right: 0px;
z-index: 9999;
opacity: 1;
-webkit-transition: opacity 0.3s; 
transition: opacity 0.3s;
}
body > .site-loader.pe-disabled {
opacity: 0;
}
.pageloader-new {
  background: none;
  z-index: 100;
  position: absolute;
  top: 42%; 
  left: 48%;
  }
 .cube1 {
   background-color: rgba(0, 0, 0, 0);
  border: 5px solid rgba(255, 255, 255, 0.9);
  width: 60px;
  height: 60px;
   margin: 0 auto; 
  -webkit-animation: cubeanim 2s infinite ease-in-out;
  -moz-animation: cubeanim 2s infinite ease-in-out;
  -o-animation: cubeanim 2s infinite ease-in-out;
  animation: cubeanim 2s infinite ease-in-out;
  }
	</style> 
	<script src="<?=$baseUrl;?>template/fcubejs/custom.js"></script>
</head> 
<body>
<div class="site-loader">
<div class="pageloader-new" style="margin-bottom: -20px;margin-top: 20px;">
 <div class="cube1"></div>
</div>
</div>
<div class="parallax" style="  background: #f6f6f6;margin-bottom: 200px;width: 100%;margin-right: 0px;margin-left: 0px;">
 <header>
 <div class="container">
 <input type="hidden" id="userId" value="<?=$userId;?>">	
 <input type="hidden" id="url" value="<?=$url;?>">	
 <div id="demo">
    <div class="wrapper" style="width:auto">
        <div class="content" style="display:none">
        </div>
        <div class="parent"><i class="fa fa-bars fa-6" style="font-size: 24px;color: #5AC3A2;float: right;width: 40px;height: 40px;margin-top: 4px;margin-right: -1px;"></i></div>
    </div>
    <!--/.nav-collapse -->
      </div>
	 <div style="position: relative;margin: 82px -50px -82px 130px;"> 
	 <div id="dropdown_menu" class="logout_btn slide"  onclick="" >
	  <div class="gb_qa"></div>
	  <div class="gb_pa"></div>
	  <div class="profileEdit"><a class="menu-about" href="aboutus" style="color: #454545;">About Us</a></div>
	  <div class="accountSet"><a class="menu-how" href="howitworks" style="color: #454545;">How It Works</a></div> 
	  <div class="signOut"><a class="menu-store" href="store" style="color: #454545;">Fqube Store</a></div></div></div>
	  
	<!--<ul class="mainMenu">
    <li><i class="fa fa-bars fa-6" style="font-size: 24px;color: #5AC3A2;float: right;width: 40px;height: 40px;margin-top: 4px;margin-right: -1px;"></i>
        <ul class="subMenu">
            <li>Sub</li>
            <li>Sub</li>
            <li>Sub</li>
            <li>Sub</li>
        </ul>
    </li>
</ul>-->
  <div class="logo-img"><a href="/fqube" style="text-decoration:none;outline:none;"><img class="logo" src="<?=$baseUrl;?>/template/img/logo-fqube.png" alt="" style="width: 80%;"></a></div>
  <div class="login-btn"><ul style="float: right;"><li class="login fade1_open"  id="log">Login</li><li class="sign-up overlayLink1 register registerPop_open" id="sig">Sign Up</li></ul></div>
 </div></header> 
 
 <div class="row header-new">
