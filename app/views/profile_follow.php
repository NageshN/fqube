<?php
include('header2.php');
?>
<style>
.desc {
font-size: 22px;
}
.desc-text {
font-size: 17px;
line-height: 20px;
font-weight: lighter;
text-align: justify;
}
.btn-default2{
width:100%;
}
.one-liners{
color:#cccccc;
font-size:17px;
font-weight:lighter;
padding: 5px 0 5px 0px;
}
/*.glyphicon-tag{
transform:rotate(90deg);
}*/
.cover{

    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
	padding:0px!important;
	
}
.tagging{
width:26%;
float:left;
padding:9px;
}
.tag-author{
width:74%;
float:left;

}
.blocks{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
padding: 0px;
margin: 15px;
width:22%;
}
.edd_download_inner{
margin:0px!important;
padding:0px!important;
}
.tag-image{
border-radius:9px;
}
.hero-line{
margin-bottom:21px;
}
.similar-products{
color:#454545!important;
}
.new-short{
background:#ccc!important;
border:1px solid #ccc!important;
text-shadow:0 1px 0 #454545!important;
color:#454545!important;
}
.fa-heart{
color:white;
}
@media only screen and (max-width: 949px) {
.blocks{
width:auto!important;
}
}
a.close1 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
background-color: transparent!important; 
} 
	
	a.close1:hover {
		background-color: transparent!important; 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	} 

.collect{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
background:white;
padding-right:0px;
}
.main-image{
padding:15px 0px 15px 0px;
}
.small-image{
padding:0px 15px 15px 0px;
}
.follow{
float: right;
font-size: 13px;
border: none!important;
background: #5AC3A2!important;
color: white;
text-shadow: none;
}
.total{
padding-bottom:53px;
}
.follow-btn{
margin-top: 20px;
float: right;
font-weight: 100;
}
.follow-top {
width: 100%;
float: left;
margin-bottom: 12px;
}
.box-div {
background-color: #fff;
padding: 10px 0px 10px 0px;
margin-left: 60px;
border-radius: 2px;
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
}
.attachment-img {
border-radius: 5px;
height: 140px;
width: 140px;
margin-left: 12px;
}
.btn-default3:hover {
border-color: #3b3b3b;
background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
}
.btn-default3 {
background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
background-repeat: repeat-x;
}
.btn3 {
display: inline-block;
outline: none;
padding: 6px 35px;
margin-bottom: 0;
font-size: 15px;
color:#fff;
font-weight: 100;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
background:#5AC3A2;
}
.attachment-img1{
border-radius:5px;
}
#tab1:hover{
	color:#5AC3A2!important;
	}
	#tab2:hover{
	color:#5AC3A2!important;
	}
	#tab3:hover{
	color:#5AC3A2!important;
	}
	#tab4:hover{
	color:#5AC3A2!important;
	}
</style> 
<div class="row" style="padding: 41px 0px; background: none repeat scroll 0% 0% rgb(255, 255, 255);">
<div class="container" style="padding-bottom:40px;padding-right:35px;padding-left:35px">
	<div class="col-md-3">
		<img src="<?=$baseUrl;?>app/views/img/vidhya1.jpg" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square" style="width:181px;height:181px;float:right">
	</div>
	<div class="col-md-9">
			<div class="col-md-12" style="padding: 0px 0px 26px;">
			<div class="col-md-6" style="padding:0px">
			<span style="font-size:26px">CLARA PINTO</span></div>
			<div class="col-md-6" style="padding:0px">
			<a href="#" class="btn2 btn-default2" role="button" style="float:right;width:auto;padding:6px 54px">FOLLOW</a></div>
			</div>
			<div class="col-md-12" style="padding: 0px 0px 36px;">
			<span style="font-weight:lighter"><i>"a shopoholic, writer and music lover"</i></span></div>
			<div class="col-md-12" style="padding:0px;width:83%">
			<div class="col-md-3" style="padding:0px"><a href="http://codewave.co.in/fancythat_old/profile-collect" id="tab1" style="color:#454545"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>app/views/img/icon-1.png" alt=""></div>
			<div class="col-md-7" style="padding-right:0px"><span style="color:#5AC3A2;font-size:24px">42</span><span style="font-size:14px"> collections</span></div></a></div>
			<div class="col-md-3" style="padding:0px"><a href="http://codewave.co.in/fancythat_old/product-profile" id="tab2" style="color:#454545"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>app/views/img/icon-2.png" alt=""></div>
			<div class="col-md-7" style=""><span style="color:#5AC3A2;font-size:24px">308</span><span style="font-size:14px"> products</span></div></a></div>
			<div class="col-md-3" style="padding:0px"><a href="#" style="color:#454545" id="tab3"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>app/views/img/icon-3.png" alt=""></div>
			<div class="col-md-7" style=""><span style="color:#5AC3A2;font-size:24px">20</span><span style="font-size:14px"> followers</span></div></a></div>
			<div class="col-md-3" style="padding:0px"><a href="#tabs-4" style="color:#454545" id="tab4"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>app/views/img/icon-4.png" alt=""></div>
			<div class="col-md-7" style=""><span style="color:#5AC3A2;font-size:24px">15</span><span style="font-size:14px"> following</span></div></a></div>
		</div>
	</div>
	
</div>
</div>
 <div class="row" style="margin-top: 60px;margin-bottom: 60px;">
 <div class="container">
  <div class="col-md-5 col-md-offset-0 box-div">
  <div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/abhi.jpg" class="attachment-img1" alt=""></div><div class="col-md-9" style=""><div style="padding-bottom:5px;padding-top: 5px;">Abhijith</div><div style=""><div class="col-md-5" style="padding-left:0px"><img src="<?=$baseUrl;?>app/views/img/cogs.png" class="cogs" alt="">&nbsp;58</div> <div class="col-md-5" style="padding-left:0px;margin-left: -20px;"><i style="color:#5AC3A2;" class="fa fa-heart"></i>&nbsp;24</div></div></div></div><div class="col-md-4"><button id="1" onclick="followuser(this);" class="btn3 btn-default3 follow-btn" value="0" role="button">FOLLOW</button></div></div>
  <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/jacket.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/glass.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/watch.jpg" class="attachment-img" alt=""></div></div>
  </div>
  <div class="col-md-5 col-md-offset-1 box-div">
  <div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/vidhya.jpg" class="attachment-img1" alt=""></div><div class="col-md-9" style=""><div style="padding-bottom:5px;padding-top: 5px;">Vidhya</div><div style=""><div class="col-md-5" style="padding-left:0px"><img src="<?=$baseUrl;?>app/views/img/cogs.png" class="cogs" alt="">&nbsp;27</div> <div class="col-md-5" style="padding-left:0px;margin-left: -20px;"><i style="color:#5AC3A2;" class="fa fa-heart"></i>&nbsp;14</div></div></div></div><div class="col-md-4"><button id="2" onclick="followuser(this);" value="0" class="btn3 btn-default3 follow-btn" role="button">FOLLOW</button></div></div>
  <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/wallet.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/watch.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/shirt.jpg" class="attachment-img" alt=""></div></div>
  </div>
  </div>  
  </div>
  <div class="row" style="margin-top: 60px;margin-bottom: 60px;">
 <div class="container">
  <div class="col-md-5 col-md-offset-0 box-div">
  <div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/abhi.jpg" class="attachment-img1" alt=""></div><div class="col-md-9" style=""><div style="padding-bottom:5px;padding-top: 5px;">Abhijith</div><div style=""><div class="col-md-5" style="padding-left:0px"><img src="<?=$baseUrl;?>app/views/img/cogs.png" class="cogs" alt="">&nbsp;58</div> <div class="col-md-5" style="padding-left:0px;margin-left: -20px;"><i style="color:#5AC3A2;" class="fa fa-heart"></i>&nbsp;24</div></div></div></div><div class="col-md-4"><button id="1" onclick="followuser(this);" class="btn3 btn-default3 follow-btn" value="0" role="button">FOLLOW</button></div></div>
  <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/jacket.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/glass.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/watch.jpg" class="attachment-img" alt=""></div></div>
  </div>
  <div class="col-md-5 col-md-offset-1 box-div">
  <div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/vidhya.jpg" class="attachment-img1" alt=""></div><div class="col-md-9" style=""><div style="padding-bottom:5px;padding-top: 5px;">Vidhya</div><div style=""><div class="col-md-5" style="padding-left:0px"><img src="<?=$baseUrl;?>app/views/img/cogs.png" class="cogs" alt="">&nbsp;27</div> <div class="col-md-5" style="padding-left:0px;margin-left: -20px;"><i style="color:#5AC3A2;" class="fa fa-heart"></i>&nbsp;14</div></div></div></div><div class="col-md-4"><button id="2" onclick="followuser(this);" value="0" class="btn3 btn-default3 follow-btn" role="button">FOLLOW</button></div></div>
  <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/wallet.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/watch.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/shirt.jpg" class="attachment-img" alt=""></div></div>
  </div>
  </div>  
  </div>
    <div class="row" style="margin-top: 60px;margin-bottom: 60px;">
 <div class="container">
  <div class="col-md-5 col-md-offset-0 box-div">
  <div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/abhi.jpg" class="attachment-img1" alt=""></div><div class="col-md-9" style=""><div style="padding-bottom:5px;padding-top: 5px;">Abhijith</div><div style=""><div class="col-md-5" style="padding-left:0px"><img src="<?=$baseUrl;?>app/views/img/cogs.png" class="cogs" alt="">&nbsp;58</div> <div class="col-md-5" style="padding-left:0px;margin-left: -20px;"><i style="color:#5AC3A2;" class="fa fa-heart"></i>&nbsp;24</div></div></div></div><div class="col-md-4"><button id="1" onclick="followuser(this);" class="btn3 btn-default3 follow-btn" value="0" role="button">FOLLOW</button></div></div>
  <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/jacket.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/glass.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/watch.jpg" class="attachment-img" alt=""></div></div>
  </div>
  <div class="col-md-5 col-md-offset-1 box-div">
  <div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/vidhya.jpg" class="attachment-img1" alt=""></div><div class="col-md-9" style=""><div style="padding-bottom:5px;padding-top: 5px;">Vidhya</div><div style=""><div class="col-md-5" style="padding-left:0px"><img src="<?=$baseUrl;?>app/views/img/cogs.png" class="cogs" alt="">&nbsp;27</div> <div class="col-md-5" style="padding-left:0px;margin-left: -20px;"><i style="color:#5AC3A2;" class="fa fa-heart"></i>&nbsp;14</div></div></div></div><div class="col-md-4"><button id="2" onclick="followuser(this);" value="0" class="btn3 btn-default3 follow-btn" role="button">FOLLOW</button></div></div>
  <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/wallet.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="<?=$baseUrl;?>app/views/img/watch.jpg" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="<?=$baseUrl;?>app/views/img/shirt.jpg" class="attachment-img" alt=""></div></div>
  </div>
  </div>  
  </div>
  
  
<?php
include('footer1.php');
?>
