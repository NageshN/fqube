<!DOCTYPE html>
<html lang="en">
<?php 
$baseUrl ="http://codewave.co.in/fqube/";
$userId = Session::get('admin_id');
$userName = Session::get('email_id');
?>
	<head>
		<meta charset="utf-8" />
		<title>Fqube Admin Panel</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" href="http://codewave.co.in/fqube/template/img/favicon.ico">
		<!--basic styles-->
		<link href="app/views/admin/css/bootstrap.min.css" rel="stylesheet" />
		<link href="app/views/admin/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="app/views/admin/css/font-awesome.min.css" />
		<style>
		.table tr{
		text-align: center;
        vertical-align: middle;
		}
		.table th{
		text-align: center;
        vertical-align: middle;
		}
		</style>
		<!--<link href="http://fontawesome.io//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/fonts/fontawesome-webfont.woff" rel="stylesheet">-->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="app/views/admin/css/ace.min.css" />
		<link rel="stylesheet" href="app/views/admin/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="app/views/admin/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid" style="  background: #f3f3f3;border-bottom: 1px solid #e0e0e0;">
					<a href="#" class="brand">
						<img style="  width: 60%; margin-left: 25px;" src="app/views/admin/images/logo.png">
					</a><!--/.brand-->
<div style="float:right;margin-top:22px;">
            <button id="logout"><i class="icon-off"></i>logout</button>
           
          </div>
							</div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
			
			
		</div>
		
		

		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!--#sidebar-shortcuts-->

				<ul class="nav nav-list">
					<li>
						<a href="#" class="dropdown-toggle">
							<i class="icon-desktop"></i>
							<span class="menu-text">Delete Abused Products</span>

							<b class="arrow icon-angle-down"></b>
						</a>

						<ul class="submenu">
						    <li>
								<a href="adminHome">
									<i class="icon-double-angle-right"></i>
									View Abused Products
								</a>
							</li>
				
						</ul>
					</li>
					
					<li>
						<a href="#" class="dropdown-toggle">
							<i class="icon-desktop"></i>
							<span class="menu-text">Add New Category</span>

							<b class="arrow icon-angle-down"></b>
						</a>

						<ul class="submenu">
						    <li>
								<a href="#">
									<i class="icon-double-angle-right"></i>
									Add Category
								</a>
							</li>
							<li>
								<a href="#">
									<i class="icon-double-angle-right"></i>
									Edit Category
								</a>
							</li>
						</ul>
					</li>
					
					<li>
						<a href="#" class="dropdown-toggle">
							<i class="icon-desktop"></i>
							<span class="menu-text">Tag Product to Category</span>

							<b class="arrow icon-angle-down"></b>
						</a>

						<ul class="submenu">
						    <li>
								<a href="tagProducts">
									<i class="icon-double-angle-right"></i>
								   View products tagged to others
								</a>
							</li>
						</ul>
					</li>
					
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>

			<!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		<!--<![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<!--<script src="js/bootstrap.min.js"></script>-->

		<!--page specific plugin scripts-->

		<!--ace scripts-->

		<script src="app/views/admin/js/ace-elements.min.js"></script>
		<script src="<?=$baseUrl;?>template/fcubejs/custom.js" /></script>
		<script>
		jQuery('#logout').click(function(){ 
		
		var session=$('#session').val();
		$.ajax({
				type : 'POST',  
				data :{session:session},    
				url: 'logout.php',
				dataType: 'text',
				cache: false,
				success: function(data) 
				{
					window.location = "index.html";
				
				}
		
		
		
		
		});
		});
		</script>

		<!--inline scripts related to this page-->
	</body>
<script src="app/views/admin/js/ace.min.js"></script>
<link rel="stylesheet" type="text/css" href="app/views/admin/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="app/views/admin/css/demo.css">
<script type="text/javascript" language="javascript" src="app/views/admin/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('#example').dataTable();
} );
</script>
</html>
