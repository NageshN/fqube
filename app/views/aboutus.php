<?php
include('header1.php');
include('connect.php');
?>
<style>
.login-btn{
display:none;
}
.header-new{
    background-image: url(http://codewave.co.in/fqube/template/img/about-us.jpg);
  background-size: 100% 570px;
  background-repeat: no-repeat;
  height: 570px;
  background-color: #f6f6f6;
  }
.sub-points li {
  list-style-type: disc!important;
  font-size:24px;
  color:#5AC3A2;
  padding:13px;
}
.sub-points li span{
font-size:18px;
color:#454545;
}
.btn-default2:hover{
background-image:linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
border:1px solid #3b3b3b;
text-shadow:none!important;
}
.fa-check{
color:#5AC3A2;
}
.main-top{
  text-align: center;
  padding-top: 15px;
}
</style>
<div class="row heads-up" style="">
<div class="top-head" style="font-size:39px;margin-top:52px;color:#3b3b3b"><span>ABOUT US<br></span></div>
<div class="head-desc">A personalised shopping experience for you</div>
<div class="col-md-12" style="">
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="head-points col-md-8"><div class="col-md-1"><i class="fa fa-check-square-o"></i></div><div class="col-md-11 head-points-desc">FQUBE is the place for you to discover and buy beautiful products curated by our community. Being a product discovery and social networking platform, it allows users to browse through product feeds curated specifically for them.</div></div>
<div class="col-md-2"></div>
</div>
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="head-points col-md-8"><div class="col-md-1"><i class="fa fa-check-square-o"></i></div><div class="col-md-11 head-points-desc">Individuals on FQUBE are able to express their personal lifestyle and fashion preferences by creating as many collections of products as they appreciate. Thus FQUBE as a platform helps to reaffirm and guide one's purchasing decisions whilst establishing a seemingly collaborative shopping experience.</div></div>
<div class="col-md-2"></div>
</div>
</div>
 </div>
</div>
<link rel="stylesheet" id="edd-styles-css" href="<?=$baseUrl;?>template/assets/edd.min.css?ver=2.2.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-retemplate-css" href="<?=$baseUrl;?>template/assets/edd-retemplate.css?ver=1.3.7" type="text/css" media="all">
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1' type='text/css' media='all' />
<link rel="stylesheet" id="jquery-fancybox-css" href="<?=$baseUrl;?>template/assets/jquery.fancybox.css?ver=4.1" type="text/css" media="all">
<link rel="stylesheet" id="jetpack_css-css" href="<?=$baseUrl;?>template/assets/jetpack.css?ver=3.3" type="text/css" media="all">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/assets/style-public.css" type="text/css" media="all">
<script type="text/javascript" async="" src="http://tag.perfectaudience.com/serve/53ef64f22e4ef322e0000047.js"></script><script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.js?ver=1.11.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"http:\/\/themes.designcrumbs.com\/stocky\/wp-admin\/admin-ajax.php","position_in_cart":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","ajax_loader":"\/stocky\/wp-content\/plugins\/easy-digital-downloads\/assets\/images\/loading.gif","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"http:\/\/themes.designcrumbs.com\/stocky\/checkout\/","permalinks":"1","quantities_enabled":""};
/* ]]> */
</script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/edd-ajax.min.js?ver=2.2.3"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fancybox.pack.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.stellar.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.mobilemenu.js?ver=4.1"></script>
<script type="text/javascript" src="<?=$baseUrl;?>template/assets/jquery.fitvids.js?ver=4.1"></script>
<link rel="stylesheet" id="css-stocky-css" href="<?=$baseUrl;?>template/assets/style.css?ver=4.1" type="text/css" media="all">
<?php
include('footer.php');
?>
<script>
$("#anchor1").on("click",function( e ){
    e.preventDefault();
    $("body, html").animate({ 
        scrollTop: $( $(this).attr('href') ).offset().top 
    }, 600);
});
</script>