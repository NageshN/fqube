</div>
<div class="row" style="margin-left: 0px;margin-right: 0px;background:#3b3b3b;position: fixed;bottom: 0;width: 100%;z-index: -1;-webkit-transform: translateZ(0px);padding-top: 70px;">
  <div id="socnets_wrap"><div id="socnets">
	    <a href="#" title="Facebook" class="socnet-facebook"></a>
		<a href="#" title="Twitter" class="socnet-twitter"></a>
		<a href="#" title="Google+" class="socnet-google"></a>
		<a href="#" title="Pinterest" class="socnet-pinterest"></a>
		<a href="#" title="Flickr" class="socnet-flickr"></a>
		</div>
		<div class="clear"></div>  
  </div>
  <div class="copyright"><p><i class="fa fa-copyright"></i>&nbsp;&nbsp;fqube 2015</p></div>
</div>		
</div>	
<script>
  var seq = 0;

  jQuery(document).ready(function() {
	jQuery("html").niceScroll({styler:"fb",cursorcolor:"#000"});
	jQuery("#mainlogo img").eq(1).hide();
	function goSeq() {
	  var nxt = (seq+1)%2;
	  jQuery("#mainlogo img").eq(seq).fadeIn(2000);
	  jQuery("#mainlogo img").eq(nxt).fadeOut(2000);
	  seq = nxt;
	  setTimeout(goSeq,2500);
	};
	goSeq();
	
	jQuery(window).load(function(){
	  setTimeout(function(){
	    jQuery("#gmbox div").animate({'top':60},1500,"easeOutElastic");
	  },1500);
	});
	
	function trackLink(link, category, action) {
	  try {
		_gaq.push(['_trackEvent', 'tracklink' ,'click',link.href ]);
		setTimeout('document.location = "' + link.href + '"', 100)
	  }catch(err){}
	}
	
	jQuery('[rel="outbound"]').click(function(e){	  
	  try {
		_gaq.push(['_trackEvent','outbound','click',this.href]);
	  }catch(err){}
	});
	
  });
</script>
<script>
jQuery(document).ready(function(){
    var animObj = jQuery('#up');
    jQuery(window).scroll(function(){
        if (jQuery(window).scrollTop() > 200 && animObj.css("bottom") == "-50px"){
            animObj.stop().css({opacity:"1"}).animate({ bottom: '0px' }, 300, "easeOutBounce");
        }
        else if (jQuery(window).scrollTop() < 200 && animObj.css('opacity') != "0"){
            animObj.stop().animate({ opacity: '0' }, 300, "linear").queue(function(){ $(this).css({bottom:'-50px'}) });
        }
    });
});

jQuery.extend(jQuery.easing,
{
    easeOutBounce: function (x, t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
        } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
        }
    }
});
</script> 
<script>
jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > 1){  
        jQuery('header').addClass("sticky");
        jQuery('.logo-img').attr('style','padding: 14px 0px 0px 0px!important;'); 
        jQuery('.login-btn').attr('style','padding: 16px 0px 0px 0px!important;');
        jQuery('.login').attr('style','color:#5AC3A2!important;');
        jQuery('.login').removeClass("hover-black");
        jQuery('.sign-up').attr('style','color:#5AC3A2!important;');
        jQuery('.sign-up').removeClass("hover-black");
        jQuery('.logo').attr('style','width: 70%;');	
        jQuery('.logo').attr('src','<?=$baseUrl;?>template/img/logo-fqube.png');	
		jQuery('.login').mouseenter(function(){
          jQuery(".login").css("border","1px solid #5AC3A2");
        }); 
        jQuery('.login').mouseout(function(){
          jQuery(".login").css("border","none");
        });
		jQuery('.sign-up').mouseenter(function(){
          jQuery(".sign-up").css("border","1px solid #5AC3A2");
        }); 
        jQuery('.sign-up').mouseout(function(){
          jQuery(".sign-up").css("border","none");
        }); 
    } 
    else{
        jQuery('header').removeClass("sticky");
		jQuery('.logo-img').attr('style','padding: 14px 0px 0px 0px!important;'); 
        jQuery('.login-btn').attr('style','padding: 16px 0px 0px 0px!important;');
		jQuery('.login').attr('style','color:#5AC3A2!important;');
        jQuery('.login').addClass("hover-black");
        jQuery('.sign-up').attr('style','color:#5AC3A2!important;');
        jQuery('.sign-up').addClass("hover-black");
        jQuery('.logo').attr('style','width: 70%;');
		jQuery('.logo').attr('src','<?=$baseUrl;?>template/img/logo-fqube.png');	
        jQuery('.login').mouseenter(function(){
          jQuery(".login").css("border","1px solid #5AC3A2");
        }); 
        jQuery('.login').mouseout(function(){
          jQuery(".login").css("border","none");
        }); 
		jQuery('.sign-up').mouseenter(function(){
          jQuery(".sign-up").css("border","1px solid #5AC3A2");
        }); 
        jQuery('.sign-up').mouseout(function(){
          jQuery(".sign-up").css("border","none"); 
        });  
    } 
});
</script>
<script>
// jQuery(".logout_btn").hide().click(function(){
    // return false;
// });
// jQuery('.parent').show().click(function(){
// jQuery('.logout_btn').slideToggle('slow');
// jQuery('.gb_pa').show('slow');
// jQuery('.gb_qa').show('slow');
    // return false;
// });
// jQuery(document).click(function(){
    // jQuery(".logout_btn ").slideUp('slow');
// });
(function($) {
  
  $(".parent").click(function(e){
    e.stopPropagation();
    var div = $(".logout_btn");
    jQuery('.gb_pa').show('fast');
    jQuery('.gb_qa').show('fast');
    // Make it visible off-page so
    // we can measure it
    div.css({
      "display": "block"
    });
    
    // Move it where we want it to be
  });
$(document).click(function(e){
  $('.logout_btn').slideUp('fast');
});
})(jQuery);

(function($) {
  
  $(".parent1").click(function(e){
    e.stopPropagation();
    var div = $(".logout_btn1");
    jQuery('.gb_pa').show('fast');
    jQuery('.gb_qa').show('fast');
    // Make it visible off-page so
    // we can measure it
    div.css({
      "display": "block"
    });
    
    // Move it where we want it to be
  });
$(document).click(function(e){
  $('.logout_btn1').slideUp('fast');
});
})(jQuery);
</script>
<script>
jQuery(".stocky_hover_details").mouseover(function(){
alert('hi');
jQuery('.like-bottom').attr('style','display:none!important');
});
jQuery(".stocky_hover_details").mouseout(function(){
alert('bye');
jQuery('.like-bottom').attr('style','display:block!important');
});
</script>
<script>
//Page Loader
jQuery(window).load(function(){
   function show_popup(){
     jQuery('.site-loader').attr("style","visibility:hidden;");
   };
   window.setTimeout( show_popup, 500 ); // 5 seconds
})
</script>
<style>
#loader {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader1 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader2 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader3 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader4 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: 41%;
  margin-right: auto;
  text-align: center;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
.loader {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
.loader-save {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
.cube {
  background-color: rgba(0, 0, 0, 0);
  border: 5px solid rgba(255, 255, 255, 0.9);
  width: 30px;
  height: 30px;
  margin: 0 auto;
  -webkit-animation: cubeanim 2s infinite ease-in-out;
  -moz-animation: cubeanim 2s infinite ease-in-out;
  -o-animation: cubeanim 2s infinite ease-in-out;
  animation: cubeanim 2s infinite ease-in-out;
}
@keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
@-o-keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
@-moz-keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
@-webkit-keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
#preview_lp2{
width: 86%!important;
}
// #postPreviewButton_lp1{
// margin: 70px 0px 0px 20px!important;
// }
#postPreviewButton_lp2{
right:12%!important;
bottom:42%!important;
}

#text_lp2{
width: 80.5%!important;
margin: 0px 0px 10px -1px!important;
}
#text_lp1{
margin: -40px 0px 10px 44px!important;
width: 93.5%!important;
}
</style>
	<script src="<?=$baseUrl;?>template/js/bootstrap.js"></script>
	<script src="<?=$baseUrl;?>template/slider/jquery-1.8.0.min.js"></script>
	<!--<script src="<?=$baseUrl;?>template/js/jquery-1.4.2.min.js"></script>-->
   <script src="<?=$baseUrl;?>template/js/jquery-2.0.0b1.js"></script>
	<script src="<?=$baseUrl;?>template/js/jquery-ui.js"></script>
	<script src="<?=$baseUrl;?>template/slider/jquery.bxslider.js"></script>
	<script src="<?=$baseUrl;?>template/fcubejs/custom.js"></script> 
	<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
    <link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
	<script src="http://codewave.co.in/fqube/template/fcubejs/jquery.popupoverlay.js"></script>
	<script src="<?=$baseUrl;?>template/fcubejs/follow.js"></script> 
	<script src="<?=$baseUrl;?>template/js/togetherjs.js"></script> 
    <script src="<?=$baseUrl;?>template/js/bootstrap.min.js"></script>
    <script src="<?=$baseUrl;?>template/js/jquery.easing.1.3.js"></script>
    <script src="<?=$baseUrl;?>template/js/jquery.nicescroll.min.js"></script>
    <script src="<?=$baseUrl;?>template/js/jquery.nicescroll.plus.js"></script> 
   
      <script src="http://codewave.co.in/fqube/template/jquery.liveurl.js"> </script>
	  
<link rel="stylesheet" href="<?=$baseUrl;?>template/slider/jquery.bxslider.css" type="text/css">
<link rel="stylesheet" id="edd-slg-public-style-css" href="<?=$baseUrl;?>template/fcubejs/jquery.urlive.css" type="text/css" media="all">
<script src="<?=$baseUrl;?>template/slider/rainbow.min.js"></script>
<script src="<?=$baseUrl;?>template/fcubejs/menu.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://codewave.co.in/fqube/template/liveurl.css" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/posturl/css/stylesheet.css" />
		<link rel="stylesheet" type="text/css" href="<?=$baseUrl;?>template/posturl/css/linkPreview.css" />
		<script type="text/javascript" src="<?=$baseUrl;?>template/posturl/js/linkPreview.js" ></script>
		<script type="text/javascript" src="<?=$baseUrl;?>template/posturl/js/linkPreviewRetrieve.js" ></script>
		<script>
			$(document).ready(function() {
				$('#retrieveFromDatabase').linkPreviewRetrieve();
				$('#lp1').linkPreview();
				$('#lp2').linkPreview({placeholder: "post a link you found..."});
			});
		</script> 
</body>
</html>