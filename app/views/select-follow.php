<?php
include('header2.php');
?>
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
<div id="fade2" class="popup-success" style="color:#6B6969;font-weight: 100;padding:10px;width:400px;background:#f6f6f6;height:260px;display:none;border-radius:5px;"><div class="col-md-12"><div class="col-md-1"></div><div class="col-md-10" style="font-size: 23px;font-family:Verdana, sans-serif;padding-top: 41px;padding-left:0px;padding-right:0px">Start creating your feed!</div><div class="col-md-1"></div></div><div class="col-md-12" style="font-size: 15px;color: grey;line-height: 22px;text-align: center;padding-top: 26px;padding-bottom:30px">This is where you will see your personal feed of products added by people and stores you follow.</div><div class="col-md-12" style="text-align:center"><input type="button" style="  width: 90%;/* height: 37px; *//* float: left; */background-color: #5AC3A2;color: white;border: none;border-radius: 3px;text-transform: uppercase;height: 41px;/* padding: 2px; */line-height: 0px;" value="get started" class="fade2_close" id="get_started"></div></div>
<link href="<?=$baseUrl;?>template/fqubecss/select-follow.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?=$baseUrl;?>template/slider/styles.css" type="text/css">
<div class="row" style="margin-left: 0px;height: 100px;background:#5AC3A2;padding: 25px 0 0 0;margin-top:6px">
 <div class="container">
  <div class="writeup"><div class="writeup-head">CHOOSE THINGS THAT INTEREST YOU</div><div class="writeup-subhead">Please select atleast 3 categories that interest you to create your personalised feed.</div></div>
  </div>
</div>
<div class="dot-move dotted-one active" onclick="clickme1();">1</div>
<div class="dot-move dotted-two" onclick="clickme2();">2</div>
<div class="dot-move dotted-three" onclick="clickme3();">3</div>
<div class="box">
<div class="inner clearfix">
<div id="primary">
<div class="slider">
 <div class="next-category" onclick="clickme();" style="z-index: 100;position: fixed;right: 30px;top: 295px;"><div class="outside">
<p><span style="color:black;" id="slider-next" onclick="window.scrollTo(0, 0);"></span></p>
</div></div>
<div class="next-finish" style="z-index: 10000000;position: fixed;right: 30px;top: 295px;display:none;"><div class="outside">
<p><span style="color:black;" id="slider-next1"><a class="bx-next1" onclick="FQUBE.fqube.register_finish(this)" style="color:#5AC3A2;"><img class="circle-icon25" src="http://codewave.co.in/fqube/template/img/arrow.png" alt="" style="margin-left: 1px;margin-bottom: 5px;width: 55%;"><br>FINISH</a></span></p>
</div></div>
<ul class="bxslider">
<li>
<div class="box1" id="category" style="">
<div class="row category-search-div" style="margin-top: 35px;margin-bottom: 35px;">
<div class="col-md-3" style=""></div>
<div class="col-md-6" style=""><form id="category-search-form"><div class="input-group">
<input type="text" id="category-search" class="form-control" placeholder="find a category to follow">
<span class="input-group-btn">
<button class="btn btn-default1 search-btn" id="category-search-submit" onclick="FQUBE.search.search_text_cat(this)" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
</span></div></form></div>
 <div class="col-md-3" style=""></div>
 </div> 
<div class="row" style="background: #f6f6f6;">
<div class="container" style="padding-bottom: 30px;padding-left:130px;">
    <div class="row category-sub-search">
	 
	</div>
	 <div id="loader4">
  <div class="cube"></div>
</div> 
	<div class="row">
	<ul class="ul-attribute80" style="margin-top: 10px;">
	
	</ul>
    </div>
</div>
<input type="hidden" id="selectedHidden" data-1='' data-2='' data-3='' data=''>	

</div>
</div>
</li>
<li>
<div class="box2" id="follow" style="">
<div class="row people-search-div" style="margin-top: 35px;margin-bottom: 35px;display:none;">
<div class="col-md-4" style=""></div>
<div class="col-md-4" style=""><form id="people-search-form"><div class="input-group">
      <input type="text" id="people-search" class="form-control" placeholder="find a person to follow">
      <span class="input-group-btn">
        <button class="btn btn-default1 search-btn" id="people-search-submit" onclick="FQUBE.search.search_text_people(this)" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
</span></div></form></div>
 <div class="col-md-4" style=""></div>
 </div>   
<div class="row" style="margin-top: 10px;margin-bottom: 10px; height:auto;">
<div class="container people-new-search" style="height:auto;">
  
  </div> 
   <div id="loader3">
  <div class="cube"></div>
</div>  
 <div class="container people-new" style="height:auto;">
  
  </div>  
    <div id="loader1">
  <div class="cube"></div>
</div>
  </div>    
  <div class="row people-more-div" style="margin-top: 50px;margin-bottom: 25px;display:none;">
 <div class="container">
  <div style="text-align:center;"><span class="load-more btn2 btn-default2" id="viewmorepeople" data-id="2" onclick="FQUBE.fqube.viewmore_people(this);">VIEW MORE PROFILES</span></div>
  </div>  
  </div>  
</div> 
</li>
<li> 
<div class="box3" id="store" style="">
<div class="row store-search-div" style="margin-top: 35px;margin-bottom: 35px;display:none;">
<div class="col-md-4" style=""></div>
<div class="col-md-4" style=""><form id="store-search-form"><div class="input-group">
      <input type="text" id="store-search" class="form-control" placeholder="find a store to follow">
      <span class="input-group-btn">
        <button class="btn btn-default1 search-btn" id="store-search-submit" onclick="FQUBE.search.search_text_store(this)" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
</span></div></form></div>
 <div class="col-md-4" style=""></div>
 </div>  
 <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
 <div class="container store-new-search">
  
  </div>
   <div id="loader2">
  <div class="cube"></div>
</div>  
 <div class="container store-new">
  
  </div>  
   <input type="hidden" id="selectedHiddenstore" data-1='' data-2='' data-3='' data=''>	
   <div id="loader">
  <div class="cube"></div>
</div>
  </div>    
  <div class="row store-more-div" style="margin-top: 50px;margin-bottom: 25px;display:none;">
 <div class="container">
  <div style="text-align:center;"><span class="load-more btn2 btn-default2" id="viewmorestore" data-id="2" onclick="FQUBE.fqube.viewmore_store(this);">VIEW MORE STORES</span></div>
  </div>  
  </div>  
</div>
</li>
<li>
</li>
</ul>
</div>
</div>
</div>
</div>
<script src="<?=$baseUrl;?>template/fcubejs/registerpopup.js"></script>
<?php
include('footer1.php');
?>	
<script src="<?=$baseUrl;?>template/fcubejs/fqube.js"></script>