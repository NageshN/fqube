<div class="row" style="margin-left: 0px;margin-right: 0px;background:#3b3b3b;position: fixed;bottom: 0;width: 100%;z-index: -1;-webkit-transform: translateZ(0px);padding-top: 70px;">
 <div class="container">
  <div id="socnets_wrap"><div id="socnets">
	    <a href="#" title="Facebook" class="socnet-facebook"></a>
		<a href="#" title="Twitter" class="socnet-twitter"></a>
		<a href="#" title="Google+" class="socnet-google"></a>
		<a href="#" title="Pinterest" class="socnet-pinterest"></a>
		<a href="#" title="Flickr" class="socnet-flickr"></a>
		</div>
		<div class="clear"></div> 
  </div>
  <div class="copyright"><p><i class="fa fa-copyright"></i>&nbsp;&nbsp;fqube 2015</p></div>
</div>
</div>
</div>



<script>
  var seq = 0;

  jQuery(document).ready(function() {
	jQuery("html").niceScroll({styler:"fb",cursorcolor:"#000"});
	
	jQuery("#mainlogo img").eq(1).hide();
	function goSeq() {
	  var nxt = (seq+1)%2;
	  jQuery("#mainlogo img").eq(seq).fadeIn(2000);
	  jQuery("#mainlogo img").eq(nxt).fadeOut(2000);
	  seq = nxt;
	  setTimeout(goSeq,2500);
	};
	goSeq();
	
	jQuery(window).load(function(){
	  setTimeout(function(){
	    jQuery("#gmbox div").animate({'top':60},1500,"easeOutElastic");
	  },1500);
	});
	
	function trackLink(link, category, action) {
	  try {
		_gaq.push(['_trackEvent', 'tracklink' ,'click',link.href ]);
		setTimeout('document.location = "' + link.href + '"', 100)
	  }catch(err){}
	}
	
	jQuery('[rel="outbound"]').click(function(e){	  
	  try {
		_gaq.push(['_trackEvent','outbound','click',this.href]);
	  }catch(err){}
	});
	
  });
</script>
<script>
jQuery(document).ready(function(){
    var animObj = jQuery('#up');
    jQuery(window).scroll(function(){
        if (jQuery(window).scrollTop() > 200 && animObj.css("bottom") == "-50px"){
            animObj.stop().css({opacity:"1"}).animate({ bottom: '0px' }, 300, "easeOutBounce");
        }
        else if (jQuery(window).scrollTop() < 200 && animObj.css('opacity') != "0"){
            animObj.stop().animate({ opacity: '0' }, 300, "linear").queue(function(){ $(this).css({bottom:'-50px'}) });
        }
    });
});

jQuery.extend(jQuery.easing,
{
    easeOutBounce: function (x, t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
        } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
        }
    }
});
</script>

<script>
     
jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > 1){   
        jQuery('header').addClass("sticky");
        jQuery('.logo-img').attr('style','padding: 4px 0px 0px 0px!important;margin-top: 4px;'); 
        jQuery('.login-btn').attr('style','padding: 16px 0px 0px 0px!important;');
        jQuery('.fa-bars').attr('style','font-size: 24px;color: #5AC3A2;float: right;width: 40px;height: 40px;margin-right: -1px;margin-top: -8px!important;');
        jQuery('.login').attr('style','color:#5AC3A2;/*border:1px solid #5AC3A2;*/');
        jQuery('.login').removeClass("hover-black");
        jQuery('.sign-up').attr('style','color:#fff;/*border:1px solid #5AC3A2;*/background-color:#5AC3A2;');
        jQuery('.sign-up').removeClass("hover-black");
		jQuery('.homepage-caption').hide();
        jQuery('.logo').attr('style','width: 70%;');	
        jQuery('.logo').attr('src','<?=$baseUrl;?>template/img/logo-fqube.png');	
		jQuery('.login').mouseenter(function(){
          jQuery(".login").attr('style','color:#fff!important;background-color:#5AC3A2!important;');
        }); 
        jQuery('.login').mouseout(function(){
          jQuery(".login").attr('style','color:#5AC3A2!important;background-color:transparent!important;/*border:1px solid #5AC3A2!important;*/');
        });
		jQuery('.sign-up').mouseenter(function(){
          jQuery(".sign-up").attr('style','color:#fff!important;background-color:#468688!important;');
        }); 
        jQuery('.sign-up').mouseout(function(){
          jQuery(".sign-up").attr('style','color:#fff!important;background-color:#5AC3A2!important;/*border:1px solid #5AC3A2!important;*/');
        }); 
    } 
    else{
        jQuery('header').removeClass("sticky");
		jQuery('.homepage-caption').show();
		jQuery('.logo-img').attr('style','padding: 18px 0px 0px 0px!important;'); 
        jQuery('.login-btn').attr('style','padding: 30px 0px 0px 0px!important;');
        jQuery('.fa-bars').attr('style','font-size: 24px;color: #5AC3A2;float: right;width: 40px;height: 40px;margin-right: -1px;margin-top: 4px!important;');
		jQuery('.login').attr('style','color:#5AC3A2;');
        jQuery('.sign-up').attr('style','color:#fff;background-color:#5AC3A2;');
        jQuery('.logo').attr('style','width: 80%;');
		jQuery('.logo').attr('src','<?=$baseUrl;?>template/img/logo-fqube.png');	
        jQuery('.login').mouseenter(function(){
          jQuery(".login").attr('style','color:#51b595!important;background-color:#transparent!important;');
        }); 
        jQuery('.login').mouseout(function(){
          jQuery(".login").attr('style','color:#5AC3A2!important;background-color:transparent!important;');
        });
		jQuery('.sign-up').mouseenter(function(){
          jQuery(".sign-up").attr('style','color:#fff!important;background-color:#51b595!important;');
        }); 
        jQuery('.sign-up').mouseout(function(){
          jQuery(".sign-up").attr('style','color:#fff!important;background-color:#5AC3A2!important;');
        });		
    }
});
</script>
<script>
//Page Loader
jQuery(window).load(function(){
   function show_popup(){
     jQuery('.site-loader').attr("style","visibility:hidden;");
   };
   window.setTimeout( show_popup, 500 ); // 5 seconds
})
</script>
<style>
#loader {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader1 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader2 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader3 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
#loader4 {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: 41%;
  margin-right: auto;
  text-align: center;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
.loader {
  background: none;
  z-index: 100;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 60px;
  height: 60px;
  display:none;
  text-indent: -999em;
}
.cube {
  background-color: rgba(0, 0, 0, 0);
  border: 5px solid rgba(255, 255, 255, 0.9);
  width: 30px;
  height: 30px;
  margin: 0 auto;
  -webkit-animation: cubeanim 2s infinite ease-in-out;
  -moz-animation: cubeanim 2s infinite ease-in-out;
  -o-animation: cubeanim 2s infinite ease-in-out;
  animation: cubeanim 2s infinite ease-in-out;
}
@keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
@-o-keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
@-moz-keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}
@-webkit-keyframes cubeanim {
  0% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-right: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
  33% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-bottom: 5px solid #5AC3A2;
  }
  66% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-left: 5px solid #5AC3A2;
    opacity: 1;
    filter: alpha(opacity=100);
  }
  100% {
    border: 5px solid rgba(255, 255, 255, 0.9);
    border-top: 5px solid #5AC3A2;
    opacity: 0.5;
    filter: alpha(opacity=50);
  }
}

</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?=$baseUrl;?>template/js/bootstrap.js"></script>
	<script src="<?=$baseUrl;?>template/js/jquery-1.4.2.min.js"></script>
	<script src="<?=$baseUrl;?>template/js/jquery-2.0.0b1.js"></script>
    <script src="<?=$baseUrl;?>template/js/bootstrap.min.js"></script>
    <script src="<?=$baseUrl;?>template/js/jquery.easing.1.3.js"></script>
    <script src="<?=$baseUrl;?>template/js/jquery.nicescroll.min.js"></script>
    <script src="<?=$baseUrl;?>template/js/jquery.nicescroll.plus.js"></script>
    
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
<script>
// jQuery(".logout_btn ").hide().click(function(){
    // return false;
// });
// jQuery('.parent').show().click(function(){
// jQuery('.logout_btn').slideToggle('slow');
// jQuery('.gb_pa').show('slow');
// jQuery('.gb_qa').show('slow');
// event.preventDefault()
// });
// jQuery(document).click(function(){
    // jQuery(".logout_btn ").slideUp('slow');
// });
(function($) {
  
  $(".parent").click(function(e){
    e.stopPropagation();
    var div = $(".logout_btn");
    jQuery('.gb_pa').show('fast');
    jQuery('.gb_qa').show('fast');
    // Make it visible off-page so
    // we can measure it
    div.css({
      "display": "block"
    });
    
    // Move it where we want it to be
  });
$(document).click(function(e){
  $('.logout_btn').css('display','none');
});
})(jQuery);
</script>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
<script>
// var url = jQuery('#url').val();
// if(url == "/fqube/login") {
	  // toastr.error('To avail this feature you have to login first');
	// }
// </script>
<script>
// $('.mainMenu').children('li').on('click', function() {
       // $(this).children('ul').slideToggle('slow'); 
// });
// </script>
</body>
</html>
