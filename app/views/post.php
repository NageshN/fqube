<?php
include('header2.php');
?> 
<?php 
$getUrl = $_GET['images'];
$title = $_GET['title'];
$price = $_GET['price'];
$availability = $_GET['availability'];
$url = $_GET['url'];
$price = str_replace(',', '', $price);
$res = preg_replace("/[^a-zA-Z]/", "", $price);
$priceCurrency=$res;
$string=$price; 
$numbers_array = extract_numbers($string);
$price=$numbers_array[0];
function extract_numbers($string)
{
preg_match_all('/([\d]+)/', $string, $match);
 
return $match[0];
}
?>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
<style>
.well{
width:32%;
}
@media screen and (min-width: 380px) and (max-width: 700px){
.myfeed12{
display:none;
}
.explore{
display:none;
}
.post{
display:none;
}
#demo{
margin-top: -68px;
}
.logo{ 
width: 45%!important;
margin-left: 30px;
margin-top: -8px;
}
}
#myCustomSelector option.option-col:nth-child(1){
   display:none;
}
</style>
<input type="hidden" id="images" value="<?=$getUrl[0];?>">
<input type="hidden" id="title" value="<?=$title;?>">
<input type="hidden" id="description" value="<?=$title;?>">
<input type="hidden" id="price" value="<?=$price;?>">
<input type="hidden" id="priceCur" value="<?=$res;?>">
<input type="hidden" id="availability" value="<?=$availability;?>">
<input type="hidden" id="url" value="<?=$url;?>">

<!--Save popup starts-->
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<div id="save12" class="well" style="background:white;box-shadow:none;margin-bottom:0px;display:none;"> 
<a class="close1 save12_close" onclick="closeTab(this);"><i class="fa fa-times"></i></a>
	<div class="login-wrapper1 main-div" style="background:white">
		<div class="login-content1" style="height:189px;background:white">
			<div style="padding: 0px 0 31px 0;font-size: 20px;color: #454545;font-weight: 100;">Add this product to a collection</div>
			<form style="font-weight:lighter" action="javascript:void(0);" onsubmit="addCollection1(this);">
			<select id="myCustomSelector" style="padding: 4px 10px;width: 100%;height: 45px;background: #f6f6f6;border: none;border-radius: 4px;">
		    <option value="choose">Loading...</option>
            </select> 
		<div class="horizontal" style="width: 50%;margin-left: auto; margin-right: auto;"><span class="row1">Or</span></div>
		<input type="hidden" value="" id="productID">
		<input style="  font-family: 'FontAwesome','roboto';font-weight:lighter;border:1px solid #ccc;border-radius:3px; width: 88%;float:left;  margin-right: 8px; margin-top: 0px;height:45px  background: #fff;" type='text' id="collection" name='collection' placeholder='Add a new collection' /><div onclick="addSave1(this)" style="width: 8%;height:37px;font-size: 16px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px"><div class="glyphicon glyphicon-plus" aria-hidden="true" style="padding: 4px 10px 0px 11px;color: #fff;font-size: 20px;float:left" id="profile_pic"></div></div>
        <input class="feed_open" style="width: 100%;height:37px;float:left;background-color:#5AC3A2;color:white;border:none;border-radius:4px" type="submit" value="Add to Collection">
	    </form>
		<br><br>
		<div>
		</div>
		</div>
		<div style="width: 100%;float: left;">
		<div id="loader-save" class="loader" style="margin-bottom: -20px;margin-top: 20px;">
        <div class="cube"></div>
        </div></div>
	</div>
	<div class="login-wrapper1 done-div" style="background:white;display:none;">
	
	 </div>
</div>

<!--post popup starts-->
<script>
 jQuery('#save12').popup({
      transition: 'all 0.3s',
      scrolllock: false,
	  autoopen: true
});
</script>
<!--post popup ends-->

<script>
jQuery(document).ready(function(){
 
			  jQuery('#loader3').show();  
			  var product_image=jQuery('#images').val(); 
			  var product_name=jQuery('#title').val();  
			  var product_description=jQuery('#description').val(); 
			  var price=jQuery('#price').val(); 
			  var product_currency=jQuery('#priceCur').val();
              if(price == 0 || !price || price == ""){  
				    price="";
				    product_currency="";
				  }
				  else{
				      var price=jQuery('#price').val(); 
			          var product_currency=jQuery('#priceCur').val(); 
				  }			  
			  var products_site_url=jQuery('#url').val(); 
			  var uri=products_site_url.split("/");
			  var store_link=uri['2'];	
			  var user_id=jQuery('#user_id').val();
			  // alert(product_image);
			  // alert(product_name);
			  // alert(product_description);
			  // alert(price);
			  // alert(product_currency);
			  // alert(products_site_url);
			  // alert(store_link);
			  // alert(user_id);
			  var url="http://codewave.co.in/fqube/user/postProduct";
			  var type="post";
			  var data= {"products_site_url":products_site_url,"product_image":product_image,"product_description":product_description,"product_name":product_name,"user_id":user_id,"store_link":store_link,"product_currency":product_currency,"product_price":price} ;
			 
			  jQuery.ajax({ 
			  type:type,    
			  //url:'http://codewave.co.in/fqube/user/postProductMark/'+products_site_url+'/'+product_image+'/'+product_description+'/'+product_name+'/'+user_id+'/'+store_link+'/'+product_currency+'/'+price,    		  
			  url:url,
			  data:data,
			  success: function (data) 
			    {  
					if(data.status=='failure')
					{
					    jQuery('#loader3').hide();
					    toastr.error('You have already shared this post.');
						setTimeout (function() {window.close();},3000);
					}
					else
					{
					jQuery('#productID').val(data.product_info[0].product_id);
					jQuery('.done-div').append('<div style="padding: 0px 0 31px 0;font-size: 30px;color: #454545;font-weight: 100;color:#5AC3A2;text-align:center;">Done!</div><div style="text-align: center; margin-left: auto;margin-right: auto;"><a href="<?=$baseUrl;?>landing?product_id='+data.product_info[0].product_id+'&id=<?=$userId;?>&userName=<?=$userName;?>" target="_blank" onclick="closeTab(this);" class="feed_open" style="width: 50%;height:37px;background-color:#5AC3A2;padding: 8px 50px 8px 50px;text-align: center;color:white;border:none;border-radius:4px" type="button">See  It</a></div>');
					}
				}
				});
				});
</script>
<?php
include('footer1.php');
?>
<style>
</style>
<script>
</script>