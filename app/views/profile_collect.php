<?php
include('header2.php');
$user_id=$_REQUEST['id'];
$user_name=$_REQUEST['name'];

?>
<script src="<?=$baseUrl;?>template/slider/toastr.min.js"></script>
<link href="<?=$baseUrl;?>template/slider/toastr.min.css" rel="stylesheet"/>
<style>

div#save1_wrapper
{
overflow:inherit !important;
}
.followed
{
	border-color: #3b3b3b !important;
	text-decoration:none;
  background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%) !important;
  padding: 6px 15px!important;
}
div#edit_profile_wrapper
{
overflow:inherit !important;
}
.small-image img{
width:91px;
height:91px;
}
.about{
font-size:17px;
font-weight:100;
text-transform:none;
float:right;
color:#3b3b3b;
}
.about-me{
  width: 100%;
  border-radius: 3px;
  border: 1px solid #ccc;
  outline:none;

}
#name{
  width: 100%;
  border-radius: 3px;
  border: 1px solid #ccc;
  outline:none;

}
.half{
	padding: 19px 0px 0px 0px;
}
.btn-default2{
margin:33px;
}
.after{
margin-top: -28px;
  background: rgba(0, 0, 0, .6);
  z-index: 2;
  position: absolute;
  left: 198px;
  width: 148px;
  color: white;
  font-size: 12px;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
}
textarea {
    resize: none;
}
.noCollections
{
display:none;
}
.desc {
font-size: 22px;
}
.desc-text {
font-size: 17px;
line-height: 20px;
font-weight: lighter;
text-align: justify;
}
.btn-default2{

}
.one-liners{
color:#cccccc;
font-size:17px;
font-weight:lighter;
padding: 5px 0 5px 0px;
}
/*.glyphicon-tag{
transform:rotate(90deg);
}*/
.cover{
box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
padding:0px!important;
}
.tagging{
width:26%;
float:left;
padding:9px;
}
.tag-author{
width:74%;
float:left;
}
.blocks{
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
padding: 0px;
margin: 15px;
width:22%;
}
.edd_download_inner{
margin:0px!important;
padding:0px!important;
}
.tag-image{
border-radius:9px;
}
.hero-line{
margin-bottom:21px;
}
.similar-products{
color:#454545!important;
}
.new-short{
background:#ccc!important;
border:1px solid #ccc!important;
text-shadow:0 1px 0 #454545!important;
color:#454545!important;
}
.fa-heart{
color:white;
}
/*@media only screen and (max-width: 949px) {
.blocks{
width:auto!important;
}
}*/
a.close1 {
color: rgb(204,204,204);
display: block;
font-family: 'Varela Round', sans-serif;
font-size: 17px;
padding: 3px 9px 1px 9px;
position: absolute;
top: 1.25rem;
transition: all 400ms ease;
right: 1.25rem;
border: 1px solid #cccccc;
border-radius: 41px;
background-color: transparent!important; 
} 
	
	a.close1:hover {
		background-color: transparent!important; 
		cursor: pointer;
		color:#5AC3A2!important;
		border: 1px solid #5AC3A2;
	} 

.collects{
box-shadow: -4px 5px 5px 0px rgba(0, 0, 0, 0.1);
background:white;
padding-right:0px;
}
.main-image{
padding:15px 0px 15px 0px;
}
.small-image{
padding:0px 15px 15px 0px;
}
.follow{
float: right;
font-size: 13px;
border: none!important;
background: #5AC3A2;
color: white;
text-shadow: none;
}
.total{
padding-bottom:53px;
}

#tab1:hover{
	color:#5AC3A2!important;
	}
	#tab2:hover{
	color:#5AC3A2!important;
	}
	#tab3:hover{
	color:#5AC3A2!important;
	}
	#tab4:hover{
	color:#5AC3A2!important;
	}
	#tabs-2{
	display:none;
	}
	#tabs-3{
	display:none;
	}
	#tabs-5{
	display:none;
	}
	#tabs-6{
	display:none;
	}

.total{
padding-bottom:53px;
}
.follow-btn{
margin-top: 20px;
float: right;
font-weight: 100;
}
.follow-top {
width: 100%;
float: left;
margin-bottom: 12px;
}
.box-div {
background-color: #fff;
padding: 10px 0px 10px 0px;

border-radius: 2px;
box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);
}
.attachment-img {
border-radius: 5px;
height: 140px;
width: 140px;
margin-left: 12px;
}
.btn-default3:hover {
border-color: #3b3b3b;
background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);
}
.btn-default3 {
background-image: -webkit-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -o-linear-gradient(top,#fff 0,#e0e0e0 100%);
background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#e0e0e0));
background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe0e0e0', GradientType=0);
filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
background-repeat: repeat-x;
}
.btn3 {

outline: none;
padding: 6px 35px;
margin-bottom: 0;
font-size: 15px;
color:#fff;
font-weight: 100;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 3px;
background:#5AC3A2;
}
.attachment-img1{
border-radius:5px;
height:60px;
}
.upload-new {
  position: relative;
  overflow: hidden;
}
upload-new {
  color: #fff;
}
.upload-new {
  display: inline-block;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.upload-new input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  -ms-filter: 'alpha(opacity=0)';
  font-size: 200px;
  direction: ltr;
  cursor: pointer;
}
</style> 

<input type="hidden" id="user_profile_id" value="<?php echo $user_id;?>">
<input type="hidden" id="user_profile_name" value="<?php echo $user_name;?>">
<div class="row" style="padding: 24px 0px; background: none repeat scroll 0% 0% rgb(255, 255, 255);">
<div class="container" style="padding:12px 35px 0">
	<div class="col-md-3 left-image">
		<img src="" id="profile_picture" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square" style="width:153px;height:153px;float:right">
	</div>
	<div class="col-md-9 profile-desc">
			<div class="col-md-12 profile-name" style="padding: 0px 0px 5px;margin: 0px 0px -20px 0px;">
			<div class="col-md-6 name1" style="padding:0px">
			<span style="font-size:26px" id="profile_name"></span></div>
			<div class="col-md-6 follow-button" style="padding:0px">
			<button id="edit<?php echo $user_id;?>"  class="btn3 btn-default3 follow-btn edit_profile_open" style="display:none;">EDIT</button>
			<button id="<?php echo $user_id;?>" onclick="followUser(this);"  class="btn3 btn-default3 follow-btn" style="display:none;">FOLLOW</button>
			<input id="hiddenid-<?php echo $user_id;?>" type="hidden" value="0"></div>
			</div>
			<div class="col-md-12 person-desc" style="padding: 6px 0px 20px;  text-overflow: ellipsis;overflow: hidden;white-space: nowrap;width: 400px;">
			<span style="font-weight:lighter"><i><span id="aboutMe" ></span></i></span></div>
			<div class="col-md-12 profile-category" style="padding:0px;width:83%">
			<div class="col-md-3 sub-tab" style="padding:0px"><a href="javascript:void(0)" id="tab3" style="color:#454545"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>template/img/icon-3.png" alt=""></div>
			<div class="col-md-7" style=""><div style="color:#5AC3A2;font-size:24px" id="followers_count"></div><div style="font-size:15px"> followers</div></div></a></div>
			<div class="col-md-3 sub-tab" style="padding:0px"><a href="#tabs-4" style="color:#454545" id="tab4"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>template/img/profile-following.png" alt=""></div>
			<div class="col-md-7" style=""><div style="color:#5AC3A2;font-size:24px" id="following_count"></div><div style="font-size:15px"> following</div></div></a></div>
			<div class="col-md-3 sub-tab" style="padding:0px"><a href="javascript:void(0)" style="color:#454545" id="tab6"><div class="col-md-4" style="padding:0px"><img src="<?=$baseUrl;?>template/img/profile-store1.png" alt=""></div>
			<div class="col-md-7" style=""><div style="color:#5AC3A2;font-size:24px" id="store_count"></div><div style="font-size:15px"> stores</div></div></a></div>

</div>
</div>
	
</div>
</div>
 
<div class="row" style="padding: 24px 0px; ">
<div class="container" style="background:white;padding:0px;border:1px solid #ccc;width:78%!important">
	<div class="sub-tab" style="padding:10px;float:left;border-right:1px solid #ccc"><a href="javascript:void(0)" id="tab1" style="color:#5AC3A2">
			<div style="padding-right:0px"><div style="color:#5AC3A2;font-size:24px;" id="collection_count"></div><div style="font-size:15px"> collections</div></div></a></div>
			<div class="sub-tab" style="padding:10px;float:left;border-right:1px solid #ccc"><a href="javascript:void(0)" id="tab2" style="color:#454545">
			<div style=""><div style="color:#5AC3A2;font-size:24px;" id="products_count"></div><div style="font-size:15px"> products</div></div></a></div>
	
</div>
</div>
<div id="tabs-1">
 <div class="row" style="">
 <div class="container" style="padding-bottom:10px;padding-top:26px">
 <!--<div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);margin-bottom:40px"></div>-->
 <div class="col-md-12" id="collect_it">

  </div>
  </div> 
<div id="loader" style="margin-bottom: -10px;">
       <div class="cube"></div>
      </div>
   <div class="load-more" id="products_more"><button class="btn2 btn-default2" role="button">Load More Collections</button></div>  
  </div>
  <div class="row" style="background:#fff">
 <div class="container" style="padding-bottom:42px;padding-top:26px">
 <div class="similar-products"><h2>FROM COLLECTIONS I FOLLOW</h2></div>
 <div class="view-all-coll"></div>
 <div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>
  <div class="col-md-12" id="collection_prods">
  
 


  </div>
 
  </div>  
  </div>  
  </div>
  <div id="tabs-2">
  <div class="row" style="">
 <div class="container" style="padding-bottom:40px;padding-top:26px;padding-left:26px" id="prod_profile">

</div>
 <div id="loader1" style="margin-bottom: -10px;">
       <div class="cube"></div>
      </div>
<div class="col-md-4"></div>
<div class="col-md-4 view-collections" style="margin-top: 10px;margin-bottom: 30px;">
  <div class="load-more" style="margin-bottom:0px" id="product_loadMore"><button class="btn2 btn-default2" role="button" style="">View More Products</button></div></div>
  <div class="col-md-4"></div>
  </div>  
   <div class="row" style="background:#fff">
 <div class="container" style="padding-bottom:42px">
 <div class="similar-products"><h2>FROM PEOPLE I FOLLOW</h2></div>
 <div class="view-all-pro"></div>
 <div class="hero-line" data-ix="scroll-fade-out-21" style="transition: opacity 800ms, -webkit-transform 800ms; -webkit-transition: opacity 800ms, -webkit-transform 800ms; opacity: 1; -webkit-transform: translateX(0px) translateY(0px);"></div>
<div id="i_follow">

  

  </div>
  </div>  
  </div>  
  </div>  

  
  <div id="tabs-3" >
  <div class="row" style="margin-top: 26px;margin-bottom: 60px;">
 <div class="container" id="tablet-3">
 
 </div>
  </div>
    
 </div>

  
  
    <div id="tabs-5">
	<div class="row" style="margin-top: 26px;margin-bottom: 60px;">
 <div class="container" id="tablet-5">
 
 </div>
  </div>
  
 </div>
  
<div id="tabs-6">
<div class="row" style="margin-top: 26px;margin-bottom: 60px;">
 <div class="container" id="tablet-6">
 
 </div>
  </div>
 </div>
  
  
<?php
include('footer1.php');
?>

<script>
$("#tab1").click(function(){
jQuery('#tabs-1').show();
jQuery('#tab1').css({"color":"#5AC3A2"});
jQuery('#tab2').css({"color":"#454545"});
jQuery('#tab3').css({"color":"#454545"});
jQuery('#tab4').css({"color":"#454545"});
jQuery('#tab6').css({"color":"#454545"});
jQuery('#tabs-2').hide();
jQuery('#tabs-3').hide();
jQuery('#tabs-5').hide();
jQuery('#tabs-6').hide();
});
$("#tab2").click(function(){
jQuery('#tabs-1').hide();
jQuery('#tab2').css({"color":"#5AC3A2"});
jQuery('#tab1').css({"color":"#454545"});
jQuery('#tab3').css({"color":"#454545"});
jQuery('#tab4').css({"color":"#454545"});
jQuery('#tab6').css({"color":"#454545"});
jQuery('#tabs-2').show();
jQuery('#tabs-3').hide();
jQuery('#tabs-5').hide();
jQuery('#tabs-6').hide();
});
$("#tab3").click(function(){
jQuery('#tabs-1').hide();
jQuery('#tab3').css({"color":"#5AC3A2"});
jQuery('#tab6').css({"color":"#454545"});
jQuery('#tab2').css({"color":"#454545"});
jQuery('#tab1').css({"color":"#454545"});
jQuery('#tab4').css({"color":"#454545"});
jQuery('#tabs-3').show();
jQuery('#tabs-2').hide();
jQuery('#tabs-5').hide();
jQuery('#tabs-6').hide();
});
$("#tab4").click(function(){
jQuery('#tabs-3').hide();
jQuery('#tab4').css({"color":"#5AC3A2"});
jQuery('#tab2').css({"color":"#454545"});
jQuery('#tab1').css({"color":"#454545"});
jQuery('#tab3').css({"color":"#454545"});
jQuery('#tab6').css({"color":"#454545"});
jQuery('#tabs-5').show();
jQuery('#tabs-2').hide();
jQuery('#tabs-6').hide();
jQuery('#tabs-1').hide();
});
$("#tab6").click(function(){
jQuery('#tabs-3').hide();
jQuery('#tab6').css({"color":"#5AC3A2"});
jQuery('#tab2').css({"color":"#454545"});
jQuery('#tab1').css({"color":"#454545"});
jQuery('#tab3').css({"color":"#454545"});
jQuery('#tab4').css({"color":"#454545"});
jQuery('#tabs-5').hide();
jQuery('#tabs-6').show();
jQuery('#tabs-2').hide();
jQuery('#tabs-1').hide();
jQuery('#tabs-4').hide();
});

</script>




<script>
var id=jQuery('#user_profile_id').val();
var profile_id=jQuery('#user_id').val();
var pageNos=1;
var numProducts=3;
jQuery('#products_more').click(function(){
var url_id=jQuery('#user_profile_id').val();

var user_id=jQuery('#user_id').val();
jQuery('#loader').show();
pageNos=++pageNos;
numProducts=3;
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/NumofCollections/'+id+'/'+profile_id+'/'+pageNos+'/'+numProducts,
		cache: false,
		success: function(data) 
		{
			var m;
			//assign it to collection count
			jQuery('#collection_count').html(data.collection_count);
			
			if(data.count<3)
			{
				jQuery('#products_more').hide();
			}
			else
			{
				jQuery('#products_more').show();
			}
			if(length=='0')
			{
			    jQuery('#collect_it').html("<div class='message-error'>No collections Found.</div>");	
                jQuery('.message-error').attr("style","font-size: 20px;text-align: center;margin-top: 10px;margin-bottom: 40px;");
			}
			else
			{
			
			var i;
			var length=data.count;
			
			for(i=0;i<length;i++)
			{
				jQuery.ajax({
				type : 'get',  
				url: 'http://codewave.co.in/fqube/user/collectionNames/'+id+'/'+data.data[i]['collection_id'],
				cache: false,
				success: function(collections) 
				{ 
				var i;
				var image1;
				var image2;
				var image3;
				var image4;
				
				
				for(i=0;i<collections.data.length;i++)
				{	
				var collect1=""
				var length_collections=collections.data.length;
				if(length_collections==0)
				{
				 image1='http://codewave.co.in/fqube/template/img/thingsgifts.jpg';
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==0 && collections.data[i].parent=="Things I want as gifts")
				{
				 image1='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==0 && collections.data[i].parent=="Things I Like")
				{
				 image1='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==1)
				{
				 image1=collections.data[0].product_image;
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==2)
				{
				image1=	collections.data[0].product_image;
				 image2=collections.data[1].product_image;
				image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==3)
				{
				image1=collections.data[0].product_image;
				 image2=collections.data[1].product_image;
				 image3=collections.data[2].product_image;
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections>=4)
				{
				image1=collections.data[0].product_image;
				 image2=collections.data[1].product_image;
				 image3=collections.data[2].product_image;
				 image4=collections.data[3].product_image;
				}
			
				
				/* var m;
				alert(data.followed_collection_id.length);
					for(m=0;m<data.followed_collection_id.length;m++)
					{
					collect1="";
						alert(data.followed_collection_id[m].colletion_id);
						if(data.followed_collection_id[m].colletion_id==collections.data[i]['collection_id'])
						{
						var queryt=jQuery('#collection'+data.followed_collection_id[m].colletion_id).addClass('followed');
						
						var span='&nbsp;&nbsp;<span class="fa fa-check" id=""  aria-hidden="true"></span>';
					
				
						}
						else
						{
							var queryt=jQuery('#collection'+data.followed_collection_id[m].colletion_id).addClass('followed');
						}
						
						
					} */
					
						
						
						 collect1 += '<div class="col-md-4 total">'+
						'<div class="col-md-12 fqube-grey" style="padding-top:15px;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);padding-bottom: 12px;"><a href="/fqube/collection?collection_id='+collections.data[i]['collection_id']+'" style="color:#454545">'+collections.data[i].parent.substr(0,22)+'</a><input id="hiddenidcollection-collection'+collections.data[i]['collection_id']+'" type="hidden" value="0"> <button type="button" onclick="followCollection(this);" class="custom_class btn3 btn-default3 follow" id="collection'+collections.data[i]['collection_id']+'"><span class="coll_'+collections.data[i]['collection_id']+'">FOLLOW</span></button></div>'+
						'<div class="col-md-12 collects">'+
						'<a href="/fqube/collection?collection_id='+collections.data[i]['collection_id']+'"><div class="main-image" style="background:url('+image1+') 5px 8px no-repeat;background-size:100% 100%;border-radius:3px;height:250px;margin-bottom: 15px;margin-top: 8px;width:298px;"></div></a>'+
						'<div class="col-md-4 small-image"><img src="'+image2+'" style="border-radius:3px"/></div>'+
						'<div class="col-md-4 small-image"><img src="'+image3+'" style="border-radius:3px"/></div>'+
						'<div class="col-md-4 small-image"><img src="'+image4+'" style="border-radius:3px"/></div></div></div>'
			
				
				} 
				
					
						jQuery('#collect_it').append(collect1);
						
						
					for(m=0;m<data.followed_collection_id.length;m++)
			{
			if(data.followed_collection_id[m].collection_id==collections.data[i]['collection_id']){
				jQuery('#hiddenidcollection-collection'+data.followed_collection_id[m].collection_id).val("1");
			    jQuery('#collection'+data.followed_collection_id[m].collection_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			    jQuery('#collection'+data.followed_collection_id[m].collection_id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			  }
			else
			  {
			    jQuery('#hiddenidcollection-collection'+data.followed_collection_id[m].collection_id).val("0");
			    jQuery('#collection'+data.followed_collection_id[m].collection_id).html('FOLLOW');
			    jQuery('#collection'+data.followed_collection_id[m].collection_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			  }
				
				
			}
			if(url_id==user_id)
					{
						jQuery('.custom_class').attr('style','display:none!important');
					}
					else
					{
					    jQuery('.custom_class').attr('style','display:block!important');
					}
				
				}
			
				});
				
			}
			
			
			}
		jQuery('#loader').hide();
		}
		});




});
jQuery('document').ready(function(){
var url_id=jQuery('#user_profile_id').val();

var user_id=jQuery('#user_id').val();

jQuery('#loader').show();
var followed_id;
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/NumofCollections/'+id+'/'+profile_id+'/'+pageNos+'/'+numProducts,
		cache: false,
		success: function(data) 
		{
		   if(data.collection_count !=0){
			var m;
			//assign it to collection count
			jQuery('#collection_count').html(data.collection_count);
			
			if(data.collection_count>3)
			{
				jQuery('#products_more').show();
			}
			else
			{
				jQuery('#products_more').hide();
			}
			if(length=='0')
			{
				jQuery('#collect_it').html("<div class='message-error'>No collections Found.</div>");	
                jQuery('.message-error').attr("style","font-size: 20px;text-align: center;margin-top: 10px;margin-bottom: 40px;");
			}
			else
			{
			
			var i;
			var length=data.count;
			
			for(i=0;i<length;i++)
			{
				jQuery.ajax({
				type : 'get',  
				url: 'http://codewave.co.in/fqube/user/collectionNames/'+id+'/'+data.data[i]['collection_id'],
				cache: false,
				success: function(collections) 
				{
				var i;
				var image1;
				var image2;
				var image3;
				var image4;
				
				
				for(i=0;i<collections.data.length;i++)
				{	
				var collect1=""
				var length_collections=collections.data.length;
				if(!collections.data[i].product_name && collections.data[i].parent=="Things I want as gifts")
				{
				 image1='http://codewave.co.in/fqube/template/img/thingsgifts.jpg';
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(!collections.data[i].product_name && collections.data[i].parent=="Things I Like")
				{
				 image1='http://codewave.co.in/fqube/template/img/thingslike.jpg';
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==0)
				{
				 image1='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==1)
				{
				 image1=collections.data[0].product_image;
				 image2='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==2)
				{
				image1=	collections.data[0].product_image;
				 image2=collections.data[1].product_image;
				image3='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==3)
				{
				image1=collections.data[0].product_image;
				 image2=collections.data[1].product_image;
				 image3=collections.data[2].product_image;
				 image4='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections>=4)
				{
				image1=collections.data[0].product_image;
				 image2=collections.data[1].product_image;
				 image3=collections.data[2].product_image;
				 image4=collections.data[3].product_image;
				}
				
				/* var m;
				alert(data.followed_collection_id.length);
					for(m=0;m<data.followed_collection_id.length;m++)
					{
					collect1="";
						alert(data.followed_collection_id[m].colletion_id);
						if(data.followed_collection_id[m].colletion_id==collections.data[i]['collection_id'])
						{
						var queryt=jQuery('#collection'+data.followed_collection_id[m].colletion_id).addClass('followed');
						var span='&nbsp;&nbsp;<span class="fa fa-check" id=""  aria-hidden="true"></span>';
					
				
						}
						else
						{
							var queryt=jQuery('#collection'+data.followed_collection_id[m].colletion_id).addClass('followed');
						}
						
						
					} */
					
						
						
						 collect1 += '<div class="col-md-4 total">'+
						'<div class="col-md-12 fqube-grey" style="padding-top:15px;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);padding-bottom: 12px;"><a href="/fqube/collection?collection_id='+collections.data[i]['collection_id']+'" style="color:#454545">'+collections.data[i].parent.substr(0,22)+'</a><input id="hiddenidcollection-collection'+collections.data[i]['collection_id']+'" type="hidden" value="0"> <button type="button" onclick="followCollection(this);" class="customClass btn3 btn-default3 follow " id="collection'+collections.data[i]['collection_id']+'"><span class="coll_'+collections.data[i]['collection_id']+'">FOLLOW</span></button></div>'+
						'<div class="col-md-12 collects">'+
						'<a href="/fqube/collection?collection_id='+collections.data[i]['collection_id']+'"><div class="main-image" style="background:url('+image1+') 5px 8px no-repeat;background-size:100% 100%;border-radius:3px;height:250px;margin-bottom: 15px;margin-top: 8px;width:298px;"></div></a>'+
						'<div class="col-md-4 small-image"><img src="'+image2+'" style="border-radius:3px"/></div>'+
						'<div class="col-md-4 small-image"><img src="'+image3+'" style="border-radius:3px"/></div>'+
						'<div class="col-md-4 small-image"><img src="'+image4+'" style="border-radius:3px"/></div></div></div>'

				
				
				}
				
					
						jQuery('#collect_it').append(collect1).hide().fadeIn('slow');
							
			for(m=0;m<data.followed_collection_id.length;m++)
			{
			
				jQuery('#collection'+data.followed_collection_id[m].collection_id).addClass('followed');
				jQuery('#collection'+data.followed_collection_id[m].collection_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
				jQuery('#hiddenidcollection-collection'+data.followed_collection_id[m].collection_id).attr('value','1');
				
			}
				if(url_id==user_id)
				{
	
				jQuery('.customClass').attr('style','display:none !important');
				jQuery('.customClass').hide();
					}
				else
				{
	
					jQuery('.customClass').attr('style','display:block !important');
	
				}
				
				
				}
			
				});
				
			}
			
			
			}
		jQuery('#loader').hide();
		}
		else{
		jQuery('#collect_it').append('<div class="message-error">No collections Found.</div>');
		jQuery('#loader').hide();
		}
		}
		});

});

</script>

<!--Second collection-->
<script>
var id=jQuery('#user_profile_id').val();
var name=jQuery('#user_profile_name').val();
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/followingCollection/'+id,
		cache: false,
		success: function(data) 
		{
			var i;
			var length=data.count;
			//jQuery('#collection_count').html(length);
			if(length > 4){
			  jQuery('.view-all-coll').html('<a href="'+FQUBE.baseUrl+'collection-ifollow?id='+id+'&name='+name+'" style="color:#5AC3A2;text-decoration:underline;float:right">more<i class="fa fa-angle-right" style="padding-left:7px"></i></a>');	
			}
			else {
			 }
			 
			if(length==0)
			{
				jQuery('#collection_prods').html("<div class='message-error'>No collections Found.</div>");	
                jQuery('.message-error').attr("style","font-size: 20px;text-align: center;margin-top: 10px;margin-bottom: 40px;");
			}
			else
			{
			
			for(i=0;i<length;i++)
			{
		
				jQuery.ajax({
				type : 'get',  
				url: 'http://codewave.co.in/fqube/user/collectionNames/'+data.dataUser[i]['user_id']+'/'+data.data[i]['collections_id'],
				cache: false,
				success: function(follCollections) 
				{
				var i;
				var image11;
				var image22;
				var image33;
				var image44;
				
				for(i=0;i<follCollections.data.length;i++)
				{
				var collect ="";
				var length_collections=follCollections.data.length;
				if(length_collections==0)
				{
				 image11='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image22='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image33='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==1)
				{
				 image11=follCollections.data[0].product_image;
				 image22='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image33='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==2)
				{
				image11=	follCollections.data[0].product_image;
				 image22=follCollections.data[1].product_image;
				image33='http://codewave.co.in/fqube/template/assets/no-image.png';
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections==3)
				{
				image11=follCollections.data[0].product_image;
				 image22=follCollections.data[1].product_image;
				 image33=follCollections.data[2].product_image;
				 image44='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else if(length_collections>=4)
				{
				image11=follCollections.data[0].product_image;
				 image22=follCollections.data[1].product_image;
				 image33=follCollections.data[2].product_image;
				 image44=follCollections.data[3].product_image;
				}
				
					collect += '<div class="col-md-4 total">'+
  '<div class="col-md-12" style="background: #f6f6f6;padding-top:15px;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);padding-bottom: 12px;"><a href="/fqube/collection?collection_id='+follCollections.data[i].collection_id+'" style="color:#454545">'+follCollections.data[i].parent+'</a> <input id="hiddenidcollection-collection'+follCollections.data[i].collection_id+'" type="hidden" value="1"> <button type="button" onclick="followCollectionFollow(this);" class="btn3 btn-default3 hide-collection follow followed" id="collection'+follCollections.data[i].collection_id+'">FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span></button></div>'+
   '<div class="col-md-12 collects">'+
 '<a href="/fqube/collection?collection_id='+follCollections.data[i].collection_id+'"><div class="main-image" style="background:url('+image11+') 5px 8px no-repeat;background-size:100% 100%;border-radius:3px;height:250px;width:298px;margin-bottom: 15px;margin-top: 8px;"></div></a>'+
  '<div class="col-md-4 small-image"><img src="'+image22+'" style="border-radius:3px"/></div>'+
  '<div class="col-md-4 small-image"><img src="'+image33+'" style="border-radius:3px"/></div>'+
  '<div class="col-md-4 small-image"><img src="'+image44+'" style="border-radius:3px"/></div>'+
  '</div> </div>'

				
				
				}
					jQuery('#collection_prods').append(collect);
var url_id=jQuery('#user_profile_id').val();
var user_id=jQuery('#user_id').val();
if(url_id==user_id)
{
	jQuery('#'+url_id).hide();
	jQuery('#edit'+url_id).show();
}
else
{
	jQuery('#'+url_id).show();
	jQuery('#edit'+url_id).hide();
	jQuery('.hide-collection').hide();
}
					
				}
				});
			}
			}
		
		}
		});
</script>


<script>
jQuery('document').ready(function(){ 
var profile_id=jQuery('#user_profile_id').val();
var my_id=jQuery('#user_id').val();
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/getFollowersDetails1/'+profile_id+'/'+my_id,
		cache: false,
		success:function(data)
		{
		var j;
		var i;
		var repeat="";
		
		for(i=0;i<data.count;i++)
		{
	
			
			

		var tarper="";
			
			//alert(mecked);
			
		
				
		
			repeat +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+  
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'"><img src="'+data.data[i][0].profile_picture+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].username+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px" class="collection-count">'+data.data[i].length+'</span><br> products</span></div></div>'+ 
	                '<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].user_followers_count+'</span><br> followers</span></div></div></div>'+
					'</div></div><div class="col-md-4" style="z-index: 1000000000000;"><input id="hiddenid-followers_'+data.data[i][0].user_id+'" type="hidden" value="0"><button id="followers_'+data.data[i][0].user_id+'" onclick="followUserFollowers(this);" class="btn3 btn-default3 follow-btn"><span class="followers_html_'+data.data[i][0].user_id+'">FOLLOW</span> &nbsp;&nbsp;<span class="fa fa-check" id="followersClass_'+data.data[i][0].user_id+'" style="display:none;" aria-hidden="true"></span></button>'+
					'</div></div><div class="follow-below">'  
						
				for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')    
				{
				tarper="";	
					for(f=0;f<3;f++)
					{
					
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images_follow='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images_follow=data.data[i][j].product_image;
					
				}
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images_follow+'" class="attachment-img" alt=""></div>'
				
				}
				
			
					
			
			}
			repeat = repeat+tarper+'</div></div>'
			
			
			
		}
		
		jQuery('#tablet-3').append(repeat);
		
		var m;
		for(i=0;i<data.count;i++)
		{
		var mecked=data.data[i][0].user_id;
	
		var meckedFollowers='followers_html_'+data.data[i][0].user_id;
		var meckedClass='followersClass_'+data.data[i][0].user_id;
		
			//var id=data.data[i][0].user_id;
			for(m=0;m<data.follow_status.length;m++)
			{
			
				if(data.follow_status[m].following_user_id==data.data[i][0].user_id)
				{
				
				
				jQuery('#hiddenid-followers_'+data.follow_status[m].following_user_id).attr('value','1');
				jQuery('#followers_'+data.follow_status[m].following_user_id).addClass('followed');
				jQuery('.followers_html_'+data.follow_status[m].following_user_id).html('FOLLOWED');
				
				jQuery('#followersClass_'+data.follow_status[m].following_user_id).show();
			
				}
				else
				{
					//var saja=jQuery('.followersClass_'+data.follow_status[m].following_user_id).addClass('check_mark');
					//jQuery('#followers_'+data.follow_status[m].following_user_id).removeClass('followed');
					jQuery('#hiddenid-followers_'+data.data[m][0].following_user_id).attr('value','0');
					//jQuery('#followers_'+data.follow_status[m].following_user_id).html("FOLLOW");
					jQuery('#followers_'+data.follow_status[m].following_user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);');
				}
			}
		
	
		
		
		}
		
		
		//jQuery('.follow_repeat').append(tarper);
		}
		});

});
</script>

<script>
jQuery('document').ready(function(){
var profile_id=jQuery('#user_profile_id').val();
var my_id=jQuery('#user_id').val();
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/getFollowingUsersDetails/'+profile_id+'/'+my_id,
		cache: false,
		success:function(data)
		{
		if(data.count==0){
		jQuery('#tablet-5').append('<div class="message-error" style="margin-top: 10px;margin-bottom: 10px;">No People Found.</div>');
		}
		var j;
		var i;
		var repeat_following="";
		var f;
		
		for(i=0;i<data.count;i++)
		{
			var tarper_following="";
			repeat_following +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'"><img src="'+data.data[i][0].profile_picture+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].username+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px" class="collection-count">'+data.data[i].length+'</span><br> products</span></div></div>'+ 
	                '<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].user_followers_count+'</span><br> followers</span></div></div></div>'+
					'</div></div><div class="col-md-4" style="z-index: 1000000000000;"><input id="hiddenid-following_'+data.data[i][0].following_user_id+'" type="hidden" value="0"><button onclick="followUserFollowing(this);" id="following_'+data.data[i][0].following_user_id+'" class="btn3 btn-default3 follow-btn" padding: 6px 15px;"><span class="fol_html_'+data.data[i][0].following_user_id+'">FOLLOW</span> &nbsp;&nbsp;<span class="fa fa-check" id="followingClass_'+data.data[i][0].following_user_id+'" style="display:none;" aria-hidden="true"></span></button>'+
					'</div></div><div class="follow-below">'
			for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')
				{
				tarper_following="";
					for(f=0;f<3;f++)
					{
					
				tarper_following += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images=data.data[i][j].product_image;
				}
			
				tarper_following += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images+'" class="attachment-img" alt=""></div>'
				}
				
			
					
			
			}
			
			repeat_following = repeat_following+tarper_following+'</div></div>' 
			
		}
		jQuery('#tablet-5').append(repeat_following);
		var m;
		for(i=0;i<data.count;i++)
		{
		var mecked='following_'+data.data[i][0].following_user_id;
		var meckedClass='followingClass_'+data.data[i][0].following_user_id;
		var meckedHtml='fol_html_'+data.data[i][0].following_user_id;
			//var id=data.data[i][0].user_id;
			for(m=0;m<data.follow_status.length;m++)
			{
				
				if(data.follow_status[m].following_user_id==data.data[i][0].following_user_id) 
				{
				
				jQuery('#hiddenid-following_'+data.follow_status[m].following_user_id).attr('value','1');
				//jQuery('#following_'+data.follow_status[m].following_user_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
				jQuery('#following_'+data.follow_status[m].following_user_id).addClass('followed');
				var fol= jQuery('.fol_html_'+data.follow_status[m].following_user_id).html('FOLLOWED');
				jQuery('#followingClass_'+data.follow_status[m].following_user_id).show();
				//jQuery('#following_'+data.follow_status[m].following_user_id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			//jQuery('#following_'+data.follow_status[m].following_user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
				}
				else
				{
				
				//jQuery('#hiddenid-following_'+data.follow_status[m].following_user_id).attr('value','0');
				//jQuery('#following_'+data.follow_status[m].following_user_id).removeClass('followed');
					//jQuery('#following_'+data.data[i][0].following_user_id).html("FOLLOW");
					//jQuery('#following_'+data.follow_status[m].following_user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
				jQuery('#following_'+data.follow_status[m].following_user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);');
				}
			}
		
	
		
		
		}
		//jQuery('.follow_repeat').append(tarper);
		
		}
		});

});
</script>

<script>
jQuery('document').ready(function(){
var profile_id=jQuery('#user_profile_id').val();
var my_id=jQuery('#user_id').val();
jQuery.ajax({
		type : 'get',  
		url: 'http://codewave.co.in/fqube/user/getStoreDetails/'+profile_id+'/'+my_id,
		cache: false,
		success:function(data)
		{
		var j;
		var i;
		var repeat_stores="";
		var f;
		jQuery('#store_count').html(data.count);
		for(i=0;i<data.count;i++)
		{ 
			
			var tarper_stores="";
			repeat_stores +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'store-profile?store_id='+data.data[i][0].store_id+'"><img src="'+data.data[i][0].store_image+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'store-profile?store_id='+data.data[i][0].store_id+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].store_name+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px" class="collection-count">'+data.data[i].length+'</span><br> products</span></div></div>'+ 
	                '<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].store_follow_count+'</span><br> followers</span></div></div></div>'+
					'</div></div><div class="col-md-4" style="z-index: 1000000000000;"><input id="hiddenstoreid-store'+data.data[i][0].store_id+'" type="hidden" value="0"><button  id="store'+data.data[i][0].store_id+'" onclick="followStore(this);" class="btn3 btn-default3 follow-btn"><span id="store_html_'+data.data[i][0].store_id+'">FOLLOW</span> &nbsp;&nbsp;<span class="fa fa-check" id="storeClass_'+data.data[i][0].store_id+'" style="display:none;" aria-hidden="true"></span></button>'+
					'</div></div><div class="follow-below">'
					
			for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')
				{
				tarper_stores="";
					for(f=0;f<3;f++)
					{
					
				tarper_stores += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images=data.data[i][j].product_image;
				}
				tarper_stores += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images+'" class="attachment-img" alt=""></div>'
				}
				
			
					
			
			}
			
			repeat_stores = repeat_stores+tarper_stores+'</div></div>' 
			
		}
		jQuery('#tablet-6').append(repeat_stores);
		var m;
		
		for(i=0;i<data.count;i++)
		{
			
		
		for(m=0;m<data.follow_status.length;m++)
		{
		
		
			if(data.data[i][0].store_id==data.follow_status[m].following_store_id)
			{
				jQuery('#store'+data.follow_status[m].following_store_id).addClass('followed');
				jQuery('#storeClass_'+data.follow_status[m].following_store_id).show('');
				jQuery('#store_html_'+data.follow_status[m].following_store_id).html('FOLLOWED');
			jQuery('#hiddenstoreid-store'+data.follow_status[m].following_store_id).attr('value','1');
			}
			else
			{
			//jQuery('#hiddenstoreid-store'+data.follow_status[m].following_store_id).attr('value','0');
			//jQuery('#'+data.follow_status[i].following_store_id).removeClass('followed');
			}
		//var mecked='store'+data.follow_status[i].following_store_id; 
		
			
		
	
		
		
		}
		}
		//jQuery('.follow_repeat').append(tarper);
		}
		});

});
</script>
<script src="<?=$baseUrl;?>template/fcubejs/jquery.popupoverlay.js"></script>
<script>
jQuery('#edit_profile').popup({

 transition: 'all 0.3s'
});
</script>

<script>
jQuery(document).ready(function(){

jQuery('.image-container').hover(function(){
        jQuery('.after').show();
},function(){
        jQuery('.after').hide();
});
});
</script>
<script>
jQuery(document).ready(function(){ 
jQuery('.after').hover(function(){
        jQuery('.after').show();
},function(){
        jQuery('.after').hide();
});
});
</script>

<script>
jQuery('document').ready(function()
{
var url_id=jQuery('#user_profile_id').val();
var user_id=jQuery('#user_id').val();
if(url_id==user_id)
{
	jQuery('#'+url_id).hide();
	jQuery('#edit'+url_id).show();
}
else
{
	jQuery('#'+url_id).show();
	jQuery('#edit'+url_id).hide();
	//jQuery('.hide-collection').hide();
}
});

</script>



 <!--fsa<script type="text/javascript">                                     
           jQuery('#save_it').click(function() {
			//alert($('#chkveg').val());
			var name = document.getElementById('edit_name').value;
			var about = document.getElementById('edit_about').value;
			
			
				
				if(name == "")
				{
					jQuery('#edit_name').css({"border":"1px solid red"});
					jQuery('#error').show('slow');
				}
				else
				{
					jQuery('#edit_name').css({"border":"1px solid #ccc"});
					jQuery('#error').hide('slow');
				}
				if(about == "")
				{
					jQuery('#edit_about').css({"border":"1px solid red"});
					jQuery('#error').show('slow');
				}
				else
				{
					jQuery('#edit_about').css({"border":"1px solid #ccc"});
					jQuery('#error').hide('slow');
				}
			if((name != "") && (about != ""))
				jQuery.ajax({
				type : 'POST',  
				data :{name:name,about:about},    
				url: 'edit_pre_coop_details_new_kn.php',
				dataType: 'text',
				cache: false,
				success: function(data)   
				{	
				
				}
				 
				
				});
				
				});
			
				
                     
                        </script>-->

 <script src="<?=$baseUrl;?>template/fcubejs/profile.js"></script>

 