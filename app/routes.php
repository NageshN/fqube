<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//-----------------------------
//routing for users Starts here 
//-----------------------------
Route::group(array('prefix' => 'user'), function()
{ 
	
	//naegsh  
	//user signin  
	Route::post('signin/{emailId}/{password}', 'UserController@signin');
	//user Signup
	Route::post('signup/{emailId}/{name}/{password}', 'UserController@signup');
		 
			//forgot password
			Route::get('forgotPassword/{email}',array('uses'=>'UserController@forgotPassword'));
			Route::get('checkPassword/{password}/{userId}',array('uses'=>'UserController@checkPassword'));
			Route::get('updatePassword/{password}/{userId}',array('uses'=>'UserController@updatePassword'));
			Route::get('facebookApi/{image}/{title}/{desc}',array('uses'=>'UserController@facebookApi'));
			Route::get('getMyCollections/{userId}',array('uses'=>'CollectionController@getMyCollections')); 
			Route::post('collectProduct',array('uses'=>'CollectionController@collectProduct'));
			Route::post('collectProductOnUrl',array('uses'=>'CollectionController@collectProductOnUrl'));
			Route::post('likeProduct',array('uses'=>'ProductController@likeProduct'));
			Route::get('searchProduct/{searchText}',array('uses'=>'ProductController@searchProduct'));
			Route::get('myfeed/{userId}/{num_products}/{page_nos}',array('uses' => 'UserController@myfeed')); 
			Route::get('highestNoOfCollection',array('uses' => 'UserController@highestCollection'));
			Route::get('highestNoOfLike/{userId}/{num_products}/{page_nos}',array('uses' => 'UserController@highestLike'));
			//Route::get('collectionofProducts',array('uses' => 'UserController@products_of_collection'));
			//pending
			Route::post('postProduct','ProductController@postProduct');
			Route::get('postProductMark/{products_site_url}/{product_image}/{product_description}/{product_name}/{user_id}/{store_link}/{product_currency}/{price}','ProductController@postProductMark');
			
			Route::get('product','ProductController@product');
	 		Route::get('collectionofProducts/{coll_id}','UserController@products_of_collection');
			Route::get('getProductDetails/{productId}','ProductController@getProductDetails');
			Route::get('getProductAbusedResult/{userId}/{productId}','ProductController@getProductAbusedResult');
			Route::get('getProductFromTheSameCategory/{categoryId}/{productId}/{userId}','ProductController@getProductFromTheSameCategory');
 			Route::get('sameStore/{storeId}','ProductController@sameStore');
			Route::get('getProductFromThePostedUser/{productId}','ProductController@getProductFromThePostedUser');
			Route::get('abuseAnProduct/{userId}/{productId}','ProductController@abuseAnProduct');
			//no of products in that Category starts
			Route::get('noOfProductsinCategory/{categoryId}','ProductController@noOfProductsinCategory');
			//no of products in user profile starts
            Route::get('noOfProductsOfUser/{userId}','ProductController@noOfProductsOfUser');
			Route::get('homePageProduct/{userId}','ProductController@homePageProduct');
			Route::post('createDefaultCollection/{userId}','CollectionController@createDefaultCollection');
			Route::post('followCollection','CollectionController@followCollection');
			Route::post('unFollowCollection','CollectionController@unFollowCollection');
			Route::get('followingCollection/{id}','UserController@collectionfollowing');
			Route::get('followersProfileproducts/{id}','UserController@followers_profile_products');
			Route::get('editUser/{id}/{name}','UserController@editUser');
			  
			 
			Route::get('collectionProfile/{userId}/{collectionid}/{pageNumber}/{numberOfProduct}','CollectionController@collectionProfile');
			Route::get('collectionProfileNew/{userId}/{collectionid}/{pageNumber}/{numberOfProduct}','CollectionController@collectionProfile');
			Route::get('collectionProfileDetails/{collectionid}','CollectionController@collectionProfileDetails');
			Route::get('collectionFollowersDetails/{collectionId}','CollectionController@collectionFollowersDetails');
		//	Route::get('/{collectionId}','CollectionController@collectionFollowersDetails');
		Route::get('userProfile/{userId}','UserController@userProfile');
		Route::get('collectionFollowCount/{collectionId}/{pageNumber}/{numberOfProduct}','CollectionController@collectionFollowCount');
		Route::get('Ifollow/{id}','UserController@peopleIfollow');
		Route::get('homePageproducts','UserController@homePageproducts');
		Route::get('getProductsByCategory/{categoryId}/{pageNumber}/{numberOfProduct}','CategoryProductController@getProductsByCategory');
				//delete product
		Route::post('deleteProduct/{productId}','UserController@deleteProduct');		
	
	 
	//facebook login
	Route::get('loginWithFacebook',array('as'=>'loginWithFacebook','uses'=>'UserController@loginWithFacebook'));
	//gmail login 
	Route::get('loginWithGoogle',array('uses'=>'UserController@loginWithGoogle'));
	 
	 
	//profile followers
	Route::get('getFollowersDetails/{userId}','UserController@getFollowersDetails');
	Route::get('getFollowersDetails1/{userId}/{profile_id}','UserController@getFollowersDetails1');  
	 
	Route::get('getFollowingUsersDetails/{userId}/{profile_id}','UserController@getFollowingUsersDetails');
	Route::post('editProfile','UserController@editProfile');
	Route::get('changePasswordset/{password}/{email}','UserController@changePasswordset');
	//follow Collection details by people
	Route::get('getFollowPeopleDetailsCollection/{userId}/{profile_id}/{user}','CategoryProductController@getFollowPeopleDetailsCollection');
	//follow Store details by people
	Route::get('getFollowPeopleDetailsStore/{userId}/{profile_id}/{user}','CategoryProductController@getFollowPeopleDetailsStore');
	//store list in register page
	Route::get('getStoreDetailsToFollow/{storeId}/{user}','CategoryProductController@getStoreDetailsToFollow');
	//people list in register page
	Route::get('getPeopleDetailsToFollow/{userId}/{user}','CategoryProductController@getPeopleDetailsToFollow');
	
	Route::get('getStoreDetails/{userId}/{profile_id}','UserController@getStoreDetails');
	//praveen
	//getCategoryBasedUsers
	Route::get('getCategoryBasedUsers/{category}/{pageNumber}/{numberOfProduct}','FollowController@getCategoryBasedUsers');
	Route::post('categorytag/{user_id}/{category}','categoryController@categoryTag'); 
	
	Route::post('categorytagRemove/{user_id}/{categoryId}','categoryController@categoryTagRemove'); 
	 
	Route::post('follow/{user_id}/{following_user_id}','FollowController@followUser'); 
	Route::post('unfollow/{user_id}/{following_user_id}','FollowController@unfollowUser'); 
	Route::post('storeFollow/{user_id}/{following_user_id}','storeController@storeFollow'); 
	Route::post('storeUnfollow/{user_id}/{id}','storeController@storeUnfollow');
	Route::get('getAllCategories','categoryController@getAllCategories');
	Route::get('getAllCategoriesSelected/{user_id}','categoryController@getAllCategoriesSelected');
	Route::get('getAllStoreFollowedByUser/{user_id}','categoryController@getAllStoreFollowedByUser');
	Route::get('searchByCategoryName/{searchStr}','categoryController@searchByCategoryName');
	Route::get('searchByUserName/{searchStr}','FollowController@searchByUserName');
	Route::get('getUserDetails/{id}/{profile_id}','UserController@getUserDetails'); 
	Route::get('getFollowedUsers/{id}','UserController@getFollowedUsers'); 
	Route::get('getAllStores/{pageNumber}/{numberOfProduct}','storeController@getAllStores'); 
	Route::get('searchByStore/{str}','storeController@searchByStore'); 
	Route::get('followingUsers/{id}','UserController@followingUsers');
	Route::get('ProductsofUsers/{id}/{pageNos}/{productNos}','UserController@usersProducts');
	//Route::get('collectionsOfUser/{id}','UserController@userCollection');
	Route::get('NumofCollections/{id}/{profile_id}/{pageNos}/{numProducts}','UserController@numCollections');
	Route::get('collectionNames/{id}/{collectionId}','UserController@collection_name');
	Route::get('getUserCollections/{id}','UserController@getUserCollections');
	Route::get('storeProfile/{storeId}','storeController@storeProfile');
	Route::get('storeProfileUserFollowStatus/{storeId}/{userId}','storeController@storeProfileUserFollowStatus');
	Route::get('collectionProfileUserFollowStatus/{collectionId}/{userId}','CollectionController@collectionProfileUserFollowStatus');
	Route::get('getProductsByStore/{storeId}/{pageNumber}/{numberOfProduct}','storeController@getProductsByStore');
	  
	Route::get('getStoreFollowingUsers/{storeId}/{pageNumber}/{numberOfProduct}','storeController@getStoreFollowingUsers');
	Route::get('test','storeController@test');
	Route :: post('logout',function(){
		Auth::logout();
		Session::put('userInfo',null);
		return array('status'=>'success');
	});
    
});
 

 //Admin Panel Apis
Route::group(array('prefix' => 'admin'), function()
{ 
 Route::post('signinadmin/{emailId}/{password}', 'AdminController@signinadmin');
 Route::get('getAbusedProducts', 'AdminController@getAbusedProducts');  
 Route::get('deleteProduct/{productId}', 'AdminController@deleteProduct');  
 Route::get('getProductsTaggedToOthers', 'AdminController@getProductsTaggedToOthers');  
 
});

Route::group(array('before'=>'auth'), function()
{
Route::get('/my-feed',array('uses'=>'UserController@userFeed'));
Route::get('/myfeed', function()
{
	return View::make('myfeed');
});
Route::get('/peopletofollow', function()
{
	return View::make('peopletofollow');
});
Route::get('/storetofollow', function()
{
	return View::make('storetofollow');
});
Route::get('/tagcategory', function()
{
	return View::make('tagcategory');
});
Route::get('/test', function()
{
	return View::make('test');
});
Route::get('/post', function()
{
	return View::make('post');
});
Route::get('/select-follow', function()
{
	return View::make('select-follow');
});
Route::get('/changePassword', function()
{
	return View::make('changePassword');
});
Route::get('/post-url', function()
{
	return View::make('post-url');
});
Route::get('/landing', function()
{
	return View::make('product_landing');
});
Route::get('/category_products', function()
{
	return View::make('category-men'); 
});
Route::get('/collection', function()
{
	return View::make('collection');
});
Route::get('/profile', function()
{
	return View::make('profile_collect');
});
Route::get('/store-profile', function()
{
	return View::make('store-profile');
});
Route::get('/collection-ifollow', function()
{
	return View::make('collection-ifollow');
});
Route::get('/aboutus', function()
{
	return View::make('aboutus');
});
Route::get('/howitworks', function()
{
	return View::make('howitworks');
});
Route::get('/store', function()
{
	return View::make('store');
});
}); 

Route::get('/aboutus', function()
{
	return View::make('aboutus');
});
Route::get('/howitworks', function()
{
	return View::make('howitworks');
});
Route::get('/store', function()
{
	return View::make('store');
});
Route::get('/adminPanel', function()
{
	return View::make('index');
});
Route::get('/adminHome', function()
{
	return View::make('view-abused-products');
});
Route::get('/tagProducts', function()
{
	return View::make('view-products-tagto-others');
});
Route::get('/deleteProducts', function()
{
	return View::make('delete-abused-products');
});
Route::get('/login-new1', function()
{
	return View::make('home_login');
});


//---------------------------
//routing for users Ends here 
//---------------------------




//-----------------------------
//routing for users Starts here 
//-----------------------------
Route::group(array('prefix' => 'admin'), function()
{ 
	//admin signin
	Route::get('/', 'AdminController@home');
	
	Route::post('signin/{emailId}/{password}', 'UserController@signin');
	


});
//---------------------------
//routing for users Ends here 
//---------------------------


//Route::get('sign-in/{email}/{password}','AccountController@postSignIn');

Route::get('/', function()
{
	return View::make('home');
});

Route::get('/login', function()
{
	return View::make('home');
}); 
Route::get('/loginpage', function()
{
	return View::make('home_login');
});

Route::get('/name', function()
{
	$users=DB::table('users')->get();   
	return $users;
}); 
Route::group(array('before' => 'guest'), function(){

Route::group(array('before' => 'csrf'), function(){
 
Route::post('account/create',array('as' =>'account-create-post',
							'uses' => 'AccountController@postCreate'
));


Route::post('account/signin',array('as' =>'account-sign-in-post',
							'uses' => 'AccountController@postSignIn'
)); 
});
Route::get('account/signin',array('as' =>'account-sign-in',
							'uses' => 'AccountController@getSignIn'
));
Route::get('account/create',array('as' =>'account-create',
							'uses' => 'AccountController@getCreate'
));

//Route::get('/register/{email}/{username}','AccountController@getCreate');

});

/*
Route::get('users/{login}','loginController@login');
*/




//-----------------------------
//routing for Mobile App Starts here 
//-----------------------------
Route::group(array('prefix' => 'app'), function()
{ 
	//keerthi
	//user signin 
	Route::post('signin/{loginInfo}','AppUserController@signin');
	//user Signup
	Route::post('signup/{signupInfo}','AppUserController@signup');
		
			//forgot password
			Route::get('forgotPassword/{email}',array('uses'=>'UserController@forgotPassword'));
			Route::get('getMyCollections/{userId}',array('uses'=>'CollectionController@getMyCollections'));
			Route::post('collectProduct',array('uses'=>'CollectionController@collectProduct'));
			Route::post('likeProduct',array('uses'=>'ProductController@likeProduct'));
			Route::get('searchProduct/{searchText}',array('uses'=>'ProductController@searchProduct'));
			Route::get('myfeed/{userId}/{num_products}/{page_nos}',array('uses' => 'UserController@myfeed')); 
			Route::get('highestNoOfCollection',array('uses' => 'UserController@highestCollection'));
			Route::get('highestNoOfLike/{num_products}/{page_nos}',array('uses' => 'UserController@highestLike'));
			//Route::get('collectionofProducts',array('uses' => 'UserController@products_of_collection'));
			//pending
			Route::post('postProduct','ProductController@postProduct');
			
			Route::get('product','ProductController@product');
			Route::get('collectionofProducts/{coll_id}/{id}','UserController@products_of_collection');
			Route::get('getProductDetails/{productId}','ProductController@getProductDetails');
			Route::get('getProductFromTheSameCategory/{categoryId}/{productId}','ProductController@getProductFromTheSameCategory');
			Route::get('sameStore/{storeId}','ProductController@sameStore');
			Route::get('getProductFromThePostedUser/{productId}','ProductController@getProductFromThePostedUser');
			Route::get('abuseAnProduct/{userId}/{productId}','ProductController@abuseAnProduct');
			Route::get('homePageProduct/{userId}','ProductController@homePageProduct');
			Route::post('followCollection','CollectionController@followCollection');
			Route::post('unFollowCollection','CollectionController@unFollowCollection');
			Route::get('followingCollection/{id}','UserController@collectionfollowing');
			Route::get('followersProfileproducts/{id}','UserController@followers_profile_products');
			Route::get('editUser/{id}/{name}','UserController@editUser');
			  
			 
			Route::get('collectionProfile/{userId}/{collectionid}/{pageNumber}/{numberOfProduct}','CollectionController@collectionProfile');
			Route::get('collectionProfileNew/{userId}/{collectionid}/{pageNumber}/{numberOfProduct}','CollectionController@collectionProfile');
			Route::get('collectionProfileDetails/{collectionid}','CollectionController@collectionProfileDetails');
			Route::get('collectionFollowersDetails/{collectionId}','CollectionController@collectionFollowersDetails');
		//	Route::get('/{collectionId}','CollectionController@collectionFollowersDetails');
		Route::get('userProfile/{userId}','UserController@userProfile');
		Route::get('collectionFollowCount/{collectionId}/{pageNumber}/{numberOfProduct}','CollectionController@collectionFollowCount');
		Route::get('Ifollow/{id}','UserController@peopleIfollow');
		Route::get('homePageproducts','UserController@homePageproducts');
		Route::get('getProductsByCategory/{categoryId}/{pageNumber}/{numberOfProduct}','CategoryProductController@getProductsByCategory');

	
	 
	//facebook login
	Route::post('loginWithFacebook/{loginInfo}',array('as'=>'loginWithFacebook','uses'=>'AppUserController@loginWithFacebook'));
	//gmail login
	Route::get('loginWithGoogle',array('uses'=>'UserController@loginWithGoogle'));
	 
	 
	//profile followers 
	Route::get('getFollowersDetails/{userId}','UserController@getFollowersDetails');
	Route::get('getFollowersDetails1/{userId}/{profile_id}','UserController@getFollowersDetails1');  
	 
	Route::get('getFollowingUsersDetails/{userId}/{profile_id}','UserController@getFollowingUsersDetails');
	Route::post('editProfile','UserController@editProfile');
	
	//follow Collection details by people
	Route::get('getFollowPeopleDetailsCollection/{userId}/{profile_id}/{user}','CategoryProductController@getFollowPeopleDetailsCollection');
	//follow Store details by people
	Route::get('getFollowPeopleDetailsStore/{userId}/{profile_id}/{user}','CategoryProductController@getFollowPeopleDetailsStore');
	//store list in register page
	Route::get('getStoreDetailsToFollow/{storeId}/{user}','CategoryProductController@getStoreDetailsToFollow');
	//people list in register page
	Route::get('getPeopleDetailsToFollow/{userId}/{user}','CategoryProductController@getPeopleDetailsToFollow');
	 
	Route::get('getStoreDetails/{userId}/{profile_id}','UserController@getStoreDetails');
	//praveen 
	//getCategoryBasedUsers
	Route::get('getCategoryBasedUsers/{categoryIdInfo}','AppFollowController@getCategoryBasedUsers');
	Route::post('categorytag/{categoryInfo}','AppcategoryController@categoryTag');  
	
	Route::post('categorytagRemove/{categoryInfo}','AppcategoryController@categoryTagRemove'); 
	 
	Route::post('follow/{userIdInfo}','AppFollowController@followUser'); 
	Route::post('unfollow/{userIdInfo}','AppFollowController@unfollowUser'); 
	Route::post('storeFollow/{userIdInfo}','AppstoreController@storeFollow'); 
	Route::post('storeUnfollow/{userIdInfo}','AppstoreController@storeUnfollow');
	Route::get('getAllCategories','AppcategoryController@getAllCategories');
	Route::get('getAllCategoriesSelected/{user_id}','categoryController@getAllCategoriesSelected');
	Route::get('searchByCategoryName/{searchStr}','categoryController@searchByCategoryName');
	Route::get('searchByUserName/{searchStr}','FollowController@searchByUserName');
	Route::get('getUserDetails/{id}/{profile_id}','UserController@getUserDetails'); 
	Route::get('getFollowedUsers/{id}','UserController@getFollowedUsers'); 
	Route::get('getAllStores/{storeInfo}','AppstoreController@getAllStores'); 
	Route::get('searchByStore/{str}','storeController@searchByStore'); 
	Route::get('followingUsers/{id}','UserController@followingUsers');
	Route::get('ProductsofUsers/{id}/{pageNos}/{productNos}','UserController@usersProducts');
	//Route::get('collectionsOfUser/{id}','UserController@userCollection');
	Route::get('NumofCollections/{id}/{profile_id}/{pageNos}/{numProducts}','UserController@numCollections');
	Route::get('collectionNames/{id}/{collectionId}','UserController@collection_name');
	
	  

	Route::get('getUserCollections/{id}','UserController@getUserCollections');
	Route::get('storeProfile/{storeId}','storeController@storeProfile');
	Route::get('storeProfileUserFollowStatus/{storeId}/{userId}','storeController@storeProfileUserFollowStatus');
	Route::get('collectionProfileUserFollowStatus/{collectionId}/{userId}','CollectionController@collectionProfileUserFollowStatus');
	Route::get('getProductsByStore/{storeId}/{pageNumber}/{numberOfProduct}','storeController@getProductsByStore');
	 
	Route::get('getStoreFollowingUsers/{storeId}/{pageNumber}/{numberOfProduct}','storeController@getStoreFollowingUsers');
	Route::get('test','storeController@test');
	Route :: post('logout',function(){
		Auth::logout();
		Session::put('userInfo',null);
		return array('status'=>'success');
		
	});

});