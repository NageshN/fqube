<?php

class AdminController extends BaseController{
	public function home(){
		return View :: make('home');
	}
	//Admin login function
	public function signinadmin($username,$passwrod){ 
			$userdata = array(
				'email_id' => $username,
				'password' => $passwrod
			);
				//check weather he is a first time login 
				//check he has selected min 3 category
				$adminInfo = Admin :: getAdminInfo($userdata['email_id']);
				if(count($adminInfo) > 0){
				Session::put('admin_id', $adminInfo[0]['admin_id']);
				Session::put('email_id', $adminInfo[0]['email_id']);
				$response = array('status' => 'success','response' => 'admin login success','user_info' => $adminInfo);
			    }
				else{
				$response = array('status' => 'failure','response'=> 'login failure');
				}
			return $response;
	}
	
	//get all abused products
	public function getAbusedProducts(){
		$result = Admin::getAbusedProducts();
		return $result;
	}
	//get product to be deleted
	public function deleteProduct($productId){
		$result = Admin::deleteProduct($productId);
		return $result;
	}
	//get all abused products
	public function getProductsTaggedToOthers(){
		$result = Admin::getProductsTaggedToOthers();
		return $result;
	}
}