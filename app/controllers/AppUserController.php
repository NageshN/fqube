<?php

class AppUserController extends BaseController{
	
	public function signin($loginInfo){
			$loginInfo = base64_decode($loginInfo);
		    $loginInfo = json_decode($loginInfo,true);
	        $username = $loginInfo['username'];
	        $passwrod = $loginInfo['password'];
			
			$userdata = array(
				'email_address' => $username,
				'password' => $passwrod
			);
		    $userInfo = User :: getUserInfo($userdata['email_address']);
			if(count($userInfo) == 0){
			   $response = array('status' => 'failure','response_code'=> '201','response'=> 'user not registered');
			}
			else{
			/* Check if 'Remember Me' is checked */
			
			$isAuth = Auth::attempt($userdata);
			if($isAuth)
			{
				//check weather he is a first time login 
				//check he has selected min 3 category
				$userInfo = User :: getUserInfo($userdata['email_address']);
			    Session::put('userInfo', $userInfo[0]['user_id']);
				Session::put('username', $userInfo[0]['username']);
				Session::put('profile_picture', $userInfo[0]['profile_picture']);
				$category = CategoryModel :: getCategory($userInfo[0]['user_id']);
				if(count($category) > 0){
					
					$response = array('status' => 'success','response_code'=> '200','response' => 'not first time login user','user_info' => $userInfo);
				}
				else{
					
					$response = array('status' => 'success','response_code'=> '100','response' => 'first Time login user','user_info' => $userInfo);
				}
			 
			}
			
			else{
				$response = array('status' => 'failure','response_code'=> '202','response'=> 'invalid login credential');
			}
			}
			return $response;
	}
	//user signup
	public function signup($signupInfo){
	        $signupInfo = base64_decode($signupInfo);
		    $signupInfo = json_decode($signupInfo,true);
	        $email = $signupInfo['email_id'];
	        $name = $signupInfo['name'];
	        $password = $signupInfo['password'];
			
		$details = array('username' => $name, 'email_address' => $email, 'password' => $password, 'registerd_through' => 'self');
		$response = User :: createUser($details);
		if($response['status'] == 'success'){
		    $loginInfo = array('username' => $response['email_address'], 'password' => $response['password']);
			$loginInfo = json_encode($loginInfo,true);
			$loginInfo = base64_encode($loginInfo);
			$response = $this->signin($loginInfo);
		}
		else{
			$response = $response;
		}
		return $response;
	}
	
	//login with facebook
	public function loginWithFacebook($loginInfo) {
            $loginInfo = base64_decode($loginInfo);
		    $loginInfo = json_decode($loginInfo,true);
	        $email = $loginInfo['email'];
	        $name = $loginInfo['name'];
	        $first_name = $loginInfo['first_name'];
	        $last_name = $loginInfo['last_name'];
	        $id = $loginInfo['id'];
	        $gender = $loginInfo['gender'];
    
	    $resultFromFacebook = array(
				'email' => $email,
				'name' => $name,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'id' => $id,
				'gender' => $gender
			);
			
		$result = User ::createUserFromFacebook($resultFromFacebook);
		
		if($result == 409)
			{
				$response = array('status' => 'faiure','response_code'=> '301','response' => 'email id already exist');
				
			}
		else if($result)
			{
				
				//$response = $this->signin($resultFromFacebook['email'],'fqubefbuser');
				$userdata = array(
					'email_address' => $resultFromFacebook['email'],
					'password' => 'fqubefbuser'
				);

				/* Check if 'Remember Me' is checked */

				$isAuth = Auth::attempt($userdata);
				if($isAuth)
				{

					//check weather he is a first time login 
					//check he has selected min 3 category
					$userInfo = User :: getUserInfo($userdata['email_address']);
					Session::put('userInfo', $userInfo[0]['user_id']);
					Session::put('username', $userInfo[0]['username']);
					Session::put('profile_picture', $userInfo[0]['profile_picture']);
					$category = CategoryModel :: getCategory($userInfo[0]['user_id']);
					if(count($category) > 0){
                     $response = array('status' => 'success','response_code'=> '300','response'=> 'first time login success');
					//return Redirect::to('select-follow');
					} 
					else{
                     $response = array('status' => 'success','response_code'=> '303','response'=> 'login success');
					//return Redirect::to('my-feed');
					}

				}
				else{
					$response = array('status' => 'failure','response_code'=> '302','response'=> 'login failure');
				}
				return $response;
				
			}
		else{
				$response = array('status' => 'faiure','response_code'=> '301','response' => 'Login With Facebook Failed');
			}
		return $response;

}
		
	public function loginWithGoogle() {

    // get data from input
    $code = Input::get( 'code' );

    // get google service
    $googleService = OAuth::consumer( 'Google' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from google, get the token
        $token = $googleService->requestAccessToken( $code );

        // Send a request with it
        $resultFromGoogle = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );
		
		$result = User::createUserFromGoogle($resultFromGoogle);
		
		if($result == 409)
			{
				$response = array('status' => 'faiure','response' => 'email id already exist');
				
			}
		else if($result)
			{
				// we are now logged in, go to home
				//$response = $this->signin($resultFromGoogle['email'],'fqubefbuser');
			

				
					$userdata = array(
						'email_address' => $resultFromGoogle['email'],
						'password' => 'fqubefbuser'
					);

					/* Check if 'Remember Me' is checked */

					$isAuth = Auth::attempt($userdata);
					if($isAuth)
					{

						//check weather he is a first time login 
						//check he has selected min 3 category
						$userInfo = User :: getUserInfo($userdata['email_address']);
						Session::put('userInfo', $userInfo[0]['user_id']);
						Session::put('username', $userInfo[0]['username']);
						Session::put('profile_picture', $userInfo[0]['profile_picture']);
						$category = CategoryModel :: getCategory($userInfo[0]['user_id']);
						if(count($category) > 0){

						return Redirect::to('select-follow');
						}
						else{

						return Redirect::to('my-feed');
						}

					}
					else{
						$response = array('status' => 'failure','response'=> 'login failure');
					}
					return $response;
				
			}
		else{
				$response = array('status' => 'faiure','response' => 'Login With Gmail Failed');
			}
			
		return $response;
        /* $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        echo $message. "<br/>"; */
		//dd($resultFromGoogle);
        //Var_dump
        //display whole array().
		

    }
    // if not ask for permission first
    else {
        // get googleService authorization
        $url = $googleService->getAuthorizationUri();

        // return to google login url
        return Redirect::to( (string)$url );
    }
	}
	
	//forgot passowrd 
	
	public function forgotPassword($email){
		$email = base64_decode($email);
	$email = json_decode($email,true);
	$email = $email['emailId'];
		$response = User :: getUserInfo($email);
		if(count($response) > 0){
				$to = $email;
				$name = 'FQube';
				$fromEmail = 'hello@fqube.com';
				
				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= 'From: '.$name.'<'.$fromEmail.'>'. "\r\n";
				$subject = "Forgot Password";
				$link = "http://codewave.co.in/fqube/changePassword?email=".$emailId;
				$message = 'Please find the link below to change password <a href="'.$link.'" target="_blank">'.$link;
				//$headers .= 'Cc:'. $fromEmail."\r\n";
				$mail = mail($to,$subject,$message,wordwrap($headers));
				if($mail){
					//$forgotPasswordLinkTime = time() + 60 * 60 * 24 * 365; // one year
					$forgotPasswordLinkTime = time() + 60 * 60; // one min
					Session::put('forgotPasswordLinkTime', $forgotPasswordLinkTime);
					$response = array('status'=>'success','response'=>'email has been sent',"session" => $forgotPasswordLinkTime);
				}
				else{
					$response = array('status'=>'failure','response'=>'email has not sent');
				}
				
			}
			else{
				$response = array('status'=>'failure','response'=>'email id does not exist');
			}
	}
	public function getUserDetails($id,$profile_id){

			$result = User::getUserDetails($id,$profile_id);
			
			if($result['status'] == '1'){
				return array('status'=>'success','data'=>$result['data'],'following_count'=>$result['count'],'followers_count'=>$result['followers_count'],'store_count'=>$result['store_count'],'follow_status'=>$result['follow_status']);
			}else{
				return array('status'=>'failed','message'=>'cannot fetch user details');
			}		
	}
	public function followingUsers($id){
		$result = User::followingUsers($id);
		if($result['status'] == '1'){
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','message'=>'cannot fetch following users');
			}
	}
	public function getUserCollections($id){
		$result = User::getUserCollections($id);
		if($result['status'] == '1'){
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','message'=>'cannot fetch user collections');
			}
	}
	
	//get my feed 
	
	public function myfeed($userId,$num_products,$page_nos){
		$result = Product::myfeed($userId,$num_products,$page_nos);
		return $result;
	}
	
	//product list which is collected more time 
	public function highestCollection(){
		$result = Product::highestCollection();
		return $result;
	}
	
	public function highestLike($num_products,$page_nos){
		$result = Product::highestLike($num_products,$page_nos);
		return $result;
	}
	
	public function userFeed(){
		return View :: make('myfeed');
	}
	public function usersProducts($user_id,$pageNumber,$numberOfProduct)
	{
		$skip=(($numberOfProduct)*($pageNumber-1));
		$result=Product::leftjoin('product_likes','product_likes.product_id','=','products.product_id')
						->leftjoin('stores','stores.store_id','=','products.store_id')
						->where('user_id','=',$user_id)
						->select('stores.store_name','stores.store_id','products.product_image','products.products_site_url','products.product_id','product_likes.liked_by AS like_status','products.product_name')
						->skip($skip)
						->take($numberOfProduct)
						->get(array('product_image','products_site_url','product_name')); 
						
		
		$productCount=Product::leftjoin('product_likes','product_likes.product_id','=','products.product_id')
						->where('user_id','=',$user_id)
						->select('products.product_image','products.products_site_url','products.product_id','product_likes.liked_by AS like_status','products.product_name')
						//->skip($skip)
						//->take($numberOfProduct)
						->get(array('product_image','products_site_url','product_name')); 
		if($result !="[]")
		{
			return array("status"=>"success","products"=>$result,"count"=>count($productCount));
		}
		else
		{
			return array("status"=>"success","message"=>"No Products Found","count"=>'0');
		}
		
	}

	public function userCollection($user_id)
	{
		
		
		     $collection_objects= Collection::join('products_mappedto_collection','users_collections.collections_id','=','products_mappedto_collection.collection_id')
					->join('products','products_mappedto_collection.product_id','=','products.product_id')
					->select('collection_name as parent','products.product_image','products.product_name','collection_id')
					
					->where('users_collections.user_id','=',$user_id)
					->get();
			$obj = new stdClass();
			 $obj->data = $collection_objects;
			$json_obj= json_encode($obj);
			


$jsonObject = json_decode($json_obj);

$categories = array();
foreach($jsonObject->data as $element) {

   
	$categories[$element->collection_id][] = $element;
	
	//print_r($categories[$element->parent]);
	//unset($element->parent);
}

return array("data"=>$categories,"count"=>count($categories));
						
			  
		

	
	
}


public function numCollections($user_id,$profile_id,$page_nos,$numProducts)
{	
				 $skip=(($numProducts)*($page_nos-1));
				 
				   $collection_count= Collection::join('products_mappedto_collection','users_collections.collections_id','=','products_mappedto_collection.collection_id')
					->leftjoin('products','products_mappedto_collection.product_id','=','products.product_id')
					->select('collection_id')
					->groupBy('collection_id')
					->where('users_collections.user_id','=',$user_id)
					//->skip($skip)
					//->take($numProducts)
					->get();
					
		       $collection_id= Collection::join('products_mappedto_collection','users_collections.collections_id','=','products_mappedto_collection.collection_id')
					->leftjoin('products','products_mappedto_collection.product_id','=','products.product_id')
					->select('collection_id')
					->groupBy('collection_id')
					->where('users_collections.user_id','=',$user_id)
					->skip($skip)
					->take($numProducts)
					->get();
					if($collection_id !='[]')
					{
					for($i=0;$i<count($collection_id);$i++)
					{
						$collection_ids[$i]=$collection_id[$i]['collection_id'];
					}
					    $check_follow=FollowCollection::whereIn('collection_id',$collection_ids)->where('user_id','=',$profile_id)->get(array('collection_id'));
					 for($i=0;$i<count($check_follow);$i++)
					 {
						$collection_ids=$check_follow[$i]['collection_id'];
					 }
					 
					 return array('data'=>$collection_id,'count'=>count($collection_id),'followed_collection_id'=>$check_follow,'collection_count'=>count($collection_count));
					}
					else
					{
					return array('data'=>$collection_id,'count'=>count($collection_id),'status'=>'No collections','collection_count'=>count($collection_count));
					}
}
public function collection_name($user_id,$collectionId)
{
$collection_name=Collection::join('products_mappedto_collection','users_collections.collections_id','=','products_mappedto_collection.collection_id')
			->join('products','products_mappedto_collection.product_id','=','products.product_id')
			->select('collection_name as parent','products.product_image','products.product_name','collection_id')
			->where('users_collections.user_id','=',$user_id)
			->where('users_collections.collections_id','=',$collectionId)
			->take(1)
		->get();
return array('data'=>$collection_name);
}


public function homePageproducts()
{
	 $products=DB::table('admin_uploaded_images')->get();
	 return array('status'=>'success','data'=>$products);
}
public function peopleIfollow($user_id) 
{

		 $peopleIfollow=DB::table('following_users')
            ->join('products', 'products.user_id', '=', 'following_users.following_user_id')
			->leftjoin('product_likes','product_likes.product_id' ,'=', 'products.product_id')
			->join('users','users.user_id','=','products.user_id')
            ->where('following_users.user_id','=',$user_id)
			->take(4)
            ->select('product_likes.liked_by AS like_status','products.product_image', 'products.products_site_url', 'products.product_name','products.product_id','users.username','users.profile_picture')
            ->get();
			
			return array('status'=>'success','data'=>$peopleIfollow);
}
public function collectionfollowing($user_id) 
{

		  $collection_foll=DB::table('following_collection')
            ->join('users_collections', 'users_collections.collections_id', '=', 'following_collection.collection_id')
			->join('products_mappedto_collection','products_mappedto_collection.collection_id','=','users_collections.collections_id')
			->join('products','products.product_id','=','products_mappedto_collection.product_id')
			->where('following_collection.user_id','=',$user_id)
			->take(4)
			->groupBy('users_collections.collections_id')
            ->select('users_collections.collections_id')
            ->get();
			
			return array('status'=>'success','data'=>$collection_foll,'count'=>count($collection_foll));
}

public function products_of_collection($collection_id,$user_id)
{

			 $collection_foll_names=DB::table('following_collection')
            ->join('users_collections', 'users_collections.collections_id', '=', 'following_collection.collection_id')
			->join('products_mappedto_collection','products_mappedto_collection.collection_id','=','users_collections.collections_id')
			->join('products','products.product_id','=','products_mappedto_collection.product_id')
			->where('following_collection.user_id','=',$user_id)
			->where('following_collection.collection_id','=',$collection_id)
			->take(3)
			//->groupBy('users_collections.collections_id')
            ->select('users_collections.collections_id','products.product_name','products.product_image')
            ->get(); 
			return array('status'=>'success','data'=>$collection_foll_names);

}
public function followers_profile($user_id)
{
			
			 
           $followers_profile=FollowModel::join('users', 'users.user_id', '=', 'following_users.user_id')
			//->leftjoin('products','products.user_id','=','following_users.user_id')
			->where('following_users.following_user_id','=',$user_id)
			
			->select('users.profile_picture','users.username','users.user_id','following_users.user_id AS follower')
		
			->get();
			
			for($i=0;$i<count($followers_profile);$i++)
			{
				//$followers[$i]=$followers_profile[$i]['follower'];
				$followers_image[$i]=$followers_profile[$i]['profile_picture'];
				$followers_name[$i]=$followers_profile[$i]['username'];
				$Id[$i]=$followers_profile[$i]['follower'];
				$followerId[$i]=$followers_profile[$i]['user_id'];
				
				
				$products[$i]=Product::where('user_id','=',$followers_profile[$i]['follower'])->take(3)->get(array('product_image','product_id'));
				$userCollection[$i]=Collection::where('user_id','=',$followers_profile[$i]['follower'])->get();
				
				
				
				//return 'hi';
			echo count($products[$i]);
				if(count($products[$i])=='0')
				{
					$data[$i]=array("image"=>$followers_image[$i],"name"=>$followers_name[$i],"collection_count"=>count($userCollection[$i]),"productlikeCount"=>"No likes");
				}
				else
				{
				
				for($j=0;$j<count($products[$i]);$j++)
				{
					
					  $prod[$j]=$products[0][$j]['product_id'];
					 $productId[$i]=ProductLike::where('product_id','=',$prod[$j])->get();
				
				
				}
				$data[$i]=array("data"=>$products[$i],"image"=>$followers_image[$i],"name"=>$followers_name[$i],"id"=>$Id[$i],"data_count"=>count($products[$i]),"collection_count"=>count($userCollection[$i]),"productlikeCount"=>count($productId[$i]));
				//
				
				//$data[$i]=array('data'=>$products[$i]);
				
				
			}
			
		
			
			
			
	}		//$followers_info=array('image_path'=>$followers_image,'followers'=>$followers);
	return $data;		//return $data_prod=array('data'=>$products,'follower'=>$followers_info);
}
public function followers_profile_products($user_id)
{
			$products=Product::where('user_id','=',$user_id)->get(array('product_image'));
			return array('data'=>$products);
}


public function getFollowersDetails1($userId,$profile_id){
	$result = Product :: getFollowersDetails1($userId,$profile_id);
	//return array('data'=>$result,'count'=>count($result),'follow_status'=>$check);
return $result;
}


public function getFollowingUsersDetails($userId,$profile_id){
	$result = Product :: getFollowingUsersDetails($userId,$profile_id);
	//return array("data"=>$result,"count"=>count($result));
	return $result;
}
public function getStoreDetails($userId,$profile_id){
	return $result = Product :: getStoreDetails($userId,$profile_id);
	return array("data"=>$result,"count"=>count($result)); 
}
public function editUser($user_id,$name)
{

$details = array('id' => $user_id, 'name' => $name);
	$result=User::editUserinfo($details);
	return $result;
}

//edit user profile 
public function editProfile(){
		
		$details = array('username' => Input::get('username'),
						'about_me' => Input::get('about_me'),
						'user_id' => Input::get('user_id')
						);
		$result = User :: editProfile($details);
	return $result;					
}

}