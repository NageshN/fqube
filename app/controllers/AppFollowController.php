<?php
	class AppFollowController extends BaseController{ 
		public function followUser($userIdInfo){
		    $userIdInfo = base64_decode($userIdInfo);
		    $userIdInfo = json_decode($userIdInfo,true);
	        $user_id = $userIdInfo['user_id'];
	        $following_user_id = $userIdInfo['following_user_id'];
			try{
				$result = FollowModel :: FollowController($user_id,$following_user_id);
					if($result['status'] == 1){ 
					    Session::put('userInfo',$user_id);
						return array('status'=>'success','followId'=>$result['id']);
					}
			}catch(PDOException $exception){ 
				if($exception->getCode() == '23000'){
					return array('status'=>'failed','message'=>'already exist');
				}else{
					return array('status'=>'failed','message'=>'error');
				}
			}
		}
		public function unfollowUser($userIdInfo){
		    $userIdInfo = base64_decode($userIdInfo);
		    $userIdInfo = json_decode($userIdInfo,true);
	        $user_id = $userIdInfo['user_id'];
	        $following_user_id = $userIdInfo['following_user_id'];
			 $result = FollowModel :: UnfollowController($user_id,$following_user_id);
			if($result == 1){ 
			     
				return array('status'=>'success','message'=>'unfollow success');
			}else{
				return array('status'=>'failed','message'=>'unfollow failed');
			}
				    
		}
		public function getCategoryBasedUsers($categoryIdInfo){
		    $categoryIdInfo = base64_decode($categoryIdInfo);
		    $categoryIdInfo = json_decode($categoryIdInfo,true);
	        $categories = $categoryIdInfo['categories'];
	        $pageNumber = $categoryIdInfo['pageNumber'];
	        $numberOfProduct = $categoryIdInfo['numberOfProduct'];
				$result = CategoryModel :: getCategoryBasedUsers($categories,$pageNumber,$numberOfProduct);
					 if($result['status'] == 1){ 
					 	return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
					 }			
		}
		public function searchByUserName($categoryStr){
			
			$result = FollowModel::searchByUserName($categoryStr);
			
			if($result['status'] == '1'){
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','count'=>$result['count']);
			}
		}		
	
	}
