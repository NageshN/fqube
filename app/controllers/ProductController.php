<?php
class ProductController extends BaseController{
	public function likeProduct(){
	
		$details = array('user_id' => Input::get('user_id'),
						'product_id' => Input::get('product_id'));
					
		$result = ProductLike :: likeProduct($details);
		return $result;
	}
	public function searchProduct($searchText){
		$searchText = base64_decode($searchText);
	    $searchText = json_decode($searchText,true);
	    $numberOfProduct=8;
		$pageNumber=1;
		$result = Product :: searchProduct($searchText,$numberOfProduct,$pageNumber);
		return $result;
	}
	public function postProductoLD($detail){
		
		$tags = get_meta_tags('http://www.stackoverflow.com/');
		
		return $tags;
	}
	public function product(){
		return View::make('productPost');
	}
	
	//get the product details
	public function getProductDetails($productId){
		$result = Product :: getProductDetails($productId);
		return $result;
	}
	
	//get the product abused result
	public function getProductAbusedResult($userId,$productId){
		$result = AbusedProduct :: getProductAbusedResult($userId,$productId);
		return $result;
	}
	
	//Get product list from the same category
	public function getProductFromTheSameCategory($categoryId,$productId,$userId){
		$result = Product :: getProductFromTheSameCategory($categoryId,$productId,$userId);
		return array('data'=>$result);
	}
	public function sameStore($storeId){
		$result = store :: sameStore($storeId);
		return array('data'=>$result,'count'=>count($result)); 
	}
	
	//no of products in that Category starts
	public function noOfProductsinCategory($categoryId){
		$result = Product :: noOfProductsinCategory($categoryId);
		return array('data'=>$result);
	}
	
	//no of products in user profile starts
	public function noOfProductsOfUser($userId){
		$result = Product :: noOfProductsOfUser($userId);
		return array('data'=>$result);
	}
	
	//get Product list From The Posted User
	public function getProductFromThePostedUser($productId){
		$result = Product :: getProductFromThePostedUser($productId);
		return $result;
	}
	
	//report an abuse for the product
	
	public function abuseAnProduct($userId,$productId){
		$result = AbusedProduct :: abuseAnProduct($userId,$productId);
		return $result;
	}
	
	//homePage Products
	
	public function homePageProduct($userId){
		$result = Product :: homePageproduct($userId);
		return $result;
	}
	
	//post a product 
	public function postProduct(){
		$details = array('products_site_url' => Input::get('products_site_url'),
						'product_image' => Input::get('product_image'),
						'product_description' => Input::get('product_description'),
						'product_name' => Input::get('product_name'),
						'user_id' => Input::get('user_id'),
						'store_link' => Input::get('store_link'), 
						'product_price' => Input::get('product_price'),
						'product_currency' => Input::get('product_currency')
							);
		$result = Product :: postProduct($details);
		return $result;
	}
}