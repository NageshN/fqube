<?php
	class categoryController extends BaseController{

		public function categoryTag($user_id,$category){
		    
		    $cat= explode(',',$category);
		    
			$result = CategoryModel :: AddCategoryController($user_id,$cat);
			if($result == 409){
				return array('status'=>'failure','response'=>'Select atleast three category');
			}
			if($result == 1){ 
				return array('status'=>'success','response'=>'category tagged successfully');
			}
				   
		}
		
		public function categoryTagRemove($user_id,$categoryId){
		    
		    
			$result = CategoryModel :: categoryTagRemove($user_id,$categoryId);
			if($result == 1){
				return array('status'=>'success','response'=>'category removed');
			}
			if($result == 0){ 
				return array('status'=>'failure','response'=>'category not removed');
			}
				   
		}

		public function getAllCategories(){
			
			$result = categories::getAllCategories();
			
			if($result['status'] == '1'){
				return array('status'=>'success','data'=>$result['data']);
			}
		}
		 
		public function getAllCategoriesSelected($userId){
			
			$result = categories::getAllCategoriesSelected($userId);
			
			if($result['status'] == '1'){
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','count'=>$result['count']);
			}
		}
		
		public function getAllStoreFollowedByUser($userId){
			
			$result = store::getAllStoreFollowedByUser($userId);
			
			if($result['status'] == '1'){
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','count'=>$result['count']);
			}
		}
		
		public function searchByCategoryName($categoryStr){
			
			$result = categories::searchByCategoryName($categoryStr);
			
			if($result['status'] == '1'){
				return array('status'=>'success','data'=>$result['data'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','count'=>$result['count']);
			}
		}			
	
	}
