<?php
class CollectionController extends BaseController{
		public function getMyCollections($userId){
			return Collection :: getMyCollections($userId);
		}
		public function collectProduct(){
		
		$details = array('userId' => Input::get('userId'),
							'collectionName' => Input::get('collectionName'),
							'collectionId' => Input::get('collectionId'),
							'productId' => Input::get('productId'));
	
		$result = Collection :: collectProduct($details); 
		return $result;
		}
		//Post url collect product
		public function collectProductOnUrl(){
		$details = array('userId' => Input::get('userId'),
							'collectionName' => Input::get('collectionName'),
							'collectionId' => Input::get('collectionId'),
							'productId' => Input::get('productId'));
	
		$result = Collection :: collectProductOnUrl($details); 
		return $result;
		}
		
		//Follow collection
		public function followCollection(){
			$details = array('userId' => Input::get('userId'),
								'collectionId' => Input::get('collectionId'));
			$result = FollowCollection :: followCollectionByUser($details);
			return $result;
		}
		
		//Create Default Collection for user 
		public function createDefaultCollection($userId){
			$result = Collection :: createDefaultCollection($userId);
			return $result;
		}
		
		
		//unfollow collection 
		public function unFollowCollection(){
				$details = array('userId' => Input::get('userId'),
								'collectionId' => Input::get('collectionId'));
				$result = FollowCollection :: unFollowCollectionByUser($details);
				return $result;
		}
		
		//get collection profile Products
		
		public function collectionProfile($userId,$collectionId,$pageNumber,$numberOfProduct){
			$details = array('collectionId' => $collectionId,
								'userId' => $userId,'pageNumber' => $pageNumber,'numberOfProduct' => $numberOfProduct);
			$result = Collection :: collectionProfile($userId,$collectionId,$pageNumber,$numberOfProduct);
			return $result;
		}
		
	
		//get collection profile 
		
		public function collectionProfileDetails($collectionId){
			$result = Collection :: collectionProfileDetails($collectionId);
			if($result['status'] == 1){ 
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{  
				return array('status'=>'failed','message'=>'unfollow failed');
			} 
		}
		 
		//get collection profile follow status 
		public function collectionProfileUserFollowStatus($collectionId,$userId){
			$result = Collection::collectionProfileUserFollowStatus($collectionId,$userId);
			if($result['status'] == 1){ 
				return array('status'=>'success','message'=>'Followed');
			}else{
				return array('status'=>'failed','message'=>'Not Followed');
			}
		}
		
		//get follow collection count
		public function collectionFollowersDetails($collectionId){
			$details = array('collectionId' => $collectionId);
			$result = FollowCollection :: collectionFollowersDetails($details);
			return $result;
		}
		
		//collectionFollowCount
		public function collectionFollowCount($collectionId,$pageNumber,$numberOfProduct){
				$result = FollowCollection :: collectionFollowCount($collectionId,$pageNumber,$numberOfProduct);
				return $result;
		}
}