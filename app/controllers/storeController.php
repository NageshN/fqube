<?php
	class storeController extends BaseController{ 
		public function storeFollow($user_id,$following_store_id){
			try{
				$result = followingStore::storeFollow($user_id,$following_store_id); 
					if($result['status'] == 1){ 
						return array('status'=>'success','storeFollowId'=>$result['id']);
					}
			}catch(PDOException $exception){
				if($exception->getCode() == '23000'){
					return array('status'=>'failed','message'=>'already exist');
				}else{
					return array('status'=>'failed','message'=>'error');
				}
			}
		}
		public function storeUnfollow($userId,$follow_id){
			
			$result = followingStore::storeUnfollow($userId,$follow_id);
			
			if($result == 1){ 
				return array('status'=>'success','message'=>'unfollow success');
			}else{
				return array('status'=>'failed','message'=>'unfollow failed');
			}
				   
		}
		public function getAllStores($pageNumber,$numberOfProduct){
			$result = store::getAllStores($pageNumber,$numberOfProduct); 
			if($result['status'] == 1){ 
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','message'=>'unfollow failed');
			}
		}
		public function searchByStore($str){
			$result = store::searchByStore($str);
			return $result;
			if($result['status'] == 1){ 
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','message'=>'unfollow failed');
			}
		} 
		public function storeProfile($storeId){
			$result = store::storeProfile($storeId);
			if($result['status'] == 1){ 
				return array('status'=>'success','count'=>$result['count'],'data'=>$result['data']);
			}else{
				return array('status'=>'failed','message'=>'unfollow failed');
			}
		} 
		public function storeProfileUserFollowStatus($storeId,$userId){
			$result = store::storeProfileUserFollowStatus($storeId,$userId);
			if($result['status'] == 1){ 
				return array('status'=>'success','count'=>$result['count']);
			}else{
				return array('status'=>'failed','message'=>'Not Followed');
			}
		}
		public function getProductsByStore($storeId,$pageNumber,$numberOfProduct){
			$result = store::getProductsByStore($storeId,$pageNumber,$numberOfProduct);
			return $result;
		}
		public function getStoreFollowingUsers($storeId,$pageNumber,$numberOfProduct){
			$result = followingStore::getStoreFollowingUsers($storeId,$pageNumber,$numberOfProduct);
			return $result;
		}
	
	}
