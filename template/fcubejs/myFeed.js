FQUBE.feed={}; 
//api integration for myFeed Page first tab starts here
jQuery(document).ready(function(){
jQuery('#load_more_loader').show();
var page_number=1;
var num_products=8;

jQuery('#load_more').click(function(){
jQuery('#load_more_loader').show();
num_products=8;
page_number=++page_number;

var user=jQuery('#user_id').val();
var FQUBEfeedUrl=FQUBE.baseUrl+"user/myfeed/"+user+'/'+num_products+'/'+page_number;
var Urltype="get";
FQUBE.common.ajax(FQUBEfeedUrl,Urltype,FQUBE.feed.Loadmoresuccess,FQUBE.feed.Loadmorefailure);
});

var user=jQuery('#user_id').val();
var FQUBEfeedUrl=FQUBE.baseUrl+"user/myfeed/"+user+'/'+num_products+'/'+page_number;
var Urltype="get";
FQUBE.common.ajax(FQUBEfeedUrl,Urltype,FQUBE.feed.success,FQUBE.feed.failure);

});
//load more success function
FQUBE.feed.Loadmoresuccess=function(data)
{
	var i;
	var userId=jQuery('#user_id').val();
	var length=data.data.length;
	if(length < 8){
	 jQuery('#load_more').hide();
	}
	else{
	 jQuery('#load_more').show();
	}
	var options="";
	var liked;
	var not_liked;
	var liked_small;
	var not_liked_small;
	for(i=0;i<length;i++)
	{
		for(var j=0;j<data.likeDetails.length;j++){ 
			 if(data.likeDetails[j].liked_by == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
	options += '<div itemscope="" itemtype="" class="edd_download masonry-brick" id="edd_download_169'+i+'" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:380px;">'+
'<div class="edd_download_inner">'+
'<div class="edd_download_image" id="download-image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;">'+
'<div class="stocky_hover_details stocky_wish_list_on" style="width:250px;" id="'+data.data[i].product_id+'">'+
'<div class="stocky_hover_lines">'+
'<a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a>'+
'<a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a>'+			
'</div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div>'+
'<div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:250px;">'+
'<div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.data[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square"></a></div>'+
'<div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 0px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].username+'</a></span><span style="float:right;font-size:12.6px;margin-top: 0px;margin-right:12px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a style="color:#5AC3A2;" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_price+'</a></span></div>'+
'</div></div>'
	}
	jQuery('#stocky_downloads_list2').append(options).hide().fadeIn('slow');
	
	jQuery('#load_more_loader').hide();
}
FQUBE.feed.Loadmorefailure=function()
{
	jQuery('#load_more_loader').hide();
}
FQUBE.feed.failure=function()
{
jQuery('#load_more_loader').hide();
}



//before loadmore success function
FQUBE.feed.success=function(data)
{
 console.log(data);
	var i;
	var userId=jQuery('#user_id').val();
	var length=data.data.length;
	var options="";
	var liked;
	var not_liked;
	var liked_small;
	var not_liked_small;
	if(length < 8){
	 jQuery('#load_more').hide();
	}
	else{
	 jQuery('#load_more').show();
	}
	for(i=0;i<length;i++)
	{
	for(var j=0;j<data.likeDetails.length;j++){ 
			 if(data.likeDetails[j].liked_by == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
	options += '<div itemscope="" itemtype="" class="edd_download masonry-brick" id="edd_download_169'+i+'" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:380px;">'+
'<div class="edd_download_inner">'+
'<div class="edd_download_image" id="download-image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;">'+
'<div class="stocky_hover_details stocky_wish_list_on" style="width:250px;" id="'+data.data[i].product_id+'">'+
'<div class="stocky_hover_lines">'+
'<a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a>'+
'<a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a>'+			
'</div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div>'+
'<div class="fqube-box-shadow" style="background-color:white!important;float:left;width:250px;">'+
'<div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.data[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square"></a></div>'+
'<div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 0px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].username+'</a></span><span style="float:right;font-size:12.6px;margin-top: 0px;margin-right:12px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a style="color:#5AC3A2;" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_price+'</a></span></div>'+
'</div></div>'
	}
	jQuery('#stocky_downloads_list2').append(options);
	
	jQuery('#load_more_loader').hide();
}
//api integration for myFeed Page first tab ends here


//api integration for myFeed Page trending starts here

jQuery(document).ready(function(){

jQuery('#loading_more').show();
jQuery('#loader1').show();
var numProducts=8;
var pageNos=1;
jQuery('#loading_more').click(function(){
var userId=jQuery('#user_id').val();
jQuery('#loader1').show();
numProducts=8;
pageNos=++pageNos;
var FQUBElikebasedUrl=FQUBE.baseUrl+"user/highestNoOfLike/"+userId+'/'+numProducts+'/'+pageNos;
var typeUrl="get";

FQUBE.common.ajax(FQUBElikebasedUrl,typeUrl,FQUBE.feed.trendloadMoresuccess,FQUBE.feed.trendloadMorefailure);
});
var userId=jQuery('#user_id').val();
var FQUBElikebasedUrl=FQUBE.baseUrl+"user/highestNoOfLike/"+userId+'/'+numProducts+'/'+pageNos;
var typeUrl="get";

FQUBE.common.ajax(FQUBElikebasedUrl,typeUrl,FQUBE.feed.trendlikesuccess,FQUBE.feed.trendlikefailure);
});
FQUBE.feed.trendlikesuccess=function(data)
{
var user=jQuery('#user_id').val();

var length_of_likes=data.data.length;
var i;
var like_based="";
var liked;
var not_liked;
var liked_small;
var not_liked_small;
var userId=jQuery('#user_id').val();
if(length_of_likes < 8){
	 jQuery('#loading_more').hide();
	}
	else{
	 jQuery('#loading_more').show();
	}
for(i=0;i<length_of_likes;i++)
{
for(var j=0;j<data.likeDetails.length;j++){ 
			 if(data.likeDetails[j].like_status == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
	like_based += '<div itemscope="" itemtype="" class="edd_download masonry-brick" id="edd_download_169'+i+'" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:380px;">'+
'<div class="edd_download_inner">'+
'<div class="edd_download_image"  style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;">'+
'<div class="stocky_hover_details stocky_wish_list_on" style="width:250px;" id="'+data.data[i].product_id+'">'+
'<div class="stocky_hover_lines">'+
'<a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a>'+
'<a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a>'+			
'</div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div>'+
'<div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:250px;">'+
'<div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.data[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square"></a></div>'+
'<div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 0px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].username+'</a></span><span style="float:right;font-size:12.6px;margin-top: 0px;margin-right:12px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a style="color:#5AC3A2;" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_price+'</a></span></div>'+
'</div></div>'
}
jQuery('#stocky_downloads_list3').append(like_based); 
jQuery('#loader1').hide();
}
FQUBE.feed.trendloadMoresuccess=function(data)
{
var user=jQuery('#user_id').val();
var length_of_likes=data.data.length;
var i;
var like_based="";
var liked;
var not_liked;
var liked_small;
var not_liked_small;
var userId=jQuery('#user_id').val();
if(length_of_likes>8)
{
jQuery('#loading_more').show();
}
else
{
jQuery('#loading_more').hide(); 
}
for(i=0;i<length_of_likes;i++)
{
for(var j=0;j<data.likeDetails.length;j++){
if(data.likeDetails[j].like_status == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
	like_based += '<div itemscope="" itemtype="" class="edd_download masonry-brick" id="edd_download_169'+i+'" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:380px;">'+
'<div class="edd_download_inner">'+
'<div class="edd_download_image"  style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;">'+
'<div class="stocky_hover_details stocky_wish_list_on" style="width:250px;" id="'+data.data[i].product_id+'">'+
'<div class="stocky_hover_lines">'+
'<a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a>'+
'<a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a>'+			
'</div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div>'+
'<div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:250px;">'+
'<div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.data[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square"></a></div>'+
'<div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 0px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].username+'</a></span><span style="float:right;font-size:12.6px;margin-top: 0px;margin-right:12px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a style="color:#5AC3A2;" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'">'+data.data[i].product_price+'</a></span></div>'+
'</div></div>'
}
jQuery('#stocky_downloads_list3').append(like_based); 

/*$('body,html').animate({
    scrollTop: jQuery('div#stocky_downloads_list3 div.edd_download:last-child').offset().top + 'px'
}, 1000);*/
jQuery('#loader1').hide();
}
FQUBE.feed.trendloadMorefailure=function()
{
jQuery('#loader1').hide();
}
FQUBE.feed.trendlikefailure=function()
{
jQuery('#loader1').hide();
}
/////////////////api integration for myFeed Page first tab ends here/////////////////////////////////////////////////////////////////////////////////////////////


FQUBE.product.submitProduct = function(){
			  jQuery('#loader3').show(); 
			  var product_image=jQuery('.image').find('img').attr("src"); 
			  var product_name=jQuery('.title').html(); 
			  var product_description=jQuery('.description').html(); 
			  var price=jQuery('.price').html(); 
			  var product_currency=jQuery('.priceCurrency').html(); 
			  var products_site_url=jQuery('.url').html(); 
			  var uri=products_site_url.split("/");
			  var store_link=uri['2'];	
				var user_id=jQuery('#user_id').val();
				var url=FQUBE.baseUrl+"user/postProduct";
				var type="post";
				var data='{"products_site_url":"'+products_site_url+'","product_image":"'+product_image+'","product_description":"'+product_description+'","product_name":"'+product_name+'","user_id":"'+user_id+'","store_link":"'+store_link+'","product_currency":"'+product_currency+'","product_price":"'+price+'"}';
			
				
				if((jQuery('.image').children().length != 0)||(jQuery('.description').children().length != 0))
				{
				FQUBE.post.ajax(url,type,data,FQUBE.feed.previewSuccess);
				}
				else
				{
				jQuery('#loader3').hide();
				toastr.error('Internet Connection too slow. Please try after a while.');
				jQuery('.liveurl').hide();
				jQuery('.post-submit-button').hide();
				//jQuery('#post-link').reset();
				}
				}
			 FQUBE.feed.previewSuccess=function(data)
				{
					if(data.status=='failure')
					{
					    jQuery('#loader3').hide();
					    toastr.error('You have already shared this post.');
						jQuery('.liveurl').hide();
						jQuery('.post-submit-button').hide();
					}
					else
					{
					 jQuery('#loader3').hide();
					 jQuery('#save12').popup('show');
					  var strWindowFeatures = "location=yes,height=500px,width=400px,scrollbars=yes,status=yes";
                      var URL =FQUBE.baseUrl+"post-url?product_id="+data.product_info[0].product_id;
                      var win = window.open(URL, "_blank", strWindowFeatures);
					 //toastr.success('Your post was successful.', 'Done!');
					 jQuery('.liveurl').hide();
					 jQuery('.post-submit-button').hide();
					 window.location.href=FQUBE.baseUrl+'myfeed';
					}
				} 
				
	function go_to_landing(e)
	{
		window.location.href=FQUBE.baseUrl+'landing?product_id='+e.id;
	}
	
     
	 jQuery('#save').popup({
      transition: 'all 0.3s',
      scrolllock: false
   
});
var product_id;
function get_product_id(e) 
	 {
		product_id=e.id;
		var product=jQuery('#productID').val(product_id);  
	 }
	 
/*	 
jQuery('document').ready(function()
{
var user_id=jQuery('#user_id').val();
var FQUBEURLForoptions="http://codewave.co.in/fqube/user/getUserCollections/"+user_id;
var optionType="get";
FQUBE.common.ajax(FQUBEURLForoptions,optionType,FQUBE.user.optionSuccess);
});
FQUBE.user.optionSuccess=function(data)
{
	var length=data.count;
	var i=0;
	var dropdown="";
	for(i=0;i<length;i++)
	{
		dropdown +='<option id='+data.data[i].collections_id+'>'+data.data[i].collection_name+'</option>';
	}
	
	jQuery('#myCustomSelector').append(dropdown);
}

function addCollection()
{
var user_id=jQuery('#user_id').val();
	var collection_name=jQuery( "#myCustomSelector option:selected" ).text();
	var collection_id=jQuery( "#myCustomSelector option:selected" ).attr('id');
	var coll_name=jQuery('#collection').val();
	var productId=jQuery('#productID').val();
	if(coll_name =="")
	{
	var collectionName="null";
	var collectionId=collection_id;
	}
	else
	{
	var collectionName=coll_name;
	
	var collectionId="null";
	}
	var FQUBEurlforadd="http://codewave.co.in/fqube/user/collectProduct";
	var FQUBEaddtype="POST";
	var addData='{"userId":"'+user_id+'","collectionName":"'+collectionName+'","collectionId":"'+collectionId+'","productId":"'+productId+'"}';
	FQUBE.post.ajax(FQUBEurlforadd,FQUBEaddtype,addData,FQUBE.user.addSuccess);
	
	
}
FQUBE.user.addSuccess=function(data)
{
}
*/
/*
var product_id;
function like(e)
{	
	 product_id=jQuery(e).attr("data-id");
	//alert(product_id);
	var user_id=jQuery('#user_id').val();
	
	
	
	var FQUBElikeurl="http://codewave.co.in/fqube/user/likeProduct";
	var FQUBEType="POST";
	var FQUBEdata='{"user_id":"'+user_id+'","product_id":"'+product_id+'"}';

	FQUBE.post.ajax(FQUBElikeurl,FQUBEType,FQUBEdata,FQUBEsuccesslike);
}
FQUBEsuccesslike=function(result)
{
//alert(result.status);
	 if(result.status=='success')
	{
		jQuery('#product_'+product_id+'').addClass('already_liked');
	} 
	else if(result.status=='failure')
	{
	
		jQuery('#product_'+product_id+'').removeClass('already_liked');
	}
}
 */

