FQUBE.profile={};

jQuery('document').ready(function(){
var id=jQuery('#user_profile_id').val();
var profile_id=jQuery('#user_id').val();

var FQUBEprofileurl=FQUBE.baseUrl+"user/getUserDetails/"+id+'/'+profile_id;
var typeofUrl="get";

FQUBE.common.ajax(FQUBEprofileurl,typeofUrl,FQUBE.profile.success);
});

FQUBE.profile.success=function(data)
{
if(data.follow_status.length > '0')
{
	var id=jQuery('#user_profile_id').val();
	jQuery('#'+id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
	jQuery('#'+id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
}
else
{
 jQuery('#'+id).html("FOLLOW");
 jQuery('#'+id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
}
	var userName=data.data[0].username;
	
	var profile_picture=data.data[0].profile_picture;
	var image=profile_picture;

	jQuery('#profile_name').html(userName.substr(0,20));
	jQuery('#profile_name').attr('title',userName);
	jQuery('#profile_picture').attr("src",image);
	jQuery('#followers_count').html(data.followers_count);
	jQuery('#following_count').html(data.following_count);
	jQuery('#aboutMe').attr('title',data.data[0].about_me);
	jQuery('#aboutMe').html(data.data[0].about_me);
	if(data.followers_count == 0){
		    jQuery('#tablet-3').html("<div class='message-error'>No People Found.</div>");	
            jQuery('.message-error').attr("style","font-size: 20px;text-align: center;margin-top: 10px;margin-bottom: 10px;");
		}
	
}

jQuery('document').ready(function(){
var page_number=1;
var num_products=8;
jQuery('#product_loadMore').click(function()
{
jQuery('#loader1').show();
page_number=++page_number;
num_products=8;
var id=jQuery('#user_profile_id').val();
var FQUBEProductsUrl=FQUBE.baseUrl+'user/ProductsofUsers/'+id+'/'+page_number+'/'+num_products;
var productType='get';
FQUBE.common.ajax(FQUBEProductsUrl,productType,FQUBE.product.Loadsuccess);
});

var id=jQuery('#user_profile_id').val();
var FQUBEProductsUrl=FQUBE.baseUrl+'user/ProductsofUsers/'+id+'/'+page_number+'/'+num_products;
var productType='get';
FQUBE.common.ajax(FQUBEProductsUrl,productType,FQUBE.product.success);
});
FQUBE.product.success=function(proData)
{
    jQuery('#products_count').html(proData.count);
    if(proData.count != 0){
	var i; 
	var profile_products="";
	var pro_length=proData.count;
	if(pro_length>8)
	{
	jQuery('#product_loadMore').show();
	}
	else
	{
	jQuery('#product_loadMore').hide();
	}

	for(i=0;i<proData.products.length;i++)
	{
	if(proData.products[i].like_status == null)
		{
			 var like_status='not_liked';
			 var like_status1='not_liked_small';
			 
		}
		else
		{
			var like_status='liked';
			var like_status1='liked_small';
		}
		var id=jQuery('#user_profile_id').val();
		var username=jQuery('#user_profile_name').val();
                  profile_products += '<div class="col-md-3 blocks">'+
							'<div class="edd_download_inner">'+
							'<div class="edd_download_image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+proData.products[i].product_image+') no-repeat;width:250px;height:246px;background-size:100% 100%;">'+
							'<div class="stocky_hover_details stocky_wish_list_on" style="height:246px">'+
'<div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like '+like_status+' " id="product_'+proData.products[i].product_id+'" data-id="'+proData.products[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label opens overlayLinks save1_open" id="'+proData.products[i].product_id+'" onclick="get_product_id(this)"  style="margin-left: -48px;">Save</span></a></div>'+
'</div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+proData.products[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+proData.products[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+proData.products[i].product_like_count+'</span></div></div>'+
'<div style="background-color:white!important;float:left;width:100%">'+
'<div class="tag-author" style="padding-left: 8px;width:100%"><span style="float:left;padding-top:4px;padding-right: 20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;line-height:20px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+proData.products[i].product_id+'&id='+id+'&userName='+username+'">'+proData.products[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: -5px;"><span style="float:left;padding-right:6px">from</span><a href="'+FQUBE.baseUrl+'store-profile?store_id='+proData.products[i].store_id+'" style="color:#5AC3A2;float:left;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;line-height:20px;padding-top:4px;">'+proData.products[i].store_name+'</a></span></div>'+
'</div></div>' 
	}
	
	jQuery('#prod_profile').append(profile_products);
	}
	else{
	      jQuery('#prod_profile').append('<div class="message-error" style="margin-left: -15px;margin-bottom: -20px;">No results to display</div>');
		  jQuery('#product_loadMore').hide();
	}
}


FQUBE.product.Loadsuccess=function(proData)
{

	var i;
	var profile_products="";
	var pro_length=proData.products.length;
	if(pro_length < 8)
	{
		jQuery('#product_loadMore').hide();
		jQuery('#loader1').hide();
	}
	else
	{
	   jQuery('#product_loadMore').show();
	   jQuery('#loader1').show();
	}
	


	for(i=0;i<proData.products.length;i++)
	{
	if(proData.products[i].like_status == null)
		{
			 var like_status='not_liked';
			 var like_status1='not_liked_small';
			 
		}
		else
		{
			var like_status='liked';
			var like_status1='liked_small';
		}
		var id=jQuery('#user_profile_id').val();
		var username=jQuery('#user_profile_name').val();
                profile_products += '<div class="col-md-3 blocks">'+
							'<div class="edd_download_inner">'+
							'<div class="edd_download_image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+proData.products[i].product_image+') no-repeat;width:250px;height:246px;background-size:100% 100%;">'+
							'<div class="stocky_hover_details stocky_wish_list_on" style="height:246px">'+
'<div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like '+like_status+' " id="product_'+proData.products[i].product_id+'" data-id="'+proData.products[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label opens overlayLinks save1_open" id="'+proData.products[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div>'+
'</div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+proData.products[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+proData.products[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+proData.products[i].product_like_count+'</span></div></div>'+
'<div style="background-color:white!important;float:left;width:100%">'+
'<div class="tag-author" style="padding-left: 8px;width:100%"><span style="float:left;padding-top:4px;padding-right: 20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;line-height:20px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+proData.products[i].product_id+'&id='+id+'&userName='+username+'">'+proData.products[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: -5px;"><span style="float:left;padding-right:6px">from</span><a href="'+FQUBE.baseUrl+'store-profile?store_id='+proData.products[i].store_id+'" style="color:#5AC3A2;float:left;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;line-height:20px;padding-top:4px;">'+proData.products[i].store_name+'</a></span></div>'+
'</div></div>' 
	}
	
	jQuery('#prod_profile').append(profile_products).hide().fadeIn('slow');
	jQuery('#loader1').hide();
}


jQuery('document').ready(function(){
var id=jQuery('#user_profile_id').val();
var name=jQuery('#user_profile_name').val();
var FQUBEuserIfollowurl=FQUBE.baseUrl+'user/Ifollow/'+id;
var Ifollowtype='get';
FQUBE.common.ajax(FQUBEuserIfollowurl,Ifollowtype,FQUBE.product.ifollowsuccess);
});
FQUBE.product.ifollowsuccess=function(data)
{			 
var i;
var follow_length=data.data.length;
if(follow_length != 0){
           if(follow_length > 4){
			  jQuery('.view-all-pro').html('<a href="'+FQUBE.baseUrl+'products-ifollow?id='+id+'&name='+name+'" style="color:#5AC3A2;text-decoration:underline;float:right">more<i class="fa fa-angle-right" style="padding-left:7px"></i></a>');	
			}
			else {
			 }
var ifollow="";
for(i=0;i<follow_length;i++)
{
if(data.data[i].like_status == null)
		{
			 var like_status='not_liked';
			 var like_status1='not_liked_small'; 
		}
		else
		{
			var like_status='liked';
		    var like_status1='liked_small';	
		}
ifollow +='<div class="col-md-3 blocks"><div class="edd_download_inner">'+
				'<div class="edd_download_image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:246px;background-size:100% 100%;">'+

'<div class="stocky_hover_details stocky_wish_list_on" style="height:246px">'+
'<div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like '+like_status+' " id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label opens overlayLinks save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)"  style="margin-left: -48px;">Save</span></a></div>'+
'</div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div>'+
'<div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:100%;">'+
'<div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.dataUser[i].user_id+'&name='+data.dataUser[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.dataUser[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" alt="DSCF0726-square"></a></div>'+
'<div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.dataUser[i].user_id+'&userName='+data.dataUser[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 0px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.dataUser[i].user_id+'" style="color:#5AC3A2;float:left">'+data.data[i].username+'</a></span>'+
'</a><span style="float: right;background: #f6f6f6;padding: 0px 5px 0px 5px;margin-top: 0px;border-radius: 5px;font-size:9px;margin-right:10px"><span style="color:#454545">FOLLOWED</span></span></div></div></div>'
}
jQuery('#i_follow').append(ifollow);
}
else{
jQuery('#i_follow').append('<div class="message-error" style="margin-top: 55px;margin-left: 10px;margin-bottom: 20px;">No results to display</div>');
}
}

/*jQuery('document').ready(function()
{
var profile_id=jQuery('#user_profile_id').val();
var followersUrl='http://codewave.co.in/fqube/user/followersProfile/'+profile_id;
var followerstype="get";
FQUBE.common.ajax(followersUrl,followerstype,FQUBE.product.followerSuccess);
});

FQUBE.product.followerSuccess=function(data)
{
var i;
var followers="";
alert(data.length);
for(i=0;i<data.length;i++)
{
if(data[i].data_count=='0')
{
	var prod1='http://codewave.co.in/fqube/template/img/jacket.jpg';
	var prod2='http://codewave.co.in/fqube/template/img/jacket.jpg';
	var prod3='http://codewave.co.in/fqube/template/img/jacket.jpg';
		
}
else if(data[i].data_count=='1')
{
	var prod1=data[i].data[0].product_image;
	var prod2='http://codewave.co.in/fqube/template/img/jacket.jpg';
	var prod3='http://codewave.co.in/fqube/template/img/jacket.jpg';

}
else if(data[i].data_count=='2')
{
	var prod1=data[i].data[0].product_image;
	var prod2=data[i].data[1].product_image;
	var prod3='http://codewave.co.in/fqube/template/img/jacket.jpg';
}
else if(data[i].data_count=='3')
{
	var prod1=data[i].data[0].product_image;
	var prod2=data[i].data[1].product_image;
	var prod3=data[i].data[2].product_image; 
	
}
	followers +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-bottom:8px;"><div class="follow-top">'+
	'<div class="col-md-8"><div class="col-md-3" style="padding: 0;"><a href="#">'+
	'<img src="'+FQUBE.baseUrl+''+data[i].image+'" class="attachment-img1" alt=""></a></div><div class="col-md-9" style="">'+
	'<a href="#"><div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data[i].name+'</div>'+
	'</a><div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px" class="collection-count'+i+'">'+data[i].collection_count+'</span><br> products</span></div></div>'+ 
	'<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data[i].productlikeCount+'</span><br> followers</span></div></div></div>'+
	'</div></div><div class="col-md-4" style="z-index: 1000000000000;"><button id="" onclick="followUser(this);" class="btn3 btn-default3 follow-btn">FOLLOW</button>'+
	'</div></div> <div class="follow-below"><div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+prod1+'" class="attachment-img" alt=""></div>'+
	'<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+prod2+'" class="attachment-img" alt=""></div><div class="col-md-4" style="padding: 0;"><img src="'+prod3+'" class="attachment-img" alt=""></div></div></div>'




}

jQuery('#tabs-3').append(followers);
}*/
/* jQuery('document').ready(function(){
var id=jQuery('#user_profile_id').val();
var FQUBEuserCollectionsUrl=FQUBE.baseUrl+"user/getUserCollections/"+id;
var Urltype="get";


FQUBE.common.ajax(FQUBEuserCollectionsUrl,Urltype,FQUBE.profile.collectionSuccess);
});
FQUBE.profile.collectionSuccess = function(data)
{
	if(data.status=='failed')
	{
		//jQuery('#collection_prods').addClass('noCollections');
		jQuery('#collection_prods').html('No Collections Found');
	}
	else
	{
		alert('bye');
		jQuery('#collection_prods').html('API is not done by Praveen');
	}
} */
// collections api integration start here
/* jQuery('document').ready(function(){
var id=jQuery('#user_profile_id').val();
var FQUBErandomCollectionUrl=FQUBE.baseUrl+"user/collectionsOfUser/"+id;
var randomType="get";
FQUBE.common.ajax(FQUBErandomCollectionUrl,randomType,FQUBE.profile.randomCollectionsuccess);
});
FQUBE.profile.randomCollectionsuccess=function(data)
{
		var length=data.count;
		//alert(length);
		var it=data.data[13][0]['parent'];
		alert(it);
		var i=0;
		var randomCollections="";
		for(i=0;i<length;i++)
		{
			randomCollections += '<div class="col-md-4 total">'+
			'<div class="col-md-12" style="padding-top:15px;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);padding-bottom: 12px;"><a href="" style="color:#454545">My Random Collection</a> <button type="button" class="btn btn-default follow">FOLLOW</button></div>'+
			'<div class="col-md-12 collects">'+
			'<div class="main-image"><img width="280" height="187" src="'+FQUBE.baseUrl+''+data.collection_items[i].product_image+'" class="attachment-product_main wp-post-image" alt="1epgtU4" style="width:95.4%;border-radius:3px"></div>'+
			'<div class="col-md-4 small-image"><img src="'+FQUBE.baseUrl+''+data.collection_items[length-1].product_image+'" style="border-radius:3px"/></div>'+
			'<div class="col-md-4 small-image"><img src="'+FQUBE.baseUrl+''+data.collection_items[length-2].product_image+'" style="border-radius:3px"/></div>'+
			'<div class="col-md-4 small-image"><img src="'+FQUBE.baseUrl+''+data.collection_items[length-3].product_image+'" style="border-radius:3px"/></div>'+
			'</div> </div>'
 
		}
		jQuery('#random_collection').append(randomCollections); 
	
} */

