FQUBE.product_landing={};

jQuery('document').ready(function()
{
var product_id=jQuery('#product_id').val();

var FQUBElandingurl= FQUBE.baseUrl+'user/getProductDetails/'+product_id;
var landingType="get";

FQUBE.common.ajax(FQUBElandingurl,landingType,FQUBE.product_landing.success);

});
FQUBE.product_landing.success = function(data)
{
	var productName=data.product_detail[0].product_name;
	var desc=data.product_detail[0].product_description;
	var image=data.product_detail[0].product_image;
	var price=data.product_detail[0].product_price;
	if(price == '' || price == 0){
	  jQuery('.price-div-hide').hide();
	  jQuery('#productPrice').hide();
	}
	var category_name=data.product_detail[0].category_name;
	var category_id=data.product_detail[0].category_id;
	var profile_pic=data.product_detail[0].profile_picture;
	var store_id=data.product_detail[0].store_id;
	var product_save_count=data.product_detail[0].product_save_count;
	var product_currency=data.product_detail[0].product_currency;
	var profile_site_url=data.product_detail[0].products_site_url;
	var product_like=data.product_like[0].like_count;
	var product_id=jQuery('#product_id').val();
	var profile_id=jQuery('#profile_id').val();
	var user_name1=jQuery('#UserName').val();  
	jQuery('#similarprotitle').html("<h2>MORE PRODUCTS IN "+category_name+"</h2>");  
	var userId=jQuery('#user_id').val(); 
	var title=productName;
	var image1 = FQUBE.base64.encode(image);
	var title1 = FQUBE.base64.encode(title);
	var desc1 = FQUBE.base64.encode(desc);
	//var fbInfo={'link':link,'image1':image1,'title1':title1,'desc1':desc1,'redirect_uri':redirect_uri}
    var url1=FQUBE.baseUrl+'user/facebookApi/'+image1+'/'+title1+'/'+desc1;
    jQuery.ajax({ 
			type: "GET",    
			url:url1,      
			success: function (data)  
			 {
			 console.log(data);
			 jQuery(".networks-5 ul").append("<a href='"+data+"'><li class='entypo-facebook'></li></a>");
			 }

});
	//more from that store start
	var FQUBEmoreurl= FQUBE.baseUrl+'user/sameStore/'+store_id; 
    var moretype="get";
    FQUBE.common.ajax(FQUBEmoreurl,moretype,FQUBE.product_landing.moreSuccessStore);
	//more from that store ends
	//similar from that category start
	var FQUBEstoreurl= FQUBE.baseUrl+'user/getProductFromTheSameCategory/'+category_id+'/'+product_id+'/'+userId; 
    var type="get";
    FQUBE.common.ajax(FQUBEstoreurl,type,FQUBE.product_landing.storeSuccess);
	//similar from that category ends
	//product abused starts
	var FQUBEabuseurl= FQUBE.baseUrl+'user/getProductAbusedResult/'+userId+'/'+product_id; 
    var type="get";
    FQUBE.common.ajax(FQUBEabuseurl,type,FQUBE.product_landing.abuseSuccess);
	//product abused ends
	//no of products in that Category starts
	var FQUBEmoreurl= FQUBE.baseUrl+'user/noOfProductsinCategory/'+category_id; 
    var moretype="get";
    FQUBE.common.ajax(FQUBEmoreurl,moretype,FQUBE.product_landing.moreSuccessCategory);
	//no of products in that Category ends
	//no of products in user products starts
	var FQUBEmoreurl= FQUBE.baseUrl+'user/noOfProductsOfUser/'+profile_id; 
    var moretype="get";
    FQUBE.common.ajax(FQUBEmoreurl,moretype,FQUBE.product_landing.moreSuccessUser);
	//no of products in user products ends
	jQuery('.desc-text').html(desc);
	jQuery('#product_image').attr('src',image);
	jQuery('#profile_site_url').attr('href',profile_site_url);
	jQuery('#productName').html(productName);
	jQuery('#productPrice').html(product_currency+"&nbsp;"+price);
	jQuery('#save_count').html(product_save_count);
	jQuery('#store-id').val(store_id);
	jQuery('#category_list').html("<a href="+FQUBE.baseUrl+"category_products?category_id="+category_id+"&category_name="+category_name+" style='color:#cccccc;'>"+category_name+"</a>");
	jQuery('#profile_pic').html('<img src="http://codewave.co.in/fqube/template/'+profile_pic+'>"');
	var collections=data.collection_details.length;
	if(collections==0)
	{
		jQuery('#myCollection').html('<a href="'+FQUBE.baseUrl+'profile?id='+profile_id+'&name='+user_name1+'" style="color:#5AC3A2!important;">Random Collection</a>');
	}
	else
	{
		jQuery('#myCollection').html('<a href="'+FQUBE.baseUrl+'collection?collection_id='+data.collection_details[0].collection_id+'" style="color:#5AC3A2!important;">'+data.collection_details[0].collection_name+'</a>');
	}
	jQuery('#likeCount').html(data.product_detail[0].product_like_count);
}

//similar from that category success function start
FQUBE.product_landing.storeSuccess=function(data)
{
console.log(data);
    var i;
    var userId=jQuery('#user_id').val();
	var append="";
	var liked;
	var not_liked;
	if(data.data.data.length != 0){ 
	for(i=0;i<data.data.data.length;i++)
	{
     for(var j=0;j<data.data.likeDetails.length;j++){
			 if(data.data.likeDetails[j].liked_by == userId && data.data.likeDetails[j].product_id==data.data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
	           append +='<div class="col-md-3 blocks">'+
				'<div class="edd_download_inner">'+
				'<div class="edd_download_image" id="image11" style="background-image:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data.data[i].product_image+');background-size:100% 100%;width:100%;height:246px;">'+
				'<div class="stocky_hover_details stocky_wish_list_on" style="height:246px">'+
				'<div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like '+like_status+'" id="product_'+data.data.data[i].product_id+'" data-id="'+data.data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left: 55px;font-size: 18px;float: left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div>'+
				'</div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data.data[i].product_like_count+'</span></div></div>'+
				'<div style="background-color:white!important;float:left;width:100%">'+
'<div class="tag-author" style="padding-left: 17px;padding-bottom:7px"><span class="product-name" style="float:left;padding-top: 9px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;width: 180px;"><a class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data.data[i].product_id+'&amp;id='+data.data.data[i].user_id+'&amp;userName='+data.data.data[i].username+'" style="color:#454545;">'+data.data.data[i].product_name+'</a></span>'+
'<span style="float:left;font-size:12.6px;margin-top: 0px;">'+
'<span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data.data[i].user_id+'&name='+data.data.data[i].username+'" style="color:#5AC3A2;float:left;">'+data.data.data[i].username.substr(0,11)+'</a></span><span style="float:right;font-size:12.6px;margin-top: 0px;margin-right:-45px"><span style="float:left;padding-right:6px">'+data.data.data[i].product_currency+'</span><a href="'+FQUBE.baseUrl+'landing?product_id='+data.data.data[i].product_id+'&amp;id='+data.data.data[i].user_id+'&amp;userName='+data.data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data.data[i].product_price+'</a></span></div>'+
'</div></div>'
	}
	jQuery('#similar_Products').append(append);
	}
	else {
	 jQuery('#similar_Products').append("<div class='message-error'>No results to display</div>");
	}
}



jQuery('document').ready(function()
{
var product_id=jQuery('#product_id').val();
var FQUBEmoreurl= FQUBE.baseUrl+'user/getProductFromThePostedUser/'+product_id; 
var moretype="get"; 

FQUBE.common.ajax(FQUBEmoreurl,moretype,FQUBE.product_landing.moreSuccess);

});
 
FQUBE.product_landing.moreSuccess=function(data)
{ 
	var i=0;
	var userId=jQuery('#user_id').val();
	var liked;
	var not_liked;
	var more="";
	if(data.data.length != 0){ 
	for(i=0;i<data.data.length;i++)
	{
	  for(var j=0;j<data.likeDetails.length;j++){
			 if(data.likeDetails[j].liked_by == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
	more += '<div class="col-md-3 blocks">'+ 
				'<div class="edd_download_inner">'+
				'<div class="edd_download_image" id="image11" style="background-image:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+');background-size:100% 100%;width:100%;height:246px;">'+
				'<div class="stocky_hover_details stocky_wish_list_on" style="height:246px">'+
				'<div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left like1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left: 55px;font-size: 18px;float: left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left collect1" data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div>'+
				'</div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div>'+
				'<div style="background-color:white!important;float:left;width:100%">'+
'<div class="tag-author" style="padding-left: 17px;padding-bottom:7px"><span class="product-name" style="float:left;padding-top: 9px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;width: 180px;"><a class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&amp;id='+data.data[i].user_id+'&amp;userName='+data.data[i].username+'" style="color:#454545;">'+data.data[i].product_name+'</a></span>'+
'<span style="float:left;font-size:12.6px;margin-top: 0px;">'+
'<span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left;">'+data.data[i].username.substr(0,11)+'</a></span><span style="float:right;font-size:12.6px;margin-top: 0px;margin-right:-45px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&amp;id='+data.data[i].user_id+'&amp;userName='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].product_price+'</a></span></div>'+
'</div></div>'
	}
	jQuery('#more_blocks').append(more);
	}
	else {
	  jQuery('#more_blocks').append("<div class='message-error'>No results to display</div>");
	}
	
}

//product abused result starts	
FQUBE.product_landing.abuseSuccess=function(data)
{
	if(data.status == "success"){
	 jQuery(".report-button").addClass("report_open");
     jQuery(".report-button").html("Report this");
	 $('#report').popup('hide');
	}
	else {
	 jQuery(".report-button").removeClass("report_open");
     jQuery(".report-button").html("Reported");
	 $('#report').popup('hide');
	}
}	

//no of products in that Category starts	
FQUBE.product_landing.moreSuccessCategory=function(data)
{
	 if(data.data.product_count > 4){
		   jQuery('#similar-products-view').html('<a href="'+FQUBE.baseUrl+'category_products?category_id='+data.data.category_details[0].category_id+'&category_name='+data.data.category_details[0].category_name+'" style="float:right;color:#5AC3A2;text-decoration:underline">view all<i class="fa fa-angle-right" style="padding-left:7px"></i></a>');
		}
}	
 
//no of products in user products starts
FQUBE.product_landing.moreSuccessUser=function(data)
{
	 if(data.data.product_count > 4){
		   jQuery('#more-from-view').html('<a href="'+FQUBE.baseUrl+'profile?id='+data.data.products[0].user_id+'&name='+data.data.username[0].username+'" style="float:right;color:#5AC3A2;text-decoration:underline">view all<i class="fa fa-angle-right" style="padding-left:7px"></i></a>');
		}
}	
 
//more from that store	
FQUBE.product_landing.moreSuccessStore=function(data)
{
    var storeImage="";
    var repeat="";
	
	repeat+='<div style="width: 100%;float: left;"><div style="width: 50%;float: left;">more from <a href="'+FQUBE.baseUrl+'store-profile?store_id='+data.data[0].store_id+'" style="color:#5AC3A2">'+data.data[0].store_name+'</a></div><div style="width: 50%;float: right;"><a href="'+FQUBE.baseUrl+'store-profile?store_id='+data.data[0].store_id+'" style="float:right;color:#5AC3A2;text-decoration:underline">view all<i class="fa fa-angle-right" style="padding-left:7px"></i></a></div></div><div style="float:left">';	
    for(var i=0;i<data.count;i++){
	storeImage+='<div id="cat_image'+i+'" style="background:url('+data.data[i].product_image+');background-size:100% 100%;width:119px;height:119px;float:left;margin-right: 8px;border-radius: 5px;"></div>';
	}
	repeat=repeat+storeImage+'</div>'
	jQuery('#landing-more-products').append(repeat);
	
}	
