//Fuction to fetch the parameter values in javascript
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)  
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    } 
}          


// Display Category Products
jQuery(document).ready(function(){
jQuery('#loader').show(); 
var category_id;
var category_name; 
jQuery('#viewproduct').attr('data-id',2);
var categoryId = getUrlParameter('category_id');
var categoryName = getUrlParameter('category_name');
jQuery('.similar-products-h2').append('&nbsp;'+categoryName);
var userId=jQuery('#user_id').val();
var pageNumber=1;
var numberOfProduct=8;
var url=FQUBE.baseUrl+'user/getProductsByCategory/'+categoryId+'/'+pageNumber+'/'+numberOfProduct;
jQuery.ajax({ 
			type: "GET",    
			url:url,       
			dataType : 'json',
			success: function (data)  
			 {
			  var liked;
	          var not_liked;
			  var liked_small;
	          var not_liked_small;
			  var userId=jQuery('#user_id').val();
			  if(data.status != 0){
			  jQuery('#viewproduct').show();
			  for(var i=0;i<data.count;i++){
			    for(var j=0;j<data.likeDetails.length;j++){
			 if(data.likeDetails[j].liked_status == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       { 
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
             jQuery('#stocky_downloads_list').append('<div itemscope="" itemtype="" class="edd_download masonry-brick" id="edd_download_1690" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:342px;"><div class="edd_download_inner"><div class="edd_download_image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;"><div class="stocky_hover_details stocky_wish_list_on" style="width:250px;"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div><div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:250px;"><div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.data[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" style="width: 50px;" alt="DSCF0726-square"></a></div><div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&amp;id='+data.data[i].user_id+'&amp;userName='+data.data[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 5px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].username.substr(0,11)+'</a></span><span style="float:right;font-size:12.6px;margin-top: 5px;margin-right:12px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&amp;id='+data.data[i].user_id+'&amp;userName='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].product_price+'</a></span></div></div></div>');
			 jQuery('#loader').hide();
			 }
			 if(data.totalCount > 8){
			  jQuery('#viewproduct').show();
			 }
			 else {
			  jQuery('#viewproduct').hide();
			 }
			 }
			 else {
			 jQuery('#stocky_downloads_list').append("<div class='message-error'>No results to display</div>");
			 jQuery('#loader').hide();
			 }
			 }

});
}); 

//Load more products
function viewMoreCategoryProducts(){
jQuery('#loader').show();
var pageNumber=jQuery('#viewproduct').attr('data-id');
var category_id;
var category_name; 
var categoryId = getUrlParameter('category_id');
var categoryName = getUrlParameter('category_name');
var userId=jQuery('#user_id').val();
var numberOfProduct=8;
var url=FQUBE.baseUrl+'user/getProductsByCategory/'+categoryId+'/'+pageNumber+'/'+numberOfProduct;
jQuery.ajax({ 
			type: "GET",    
			url:url,      
			dataType : 'json',
			success: function (data)  
			 {
			  var liked;
	          var not_liked;
			  var liked_small;
	          var not_liked_small;
			  if(data.status != 0){
			  for(var i=0;i<data.count;i++){
			     for(var j=0;j<data.likeDetails.length;j++){
			 if(data.likeDetails[j].liked_status == userId && data.likeDetails[j].product_id==data.data[i].product_id)  
		        { 
			     var like_status='liked';
				 var like_status1='liked_small';
			     break;
		        } 
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small'; 
		       }
			   }
			 jQuery('#stocky_downloads_list').append('<div itemscope="" itemtype="" class="edd_download masonry-brick" id="edd_download_1690" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:342px;"><div class="edd_download_inner"><div class="edd_download_image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;"><div class="stocky_hover_details stocky_wish_list_on" style="width:250px;"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div></div><div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:250px;"><div class="tagging"><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left"><img src="'+data.data[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" style="width: 50px;" alt="DSCF0726-square"></a></div><div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" class="proc_name" href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&amp;id='+data.data[i].user_id+'&amp;userName='+data.data[i].username+'">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 5px;"><span style="float:left;padding-right:6px">by</span><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i].user_id+'&name='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].username+'</a></span><span style="float:right;font-size:12.6px;margin-top: 5px;margin-right:12px"><span style="float:left;padding-right:6px">'+data.data[i].product_currency+'</span><a href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&amp;id='+data.data[i].user_id+'&amp;userName='+data.data[i].username+'" style="color:#5AC3A2;float:left">'+data.data[i].product_price+'</a></span></div></div></div>');
			 jQuery('#loader').hide();
			 }
			 if(data.count < 8){
			  jQuery('#viewproduct').hide();
			 } 
			 else {
			   jQuery('#viewproduct').show();
			 }
			 }
			 else {
			 jQuery('#viewproduct').hide();
			 jQuery('#loader').hide();
			 }
			 pageNumber=++pageNumber;
			 jQuery('#viewproduct').attr('data-id',pageNumber);
			 }

});
}
