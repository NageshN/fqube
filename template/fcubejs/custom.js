var FQUBE  = {};
FQUBE.common = {};
FQUBE.base64 = {};
FQUBE.post={}; 

FQUBE.product ={};
FQUBE.baseUrl = 'http://codewave.co.in/fqube/';

//user sign in
FQUBE.user = {};
FQUBE.user.signin = function(){ 
    jQuery('#loader2').show();
	var email = jQuery('#email').val();
	var username = jQuery('#username').val();
	var password=jQuery('#password').val();
	var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var pattern1= /^\w+$/;
	var hasError = false;
		if((email == '') || (!pattern.test(email))){
			jQuery('#loader2').hide();
			jQuery('.error2').show();
			jQuery('#email').addClass('error-border');
			var hasError = true;
			}
		else{
		    jQuery('#loader2').show();
			jQuery('.error2').hide();
			jQuery('#email').removeClass('error-border');
			var hasError = false;
		}
		if(username == '' || (!pattern1.test(username))){
		    jQuery('#loader2').hide();
			jQuery('.error2').show();
			jQuery('#username').addClass('error-border');
			var hasError = true;
			} 
		else{
		    jQuery('#loader2').show();
			jQuery('.error2').hide();
			jQuery('#username').removeClass('error-border');
			var hasError = false;
		}
		if(password == ''){
		    jQuery('#loader2').hide();
			jQuery('.error2').show();
			jQuery('#password').addClass('error-border');
			var hasError = true;
			} 
		else{
			jQuery('.error2').hide();
			jQuery('#loader2').show();
			jQuery('#password').removeClass('error-border');
			var hasError = false;
		}
		if(!hasError){
			FQUBEURLForSignIn = FQUBE.baseUrl+"user/signin/"+email+"/"+password;
			FQUBEType = 'POST';
			FQUBE.common.ajax(FQUBEURLForSignIn,FQUBEType,FQUBE.user.signinSuccess);
			}
		else  
		{
		    jQuery('#loader2').hide();
			jQuery('.error2').show();
			
		}
}	

//after login function 
var result;
var user_id;
var login_type; 
FQUBE.user.signinSuccess = function(result){
if((result.status=='success')&&(result.response=='first Time login user'))
{ 
	window.location.href = FQUBE.baseUrl+'select-follow';
	jQuery('#invalid_email').hide('slow');
	jQuery('#loader2').hide();
}
else if((result.status=='success')&&(result.response=='not first time login user'))
{
    var get_started = false;
	localStorage.setItem("get_started",get_started);
	window.location.href = FQUBE.baseUrl+'my-feed';
	jQuery('#invalid_email').hide('slow');
	jQuery('#loader2').hide();
}
else if(result.status=='failure')
{
	jQuery('#invalid_email').show('slow'); 
	setTimeout(function() { $("#invalid_email").hide(); }, 2000);
}

	
	
	//
}

//user signup
function checkForm(form){
      jQuery('#loader2').show();
      var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	  var hasError = false;
	  if(form.password1.value.length < 5) {
      jQuery('.error3').html('Password must contain at least five characters')
	  jQuery('.error3').show();
	  jQuery('#loader2').hide();
	  jQuery('#password1').addClass('error-border');
	  var hasError = true; 
      }
	if(form.password1.value == "") {
	  jQuery('.error3').html("Please check that you've entered your password")
	  jQuery('.error3').show();
	  jQuery('#loader2').hide();
	  var hasError = true; 
      } 	
	if(form.username.value == "") {
      jQuery('.error3').html('Mandatory fields are missing')
	  jQuery('.error3').show();
	  jQuery('#loader2').hide();
	  jQuery('#username').addClass('error-border');
	  var hasError = true; 
    }
    // re = /^\w+$/;
    // if(!re.test(form.username.value)) {
      // jQuery('.error3').html('Name must contain only letters, numbers and underscores')
	  // jQuery('.error3').show();
	  // jQuery('#loader2').hide();
	  // jQuery('#username').addClass('error-border');
	  // var hasError = true; 
    // }
    if(form.email_addr.value == ''){
		    jQuery('.error3').html('Mandatory fields are missing');
		    jQuery('#email_address').addClass('error-border');
			jQuery('.error3').show();
			jQuery('#loader2').hide();
			var hasError = true;
			}		
    if(!pattern.test(form.email_addr.value)){
		    jQuery('.error3').html('Invalid Email Address');
		    jQuery('#email_address').addClass('error-border');
			jQuery('.error3').show();
			jQuery('#loader2').hide();
			var hasError = true;
			}				
	if(!hasError){
	FQUBE.user.signup();
	}
}

FQUBE.user.signup = function(){
    jQuery('#loader2').show();
    jQuery('#email_exist').hide('slow');
	var email = jQuery('#email_address').val();
	var name = jQuery('#username').val();
	var password1=jQuery('#password1').val();
	    var FQUBEURLForSignUp = FQUBE.baseUrl+"user/signup/"+email+"/"+name+"/"+password1;
		FQUBEType = 'POST';
		//ajax call for register
		FQUBE.common.ajax(FQUBEURLForSignUp,FQUBEType,FQUBE.user.signupNormalSuccess);	
}

//signup success function
FQUBE.user.signupNormalSuccess = function(result){
// Create default collection to new user 
if((result.status=='success')&&(result.response=='first Time login user'))
{
var userId=result.user_info[0].user_id;
var url=FQUBE.baseUrl+'user/createDefaultCollection/'+userId;
jQuery.ajax({ 
			type: "POST",    
			url:url,      
			dataType : 'json',
			success: function (data) 
			{
			}
			});
jQuery('#loader2').hide();
jQuery('#email_exist').hide('slow');
			 var get_started = true;
			localStorage.setItem("get_started",get_started);
	window.location.href = FQUBE.baseUrl+'select-follow';
}
else
{
    jQuery('#loader2').hide();
	jQuery('#email_exist').show('slow');
	jQuery('#mand_exist').hide('slow');
}
}

FQUBE.user.signinfb = function(){
            jQuery('#loader2').show();
			console.log('username '+FQUBE.base64.encode(name));
			console.log('email '+FQUBE.base64.encode(email));
			console.log('password '+FQUBE.base64.encode(password1));
			FQUBEURLForSignUpFb = FQUBE.baseUrl+"loginWithFacebook";
			FQUBEType = 'get';
		//ajax call for register
		FQUBE.common.ajax(FQUBEURLForSignUpFb,FQUBEType,FQUBE.user.signupSuccessfb);
}

//signup success function 
FQUBE.user.signupSuccessfb = function(data){
jQuery('#loader2').hide();
if(data.status=='success')
{
window.location.href = FQUBE.baseUrl+'select-follow';
} 
else{}
	
}


FQUBE.user.signingp = function(){
            jQuery('#loader2').show();
			console.log('username '+FQUBE.base64.encode(name));
			console.log('email '+FQUBE.base64.encode(email));
			console.log('password '+FQUBE.base64.encode(password1));
			FQUBEURLForSignUpGp = FQUBE.baseUrl+"user/loginWithFacebook";
			FQUBEType = 'POST';
		//ajax call for register
		FQUBE.common.ajax(FQUBEURLForSignUpGp,FQUBEType,FQUBE.user.signupSuccessgp);
}

//signup success function
FQUBE.user.signupSuccessgp = function($result){
jQuery('#loader2').hide();
window.location.href = FQUBE.baseUrl+'select-follow';
}


FQUBE.base64 = {_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=FQUBE.base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=FQUBE.base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

FQUBE.common.ajax =  function (tpltURL,tpltType,callback){
	$.ajax({
      type: tpltType,
      url: tpltURL, 
      datatype: "json",
      sync : true,
      crossDomain: true,
      success: callback
    });
}


FQUBE.post.ajax =  function (tpltURL,tpltType,tpltdata,callback){ 
	$.ajax({
      type: tpltType,
      url: tpltURL,
	  data:tpltdata,
      datatype: "json",
	contentType: "application/json; charset=utf-8",
      sync : true,
      crossDomain: true,
      success: callback
    });
}

