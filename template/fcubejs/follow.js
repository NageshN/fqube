// Follow and Unfollow User Functionality
function followUser(e){ 
var follow = jQuery('#hiddenid-'+e.id).val();
var followingId=e.id;
var userId=jQuery('#user_id').val();
if(follow=="0"){ 
var url=FQUBE.baseUrl+'user/follow/'+userId+'/'+followingId; 
            jQuery.ajax({ 
			type: "POST",   
			url:url,      
			dataType : 'json',
			success: function (data)  
			{			 
			 jQuery('#hiddenid-'+e.id).val("1");
			 jQuery('#'+e.id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#'+e.id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			}
			});
			}
			else{
			var url=FQUBE.baseUrl+'user/unfollow/'+userId+'/'+followingId;
            jQuery.ajax({ 
			type: "POST",   
			url:url,      
			dataType : 'json',
			success: function (data) 
			{	
			 jQuery('#hiddenid-'+e.id).val("0");
			 jQuery('#'+e.id).html("FOLLOW");
			 jQuery('#'+e.id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			}
			});
		}
}

// Follow and Unfollow User Functionality for Profile Page followers
function followUserFollowers(e){ 

var follow = jQuery('#hiddenid-'+e.id).val();
var followingId1=e.id;
var str = followingId1; 
var followingId = str.substring(10,20);
var userId=jQuery('#user_id').val();
if(follow=="0"){ 
var url=FQUBE.baseUrl+'user/follow/'+userId+'/'+followingId; 
            jQuery.ajax({ 
			type: "POST",   
			url:url,      
			dataType : 'json',
			success: function (data)  
			{			 
			 jQuery('#hiddenid-'+e.id).val("1");
			 jQuery('#'+e.id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#'+e.id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			}
			});
			}
			else{
			var url=FQUBE.baseUrl+'user/unfollow/'+userId+'/'+followingId;
            jQuery.ajax({ 
			type: "POST",   
			url:url,      
			dataType : 'json',
			success: function (data) 
			{	
			 jQuery('#hiddenid-'+e.id).val("0");
			 jQuery('#'+e.id).html("FOLLOW");
			 jQuery('#'+e.id).removeClass("followed");
			 jQuery('#'+e.id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			}
			});
		}
}


// Follow and Unfollow User Functionality for Profile Page Following
function followUserFollowing(e){ 

var follow = jQuery('#hiddenid-'+e.id).val();

var followingId1=e.id;
var str = followingId1;
var followingId = str.substring(10,20);
var userId=jQuery('#user_id').val();
if(follow=="0"){ 
var url=FQUBE.baseUrl+'user/follow/'+userId+'/'+followingId; 

            jQuery.ajax({ 
			type: "POST",   
			url:url,      
			//dataType : 'json',
			success: function (data)  
			{		
				
			 jQuery('#hiddenid-'+e.id).val("1");
			  
			 jQuery('#'+e.id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#'+e.id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			}
			});
			}
			else{
			var url=FQUBE.baseUrl+'user/unfollow/'+userId+'/'+followingId;
            jQuery.ajax({ 
			type: "POST",   
			url:url,      
			dataType : 'json',
			success: function (data) 
			{	
		
			 jQuery('#hiddenid-'+e.id).val("0");
			 var nara=jQuery('#'+e.id).removeClass("followed");
			 
			
			 jQuery('#'+e.id).html("FOLLOW");
			 
			 jQuery('#'+e.id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			}
			});
		}
}




// Follow and Unfollow Collection Functionality
var selected2 = [];
function followCollection(e)
{
var userId=jQuery('#user_id').val();
var follow2 = jQuery('#hiddenidcollection-'+e.id).val();
var userId1=e.id;
var str = userId1;
var collectionId = str.substring(10,20);
if(follow2=="0"){ 
var url=FQUBE.baseUrl+'user/followCollection'; 
            jQuery.ajax({ 
			type: "POST",   
			url:url,     
            data:{userId:userId,collectionId:collectionId},			
			dataType : 'json',
			success: function (data)   
			{			
			 jQuery('#hiddenidcollection-'+userId1).val("1");
			 jQuery('#'+userId1).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#'+userId1).removeClass("followed");
			 jQuery('#'+userId1).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			}
			});
	}
	else { 
var url=FQUBE.baseUrl+'user/unFollowCollection';
            jQuery.ajax({ 
			type: "POST",    
			url:url,     
            data:{userId:userId,collectionId:collectionId},			
			dataType : 'json',
			success: function (data) 
			{	
			 jQuery('#hiddenidcollection-'+userId1).val("0");
			 jQuery('#'+userId1).html("FOLLOW"); 
			 jQuery('#'+userId1).removeClass("followed");
			 jQuery('#'+userId1).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			}
			});
	}	
	}

	
// Follow and Unfollow Collection Functionality for profile page i follow
var selected2 = [];
function followCollectionFollow(e)
{
var userId=jQuery('#user_id').val();

var follow2 = jQuery('#hiddenidcollection-'+e.id).val();
var userId1=e.id;
var str = userId1;
var collectionId = str.substring(10,20);
if(follow2=="0"){ 
var url=FQUBE.baseUrl+'user/followCollection'; 
            jQuery.ajax({ 
			type: "POST",   
			url:url,     
            data:{userId:userId,collectionId:collectionId},			
			dataType : 'json',
			success: function (data)   
			{			
			 jQuery('#hiddenidcollection-'+userId1).val("1");
			 jQuery('#'+userId1).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#'+userId1).removeClass("followed");
			 jQuery('#'+userId1).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			}
			});
	}
	else { 
var url=FQUBE.baseUrl+'user/unFollowCollection';
            jQuery.ajax({ 
			type: "POST",    
			url:url,     
            data:{userId:userId,collectionId:collectionId},			
			dataType : 'json',
			success: function (data) 
			{	
			 jQuery('#hiddenidcollection-'+userId1).val("0");
			 jQuery('#'+userId1).html("FOLLOW"); 
			 jQuery('#'+userId1).removeClass("followed");
			 jQuery('#'+userId1).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			}
			});
	}	
	}	
	
	
	

// Follow and Unfollow Store Functionality
var selected1 = [];
function followStore(e)
{
var follow1 = jQuery('#hiddenstoreid-'+e.id).val();
var followingId=e.id;
var str = followingId;
var res = str.substring(5, 10);
var userId=jQuery('#user_id').val();
if(follow1=="0"){ 
    jQuery('.error-box').hide(); 
    if (jQuery.inArray(jQuery(e).attr('id'), selected1) == -1) {
        selected1.push(jQuery(e).attr('id'));
    } else {
        selected1.splice(jQuery.inArray(jQuery(e).attr('id'), selected1), 1);
    }
    jQuery.each(selected1, function () {
        jQuery('#' + e.id).addClass("selected1"); 
    })
	jQuery("#selectedHiddenstore").attr("data", selected1);
	
var url=FQUBE.baseUrl+'user/storeFollow/'+userId+'/'+res; 
            jQuery.ajax({ 
			type: "POST",   
			url:url,       
			dataType : 'json',
			success: function (data)  
			{			
			 jQuery('#hiddenstoreid-'+followingId).val("1");
			 jQuery('#'+followingId).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#'+followingId).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			}
			});
	}
	else { 
    jQuery('.error-box').hide();
    if (jQuery.inArray(jQuery(e).attr('id'), selected1) == -1) {
        selected1.push(jQuery(e).attr('id'));
    } else {
        selected1.splice(jQuery.inArray(jQuery(e).attr('id'), selected1), 1);
    }
    jQuery.each(selected1, function () {
        jQuery('#' + e.id).removeClass("selected1"); 
    })
var url=FQUBE.baseUrl+'user/storeUnfollow/'+userId+'/'+res;
            jQuery.ajax({ 
			type: "POST",    
			url:url,      
			dataType : 'json',
			success: function (data) 
			{	
			 jQuery('#hiddenstoreid-'+followingId).val("0");
			 jQuery('#'+followingId).html("FOLLOW");
			 jQuery('#'+followingId).removeClass("followed");
			 jQuery('#'+followingId).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			}
			});
	jQuery("#selectedHiddenstore").attr("data", selected1);
	}	
	}

var product_id; 
function like(e)
{	
	 product_id=jQuery(e).attr("data-id");
	var user_id=jQuery('#user_id').val();
	var FQUBElikeurl=FQUBE.baseUrl+"user/likeProduct";
	var FQUBEType="POST";
	var FQUBEdata='{"user_id":"'+user_id+'","product_id":"'+product_id+'"}';
	FQUBE.post.ajax(FQUBElikeurl,FQUBEType,FQUBEdata,FQUBEsuccesslike);
}
FQUBEsuccesslike=function(result)
{
     
	 if(result.status=='success') 
	{
		jQuery('#product_'+product_id).addClass('already_liked');
		jQuery('#product_'+product_id).removeClass('not_liked');
		var count_value=jQuery('#product_like_count'+product_id).html();
	    count_value=++count_value;
		jQuery('#product_like_count'+product_id).html(count_value);
		jQuery('#product_count_text'+product_id).addClass("liked_small");
		jQuery('#product_count_text'+product_id).removeClass("not_liked_small");
	} 
	else if(result.status=='failure')
	{
	
		// jQuery('#product_'+product_id+'').removeClass('already_liked');
	}
}


function get_product_id(e) 
	 {
		product_id=e.id;
		var product=jQuery('#productID').val(product_id);	
	 }
	 
jQuery('document').ready(function()
{
var user_id=jQuery('#user_id').val();
var FQUBEURLForoptions="http://codewave.co.in/fqube/user/getUserCollections/"+user_id;
var optionType="get";
FQUBE.common.ajax(FQUBEURLForoptions,optionType,FQUBE.user.optionSuccess);
});
FQUBE.user.optionSuccess=function(data)
{
	var length=data.count;
	var i=0;
	var dropdown=""; 
	for(i=0;i<length;i++)
	{
		dropdown +='<option id='+data.data[i].collections_id+' class="option-col">'+data.data[i].collection_name+'</option>';
	}
	
	jQuery('#myCustomSelector').html(dropdown);
	jQuery('#myCustomSelector1').html(dropdown);
}



function addSave()
{
var coll_name=jQuery('#collection').val();
if(coll_name == ""){
}
else{
$("#myCustomSelector1").append($("<option selected='selected' id='new-collection'></option>").val(1).html(coll_name));
document.getElementById("form-coll").reset();
}
} 
function addCollection()
{
    jQuery('#loader-save').show();
    var user_id=jQuery('#user_id').val();
	var collection_name=jQuery( "#myCustomSelector1 option:selected" ).text();
	var collection_id=jQuery( "#myCustomSelector1 option:selected" ).attr('id');
	//var coll_name=jQuery('#collection').val();
	var productId=jQuery('#productID').val();
	if(collection_id == "new-collection"){
	var collectionName=collection_name;
	var collectionId="null"; 
	}
	else {
	if(collection_id!="new-collection" && collection_name!=""){
	var collectionName="null";
	var collectionId=collection_id;
	}
	}
	var FQUBEurlforadd="http://codewave.co.in/fqube/user/collectProduct";
	var FQUBEaddtype="POST";
	var addData='{"userId":"'+user_id+'","collectionName":"'+collectionName+'","collectionId":"'+collectionId+'","productId":"'+productId+'"}';
	FQUBE.post.ajax(FQUBEurlforadd,FQUBEaddtype,addData,FQUBE.user.addSuccess);
	
}
FQUBE.user.addSuccess=function(data)
{
if(data.status == "success"){
toastr.success('The product has been saved to your collection.', 'Done!');
jQuery('#loader-save').hide();
// $('#save').popup('hide');
$('#save1').popup('hide');
var user_id=jQuery('#user_id').val();
var FQUBEURLForoptions="http://codewave.co.in/fqube/user/getUserCollections/"+user_id;
var optionType="get";
FQUBE.common.ajax(FQUBEURLForoptions,optionType,FQUBE.user.optionSuccess);	
}
else {
jQuery('#loader-save').hide();
toastr.error('A collection with the same name already exists in your profile. Please give it a new name.');
}
}

//edit profile
jQuery("#save_it").click(function(){
var uname=jQuery('#edit_name').val();
var about=jQuery('#edit_about').val();
var userId=jQuery('#user_id').val();
	         if(uname == "")
				{
					jQuery('#edit_name').css({"border":"1px solid red"});
					jQuery('#error').show('slow');
				}
				else
				{
					jQuery('#edit_name').css({"border":"1px solid #ccc"});
					jQuery('#error').hide('slow');
				}

			data = new FormData();
			if(jQuery( '#input_file' )[0].files[0] == "undefined"){
			data.append( 'picture', "null" );
			}
			else {
			data.append( 'picture', jQuery( '#input_file' )[0].files[0] );
			}
			data.append('user_id',userId);
			data.append('about_me',about);
			data.append('username',uname);
			if((uname !=""))
			{
			jQuery.ajax({
					url : 'http://codewave.co.in/fqube/user/editProfile',
					type: 'post',
					data: data,
					async: false,
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if(data.status=='success')
							{ 
								jQuery('#success').fadeIn('slow');
								jQuery('#error').hide();
								//jQuery('#edit_profile').popup('hide');
								location.reload();
							}
							else
							{
								jQuery('#error').show('slow');
								jQuery('#error').html('Oops! there was an error while saving.');
								jQuery('#success').hide();
							}
					}
				});
				}
				else
				{
					jQuery('#error').html('Oops! there was an error while saving.');
				}

});

//change password 
jQuery('#compare_pass').click(function(){
 var password=jQuery('#current_password').val();
 var userId=jQuery('#user_id').val();  
 var FQUBEURLForforgot=FQUBE.baseUrl+"user/checkPassword/"+password+'/'+userId;  
 var type="get";
 FQUBE.common.ajax(FQUBEURLForforgot,type,FQUBE.forgotSuccess);
 });
 FQUBE.forgotSuccess=function(data) 
 {
     if(data.status == "success"){
	 jQuery('#change_password').hide();
	 jQuery('#new_password').show();
	 document.getElementById("change_password").reset();
	 }
	 else {
	    jQuery('.error5').html("Your password appears incorrect. Please check the letters.");
	    jQuery('.error5').show();
	    //toastr.error('Your password appears incorrect. Please check the letters.');
		document.getElementById("change_password").reset();
	 }
}

function checkFormPassword(form){
	var hasError = false;  
	  if(form.new_password.value == "" || form.re_new_password.value == ""){
	  jQuery('.error3').html("Please check that you've entered and confirmed your password");
	  jQuery('.error3').show();
	  var hasError = true; 
      } 	
	   if(form.new_password.value == form.re_new_password.value) {
	  if(form.new_password.value.length < 8) {
	  jQuery('.error3').html("Password must contain at least eight characters");
	  jQuery('.error3').show();
	  var hasError = true; 
      }
	  else{ 
jQuery('.error3').hide();
 var password1=jQuery('#new_password').val();
 var password2=jQuery('#re_new_password').val();
 var userId=jQuery('#user_id').val();
 var url=FQUBE.baseUrl+"user/updatePassword/"+password2+'/'+userId; 
jQuery.ajax({ 
			type: "GET",     
			url:url,      
			dataType : 'json',
			success: function (data) 
			{
            if(data.status == "success"){
	        jQuery('#success1').show();
	        location.reload();
	        document.getElementById("new_password").reset();
	        }
	        else {
	 
	        }
			}
			});
	
	  }
      }  
      else
	  {
	  jQuery('#re_new_password').css({"border":"1px solid #ccc"});
	  jQuery('.error3').html("Password does not match. Please check the letters");
	  jQuery('.error3').show();
	  var hasError = true; 
	  }	
}


function checkForm(form){
	var hasError = false;
     if(form.password.value == form.re_password.value) {
	  if(form.password.value.length < 8) {
	  jQuery('.error3').html("Password must contain at least eight characters")
	  jQuery('.error3').show();
	  var hasError = true; 
      }
      }  
      else
	  {
	  jQuery('#re_password').css({"border":"1px solid #ccc"});
	  jQuery('.error3').html("Password does not match. Please check the letters")
	  jQuery('.error3').show();
	  var hasError = true; 
	  }	  
	  if(form.password.value == "" || form.re_password.value == "") {
	  jQuery('.error3').html("Please check that you've entered and confirmed your password")
	  jQuery('.error3').show();
	  var hasError = true; 
      } 	
	if(!hasError){
	changePassword();
	}
}
function changePassword(){
var password=jQuery('#password').val();
var repassword=jQuery('#re_password').val();
var email_addr=jQuery('#email_addr').val();
	       
			var url = FQUBE.baseUrl+'user/changePasswordset/'+password+'/'+email_addr;
			jQuery.ajax({ 
					url : url,
					type: 'get',
					success: function (data) { 
					  var user_id=jQuery('#user_id').val();
					  var name=jQuery('#user_name').val();
					 if(data.status=='success')
							{ 
								toastr.success('Password changed Successfully.');
								 jQuery('.error3').hide();
								 window.location.href = FQUBE.baseUrl+'profile?id='+user_id+'/name='+name;
							}
							else
							{
							    toastr.error('Password change was Unsuccessfully.');
								 jQuery('.error3').hide();
								 window.location.href = FQUBE.baseUrl+'profile?id='+user_id+'/name='+name;
							}
					}
				});
}

//post page save fuctionality
function addSave1()
{
var coll_name=jQuery('#collection').val();
if(coll_name == ""){
}
else{
$("#myCustomSelector").append($("<option selected='selected' id='new-collection'></option>").val(1).html(coll_name));
document.getElementById("form-coll").reset();
}
} 

function addSave2()
{
var coll_name=jQuery('#collection2').val();
if(coll_name == ""){
}
else{
$("#myCustomSelector").append($("<option selected='selected' id='new-collection'></option>").val(1).html(coll_name));
document.getElementById("form-coll1").reset();
}
} 

function addCollection1()
{
    jQuery('#loader-save').show();
    var user_id=jQuery('#user_id').val();
	var collection_name=jQuery( "#myCustomSelector option:selected" ).text();
	var collection_id=jQuery( "#myCustomSelector option:selected" ).attr('id');
	//var coll_name=jQuery('#collection').val();
	var productId=jQuery('#productID').val();
	if(collection_id == "new-collection"){
	var collectionName=collection_name;
	var collectionId="null"; 
	}
	else {
	if(collection_id!="new-collection" && collection_name!=""){
	var collectionName="null";
	var collectionId=collection_id;
	}
	}
	var FQUBEurlforadd="http://codewave.co.in/fqube/user/collectProductOnUrl";
	var FQUBEaddtype="POST";
	var addData='{"userId":"'+user_id+'","collectionName":"'+collectionName+'","collectionId":"'+collectionId+'","productId":"'+productId+'"}';
	FQUBE.post.ajax(FQUBEurlforadd,FQUBEaddtype,addData,FQUBE.user.addSuccess2);
	
}
FQUBE.user.addSuccess2=function(data)
{
if(data.status == "success"){ 
toastr.success('The product has been saved to your collection.', 'Done!');
jQuery('#loader-save').hide();
jQuery('.main-div').hide();
jQuery('.done-div').show(); 
window.open('','_self');
setTimeout (function() {window.close();},3000);
}
else {
jQuery('#loader-save').hide();
toastr.error('A collection with the same name already exists in your profile. Please give it a new name.');
jQuery('.main-div').hide();
jQuery('.done-div').show();
window.open('','_self');
setTimeout (function() {window.close();},3000);
}
}

function closeTab()
{
window.open('','_self');
setTimeout (function() {window.close();},1000);
}

function addCollection2()
{
    jQuery('#loader-save').show();
    var user_id=jQuery('#user_id').val();
	var collection_name=jQuery( "#myCustomSelector option:selected" ).text();
	var collection_id=jQuery( "#myCustomSelector option:selected" ).attr('id');
	//var coll_name=jQuery('#collection').val();
	var productId=jQuery('#productID').val();
	if(collection_id == "new-collection"){
	var collectionName=collection_name;
	var collectionId="null"; 
	}
	else {
	if(collection_id!="new-collection" && collection_name!=""){
	var collectionName="null";
	var collectionId=collection_id;
	}
	}
	var FQUBEurlforadd="http://codewave.co.in/fqube/user/collectProductOnUrl";
	var FQUBEaddtype="POST";
	var addData='{"userId":"'+user_id+'","collectionName":"'+collectionName+'","collectionId":"'+collectionId+'","productId":"'+productId+'"}';
	FQUBE.post.ajax(FQUBEurlforadd,FQUBEaddtype,addData,FQUBE.user.addSuccess3);
	
}
FQUBE.user.addSuccess3=function(data)
{
if(data.status == "success"){ 
toastr.success('The product has been saved to your collection.', 'Done!');
jQuery('#loader-save').hide();
jQuery('.main-div').hide();
jQuery('.done-div').show(); 
window.open('','_self');
setTimeout (function() {window.close();},3000);
}
else {
// jQuery('#loader-save').hide();
// toastr.error('A collection with the same name already exists in your profile. Please give it a new name.');
// jQuery('.main-div').hide();
// jQuery('.done-div').show();
// window.open('','_self');
setTimeout (function() {window.close();},1000);
}
}

//Profile page profile info
jQuery(document).ready(function(){
var id=jQuery('#user_id').val();
var profile_id=jQuery('#user_id').val();
var url=FQUBE.baseUrl+"user/getUserDetails/"+id+'/'+profile_id;
jQuery.ajax({ 
			type: "GET",    
			url:url,      
			dataType : 'json',
			success: function (data) 
			{
			var userName=data.data[0].username;
			var profile_picture=data.data[0].profile_picture;
			var image=profile_picture;
			jQuery('#edit_name').val(userName);
			jQuery('#profile-pic').attr("src",image);
			jQuery('#edit_about').html(data.data[0].about_me);

			}
			});
});
