//script for fetching url parameter in javascript
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)  
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        } 
    }
}          

//get All Store Profile
jQuery(document).ready(function(){ 
var store_id;
var storeId = getUrlParameter('store_id');
var userId=jQuery('#user_id').val(); 
var url=FQUBE.baseUrl+'user/storeProfile/'+storeId;
jQuery.ajax({ 
			type: "GET",    
			url:url,      
			dataType : 'json',
			success: function (data)  
			 {
             jQuery('.store-profile1').append('<div class="col-md-3"><div class="profile-image"><img src="'+data.data.store_image+'" style="border-radius: 8px;"></div></div><div class="col-md-9"><div class="col-md-12" style="padding: 0px 0px 0px;"><div class="col-md-6" style="padding-top:14px;"><span style="font-size:26px;text-transform: uppercase;margin-left: -16px;">'+data.data.store_name+'</span></div><div class="col-md-6" style="padding:0px"><button id="store'+data.data.store_id+'" onclick="followStore(this);" class="btn3 btn-default3 follow-btn">FOLLOW</button><input id="hiddenstoreid-store'+data.data.store_id+'" type="hidden" value="0"></div></div><div class="col-md-12" style="padding: 0px 0px 18px; margin-top: -2px;"><span style="font-weight:lighter"><a href="http://'+data.data.store_url+'" style="color:#5AC3A2">'+data.data.store_url+'</a></span></div><div class="col-md-12" style="padding:0px;width:83%"><div class="col-md-3 sub-tab" style="padding:0px"><div id="tab2" onclick="changeProduct(this);" style="cursor:pointer;"><div class="col-md-4" style="padding:0px"><img src="'+FQUBE.baseUrl+'template/img/icon-2.png" alt=""></div><div class="col-md-7" style=""><div style="color:#5AC3A2;font-size:24px" class="productcount"></div><div style="font-size:15px"> products</div></div></div></div><div class="col-md-3 sub-tab" style="padding:0px"><div id="tab3" onclick="changeFollow(this);" style="cursor:pointer;"><div id="tab3" onclick="changeFollow(this);" class="col-md-4" style="padding:0px"><img src="'+FQUBE.baseUrl+'template/img/icon-3.png" alt=""></div><div class="col-md-7" style=""><div style="color:#5AC3A2;font-size:24px" class="followcount"></div><div style="font-size:15px"> followers</div></div></div></div></div></div>');
			 }

});
}); 

//get Store Follow Status
jQuery(document).ready(function(){ 
var store_id;
var storeId = getUrlParameter('store_id');
var userId=jQuery('#user_id').val();
var url=FQUBE.baseUrl+'user/storeProfileUserFollowStatus/'+storeId+'/'+userId;
jQuery.ajax({ 
			type: "GET",    
			url:url,      
			dataType : 'json',
			success: function (data)  
			 {  
             if(data.status == "success"){
			 jQuery('#hiddenstoreid-store'+storeId).val("1");
			 jQuery('#store'+storeId).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			 jQuery('#store'+storeId).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
			 }
			 else {
			 jQuery('#hiddenstoreid-store'+storeId).val("0");
			 jQuery('#store'+storeId).html("FOLLOW");
			 jQuery('#store'+storeId).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			 }
			 } 

});
}); 

//get All Store Products
jQuery(document).ready(function(){
var store_id; 
jQuery('#viewstoreproduct').attr('data-id',2);
var storeId = getUrlParameter('store_id'); 
var userId=jQuery('#user_id').val();
var pageNumber=1;
var numberOfProduct=8;
var url=FQUBE.baseUrl+'user/getProductsByStore/'+storeId+'/'+pageNumber+'/'+numberOfProduct;
jQuery.ajax({ 
			type: "GET",     
			url:url,      
			dataType : 'json',
			success: function (data)  
			{
			 jQuery('.productcount').append('&nbsp;'+data.product_count+'&nbsp;');
			  if(data.count != 0){ 
			  jQuery('#viewstoreproduct').show();
			  var liked;
	          var not_liked;
			 for(var i=0;i<data.count;i++){
			  for(var j=0;j<data.likeDetails.length;j++){
			 if(data.likeDetails[j].liked_status == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			      break;
		        }
		      else
		       {
			     var like_status='not_liked'; 
				 var like_status1='not_liked_small';
		       }
			   }
			 jQuery('.store-products').append('<div class="col-md-3 blocks"><div class="edd_download_inner"><div class="edd_download_image" style="background-image:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+');background-size:100% 100%;width:100%;height:250px;"><div class="stocky_hover_details stocky_wish_list_on" style="width:246px;height:250px;"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div><div style="background-color:white!important;float:left;width:100%;"><div class="tag-author" style="padding-left: 17px;padding-bottom:9px"><span style="float: left;padding-top: 9px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;width: 200px;"><a href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'" class="proc_name" style="color:#454545;">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: -5px;"></div></div></div>');	 
			 }
			 if(data.count < 8){
			  jQuery('#viewstoreproduct').hide();
			 }
			 else {
			  jQuery('#viewstoreproduct').show();
			 }
			 }
			 else {
			  jQuery('.store-products').append("<div class='message-error'>No results to display</div>");
			 }
	 }
});
});

//Load more store products
function viewMoreStoreProducts(){
jQuery('#loader1').show();
var store_id; 
var pageNumber=jQuery('#viewstoreproduct').attr('data-id');
var storeId = getUrlParameter('store_id'); 
var userId=jQuery('#user_id').val();
var numberOfProduct=8;
var url=FQUBE.baseUrl+'user/getProductsByStore/'+storeId+'/'+pageNumber+'/'+numberOfProduct;
jQuery.ajax({ 
			type: "GET",     
			url:url,      
			dataType : 'json',
			success: function (data)  
			{
			  if(data.count != 0){ 
			  var liked;
	          var not_liked;
			 for(var i=0;i<data.count;i++){
			  for(var j=0;j<data.likeDetails.length;j++){
			 if(data.likeDetails[j].liked_status == userId && data.likeDetails[j].product_id==data.data[i].product_id)
		        {
			     var like_status='liked';
				 var like_status1='liked_small';
			      break;
		        }
		      else
		       {
			     var like_status='not_liked';
                 var like_status1='not_liked_small';				 
		       }
			   }
			 jQuery('.store-products').append(' <div class="col-md-3 blocks"><div class="edd_download_inner"><div class="edd_download_image" style="background-image:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+data.data[i].product_image+');background-size:100% 100%;width:100%;height:250px;"><div class="stocky_hover_details stocky_wish_list_on" style="width:246px;height:250px;"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like '+like_status+'" id="product_'+data.data[i].product_id+'" data-id="'+data.data[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:55px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+data.data[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -48px;">Save</span></a></div></div></div><div class="like-bottom" style="margin-top: -35px;padding: 0px 25px 2px 10px;"><div id="product_count_text'+data.data[i].product_id+'" class="icon22 '+like_status1+'" style="float:right;"></div><span id="product_like_count'+data.data[i].product_id+'" style="float: right;padding: 4px;color: white;font-size: 17px;">'+data.data[i].product_like_count+'</span></div><div style="background-color:white!important;float:left;width:100%;"><div class="tag-author" style="padding-left: 17px;padding-bottom:9px"><span style="float: left;padding-top: 9px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;width: 200px;"><a href="'+FQUBE.baseUrl+'landing?product_id='+data.data[i].product_id+'&id='+data.data[i].user_id+'&userName='+data.data[i].username+'" class="proc_name" style="color:#454545;">'+data.data[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: -5px;"></div></div></div>');	 
	         jQuery('#loader1').hide();
			 }
			 if(data.count < 8){
			  jQuery('#viewstoreproduct').hide();
			 }
			 else {
			  jQuery('#viewstoreproduct').show();
			 }
			 }
			 else {
			  jQuery('.store-products').append("<div class='message-error'>No results to display</div>");
			  jQuery('#loader1').hide();
			 }
			 pageNumber=++pageNumber;
			 jQuery('#viewstoreproduct').attr('data-id',pageNumber);
	 }
});
}

//get All Store Following Users
jQuery(document).ready(function(){
var store_id;
jQuery('#viewstorepeople').attr('data-id',2);
var storeId = getUrlParameter('store_id'); 
var pageNumber=1;
var numberOfProduct=8; 
var url=FQUBE.baseUrl+'user/getStoreFollowingUsers/'+storeId+'/'+pageNumber+'/'+numberOfProduct;
jQuery.ajax({ 
			type: "GET",     
			url:url,       
			dataType : 'json',
			success: function (data)  
			{
			 jQuery('.followcount').append('&nbsp;'+data.people_count+'&nbsp;');
			 if(data.count != 0){ 
			 jQuery('#viewstorepeople').show();
			 for(var i=0;i<data.count;i++){
var profile_id=data.data[i].user_id;
//var my_id=jQuery('#user_id').val();
var user=jQuery('#user_id').val();
var my_id=getUrlParameter('store_id');
if(user==data.data[i].user_id){
jQuery.ajax({
		type : 'get',  
		url: FQUBE.baseUrl+'user/getFollowPeopleDetailsStore/'+my_id+'/'+profile_id+'/'+user,
		cache: false,
		success:function(data)
		{
		var j;
		var i;
		var repeat="";
		
		
		for(i=0;i<data.count;i++)
		{
		var tarper="";
		
			repeat +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+ 
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'"><img src="'+data.data[i][0].profile_picture+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].username+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" style="width: 75%;" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px">'+data.data[i].length+'</span><br> products</span></div></div>'+
					'<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].user_followers_count+'</span><br> followers</span></div></div></div></div></div><div class="col-md-4" style="z-index: 1000000000000;">'+
					'</div></div><div class="follow-below">'
						
				for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')
				{
				tarper="";	
					for(f=0;f<3;f++)
					{
					
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images_follow='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images_follow=data.data[i][j].product_image;
					
				}
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images_follow+'" class="attachment-img" alt=""></div>'
				
				}
			
			}
			repeat = repeat+tarper+'</div></div>'
			
			
			
		}
		
		jQuery('.store-followers').append(repeat);
		var m;
		for(i=0;i<data.count;i++)
		{
		var mecked=data.data[i][0].user_id;
			for(m=0;m<data.follow_status.length;m++)
			{ 
				if(data.follow_status[m].following_user_id==data.data[i][0].user_id)
				{
			    jQuery('#hiddenid-'+data.data[i][0].user_id).val("1");
			    jQuery('#'+data.data[i][0].user_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			    jQuery('#'+data.data[i][0].user_id).addClass('followed');
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
				}
				else
				{
				jQuery('#hiddenid-'+data.data[i][0].user_id).val("0");
				jQuery('#'+data.data[i][0].user_id).removeClass('followed');
			    jQuery('#'+data.data[i][0].user_id).html("FOLLOW");
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			
				}
			}
		
	
		
		
		}
		
		
		//jQuery('.follow_repeat').append(tarper);
		}
		});
		}
		else {
		jQuery.ajax({
		type : 'get',  
		url: FQUBE.baseUrl+'user/getFollowPeopleDetailsStore/'+my_id+'/'+profile_id+'/'+user,
		cache: false,
		success:function(data)
		{
		var j;
		var i;
		var repeat="";
		
		
		for(i=0;i<data.count;i++)
		{
		var tarper="";
		
			repeat +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+ 
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'"><img src="'+data.data[i][0].profile_picture+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].username+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" style="width: 75%;" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px">'+data.data[i].length+'</span><br> products</span></div></div>'+
					'<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].user_followers_count+'</span><br> followers</span></div></div></div></div></div><div class="col-md-4" style="z-index: 1000000000000;"><button id="'+data.data[i][0].user_id+'" onclick="followUser(this);" class="btn3 btn-default3 follow-btn">FOLLOW</button><input id="hiddenid-'+data.data[i][0].user_id+'" type="hidden" value="0">'+
					'</div></div><div class="follow-below">'
						
				for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')
				{
				tarper="";	
					for(f=0;f<3;f++)
					{
					
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images_follow='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images_follow=data.data[i][j].product_image;
					
				}
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images_follow+'" class="attachment-img" alt=""></div>'
				
				}
			
			}
			repeat = repeat+tarper+'</div></div>'
			
			
			
		}
		
		jQuery('.store-followers').append(repeat);
		var m;
		for(i=0;i<data.count;i++)
		{
		var mecked=data.data[i][0].user_id;
			for(m=0;m<data.follow_status.length;m++)
			{ 
				if(data.follow_status[m].following_user_id==data.data[i][0].user_id)
				{
			    jQuery('#hiddenid-'+data.data[i][0].user_id).val("1");
			    jQuery('#'+data.data[i][0].user_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			    jQuery('#'+data.data[i][0].user_id).addClass('followed');
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
				}
				else
				{
				jQuery('#hiddenid-'+data.data[i][0].user_id).val("0");
				jQuery('#'+data.data[i][0].user_id).removeClass('followed');
			    jQuery('#'+data.data[i][0].user_id).html("FOLLOW");
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			
				}
			}
		
	
		
		
		}
		
		
		//jQuery('.follow_repeat').append(tarper);
		}
		});
		}

            } 
			if(data.count < 8){
			  jQuery('#viewstorepeople').hide();
			 }
			 else {
			  jQuery('#viewstorepeople').show();
			 }
			}
			 else {
			  jQuery('.store-followers').append("<div class='message-error'>No results to display</div>");
			 }
	 }
});
});

//Load more store people
function viewMoreStorePeople(){
jQuery('#loader').show();
var store_id;
var pageNumber=jQuery('#viewstorepeople').attr('data-id');
var storeId = getUrlParameter('store_id'); 
var numberOfProduct=8;  
var url=FQUBE.baseUrl+'user/getStoreFollowingUsers/'+storeId+'/'+pageNumber+'/'+numberOfProduct;
jQuery.ajax({ 
			type: "GET",     
			url:url,       
			dataType : 'json',
			success: function (data)  
			{
			 if(data.count != 0){ 
			 for(var i=0;i<data.count;i++){
var profile_id=data.data[i].user_id;
//var my_id=jQuery('#user_id').val();
var user=jQuery('#user_id').val();
var my_id=getUrlParameter('store_id');
if(user==data.data[i].user_id){
jQuery.ajax({
		type : 'get',  
		url: FQUBE.baseUrl+'user/getFollowPeopleDetailsStore/'+my_id+'/'+profile_id+'/'+user,
		cache: false,
		success:function(data)
		{
		var j;
		var i;
		var repeat="";
		
		
		for(i=0;i<data.count;i++)
		{
		var tarper="";
		
			repeat +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+ 
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'"><img src="'+data.data[i][0].profile_picture+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].username+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" style="width: 75%;" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px">'+data.data[i].length+'</span><br> products</span></div></div>'+
					'<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].user_followers_count+'</span><br> followers</span></div></div></div></div></div><div class="col-md-4" style="z-index: 1000000000000;">'+
					'</div></div><div class="follow-below">'
						
				for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')
				{
				tarper="";	
					for(f=0;f<3;f++)
					{
					
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images_follow='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images_follow=data.data[i][j].product_image;
					
				}
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images_follow+'" class="attachment-img" alt=""></div>'
				
				}
				
			
					
			
			}
			repeat = repeat+tarper+'</div></div>'
			
			
			
		}
		
		jQuery('.store-followers').append(repeat);
		jQuery('#loader').hide();
		var m;
		for(i=0;i<data.count;i++)
		{
		var mecked=data.data[i][0].user_id;
			for(m=0;m<data.follow_status.length;m++)
			{ 
				if(data.follow_status[m].following_user_id==data.data[i][0].user_id)
				{
			    jQuery('#hiddenid-'+data.data[i][0].user_id).val("1");
			    jQuery('#'+data.data[i][0].user_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			    jQuery('#'+data.data[i][0].user_id).addClass('followed');
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
				}
				else
				{
				jQuery('#hiddenid-'+data.data[i][0].user_id).val("0");
				jQuery('#'+data.data[i][0].user_id).removeClass('followed');
			    jQuery('#'+data.data[i][0].user_id).html("FOLLOW");
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			
				}
			}
		
	
		
		
		}
	
		}
		});
        }
		else {
		jQuery.ajax({
		type : 'get',  
		url: FQUBE.baseUrl+'user/getFollowPeopleDetailsStore/'+my_id+'/'+profile_id+'/'+user,
		cache: false,
		success:function(data)
		{
		var j;
		var i;
		var repeat="";
		
		
		for(i=0;i<data.count;i++)
		{
		var tarper="";
		
			repeat +='<div class="col-md-5 col-md-offset-0 box-div" style="z-index: 100;margin-left:61px;margin-bottom:31px;">'+ 
					'<div class="follow-top"><div class="col-md-8"><div class="col-md-3" style="padding: 0;">'+
					'<a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'"><img src="'+data.data[i][0].profile_picture+'" class="attachment-img1" alt=""></a>'+
					'</div><div class="col-md-9" style=""><a href="'+FQUBE.baseUrl+'profile?id='+data.data[i][0].user_id+'&name='+data.data[i][0].username+'">'+
					'<div style="padding-bottom:5px;padding-top: 5px;color: #454545;">'+data.data[i][0].username+'</div></a>'+
					'<div style=""><div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/bag-register.png" class="cogs" style="width: 75%;" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="collection-count"><span style="font-size:20px">'+data.data[i].length+'</span><br> products</span></div></div>'+
					'<div class="col-md-6" style="padding:0px;margin-top:8px;"><div style="float:left;"><img src="'+FQUBE.baseUrl+'template/img/follo-register.png" class="follo" alt=""></div>&nbsp;<div style="float:left;line-height: 18px;margin-left: 8px;"><span class="like-count"><span style="font-size:20px">'+data.data[i][0].user_followers_count+'</span><br> followers</span></div></div></div></div></div><div class="col-md-4" style="z-index: 1000000000000;"><button id="'+data.data[i][0].user_id+'" onclick="followUser(this);" class="btn3 btn-default3 follow-btn">FOLLOW</button><input id="hiddenid-'+data.data[i][0].user_id+'" type="hidden" value="0">'+
					'</div></div><div class="follow-below">'
						
				for(j=0;j<data.data[i].length;j++)
			{
			
				if(data.data[i].length > '3')
				{
				tarper="";	
					for(f=0;f<3;f++)
					{
					
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+data.data[i][f].product_image+'" class="attachment-img" alt=""></div>'
					}
				}
				else
				{
				if(data.data[i][j].product_image==null)
				{
				
					var images_follow='http://codewave.co.in/fqube/template/assets/no-image.png';
				}
				else
				{
					var images_follow=data.data[i][j].product_image;
					
				}
				tarper += '<div class="col-md-4" style="padding: 0;margin-right: -4px;"><img src="'+images_follow+'" class="attachment-img" alt=""></div>'
				
				}
				
			
					
			
			}
			repeat = repeat+tarper+'</div></div>'
			
			
			
		}
		
		jQuery('.store-followers').append(repeat);
		jQuery('#loader').hide();
		var m;
		for(i=0;i<data.count;i++)
		{
		var mecked=data.data[i][0].user_id;
			for(m=0;m<data.follow_status.length;m++)
			{ 
				if(data.follow_status[m].following_user_id==data.data[i][0].user_id)
				{
			    jQuery('#hiddenid-'+data.data[i][0].user_id).val("1");
			    jQuery('#'+data.data[i][0].user_id).html('FOLLOWED&nbsp;&nbsp;<span class="fa fa-check" aria-hidden="true"></span>');
			    jQuery('#'+data.data[i][0].user_id).addClass('followed');
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #3b3b3b;background-image: linear-gradient(to bottom,#3b3b3b 0,#3b3b3b 100%);padding: 6px 15px!important;');
				}
				else
				{
				jQuery('#hiddenid-'+data.data[i][0].user_id).val("0");
				jQuery('#'+data.data[i][0].user_id).removeClass('followed');
			    jQuery('#'+data.data[i][0].user_id).html("FOLLOW");
			    jQuery('#'+data.data[i][0].user_id).attr('style','border-color: #5AC3A2;background-image: linear-gradient(to bottom,#5AC3A2 0,#5AC3A2 100%);padding: 6px 35px!important;');
			
				}
			}
		
	
		
		
		}
	
		}
		});
		
		}
            } 
			if(data.count < 8){
			  jQuery('#viewstorepeople').hide();
			 }
			 else {
			  jQuery('#viewstorepeople').show();
			 }
			}
			 else {
			  jQuery('.store-followers').append("<div class='message-error'>No results to display</div>");
			  jQuery('#loader').hide();
			 }
			 pageNumber=++pageNumber;
			 jQuery('#viewstorepeople').attr('data-id',pageNumber);
	 }
});
}


function changeProduct(e){
jQuery('#tabs-1').hide();
jQuery('#tab2').css({"color":"#5AC3A2"});
jQuery('#tab1').css({"color":"#454545"});
jQuery('#tab3').css({"color":"#454545"});
jQuery('#tab4').css({"color":"#454545"});
jQuery('#tabs-2').show();
jQuery('#tabs-3').hide();
jQuery('#tabs-4').hide();
}
function changeFollow(e){
jQuery('#tabs-1').hide();
jQuery('#tab3').css({"color":"#5AC3A2"});
jQuery('#tab2').css({"color":"#454545"});
jQuery('#tab1').css({"color":"#454545"});
jQuery('#tab4').css({"color":"#454545"});
jQuery('#tabs-3').show();
jQuery('#tabs-2').hide();
jQuery('#tabs-4').hide();
}
 