FQUBE.search={};

FQUBE.search.search_text=function()
{
	jQuery('#stocky_downloads').html("").fadeOut('slow');
	var text=jQuery("#home-search").val();
	if(text == ""){
	jQuery('#stocky_downloads').fadeOut('slow');
	}
	else {
	jQuery('#stocky_downloads').fadeIn('slow');
	jQuery("#loader").show();
	var FQUBEType="GET";
	var json_search_text='{"search_text":"'+text+'"}';
	var search_text=FQUBE.base64.encode(json_search_text);
	
	var FQUBEURLForsearch=FQUBE.baseUrl+'user/searchProduct/'+search_text;
	
	FQUBE.common.ajax(FQUBEURLForsearch,FQUBEType,FQUBE.user.searchSuccess);
	}
}

FQUBE.user.searchSuccess = function(search)
{
if(search.count == 8){
jQuery('#loadmorepopsearch').show();
}
 if(search.status == "success"){
	var length=search.details.length;
	var i;
	var searchresults="";
	jQuery('#stocky_downloads').fadeIn('slow');
	//jQuery('#stocky_downloads_list5').fadeOut('slow');
for(i=0;i<length;i++)
{
searchresults += '<div itemscope="" itemtype="" class="edd_download masonry-brick fade1_open" id="edd_download_1690" style="width: inherit; float: left;left: 0px; top: 0px;width:380px;height:380px;"><div class="edd_download_inner"><div class="edd_download_image" style="background:linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0) 73%,rgba(0,0,0,0.65) 100%),url('+search.details[i].product_image+') no-repeat;width:250px;height:250px;background-size:100% 100%;"><div class="stocky_hover_details stocky_wish_list_on" style="width:250px;"><div class="stocky_hover_lines"><a href="javascript:void(0)" class="edd-wl-button  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"> <div class="icon1 like" id="product_'+search.details[i].product_id+'" data-id="'+search.details[i].product_id+'" onclick="like(this)" style="background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);"><span class="label" style="margin-left:66px;font-size:18px;float:left;">Like</span></div></a><a href="javascript:void(0)" class="edd-wl-button2  edd-wl-open-modal edd-wl-action before glyph-left " data-action="edd_wl_open_modal" data-download-id="169" data-price-option="0"><div class="icon2 collect save1_open" style="background: url('+FQUBE.baseUrl+'template/img/sprite-collect.png);"></div><span class="label save1_open" id="'+search.details[i].product_id+'" onclick="get_product_id(this)" style="margin-left: -28px;">Save</span></a></div></div></div><div class="like-bottom" style="position:absolute;width:100%;margin-top:-36px;padding: 0px 25px 0px 10px;"><div style="float:right;padding:4px;color:white;font-size:17px">'+search.details[i].product_like_count+'</div><div class="" style="float:right;background: url('+FQUBE.baseUrl+'template/img/sprite-like.png);color:white;padding:3px;width: 35px;height: 33px;"></div></div></div><div style="background-color:white!important;float:left;box-shadow: -4px 7px 8px 0px rgba(0, 0, 0, 0.1);width:250px;"><div class="tagging"><a href="#" style="color:#5AC3A2;float:left"><img src="'+search.details[i].profile_picture+'" class="attachment-product_main wp-post-image tag-image" style="width: 50px;" alt="DSCF0726-square"></a></div><div class="tag-author"><span style="float:left;padding-top: 9px;line-height:20px;white-space:nowrap;overflow:hidden !important;text-overflow:ellipsis;width:180px;"><a style="color:grey;" href="#">'+search.details[i].product_name+'</a></span><span style="float:left;font-size:12.6px;margin-top: 5px;"><span style="float:left;padding-right:6px">by</span><a href="#" style="color:#5AC3A2;float:left">'+search.details[i].username+'</a></span><span style="float:right;font-size:12.6px;margin-top: 5px;margin-right:12px"><span style="float:left;padding-right:6px">'+search.details[i].product_currency+'</span><a href="#" style="color:#5AC3A2;float:left">'+search.details[i].product_price+'</a></span></div></div></div>';
}

jQuery('#stocky_downloads').append(searchresults);
jQuery("#loader").hide();
//jQuery('#loadmore').hide();
jQuery('#site_wrap').attr('style','margin-bottom: -50px;');
}
else {
jQuery('#stocky_downloads').fadeIn('slow');
jQuery('#stocky_downloads').html("<div class='message-error'>No results to display.</div>");	
jQuery('.message-error').attr("style","font-size: 20px;text-align: center;margin-top: 85px;margin-bottom: -50px;");	
jQuery("#loader").hide();
}	
}


// Search Functionality for home products on click enter
$('#home-search-form').submit(function(e) {   
FQUBE.search.search_text();
  e.preventDefault(); 
});